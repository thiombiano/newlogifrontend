// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
// tslint:disable-next-line:prefer-const
let roles;
export const environment = {
  production: false,
  // apiUrl: 'http://10.100.17.73:80/logixticbackend/public/api'
  apiUrl: 'http://10.100.35.2:8080/api',
  apiImage: 'http://10.100.35.2:8080/img',
  // apiUrl: 'http://localhost/logixtic-backend/public/api',
  // apiImage: 'http://localhost/logixtic-backend/public/img',
  GOOGLE_MAPS_API_KEY: 'AIzaSyAB_o8CVRDcH6M28a-hZvx8ew0xXBMGNpE',
  // tslint:disable-next-line:align max-line-length
  roles : { A : 'ADMINISTRATEUR', T : 'TECHNICIEN', L : 'LIVREUR', M : 'MAGASINIER', C : 'CONTROLEUR', SA: 'SERVICE ACHAT' , CSR: 'CHEF SUIVI RESSOURCES', CDC: 'CONDUCTEUR DES CHANTIERS' }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
