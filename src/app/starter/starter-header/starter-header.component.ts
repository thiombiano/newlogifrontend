import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {ChantierService} from '../../components/chantier/shared/chantier.service';
import {Router} from '@angular/router';
import {TokenService} from '../../services/token.service';
import {Chantier} from '../../components/chantier/shared/chantier';

@Component({
  selector: 'app-starter-header',
  templateUrl: './starter-header.component.html',
  styleUrls: ['./starter-header.component.css']
})
export class StarterHeaderComponent implements OnInit {
  lechantier: Chantier;
  constructor(    public authService: AuthService,
                  public chantierService: ChantierService,
                  private router: Router,
                  private tokenService: TokenService) { }

  ngOnInit() {
    this.loadtitle();
  }

  loadtitle() {
    const idchantier = window.localStorage.getItem('Idchantier');
    const theid = +idchantier;
    console.log(idchantier);

    // @ts-ignore
    this.chantierService.getLeChantier(theid).subscribe((chantier: Chantier) => {
      this.chantierService.selectedChantier = chantier;
      this.lechantier = chantier;
    });

    //   //
    //   this.chantierService.getChantier(theid).subscribe((chantiers) => {
    //     this.lechantier = chantiers.data;
    //     console.log(this.lechantier);
    //   });
  }

}
