import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {ChantierService} from '../../components/chantier/shared/chantier.service';
import {Router} from '@angular/router';
import {TokenService} from '../../services/token.service';
import {EmployeService} from '../../components/employe/shared/employe.service';
import {any} from 'codelyzer/util/function';
import {NgxPermissionsService, NgxRolesService} from 'ngx-permissions';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-starter-left-side',
  templateUrl: './starter-left-side.component.html',
  styleUrls: ['./starter-left-side.component.css']
})
export class StarterLeftSideComponent implements OnInit {
  nomutilisateur: string;
  imageutilisateur: string;
  path: string;
  lespermissions: Array<{ libelle: string}>;
  public roles: { A: string; T: string; L: string; M: string;  C: string; SA: string; CSR: string; CDC: string};

  constructor(  private RolesService: NgxRolesService,
                private permissionsService: NgxPermissionsService,
                public ApiEmploye: EmployeService,
                public authService: AuthService,
                public chantierService: ChantierService,
                private router: Router,
                private tokenService: TokenService) { }

  ngOnInit() {
    this.path = environment.apiImage;
    const permArray = [];
    const username =  window.localStorage.getItem('nomutilisateur');
    const idusername = window.localStorage.getItem('idutilisateur');
    const imageutilisateur = window.localStorage.getItem('imageutilisateur');
    console.log('id de utilisateur : ' + idusername);
    this.ApiEmploye.getEmploye(+idusername).subscribe( (data) => {
      console.log('Tous les roles pour cet utilisateur : ' + data.data[0].roles.libellerole);
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < data.data[0].roles.length; i++) {
        console.log( 'les roles : ' + data.data[0].roles[i].libellerole);
        // tslint:disable-next-line:prefer-for-of
        for (let j = 0; j < data.data[0].roles[i].permissions.length; j++) {
          permArray.push(data.data[0].roles[i].permissions[j].libellepermission);
          console.log( 'les permissions : ' + data.data[0].roles[i].permissions[j].libellepermission);
          // @ts-ignore
          this.permissionsService.addPermission(data.data[0].roles[i].permissions[j].libellepermission);
        }
        this.RolesService.addRole(data.data[0].roles[i].libellerole, permArray);
      }
      console.log('Liste des permissions : ' + permArray);
    });
    // console.log(this.permissions.libelle);
    this.nomutilisateur = username;
    this.imageutilisateur = imageutilisateur;
    this.roles = environment.roles;
  }

  logout(e: MouseEvent) {
    e.preventDefault();
    this.authService.changeAuthStatus(false);
    this.RolesService.flushRoles();
    this.permissionsService.flushPermissions();
    this.router.navigateByUrl('/');
    this.tokenService.remove();
  }

}
