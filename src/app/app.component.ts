import {Component, HostListener} from '@angular/core';
import {NavigationStart, Route, Router, RouterEvent} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {filter, tap} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'CHANTI-GEST';
  public matDialog: MatDialog;
  constructor(router: Router, matDialog: MatDialog) {
    // Close any opened dialog when route changes
    router.events.pipe(
      filter((event: RouterEvent) => event instanceof NavigationStart),
      tap(() => matDialog.closeAll())
    ).subscribe();
  }
}
