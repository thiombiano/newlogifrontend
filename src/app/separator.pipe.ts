import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'seprator'
})
export class SepratorPipe implements PipeTransform {
  transform(value: string, unit: string): string {
    // tslint:disable-next-line:triple-equals
    if (value == undefined) {
      return '';
    }
    // tslint:disable-next-line:radix
    const n = parseInt(value);

    const rx = /(\d+)(\d{3})/;
    // tslint:disable-next-line:only-arrow-functions
    return String(n).replace(/^\d+/, function(w) {
      let res = w;
      while (rx.test(res)) {
        res = res.replace(rx, '$1 $2');
      }
      return res;
    });
  }
}
