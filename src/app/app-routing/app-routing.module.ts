import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from '../components/login/login.component';
import {BeforeLoginServiceService} from '../services/before-login-service.service';
import {StarterComponent} from '../starter/starter.component';
import {AfterLoginService} from '../services/after-login.service';
import {DashboardComponent} from '../components/dashboard/dashboard.component';
import {FournisseurComponent} from '../components/fournisseur/fournisseur.component';
import {AjouterfournisseurComponent} from '../components/fournisseur/ajouterfournisseur/ajouterfournisseur.component';
import {ModifierfournisseurComponent} from '../components/fournisseur/modifierfournisseur/modifierfournisseur.component';
import {AjouterarticleComponent} from '../components/article/ajouterarticle/ajouterarticle.component';
import {ArticleComponent} from '../components/article/article.component';
import {ModifierarticleComponent} from '../components/article/modifierarticle/modifierarticle.component';
import {ChantierComponent} from '../components/chantier/chantier.component';
import {StocksComponent} from '../components/stocks/stocks.component';
import {MouvementStocksComponent} from '../components/stocks/mouvement-stocks/mouvement-stocks.component';
import {ReceptionsComponent} from '../components/receptions/receptions.component';
import {CommandesComponent} from '../components/commandes/commandes.component';
import {AjoutercommandeComponent} from '../components/commandes/ajoutercommande/ajoutercommande.component';
import {VoirdetailcommandesComponent} from '../components/commandes/voirdetailcommandes/voirdetailcommandes.component';
import {EffectuerunereceptionComponent} from '../components/receptions/effectuerunereception/effectuerunereception.component';
import {VoirunereceptionComponent} from '../components/receptions/voirunereception/voirunereception.component';
import {FonctionsComponent} from '../components/fonctions/fonctions.component';
import {PersonnelsComponent} from '../components/personnels/personnels.component';
import {AjouterfonctionsComponent} from '../components/fonctions/ajouterfonctions/ajouterfonctions.component';
import {ModifierfonctionsComponent} from '../components/fonctions/modifierfonctions/modifierfonctions.component';
import {AjouterpersonnelsComponent} from '../components/personnels/ajouterpersonnels/ajouterpersonnels.component';
import {ModifierpersonnelsComponent} from '../components/personnels/modifierpersonnels/modifierpersonnels.component';
import {ModifiercommandeComponent} from '../components/commandes/modifiercommande/modifiercommande.component';
import {ModepaiementComponent} from '../components/modepaiement/modepaiement.component';
import {AjoutermodepaiementComponent} from '../components/modepaiement/ajoutermodepaiement/ajoutermodepaiement.component';
import {ModifiermodepaiementComponent} from '../components/modepaiement/modifiermodepaiement/modifiermodepaiement.component';
import {AnnulercommandeComponent} from '../components/commandes/annulercommande/annulercommande.component';
import {AjouterpermissionComponent} from '../components/permission/ajouterpermission/ajouterpermission.component';
import {ModifierpermissionComponent} from '../components/permission/modifierpermission/modifierpermission.component';
import {AjouterroleComponent} from '../components/role/ajouterrole/ajouterrole.component';
import {ModifierroleComponent} from '../components/role/modifierrole/modifierrole.component';
import {PermissionComponent} from '../components/permission/permission.component';
import {RoleComponent} from '../components/role/role.component';
import {EmployeComponent} from '../components/employe/employe.component';
import {AjouteremployeComponent} from '../components/employe/ajouteremploye/ajouteremploye.component';
import {ModifieremployeComponent} from '../components/employe/modifieremploye/modifieremploye.component';
import {AuditingComponent} from '../components/auditing/auditing.component';
import {TypechantierComponent} from '../components/typechantier/typechantier.component';
import {AjoutertypechantierComponent} from '../components/typechantier/ajoutertypechantier/ajoutertypechantier.component';
import {ModifiertypechantierComponent} from '../components/typechantier/modifiertypechantier/modifiertypechantier.component';
import {AjouterchantierComponent} from '../components/chantier/ajouterchantier/ajouterchantier.component';
import {ModifierchantierComponent} from '../components/chantier/modifierchantier/modifierchantier.component';
import {VoirchantierComponent} from '../components/chantier/voirchantier/voirchantier.component';
import {EtapeconstructionComponent} from '../components/etapeconstruction/etapeconstruction.component';
import {AjouteretapeComponent} from '../components/etapeconstruction/ajouteretape/ajouteretape.component';
import {MofifieretapeComponent} from '../components/etapeconstruction/mofifieretape/mofifieretape.component';
import {TypemaisonComponent} from '../components/typemaison/typemaison.component';
import {AjoutertypemaisonComponent} from '../components/typemaison/ajoutertypemaison/ajoutertypemaison.component';
import {ModifiertypemaisonComponent} from '../components/typemaison/modifiertypemaison/modifiertypemaison.component';
import {SocieteconstructionComponent} from '../components/societeconstruction/societeconstruction.component';
import {AjoutersocieteComponent} from '../components/societeconstruction/ajoutersociete/ajoutersociete.component';
import {ModifiersocieteComponent} from '../components/societeconstruction/modifiersociete/modifiersociete.component';
import {RecueilbesoinComponent} from '../components/recueilbesoin/recueilbesoin.component';
import {AjouterbesoinComponent} from '../components/recueilbesoin/ajouterbesoin/ajouterbesoin.component';
import {ModifierbesoinComponent} from '../components/recueilbesoin/modifierbesoin/modifierbesoin.component';
import {VoirbesoinComponent} from '../components/recueilbesoin/voirbesoin/voirbesoin.component';
import {AnnulerbesoinComponent} from '../components/recueilbesoin/annulerbesoin/annulerbesoin.component';
import {ManageprestataireComponent} from '../components/chantier/manageprestataire/manageprestataire.component';
import {BesoinjournalierComponent} from '../components/besoinjournalier/besoinjournalier.component';
import {AjouterbesoinjournalierComponent} from '../components/besoinjournalier/ajouterbesoinjournalier/ajouterbesoinjournalier.component';
import {ModifierbesoinjournalierComponent} from '../components/besoinjournalier/modifierbesoinjournalier/modifierbesoinjournalier.component';
import {VoirdetailbesoinComponent} from '../components/besoinjournalier/voirdetailbesoin/voirdetailbesoin.component';
import {EffectuerenlevementComponent} from '../components/enlevement/effectuerenlevement/effectuerenlevement.component';
import {VoiralldetailbesoinleveloneComponent} from '../components/besoinjournalier/voiralldetailbesoinlevelone/voiralldetailbesoinlevelone.component';
import {VoiralldetailbesoinbymaisonleveltwoComponent} from '../components/besoinjournalier/voiralldetailbesoinbymaisonleveltwo/voiralldetailbesoinbymaisonleveltwo.component';
import {EnlevementComponent} from '../components/enlevement/enlevement.component';
import {VoirenlevementComponent} from '../components/enlevement/voirenlevement/voirenlevement.component';
import {MouvementStocksPrefatComponent} from '../components/stockprefat/mouvement-stocks-prefat/mouvement-stocks-prefat.component';
import {StockprefatComponent} from '../components/stockprefat/stockprefat.component';
import {MouvementStocksByArticleComponent} from '../components/stocks/mouvement-stocks-by-article/mouvement-stocks-by-article.component';
import {ManageprestataireinprefatComponent} from '../components/chantier/manageprestataireinprefat/manageprestataireinprefat.component';
import {VoirdetailbesoinforprefatComponent} from '../components/besoinjournalier/voirdetailbesoinforprefat/voirdetailbesoinforprefat.component';
import {ModifierbesoinjournalierforprefatComponent} from '../components/besoinjournalier/modifierbesoinjournalierforprefat/modifierbesoinjournalierforprefat.component';
import {ManageetapeencoursformaisonComponent} from "../components/manageetapeencoursformaison/manageetapeencoursformaison.component";


const routes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'login',  component: LoginComponent},
  {
    path: 'start',
    component: StarterComponent,
    canActivate: [AfterLoginService],
    children: [
      { path: 'dashboard', component: DashboardComponent },
      { path: 'fournisseurs', component: FournisseurComponent },
      { path: 'auditing', component: AuditingComponent },
      { path: 'typechantiers', component: TypechantierComponent },
      { path: 'ajouter-typechantier', component: AjoutertypechantierComponent },
      { path: 'modifier-typechantier', component: ModifiertypechantierComponent },
      { path: 'employes', component: EmployeComponent },
      { path: 'ajouter-employe', component: AjouteremployeComponent },
      { path: 'modifier-employe', component: ModifieremployeComponent },
      { path: 'etapes', component: EtapeconstructionComponent },
      { path: 'ajouter-etape', component: AjouteretapeComponent },
      { path: 'modifier-etape', component: MofifieretapeComponent },
      { path: 'societes', component: SocieteconstructionComponent },
      { path: 'ajouter-societe', component: AjoutersocieteComponent },
      { path: 'modifier-societe', component: ModifiersocieteComponent },
      { path: 'typemaisons', component: TypemaisonComponent },
      { path: 'ajouter-typemaison', component: AjoutertypemaisonComponent },
      { path: 'modifier-typemaison', component: ModifiertypemaisonComponent },
      { path: 'fonctions', component: FonctionsComponent },
      { path: 'ajouter-fonction', component: AjouterfonctionsComponent },
      { path: 'modifier-fonction', component: ModifierfonctionsComponent },
      { path: 'personnels', component: PersonnelsComponent },
      { path: 'manage-prestataire', component: ManageprestataireComponent },
      { path: 'manage-prestataire-in-prefat', component: ManageprestataireinprefatComponent },
      { path: 'ajouter-personnel', component: AjouterpersonnelsComponent },
      { path: 'modifier-personnel', component: ModifierpersonnelsComponent },
      { path: 'receptions', component: ReceptionsComponent },
      { path: 'recueils', component: RecueilbesoinComponent },
      { path: 'besoinjournaliers', component: BesoinjournalierComponent },
      { path: 'ajouter-besoinjournalier', component: AjouterbesoinjournalierComponent },
      { path: 'modifier-besoinjournalier', component: ModifierbesoinjournalierComponent },
      { path: 'modifier-besoinjournalierforprefat', component: ModifierbesoinjournalierforprefatComponent },
      { path: 'voir-besoinjournalier', component: VoirdetailbesoinComponent },
      { path: 'voir-besoinjournalierforprefat', component: VoirdetailbesoinforprefatComponent},
      { path: 'voir-detailbesoinjournalier', component: VoiralldetailbesoinleveloneComponent },
      { path: 'voir-detailbesoinjournalierforonehouseorprefat', component: VoiralldetailbesoinbymaisonleveltwoComponent },
      { path: 'effectuer-enlevement', component: EffectuerenlevementComponent },
      { path: 'voir-enlevement', component: VoirenlevementComponent },
      { path: 'enlevements', component: EnlevementComponent },
      { path: 'ajouter-recueil', component: AjouterbesoinComponent },
      { path: 'annuler-recueil', component: AnnulerbesoinComponent },
      { path: 'modifier-recueil', component: ModifierbesoinComponent },
      { path: 'voir-recueil', component: VoirbesoinComponent },
      { path: 'commandes', component: CommandesComponent },
      { path: 'ajouter-commande', component: AjoutercommandeComponent },
      { path: 'annuler-commande', component: AnnulercommandeComponent },
      { path: 'modifier-commande', component: ModifiercommandeComponent },
      { path: 'voir-detailcommande', component: VoirdetailcommandesComponent },
      { path: 'voir-detailreception', component: VoirunereceptionComponent },
      { path: 'articles', component: ArticleComponent },
      { path: 'modepaiements', component: ModepaiementComponent },
      { path: 'ajouter-modepaiements', component: AjoutermodepaiementComponent },
      { path: 'modifier-modepaiements', component: ModifiermodepaiementComponent },
      { path: 'chantiers', component: ChantierComponent },
      { path: 'voir-chantier', component: VoirchantierComponent },
      { path: 'ajouter-chantier', component: AjouterchantierComponent },
      { path: 'modifier-chantier', component: ModifierchantierComponent },
      { path: 'stocks', component: StocksComponent },
      { path: 'ajouter-article', component: AjouterarticleComponent },
      { path: 'modifier-article', component: ModifierarticleComponent },
      { path: 'permissions', component: PermissionComponent },
      { path: 'ajouter-permission', component: AjouterpermissionComponent },
      { path: 'modifier-permission', component: ModifierpermissionComponent },
      { path: 'roles', component: RoleComponent },
      { path: 'ajouter-role', component: AjouterroleComponent },
      { path: 'modifier-role', component: ModifierroleComponent },
      { path: 'ajouter-fournisseur', component: AjouterfournisseurComponent },
      { path: 'modifier-fournisseur', component: ModifierfournisseurComponent },
      { path: 'mouvement-stocks', component: MouvementStocksComponent },
      { path: 'mouvement-stocks-prefat', component: MouvementStocksPrefatComponent },
      { path: 'mouvement-stocks-by-article', component: MouvementStocksByArticleComponent },
      { path: 'stocksprefat', component: StockprefatComponent },
      { path: 'effectuer-reception', component: EffectuerunereceptionComponent },
      { path: 'manageetapencoursformaison', component: ManageetapeencoursformaisonComponent }
    ]
  }
];

@NgModule({
  declarations: [],
  exports: [RouterModule],
  imports: [
    RouterModule.forRoot(routes)
  ]
})
export class AppRoutingModule { }
