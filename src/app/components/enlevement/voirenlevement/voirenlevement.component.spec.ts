import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoirenlevementComponent } from './voirenlevement.component';

describe('VoirenlevementComponent', () => {
  let component: VoirenlevementComponent;
  let fixture: ComponentFixture<VoirenlevementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoirenlevementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoirenlevementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
