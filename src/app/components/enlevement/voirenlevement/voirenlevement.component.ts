import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {EnlevementService} from '../shared/enlevement.service';
import {ChantierService} from '../../chantier/shared/chantier.service';
import {Enlevement} from '../shared/detailenlevement';

@Component({
  selector: 'app-voirenlevement',
  templateUrl: './voirenlevement.component.html',
  styleUrls: ['./voirenlevement.component.css']
})
export class VoirenlevementComponent implements OnInit {
  firstlevel: string;
  secondlevel: string;
  enlevement: Enlevement;
  // tslint:disable-next-line:max-line-length
  public libelletypeenlevement: string;
  // tslint:disable-next-line:max-line-length
  constructor(private router: Router, private cdRef: ChangeDetectorRef, public ApiEnlevement: EnlevementService, public chantierService: ChantierService) { }

  ngOnInit() {
    const idenlevement =   window.localStorage.getItem('IDEnlevement');
    const idchantier =  window.localStorage.getItem('Idchantier');
    this.libelletypeenlevement = window.localStorage.getItem('libelletypeenlevement');
    console.log('id:' + idenlevement);
    if (this.libelletypeenlevement === 'Enlevement vers les maisons') {
      this.firstlevel = 'ENLEVEMENT VERS MAISONS';
      this.secondlevel = 'DETAIL ENLEVEMENT VERS MAISONS';
      this.ApiEnlevement.getSelectedEnlevement(+idenlevement, +idchantier).subscribe((data) => {
      this.enlevement = data.data;
      console.log(data);
    });
    } else {
      this.firstlevel = 'ENLEVEMENT VERS PREFATS';
      this.secondlevel = 'DETAIL ENLEVEMENT VERS PREFATS';
      this.ApiEnlevement.getSelectedEnlevement(+idenlevement, +idchantier).subscribe((data) => {
        this.enlevement = data.data;
        console.log(data);
      });
    }
  }

}
