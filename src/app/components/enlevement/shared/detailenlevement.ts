export class Enlevement {
  // tslint:disable-next-line:no-misused-new
  constructor(
    // tslint:disable-next-line:variable-name
    public numero_En: string,
    public chantier_id: number,
    public statut_En: string,
    public typeenlevement: string,
    public magasinier_id: number,
    public date_En: Date,
    public besoin_journalier_id: number,
    public livreur_id: number,
    public nomtechnicien: string,
    public nommagasinier: string,
    public telephonemagasinier: string,
    public nomlivreur: string,
    public prenomlivreur: string,
    public telephonelivreur: string,
    public telephonetechnicien: string,
    public articles: Detailenlevement[],
    public id: number) {}
}


export class Detailenlevement {
  // tslint:disable-next-line:no-misused-new
  constructor(
    public id: number,
    public maison_id: number,
    public etape_construction_id: number,
    public article_id: number,
    public quantiteBesoin: number,
    public commentaires: string,
    public article: string,
    public libelleetape: string,
    public libelleprefat: string,
    public libellemaison: string,
    public quantiteASortir: number,
    public checked: boolean,
    public reliquat: number,
    public fonction_id: number,
    public prefat_id: number,
    public libellefonction: string,
    public nomprestataire: string,
    public prenomprestataire: string,
    public besoin_jitems_id: number,
    public quantitevirtuel: number,
    // tslint:disable-next-line:align
  ) {
  }
}
