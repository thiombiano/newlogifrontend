import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Besoinjournalier} from '../../besoinjournalier/shared/besoinjournalier.model';
import {environment} from '../../../../environments/environment';
import {Enlevement} from './detailenlevement';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {Prefat} from '../../receptions/shared/articletoprefat.interface';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Access-Control-Allow-Origin': '*'
  })
};

@Injectable({
  providedIn: 'root'
})
export class EnlevementService {

  constructor(public http: HttpClient) { }

  addChantierEnlevement(idChantier: number, enlevement: Enlevement) {
    return this.http
      .post<Enlevement>(
        environment.apiUrl + '/chantiers/' + idChantier + '/enlevements',
        enlevement,
        httpOptions
      );
  }

  addChantierEnlevementDirectOnHome(idChantier: number, enlevement: Enlevement) {
    return this.http
      .post<Enlevement>(
        environment.apiUrl + '/chantiers/' + idChantier + '/enlevementforhomedirectly',
        enlevement,
        httpOptions
      );
  }

  addChantierEnlevementDirectOnPrefat(idChantier: number, enlevement: Enlevement) {
    return this.http
      .post(
        environment.apiUrl + '/chantiers/' + idChantier + '/enlevementforprefatdirectly',
        enlevement,
        httpOptions
      );
  }

  getAllEnlevementList(idChantier: number, idutilisateur: number) {
    return this.http.get(environment.apiUrl + '/enlevementsbymagasinier/' + idChantier + '/' + idutilisateur)
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Impossible dobtenir la liste de tous les enlevments!'))
      );
  }

  getSelectedEnlevement(idenlevement: number, idchantier: number) {
    // tslint:disable-next-line:max-line-length
    return  this.http.get(environment.apiUrl + '/chantiers/' + idenlevement + '/enlevements/' + idchantier ) .pipe(map((data: any) => data ),
      catchError(error => throwError('Impossible dobtenir la liste des enlèvements'))
    );
  }

}
