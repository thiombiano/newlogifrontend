import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EffectuerenlevementComponent } from './effectuerenlevement.component';

describe('EffectuerenlevementComponent', () => {
  let component: EffectuerenlevementComponent;
  let fixture: ComponentFixture<EffectuerenlevementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EffectuerenlevementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EffectuerenlevementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
