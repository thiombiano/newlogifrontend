import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {BesoinJItem, Besoinjournalier} from '../../besoinjournalier/shared/besoinjournalier.model';
import {Router} from '@angular/router';
import {BesoinjournalierService} from '../../besoinjournalier/shared/besoinjournalier.service';
import {ChantierService} from '../../chantier/shared/chantier.service';
import {MatTableDataSource} from '@angular/material';
import {Employe} from '../../employe/shared/employe.model';
import {Detailenlevement, Enlevement} from '../shared/detailenlevement';
import {EnlevementService} from '../shared/enlevement.service';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {SelectionModel} from '@angular/cdk/collections';
import {Livreur} from '../../livreur/shared/livreur.model';
import {LivreurService} from '../../livreur/shared/livreur.service';

@Component({
  selector: 'app-effectuerenlevement',
  templateUrl: './effectuerenlevement.component.html',
  styleUrls: ['./effectuerenlevement.component.css']
})
export class EffectuerenlevementComponent implements OnInit {
  firstlevel: string;
  secondlevel: string;
  // @ts-ignore
  enlevement: Enlevement;
  dataSource: any;
  numerocahierjournalier: string;
  idlivreur: number;
  quantiteasortir: any [];
  letechnicien: Employe;
  listebesoinjouralier: Besoinjournalier[];
  // @ts-ignore
  listeenlevement: Enlevement = new Detailenlevement();
  // tslint:disable-next-line:max-line-length
  headElement = ['Checked', 'N°', 'Maison', 'Fonction', 'Prestataire', 'Article', 'Quantite commande', 'Quantite', 'EtapeAvancement', 'QuantiteASortir'];
  // tslint:disable-next-line:max-line-length
  headElementForPrefat = ['Checked', 'N°', 'Prefat', 'Fonction', 'Prestataire', 'Article', 'Quantite commande', 'Quantite', 'QuantiteASortir'];
  // tslint:disable-next-line:max-line-length
  selectedtypeenlevement: string;
  // tslint:disable-next-line:max-line-length
  public stateelement: boolean;
  IsDispoForEnlevement: boolean;
  livreurs: Livreur[];
  selection = new SelectionModel<Enlevement>(true, []);
  public libelletypebesoin: string;
  // tslint:disable-next-line:max-line-length
  constructor(public ApiLivreur: LivreurService, public route: Router, private notifyService: NotificationService, private toastr: ToastrService, public ApiEnlevement: EnlevementService, private router: Router, private cdRef: ChangeDetectorRef, public ApiBesoin: BesoinjournalierService, public chantierService: ChantierService) { }

  ngOnInit() {
    this.IsDispoForEnlevement = false;
    this.stateelement = false;
    this.firstlevel = 'Enlèvement';
    this.secondlevel = 'Effectuer un enlèvement';
    this.selectedtypeenlevement = '';
    // this.clearBesionDetail();
    this.getListeDesBesoinsJournaliers();
    this. getListeDesLivreurs();
    // this.ListerTousLesBesoinsJournaliers();
    this.enlevement = {
      besoin_journalier_id: null,
      numero_En: null,
      nomtechnicien: null,
      livreur_id: null,
      nomlivreur: null,
      typeenlevement: null,
      prenomlivreur: null,
      nommagasinier: null,
      date_En: null,
      statut_En: null,
      chantier_id: null,
      telephonemagasinier: null,
      telephonetechnicien: null,
      telephonelivreur: null,
      magasinier_id: null,
      articles: new Array<Detailenlevement>(),
      id: null
    };

  }

  getListeDesBesoinsJournaliers() {
    const idchantier =  window.localStorage.getItem('Idchantier');
    this.ApiBesoin.getAllBesoinForThisDay(+idchantier).subscribe((data) => {
      this.listebesoinjouralier = data.data;
    });
  }

  getListeDesLivreurs() {
    this.ApiLivreur.getAllLivreurList().subscribe((data) => {
      this.livreurs = data;
    });
  }

  clearBesionDetail() {
    this.enlevement = undefined;
    this.enlevement = {
      besoin_journalier_id: null,
      nomtechnicien: null,
      typeenlevement: null,
      nommagasinier: null,
      numero_En: null,
      date_En: null,
      statut_En: null,
      livreur_id: null,
      nomlivreur: null,
      prenomlivreur: null,
      chantier_id: null,
      telephonemagasinier: null,
      telephonetechnicien: null,
      telephonelivreur: null,
      magasinier_id: null,
      articles: new Array<Detailenlevement>(),
      id: null
    };

    // for(let i = 0 ; i < this.dataSource._data._value.length; i++) {
    //   if(this.dataSource._data._value[i].checked == true) {
    //     this.IsDispoForEnlevement = true;
    //   }
    // }
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
  call() {
    console.log(this.selection.selected);
  }

  // ListerTousLesBesoinsJournaliers() {
  //   this.clearBesionDetail();
  //   const idchantier =  window.localStorage.getItem('Idchantier');
  //   this.ApiBesoin.getAllBesoinForThisDay(+idchantier).subscribe((data) => {
  //     console.log('Voila le nombre de données : ' + data );
  //     console.log('Voila les données : ' + JSON.stringify(data) );
  //     // tslint:disable-next-line:prefer-for-of
  //     for (let i = 0; i < data.data.length; i++) {
  //       console.log('Voila le données : ' + data.data.length );
  //       // tslint:disable-next-line:prefer-for-of
  //       for (let j = 0 ; j < data.data[i].articles.length; j++) {
  //         console.log('je suis dedans');
  //         this.listeenlevement.articles.push( {
  //           id: data.data[i].articles[j].id,
  //           article: data.data[i].articles[j].article,
  //           etape_construction_id: data.data[i].articles[j].etape_construction_id,
  //           article_id: data.data[i].articles[j].article_id,
  //           quantiteBesoin: data.data[i].articles[j].quantiteBesoin,
  //           libelleetape: data.data[i].articles[j].libelleetape,
  //           libellemaison: data.data[i].articles[j].libellemaison,
  //           quantiteASortir: data.data[i].articles[j].quantiteBesoin,
  //           maison_id: data.data[i].articles[j].maison_id,
  //           checked: null,
  //           reliquat: data.data[i].articles[j].reliquat,
  //           commentaires: null,
  //           fonction_id: data.data[i].articles[j].fonction_id,
  //           libellefonction: data.data[i].articles[j].libellefonction,
  //           nomprestataire: data.data[i].articles[j].nomprestataire,
  //           prenomprestataire: data.data[i].articles[j].prenomprestataire,
  //           besoin_jitems_id:  data.data[i].articles[j]. data.data[i].articles[j].besoin_jitems_id
  //         });
  //       }
  //       this.dataSource = new MatTableDataSource(this.listeenlevement.articles);
  //     }
  //   });
  // }


  showToasterEnlevementEnregistrer() {
    this.notifyService.showSuccessWithTimeout('Enlèvement enregistré avec succès !!', 'Notification', 5000);
  }

  showToasterEnlevementErreurQuantite() {
    this.notifyService.showErrorWithTimeout('Erreur: Veuillez verifier les quantités saisies !!', 'Notification', 5000);
  }

  showToasterErreurEnlevement() {
    this.notifyService.showErrorWithTimeout('Erreur: Veuillez sélectionner les données dans le tableau !!', 'Notification', 5000);
  }

  showToasterErreurLivreur() {
    this.notifyService.showErrorWithTimeout('Erreur: Veuillez sélectionner le livreur!!', 'Notification', 5000);
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  effectuerunenlevement() {
    console.log(this.dataSource._data._value);
    const idchantier =  window.localStorage.getItem('Idchantier');
    const idutilisateur =  window.localStorage.getItem('idutilisateur');
    const idbesoin =  window.localStorage.getItem('IdbesoinJ');
    this.enlevement.chantier_id = +idchantier;
    this.enlevement.magasinier_id = +idutilisateur;
    this.enlevement.besoin_journalier_id = +idbesoin;
    this.enlevement.livreur_id = +this.idlivreur;
    if (this.libelletypebesoin === 'Vers les maisons') {
      this.enlevement.typeenlevement = 'Enlevement vers les maisons';
    } else {
      this.enlevement.typeenlevement = 'Enlevement vers les préfats';
    }
    console.log('ID LIVREUR : ' + this.enlevement.livreur_id);
    if (isNaN(this.enlevement.livreur_id)) {
      this.showToasterErreurLivreur();
      return;
    }
    let dataArray = [];
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0 ; i < this.dataSource._data._value.length; i++) {
      if (this.dataSource._data._value[i].checked === true) {
        dataArray.push(this.dataSource._data._value[i]);
        console.log('le tableau est element is true: ' + this.dataSource._data._value[i]);
      }
    }
    dataArray = this.selection.selected;
    console.log('le tableau est : ' + JSON.stringify(dataArray));
    this.enlevement.articles = dataArray;
    if (dataArray.length > 0) {
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0 ; i < this.enlevement.articles.length; i++) {
        if (this.enlevement.articles[i].quantiteASortir > this.enlevement.articles[i].quantiteBesoin) {
          this.showToasterEnlevementErreurQuantite();
          return;
        }
      }

      this.ApiEnlevement.addChantierEnlevement(+idchantier,  this.enlevement).subscribe((enlevement: Enlevement) => {
        console.log(enlevement);
        this.showToasterEnlevementEnregistrer();
        const idbesoin = window.localStorage.getItem('IdbesoinJ');
        // this.GetEnlevementState(idbesoin);
        this.router.navigateByUrl('/start/enlevements');
      });

    } else {
      this.showToasterErreurEnlevement();
    }

  }

  GetEnlevementState(value) {
      this.clearBesionDetail();
      window.localStorage.removeItem('IdbesoinJ');
      window.localStorage.setItem('IdbesoinJ', value.id);
      this.libelletypebesoin = value.typebesoin;
      this.ApiBesoin.getSelectedBesoin(+value.id).subscribe((data) => {
        const permArray = [];
        console.log('VOICI LES DATA : ' + data.data.articles.length);

        // tslint:disable-next-line:prefer-for-of
        for (let i = 0 ; i < data.data.articles.length; i++) {
          if (data.data.articles[i].is_out !== 1 ) {
            // @ts-ignore
            this.enlevement.articles.push({
              id: data.data.articles[i].id,
              article: data.data.articles[i].article,
              etape_construction_id: data.data.articles[i].etape_construction_id,
              article_id: data.data.articles[i].article_id,
              quantiteBesoin: data.data.articles[i].quantiteBesoin,
              libelleetape: data.data.articles[i].libelleetape,
              libellemaison: data.data.articles[i].libellemaison,
              libelleprefat: data.data.articles[i].libelleprefat,
              quantiteASortir: data.data.articles[i].reliquat,
              maison_id: data.data.articles[i].maison_id,
              prefat_id: data.data.articles[i].prefat_id,
              checked: null,
              reliquat: data.data.articles[i].reliquat,
              commentaires: null,
              fonction_id: data.data.articles[i].fonction_id,
              libellefonction: data.data.articles[i].libellefonction,
              nomprestataire: data.data.articles[i].nomprestataire,
              prenomprestataire: data.data.articles[i].prenomprestataire,
              besoin_jitems_id: data.data.articles[i].besoin_jitems_id,
            });
          }
        }
        if (this.enlevement.articles.length > 0) {
          this.IsDispoForEnlevement = true;
        } else {
          this.IsDispoForEnlevement = false;
        }
        this.numerocahierjournalier = data.data.cahierjournalier;
        this.letechnicien = data.data.technicien[0];
        console.log(permArray);
        this.dataSource = new MatTableDataSource(this.enlevement.articles);
      });

  }


}
