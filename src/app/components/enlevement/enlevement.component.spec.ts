import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnlevementComponent } from './enlevement.component';

describe('EnlevementComponent', () => {
  let component: EnlevementComponent;
  let fixture: ComponentFixture<EnlevementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnlevementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnlevementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
