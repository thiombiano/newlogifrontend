import {ChangeDetectorRef, Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {BesoinjournalierService} from '../besoinjournalier/shared/besoinjournalier.service';
import {ChantierService} from '../chantier/shared/chantier.service';
import {EnlevementService} from './shared/enlevement.service';
import {Besoinjournalier} from '../besoinjournalier/shared/besoinjournalier.model';
import {Enlevement} from './shared/detailenlevement';
import {MdbTableDirective, MdbTablePaginationComponent} from 'angular-bootstrap-md';

@Component({
  selector: 'app-enlevement',
  templateUrl: './enlevement.component.html',
  styleUrls: ['./enlevement.component.css']
})
export class EnlevementComponent implements OnInit {
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  firstlevel: string;
  elements: any = [];
  display = 'none';
  previous: string;
  searchText = '';
  secondlevel: string;
  enlevements: Enlevement[];
  headElements = ['N°',  'ENLEVEMENT', 'TYPE ENLEVEMENT', 'DATE ENLEVEMENT', 'Actions'];
  public totalenlevement: number;
  // tslint:disable-next-line:max-line-length
  constructor(private router: Router, private cdRef: ChangeDetectorRef, public ApiEnlevement: EnlevementService, public chantierService: ChantierService) { }

  ngOnInit() {
    this.firstlevel = 'Tableau de bord';
    this.secondlevel = 'Liste des enlèvements';
    const id =  window.localStorage.getItem('Idchantier');
    this.getListeEnlevement(+id);
  }

  searchItems() {
    const prev = this.enlevements;

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.elements = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(15);
    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

  @HostListener('input') oninput() {
    this.searchItems();
  }

  getListeEnlevement(id) {
    const idutilisateur =  window.localStorage.getItem('idutilisateur');
    this.ApiEnlevement.getAllEnlevementList(id, +idutilisateur).subscribe(
      data => {
        this.enlevements = data.data;
        this.totalenlevement =  this.enlevements.length;
        this.mdbTable.setDataSource(this.enlevements);
        this.elements = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();
      },
      err => console.error(err),
      () => console.log('liste des enlevements obtenue')
    );
  }


  voirdetailEnlevement(id, typeenlevement) {
    window.localStorage.removeItem('IDEnlevement');
    window.localStorage.removeItem('libelletypeenlevement');
    window.localStorage.setItem('IDEnlevement', id);
    window.localStorage.setItem('libelletypeenlevement', typeenlevement);
    this.router.navigate(['/start/voir-enlevement']);
  }

}
