import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Lot} from '../shared/lot.model';
import {LotService} from '../shared/lot.service';
import {FormControl, Validators} from '@angular/forms';
import {Section} from '../../section/shared/section.model';
import {SectionService} from '../../section/shared/section.service';

@Component({
  selector: 'app-ajouterlot',
  templateUrl: './ajouterlot.component.html',
  styleUrls: ['./ajouterlot.component.css']
})
export class AjouterlotComponent {
  constructor(public ApiSection: SectionService, public dialogRef: MatDialogRef<AjouterlotComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Lot,
              @Inject(MAT_DIALOG_DATA) public datasections: Section[],
              public ApiLot: LotService) { }
  sections: Section[];
  @Output() passEntry: EventEmitter<any> = new EventEmitter();

  formControl = new FormControl('', [
    Validators.required
  ]);

  // ListSectionOfChantier(id: number) {
  //   this.ApiSection.getSectionList(+id).subscribe((section: Section[]) => {
  //     this.sections = section;
  //     console.log(section);
  //   });
  // }

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Le champ est requis' : '';
  }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    console.log('les données pour la section : ' + this.data['getallsections']);
    console.log('les données dans le modal :' + this.data.section_id);
    this.ApiLot.addLot(this.data).subscribe((lot: Lot) => {
      this.passEntry.emit(lot);
      console.log(lot);
    });
  }

}
