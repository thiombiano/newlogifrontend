import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjouterlotComponent } from './ajouterlot.component';

describe('AjouterlotComponent', () => {
  let component: AjouterlotComponent;
  let fixture: ComponentFixture<AjouterlotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjouterlotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjouterlotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
