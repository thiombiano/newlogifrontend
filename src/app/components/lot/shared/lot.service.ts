import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Section} from '../../section/shared/section.model';
import {environment} from '../../../../environments/environment';
import {Lot} from './lot.model';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LotService {

  constructor(private httpclient: HttpClient) { }

  addLot(lot: Lot) {
    return  this.httpclient.post<Lot>(environment.apiUrl +  '/lots', lot);
  }

  deleteLot(id: number) {
    // @ts-ignore
    return this.httpclient.delete<Lot>(environment.apiUrl +  '/lots/' + id);
  }

  updateLot(id, value: any ) {
    return this.httpclient.put<Lot>(environment.apiUrl + '/lots/' + id, value );
  }

  getLotList(idchantier: number) {
    return  this.httpclient.get(environment.apiUrl + '/lotstoaddhome/' + idchantier)
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Impossible les lots!'))
      );

  }

  getLotBySection(idSection: number)  {
    return  this.httpclient.get<Section[]>(environment.apiUrl + '/sections/' + idSection + '/lots')
    .pipe(map((data: any) => data ),
        catchError(error => throwError('Impossible les lots par section!'))
      );
  }

  getLotByMaison(idLot: number)  {
    return  this.httpclient.get<Lot[]>(environment.apiUrl + '/lots/' + idLot)
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Impossible les maisons par lots!'))
      );
  }

  getMaisonBySection(idSection: number)  {
    return  this.httpclient.get<Lot[]>(environment.apiUrl + '/maisonbysections/' + idSection)
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Impossible les maisons par section'))
      );
  }

  getALLMaisonByChantier(idChantier: number)  {
    return  this.httpclient.get<Lot[]>(environment.apiUrl + '/maisonbychantiers/' + idChantier)
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Impossible les maisons par chantier'))
      );
  }


}
