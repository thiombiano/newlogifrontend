export class Lot {
  id: number;
  libellelot: string;
  is_actif: boolean;
  section_id: number;
}
