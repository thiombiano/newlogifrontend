import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupprimerlotComponent } from './supprimerlot.component';

describe('SupprimerlotComponent', () => {
  let component: SupprimerlotComponent;
  let fixture: ComponentFixture<SupprimerlotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupprimerlotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupprimerlotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
