import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {SectionService} from '../../section/shared/section.service';
import {LotService} from '../shared/lot.service';
import {Section} from '../../section/shared/section.model';
import {Lot} from '../shared/lot.model';

@Component({
  selector: 'app-supprimerlot',
  templateUrl: './supprimerlot.component.html',
  styleUrls: ['./supprimerlot.component.css']
})
export class SupprimerlotComponent {
  @Output() passEntry: EventEmitter<any> = new EventEmitter();
  constructor(public dialogRef: MatDialogRef<SupprimerlotComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public ApiLot: LotService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    if(this.data.id) {
      this.ApiLot.deleteLot(this.data.id).subscribe((lot: Lot) => {
        console.log(lot);
        this.passEntry.emit(lot);
      });
    }
  }
}
