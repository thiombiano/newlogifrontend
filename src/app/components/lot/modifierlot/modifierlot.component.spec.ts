import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifierlotComponent } from './modifierlot.component';

describe('ModifierlotComponent', () => {
  let component: ModifierlotComponent;
  let fixture: ComponentFixture<ModifierlotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifierlotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifierlotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
