import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {SectionService} from '../../section/shared/section.service';
import {LotService} from '../shared/lot.service';
import {FormControl, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';
import {Section} from '../../section/shared/section.model';
import {Lot} from '../shared/lot.model';

@Component({
  selector: 'app-modifierlot',
  templateUrl: './modifierlot.component.html',
  styleUrls: ['./modifierlot.component.css']
})
export class ModifierlotComponent  {
  @Output() passEntry: EventEmitter<any> = new EventEmitter();
  constructor(public dialogRef: MatDialogRef<ModifierlotComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public ApiLot: LotService) { }

  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Champ requis' : '';
  }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  stopEdit(): void {
    console.log('les données dans le modal :' + this.data);
    const idlot = this.data.id;
    // @ts-ignore
    this.ApiLot.updateLot(+idlot , this.data)
      .pipe(first())
      .subscribe((lot: Lot) => {
        console.log(lot);
        this.passEntry.emit(lot);
      });
  }

}
