import { Injectable } from '@angular/core';
import {Fournisseur} from '../../fournisseur/shared/fournisseur';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Personnel} from './personnel';
import {Employe} from '../../employe/shared/employe.model';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PersonnelsService {

  constructor(private http: HttpClient) { }

  getAllPersonnelList() {
    return  this.http.get<Personnel[]>(environment.apiUrl + '/employes');
  }

  getPersonnel(id: number) {
    return  this.http.get(environment.apiUrl + '/employes/' + id);
  }

  updatePersonnel(id, value: any) {
    return this.http.post(environment.apiUrl + '/employes/' + id, value );
  }

  addPersonnel(personnel: Personnel) {
    return  this.http.post<Personnel>(environment.apiUrl + '/employes', personnel);
  }

  deletePersonnel(personnel: Personnel) {
    // @ts-ignore
    return this.http.delete<Personnel>(environment.apiUrl + '/employes/' +  personnel.id);
  }


  getemployebyfunction(idfonction: number) {
    return this.http.get<Personnel[]>(environment.apiUrl + '/employebyfonction/' + idfonction)
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Impossible dobtenir les employes par fonctions!'))
      );
  }

}
