import {Fonction} from '../../fonctions/shared/fonction';

export class Personnel {
  id: number;
  codeEmploye: string;
  prenom: string;
  nom: string;
  tel: string;
  cnib: string;
  fonction: Fonction[];
  email: string;
  societe_constructive_id: number;
  // tslint:disable-next-line:variable-name
  fonction_id: number;
}
