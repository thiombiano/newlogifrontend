import {ChangeDetectorRef, Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {NotificationService} from '../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {FournisseurService} from '../fournisseur/shared/fournisseur.service';
import {Router} from '@angular/router';
import {PersonnelsService} from './shared/personnels.service';
import {Fournisseur} from '../fournisseur/shared/fournisseur';
import {Personnel} from './shared/personnel';
import {MdbTableDirective, MdbTablePaginationComponent} from 'angular-bootstrap-md';

@Component({
  selector: 'app-personnels',
  templateUrl: './personnels.component.html',
  styleUrls: ['./personnels.component.css']
})
export class PersonnelsComponent implements OnInit {
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, private toastr: ToastrService, private cdRef: ChangeDetectorRef, private ApiPersonnel: PersonnelsService, private router: Router) { }
  headElements = ['N°', 'Code Employe', 'Nom', 'Prenom', 'CNIB' , 'Fonction', 'Téléphone', 'Action'];
  searchText = '';
  employes: Personnel[];
  totalpersonnel = 0;
  elements: any = [];
  previous: string;
  firstlevel: string;
  secondlevel: string;
  popoverTitle = 'Suppression de ce prestataire';
  popoverMessage = 'Voulez vous vraiment supprimer ce prestataire';
  cancelClicked = false;

  ngOnInit() {
    this.loadAllPrestataire();
  }

  loadAllPrestataire() {
    this.ApiPersonnel.getAllPersonnelList().subscribe((employes: Personnel[]) => {
      this.employes = employes;
      this.firstlevel = 'Tableau de bord';
      this.secondlevel = 'Prestataires';
      this.totalpersonnel = employes.length;
      this.mdbTable.setDataSource(employes);
      this.elements = this.mdbTable.getDataSource();
      this.previous = this.mdbTable.getDataSource();
    });
  }


  @HostListener('input') oninput() {
    this.searchItems();
  }

  get ListFournisseur() {
    return this.ApiPersonnel.getAllPersonnelList();
  }

  searchItems() {
    const prev = this.employes;

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.elements = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(6);

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

  editPersonnel(employe: Personnel): void  {
    window.localStorage.removeItem('editEmployeId');
    window.localStorage.setItem('editEmployeId', employe.id.toString());
    this.ApiPersonnel.getPersonnel(employe.id);
    this.router.navigate(['start/modifier-personnel']);
  }

  showToasterPersonnelSupprimer() {
    this.notifyService.showSuccessWithTimeout('Info de l\'employe mis à jour avec succès !!', 'Notification', 5000);
  }

  deletePersonnel(personnel: Personnel) {
    this.ApiPersonnel.deletePersonnel(personnel).subscribe((data) => {
      this.employes = this.employes.filter(s => s !== personnel);
      this.loadAllPrestataire();
      this.showToasterPersonnelSupprimer(); });
  }

}
