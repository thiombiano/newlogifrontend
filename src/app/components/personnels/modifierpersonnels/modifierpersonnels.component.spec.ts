import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifierpersonnelsComponent } from './modifierpersonnels.component';

describe('ModifierpersonnelsComponent', () => {
  let component: ModifierpersonnelsComponent;
  let fixture: ComponentFixture<ModifierpersonnelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifierpersonnelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifierpersonnelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
