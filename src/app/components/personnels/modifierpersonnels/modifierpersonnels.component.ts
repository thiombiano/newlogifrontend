import { Component, OnInit } from '@angular/core';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FournisseurService} from '../../fournisseur/shared/fournisseur.service';
import {Router} from '@angular/router';
import {PersonnelsService} from '../shared/personnels.service';
import {Personnel} from '../shared/personnel';
import {first} from 'rxjs/operators';
import {Fournisseur} from '../../fournisseur/shared/fournisseur';
import {Fonction} from '../../fonctions/shared/fonction';
import {FonctionsService} from '../../fonctions/shared/fonctions.service';
import {SocieteconstructionService} from '../../societeconstruction/shared/societeconstruction.service';
import {Societeconstruction} from '../../societeconstruction/shared/societeconstruction.model';

@Component({
  selector: 'app-modifierpersonnels',
  templateUrl: './modifierpersonnels.component.html',
  styleUrls: ['./modifierpersonnels.component.css']
})
export class ModifierpersonnelsComponent implements OnInit {
  firstlevel: string;
  secondlevel: string;
  editForm: FormGroup;
  submitted = false;
  fonctions: Fonction[];
  employe: Personnel;
  public societes: Societeconstruction[];
  // tslint:disable-next-line:max-line-length
  constructor(public ApiSocieteConstructive: SocieteconstructionService, private apiFonction: FonctionsService, private notifyService: NotificationService, private toastr: ToastrService, private formBuilder: FormBuilder,  private ApiPersonnel: PersonnelsService, private router: Router) { }

  ngOnInit() {

    // @ts-ignore
    this.apiFonction.getAllFonction().subscribe((fonctions: Fonction []) => {
      this.fonctions = fonctions;
    });


    this.ApiSocieteConstructive.getAllSocieteList().subscribe((data) => {
      this.societes = data.data;
    });

    const personnelId = window.localStorage.getItem('editEmployeId');
    if (!personnelId) {
      alert('Invalid action.');
      this.router.navigate(['start/personnels']);
      return;
    }
    console.log('ID EMPLOYE :  ' + personnelId);
    this.firstlevel = 'Prestataire';
    this.secondlevel = 'Modifier info. prestataire';

    const chantierID = window.localStorage.getItem('Idchantier');

    this.editForm = this.formBuilder.group({
      id: [''],
      codeEmploye: ['', Validators.required],
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      tel: [''],
      cnib: [''],
      societe_constructive_id: [''],
      email: ['', [Validators.email]],
      fonction_id: ['', Validators.required],
      chantier_id: [''],
    });

    this.ApiPersonnel.getPersonnel(+personnelId).subscribe( (data) => {
      console.log(data);
      this.editForm.setValue(data);
    });

  }

  showToasterPersonnelModifier() {
    this.notifyService.showSuccessWithTimeout('Info du l\'employe mis à jour avec succès !!', 'Notification', 5000);
  }

  onSubmit() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.editForm.invalid) {
      console.log('error form');
    }
    const personnelId = window.localStorage.getItem('editEmployeId');
    console.log(this.editForm.value);

    const idchantier = window.localStorage.getItem('Idchantier');
    const formData = new FormData();
    formData.append('codeEmploye', this.editForm.get('codeEmploye').value);
    formData.append('prenom', this.editForm.get('prenom').value);
    formData.append('nom', this.editForm.get('nom').value);
    formData.append('tel', this.editForm.get('tel').value);
    formData.append('email', this.editForm.get('email').value);
    formData.append('cnib', this.editForm.get('cnib').value);
    formData.append('fonction_id', this.editForm.get('fonction_id').value);
    if (this.editForm.get('societe_constructive_id').value) {
       formData.append('societe_constructive_id', this.editForm.get('societe_constructive_id').value);
    } else {
      formData.append('societe_constructive_id', '');
    }
    formData.append('chantier_id', idchantier);
    formData.append('_method', 'PUT');

    this.ApiPersonnel.updatePersonnel(+personnelId, formData)
      .pipe(first())
      .subscribe((employe: Personnel) => {
        // @ts-ignore
        console.log(employe);
        this.showToasterPersonnelModifier();
        this.router.navigate(['start/personnels']);
      });
  }


}
