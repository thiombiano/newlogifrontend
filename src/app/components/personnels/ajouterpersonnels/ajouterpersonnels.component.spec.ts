import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjouterpersonnelsComponent } from './ajouterpersonnels.component';

describe('AjouterpersonnelsComponent', () => {
  let component: AjouterpersonnelsComponent;
  let fixture: ComponentFixture<AjouterpersonnelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjouterpersonnelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjouterpersonnelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
