import { Component, OnInit } from '@angular/core';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {FournisseurService} from '../../fournisseur/shared/fournisseur.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {PersonnelsService} from '../shared/personnels.service';
import {Fournisseur} from '../../fournisseur/shared/fournisseur';
import {Personnel} from '../shared/personnel';
import {Fonction} from '../../fonctions/shared/fonction';
import {FonctionsService} from '../../fonctions/shared/fonctions.service';
import {SocieteconstructionService} from '../../societeconstruction/shared/societeconstruction.service';
import {Societeconstruction} from '../../societeconstruction/shared/societeconstruction.model';
import * as moment from 'moment';

@Component({
  selector: 'app-ajouterpersonnels',
  templateUrl: './ajouterpersonnels.component.html',
  styleUrls: ['./ajouterpersonnels.component.css']
})
export class AjouterpersonnelsComponent implements OnInit {
  firstlevel: string;
  secondlevel: string;
  fonctions: Fonction[];
  referenceval = '';
  referencevalpersonnel = '';
  totalfonction: number;
  totalpersonnel = 0;
  addForm: FormGroup;
  submitted = false;
  fournisseur: Fournisseur;
  public societes: Societeconstruction[];
  // tslint:disable-next-line:max-line-length
  constructor(public ApiSocieteConstructive: SocieteconstructionService, private apiFonction: FonctionsService, private notifyService: NotificationService, private toastr: ToastrService, private ApiPersonnel: PersonnelsService, private formBuilder: FormBuilder,  private router: Router) { }


  showToasterPersonnelEnregistrer() {
    this.notifyService.showSuccessWithTimeout('Employé enregistré avec succès !!', 'Notification', 5000);
  }

  showToasterPersonnelErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }

  ngOnInit() {

    this.ApiPersonnel.getAllPersonnelList().subscribe((employes: Personnel[]) => {
      this.referencevalpersonnel =  moment(new Date()).format('YYYY') + '/P/' + (employes.length + 1);
    });

    // @ts-ignore
    this.apiFonction.getAllFonction().subscribe((fonctions: Fonction []) => {
        this.fonctions = fonctions;
        this.totalfonction = fonctions.length;
        this.referenceval = '000' + (this.totalfonction + 1);
    });

    this.ApiSocieteConstructive.getAllSocieteList().subscribe((data) => {
      this.societes = data.data;
    });


    const chantierID = window.localStorage.getItem('Idchantier');

    this.firstlevel = 'Prestataire';
    this.secondlevel = 'Ajouter prestataire';
    this.addForm = this.formBuilder.group({
      codeEmploye: ['', Validators.required],
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      tel: [''],
      cnib: [''],
      societe_constructive_id: [''],
      email: ['', [Validators.email]],
      fonction_id: ['', Validators.required],
    });
  }


  onSubmit() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.addForm.invalid) {
      this.showToasterPersonnelErreurModifier();
      return;
    }
    console.log(this.addForm.value);
    const idchantier = window.localStorage.getItem('Idchantier');
    const formData = new FormData();
    formData.append('codeEmploye', this.addForm.get('codeEmploye').value);
    formData.append('prenom', this.addForm.get('prenom').value);
    formData.append('nom', this.addForm.get('nom').value);
    formData.append('tel', this.addForm.get('tel').value);
    formData.append('cnib', this.addForm.get('cnib').value);
    formData.append('email', this.addForm.get('email').value);
    formData.append('fonction_id', this.addForm.get('fonction_id').value);
    formData.append('societe_constructive_id', this.addForm.get('societe_constructive_id').value);
    formData.append('chantier_id', idchantier);
    // @ts-ignore
    this.ApiPersonnel.addPersonnel(formData).subscribe((employe: Personnel) => {
      this.showToasterPersonnelEnregistrer();
      this.router.navigate(['start/personnels']);
    });
  }

}
