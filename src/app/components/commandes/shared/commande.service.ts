import { Injectable } from '@angular/core';
import {Commande, DetailsCommande} from './commande';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ChantierService} from '../../chantier/shared/chantier.service';
import {SnotifyService} from 'ng-snotify';
import {Observable, throwError} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {catchError, map} from 'rxjs/operators';
import {CommandeInsert} from './commandInsert';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Access-Control-Allow-Origin': '*'
    // 'Authorization': 'Bearer '+ this.token
  })
};

@Injectable()
export class CommandeService {
  // tslint:disable-next-line:max-line-length
  formData: { adresse_fournisseur: null; is_prix_ttc: boolean; personneacontacter: null ; telephone_fournisseur: null; numero_BC: null; modepaiment_id: null; fournisseur: null; montant_ttc: null; numero_DA: null; fournisseur_id: null; date_BC: null; statut_BC: null; date_lvraison_prevue: null; adresse_chantier: null; id: null; chantier_id: null; articles: DetailsCommande[] };
  selectedCommande: Commande;
  listCommandes: Commande[];
  constructor(    private http: HttpClient,
                  private chantierService: ChantierService,
                  private snotifyService: SnotifyService) { }

  getCommande(id: number) {
    return this.http.get(environment.apiUrl + '/commandes/' + id)
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Its a Trap!'))
      );
  }



  addCommande(commande: Commande) {
    console.log('new Commande: ' + commande.numero_BC);
    return this.http.post<Commande>(
      environment.apiUrl + '/commandes',
      commande,
      httpOptions
    );
  }

  getAllCOMMANDEList(idChantier: number) {
    return  this.http.get(environment.apiUrl + '/chantiers/' + idChantier + '/commandes')
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Impossible dobtenir la liste de toutes les commandes!'))
      );
  }

  getChantierCommandesList(idChantier: number) {
   return  this.http
      .get(environment.apiUrl + '/chantiers/' + idChantier + '/commandes')
      .toPromise()
      .then((x: CommandeApiResponse) => {
        this.listCommandes = x.data;
      });
  }

  getCommandesOfChantierList(idChantier: number) {
    return  this.http.get(environment.apiUrl + '/chantiers/' + idChantier + '/commandes');
  }

  getSelectedCommande(id: number) {
    this.http
      .get(environment.apiUrl + '/commandes/' + id)
      .toPromise()
      .then((x: CommandeApiResponseOne) => {
        this.selectedCommande = x.data;
      });
  }

  getSelectedCommandeKeppFocus(id: number) {
   return  this.http
      .get(environment.apiUrl + '/commandes/' + id)
      .pipe(map((data: any) => data ),
       catchError(error => throwError('Impossible dobtenir la liste de toutes les commandes!'))
     );
  }

  // tslint:disable-next-line:max-line-length
  addChantierCommande(idChantier: number, commande: CommandeInsert) {
    return this.http
      .post<Commande>(
        environment.apiUrl + '/chantiers/' + idChantier + '/commandes',
        commande,
        httpOptions
      );
      // .toPromise()
      // .then(x => {
      //   this.getChantierCommandesList(idChantier);
      //   this.snotifyService.success('La commande a été enregistrée avec succes');
      // }).catch(error => {
      //   this.snotifyService.error(error.json());
      // } );
  }

  updateChantierCommande(idCommande, value: any) {
    const id =  window.localStorage.getItem('Idchantier');
    return this.http
      .put<Commande>(
        environment.apiUrl + '/chantiers/' + idCommande + '/commandes/' + id , value
      );
  }

  updateAnnulerChantierCommande(idCommande, value: any) {
    return this.http
      .post<Commande>(
        environment.apiUrl + '/annulationboncommande/' + idCommande , value
      );
  }

  totalSum(data) {
    // console.log(data);
    let total = 0;
    if (data) {
      data.forEach(detCom => {
        total += Math.round(detCom.prix_unitaire * detCom.quantite);
      });
    }

    return total;
  }

  // totalTVA(data, isTTC: boolean) {
  //   if (data) {
  //    return isTTC ? 0 : (data * 18) / 100;
  //   }
  // }

  totalTVA(data) {
    let totalTVA = 0;
    if (data) {
        data.forEach(detCom => {
          if (detCom.is_tva) {
            totalTVA += Math.round(detCom.prix_unitaire * detCom.quantite * (18 / 100)) ;
          }
        });
    }
    return totalTVA;
  }

  totalBIC(data) {
    let totalBIC = 0;
    if (data) {
      data.forEach(detCom => {
        if (detCom.is_bic && detCom.valueofbic && !detCom.is_tva) {
          totalBIC += Math.round( detCom.prix_unitaire * detCom.quantite * (detCom.valueofbic / 100));
        } else if (detCom.is_bic && detCom.valueofbic && detCom.is_tva) {
          // tslint:disable-next-line:max-line-length
          totalBIC += Math.round((detCom.prix_unitaire * detCom.quantite + detCom.prix_unitaire * detCom.quantite * (18 / 100)) * (detCom.valueofbic / 100)) ;
        }
      });
    }
    return totalBIC;
  }

  totalTTC(data, dataTVA,  dataBIC) {
    if (data) {
      return data + dataTVA + dataBIC;
    }
  }

}

interface CommandeApiResponse {
  data: Commande[];
}

interface CommandeApiResponseOne {
  data: Commande;
}
