export class Commande {
  constructor(
    public numero_BC: string,
    public modpaiement: string,
    public numero_DA: string,
    public date_BC: Date,
    public mode_paiement: string,
    public statut_BC: number,
    public chantier_id: number,
    public personneacontacter: string,
    public fournisseur_id: number,
    public fournisseur: string,
    public adresse_fournisseur: string,
    public telephone_fournisseur: string,
    public is_prix_ttc: boolean,
    public isreceptionpartiellefinale: boolean,
    public modepaiment_id: number,
    // public is_bic: boolean,
    // public valueofbic: number,
    public libellemodepaiement: string,
    public adresse_chantier: string,
    public canceledcommentaire: string,
    public articles: DetailsCommande[],
    public montant_ttc: number,
    public montant_tva: number,
    public montant_bic: number,
    public is_receptionne: boolean,
    public isCanceled: boolean,
    public chantier?: string,
    public date_lvraison_prevue?: Date,
    public dateLivraisonEffectif?: Date,
    public id?: number
  ) {}
}

export class DetailsCommande {
  constructor(
    public article_id: number,
    public unite: string,
    public quantite: number,
    public prix_unitaire: number,
    public prix_referent: number,
    public quantite_receptionnee: number,
    public reliquat: number,
    public is_bic: boolean,
    public is_tva: boolean,
    public valueofbic: any,
    public commentaires?: string,
    public article?: string,
    public reference?: string
  ) {}
}
