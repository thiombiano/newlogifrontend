export class CommandeInsert {
  constructor(
    public numero_BC: string,
    public modpaiement: string,
    public numero_DA: string,
    public date_BC: Date,
    public mode_paiement: string,
    public personneacontacter: string,
    public statut_BC: number,
    public chantier_id: number,
    public fournisseur_id: number,
    public fournisseur: string,
    public adresse_fournisseur: string,
    public modepaiment_id: number,
    public telephone_fournisseur: string,
    public is_prix_ttc: boolean,
    // public is_bic: boolean,
    // public valueofbic: number,
    public adresse_chantier: string,
    public articles: DetailsCommande[],
    public montant_ttc: number,
    public montant_tva: number,
    public montant_bic: number,
    public chantier?: string,
    public date_lvraison_prevue?: Date,
    public dateLivraisonEffectif?: Date,
    public id?: number,
    // tslint:disable-next-line:variable-name

  ) {}
}

export class DetailsCommande {
  constructor(
    public article_id: number,
    public unite: string,
    public quantite: number,
    public prix_unitaire: number,
    public prix_referent: number,
    public quantite_receptionnee: number,
    public is_bic: boolean,
    public is_tva: boolean,
    public valueofbic: any,
    public reliquat: number,
    public commentaires?: string,
    public article?: string,
    public reference?: string
  ) {}
}
