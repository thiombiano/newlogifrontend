import {ChangeDetectorRef, Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {Commande, DetailsCommande} from './shared/commande';
import {CommandeService} from './shared/commande.service';
import {FournisseurService} from '../fournisseur/shared/fournisseur.service';
import {ChantierService} from '../chantier/shared/chantier.service';
import {NgForm} from '@angular/forms';
import {MdbTableDirective, MdbTablePaginationComponent} from 'angular-bootstrap-md';
import {Router} from '@angular/router';
import {ReceptionService} from '../receptions/shared/reception.service';
import {Reception} from '../receptions/shared/reception';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-commandes',
  templateUrl: './commandes.component.html',
  styleUrls: ['./commandes.component.css']
})
export class CommandesComponent implements OnInit {
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  elements: any = [];
  display = 'none';
  curDate = new Date();
  searchText = '';
  totalcommande: number;
  private listecommande: Commande[];
  commandes: Commande[];
  firstlevel: string;
  secondlevel: string;
  reception: Reception;
  previous: string;
  isReceptionnne = false;
  headElements = ['N°', 'Date', 'Numéro BC' , 'Date cible', 'Montant TTC', 'Actions'];
  public roles: { A: string; T: string; L: string; M: string;  C: string; SA: string; CSR: string; CDC: string};
  // tslint:disable-next-line:max-line-length
  constructor( public ReceptionAPI: ReceptionService, private router: Router, private cdRef: ChangeDetectorRef, public commandeService: CommandeService,
               private fournisseurService: FournisseurService,
               public chantierService: ChantierService) { }


  iScommandeReceptionne(idcommande) {
    this.ReceptionAPI.getIsCommandereception(idcommande).subscribe((reception: Reception) => {
      if (reception.numero_RC) {
        return true;
      }
    });
  }


  getListeCommande(id) {
    this.commandeService.getAllCOMMANDEList(id).subscribe(
      data => {
        this.commandes = data;
        this.firstlevel = 'Tableau de bord';
        this.secondlevel = 'Commandes';
        this.totalcommande =  this.commandes.length;
        this.mdbTable.setDataSource(this.commandes);
        this.elements = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();
      },
      err => console.error(err),
      () => console.log('liste des commandes obtenues')
    );
  }

  voirdetailcommande(id) {
      window.localStorage.removeItem('IDVcommande');
      window.localStorage.setItem('IDVcommande', id);
      this.router.navigate(['/start/voir-detailcommande']);
  }


  searchItems() {
    const prev = this.commandes;

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.elements = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(15);
    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

  @HostListener('input') oninput() {
    this.searchItems();
  }

  ModifierCommande(id) {
    window.localStorage.removeItem('IDcommande');
    window.localStorage.setItem('IDcommande', id);
    // this.commandeService.getSelectedCommande(+id);
    this.router.navigate(['/start/modifier-commande']);
  }

  AnnulerCommande(id) {
    window.localStorage.removeItem('IDAcommande');
    window.localStorage.setItem('IDAcommande', id);
    // this.commandeService.getSelectedCommande(+id);
    this.router.navigate(['/start/annuler-commande']);
  }


  ngOnInit() {
    this.roles = environment.roles;
    const id =  window.localStorage.getItem('Idchantier');
    this.getListeCommande(+id);
  }


  get listChantierCommandes() {
    console.log('TEST : ' + this.commandeService.listCommandes);
    return this.commandeService.listCommandes;
  }

}
