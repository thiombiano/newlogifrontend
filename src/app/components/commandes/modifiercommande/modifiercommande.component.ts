import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {CommandeService} from '../shared/commande.service';
import {ArticleService} from '../../article/Shared/article.service';
import {ReceptionService} from '../../receptions/shared/reception.service';
import {FournisseurService} from '../../fournisseur/shared/fournisseur.service';
import {CommandeInsert} from '../shared/commandInsert';
import {DetailsReception} from '../../receptions/shared/reception';
import {Commande, DetailsCommande} from '../shared/commande';
import {DatePipe, formatDate} from '@angular/common';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatStepper} from '@angular/material';
import {startOfDay} from '@fullcalendar/core';
import {FormControl} from '@angular/forms';
import * as moment from 'moment';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {ModepaiementService} from '../../modepaiement/shared/modepaiement.service';
import {Modepaiement} from '../../modepaiement/shared/modepaiement';

@Component({
  selector: 'app-modifiercommande',
  templateUrl: './modifiercommande.component.html',
  styleUrls: ['./modifiercommande.component.css'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
  ]
})
export class ModifiercommandeComponent implements OnInit {
  firstlevel: string;
  secondlevel: string;
  quantite: number;
  prixUnitaire: number;
  thedate: Date;
  modepaiements: Modepaiement[];
  isLinear = true;
  FirstLevel = false;
  // tslint:disable-next-line:align
  // @ts-ignore
  commandinsert: CommandeInsert = new CommandeInsert();
  // @ts-ignore
  // @ts-ignore
  mthedate = Date;
  aticlesCommande = new Array<DetailsCommande>();
  currentdate =  formatDate(new Date(), 'dd/MM/yyyy:HH:mm:ss', 'en');
  unite: string;
  // tslint:disable-next-line:variable-name
  prix_referent: number;
  // @ts-ignore
  commandinsert: CommandeInsert = new CommandeInsert();
  bLigneDetailAjoutee = false;
  displayDetails = false;
  isCompleted = false;
  // bicvalue = ['1', '2'];
  // statebic = false;
  public datedbc: Date;
  public datelivraison: Date;

  // tslint:disable-next-line:max-line-length variable-name
  constructor(public ApiModePaiment: ModepaiementService, private cdref: ChangeDetectorRef, private dateAdapter: DateAdapter<Date>, private datePipe: DatePipe, private _adapter: DateAdapter<any>, public fournisseurService: FournisseurService, private notifyService: NotificationService, private toastr: ToastrService, private router: Router, private activatedRoute: ActivatedRoute, public commandeService: CommandeService, public articleService: ArticleService, private receptionService: ReceptionService) {
    this.dateAdapter.setLocale('fr');
  }

  moveltonextstep() {
    this.FirstLevel = true;
  }

  // GetIsBIC(event) {
  //   console.log( event);
  //   const bicState = event.checked;
  //   console.log('the value : ' + bicState);
  //   if (bicState === true) {
  //     this.statebic = true;
  //   } else {
  //     this.statebic = false;
  //   }
  // }


  getAlldataSelected() {
    if (this.commandeService.selectedCommande) {
      // @ts-ignore
      this.commandinsert = {
        numero_BC: null,
        modpaiement: null,
        numero_DA: null,
        personneacontacter: null,
        date_BC: null,
        mode_paiement: null,
        statut_BC: null,
        chantier_id: null,
        fournisseur_id: null,
        // is_bic: false,
        // valueofbic: 0,
        modepaiment_id: null,
        fournisseur: null,
        adresse_fournisseur: null,
        telephone_fournisseur: null,
        is_prix_ttc: null,
        adresse_chantier: null,
        montant_ttc: this.commandeService.selectedCommande.montant_ttc,
        montant_tva:  this.commandeService.selectedCommande.montant_tva,
        montant_bic:  this.commandeService.selectedCommande.montant_bic,
        chantier: null,
        date_lvraison_prevue : null,
        dateLivraisonEffectif : null,
        articles: new Array<DetailsCommande>(),
        id: null
      };



      for (const articleCommande of this.commandeService.selectedCommande.articles) {

        // @ts-ignore
        this.commandinsert.articles.push({
          article: articleCommande.article,
          unite: articleCommande.unite,
          quantite: articleCommande.quantite,
          prix_unitaire: articleCommande.prix_unitaire,
          reference: articleCommande.reference,
          article_id: articleCommande.article_id,
          quantite_receptionnee: articleCommande.reliquat,
          commentaires: ''
        });
      }

      console.log(this.commandeService.selectedCommande);

    }

  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterContentChecked(): void {
    this.cdref.detectChanges();
  }



  ngOnInit() {
    this._adapter.setLocale('fr');
    this.secondlevel = 'Commande';
    this.firstlevel = 'Modifier une commande';
    this.articleService.getArticleList();
    this.fournisseurService.getFournisseurList();
    this.articleService.getArticleList();
    const idcom = window.localStorage.getItem('IDcommande');
    this.commandeService.getSelectedCommande(+idcom);
    console.log(this.commandeService.selectedCommande);
    this.getAlldataSelected();
    this.ApiModePaiment.getAllModePaiementList().subscribe(
      data => {
        this.modepaiements = data;
      },
      err => console.error(err),
      () => console.log('Wiki Items Geladen')
    );
  }

  get CommandeSelectionne() {
    return this.commandeService.selectedCommande;
  }

  selectionnerArticle() {
    console.log(this.articleService.selectedArticle);
    // @ts-ignore
    this.unite  = this.articleService.selectedArticle.unite;
    this.prix_referent = this.articleService.selectedArticle.prix_referent;
  }

  ajouterLaLigneDetail() {

    // @ts-ignore
    // @ts-ignore

    if (this.quantite > 0 && this.prixUnitaire > 0) {
      // this.bLigneDetailAjoutee = true;
      this.commandeService.selectedCommande.articles.push({
        article_id: this.articleService.selectedArticle.id,
        article: this.articleService.selectedArticle.libelle_article,
        quantite: this.quantite,
        unite: this.articleService.selectedArticle.unite,
        prix_unitaire: this.prixUnitaire,
        is_bic: this.articleService.selectedArticle.is_bic,
        is_tva: this.articleService.selectedArticle.is_tva,
        valueofbic: this.articleService.selectedArticle.valueofbic,
        prix_referent: this.prixUnitaire,
        quantite_receptionnee: this.quantite,
        reliquat: 0,
        commentaires: ''
      });
      console.log(this.commandinsert);
      this.commandeService.selectedCommande.montant_ttc = this.commandeService.totalTTC(
        this.commandeService.totalSum(this.commandeService.selectedCommande.articles),
        this.commandeService.totalTVA(this.commandinsert.articles),
        this.commandeService.totalBIC(this.commandinsert.articles)
      );
      this.commandeService.selectedCommande.montant_tva = this.commandeService.totalTVA(this.commandinsert.articles);
      this.commandeService.selectedCommande.montant_bic = this.commandeService.totalBIC(this.commandinsert.articles);
      this.articleService.selectedArticle = null;
      this.quantite = null;
      this.prixUnitaire = null;
      this.prix_referent = null;
      this.unite = null;
      console.log('Montant TTC' + this.commandeService.selectedCommande.montant_ttc);
    } else {
      this.showToasterCommandeErreurModifier();
      this.bLigneDetailAjoutee = false;
    }
  }



  goBack(stepper: MatStepper) {
    stepper.previous();
  }

  goForward(stepper: MatStepper) {

    const idfournisseur = this.commandeService.selectedCommande.fournisseur_id;
    const dateBC = this.commandeService.selectedCommande.date_BC;
    const dateRC = this.commandeService.selectedCommande.date_lvraison_prevue;
    const numeroDA = this.commandeService.selectedCommande.numero_DA;
    const datebc = this.commandeService.selectedCommande.date_BC;
    const datelivprevu = this.commandeService.selectedCommande.date_lvraison_prevue;
    // const valueofbicm = this.commandeService.selectedCommande.valueofbic;
    if (datebc < datelivprevu) {
      console.log('Cest inferieur');
    }
    // console.log(idfournisseur);
    // // @ts-ignore
    // if (idfournisseur != null && dateBC  != null  && dateRC  != null  && numeroDA  !== ''  && datebc <= datelivprevu) {
    //   this.displayDetails = true;
    //   stepper.next();
    //   this.isCompleted = true;
    // } else {
    //   this.isCompleted = false;
    //   this.showToasterCommandeErreurModifier();
    // }

    // console.log(this.commandeService.selectedCommande.valueofbic);

    // @ts-ignore
    // if (valueofbicm !== 0) {
    //   if (this.commandeService.selectedCommande.is_bic === false || valueofbicm !== null ) {
        // @ts-ignore
    if (idfournisseur != null && dateBC  != null  && dateRC  != null  && numeroDA  != null  && datebc <= datelivprevu) {
          this.displayDetails = true;
          stepper.next();
        } else {
          this.displayDetails = false;
          this.showToasterCommandeErreurModifier();
        }
    //   }
    //   else {
    //     this.showToasterCommandeErreurModifier();
    //   }
    // }
    // else {
    //   this.showToasterCommandeErreurModifier();
    // }


  }


  ajouterLesDetails() {
    this.articleService.getArticleList();

    const idfournisseur = this.commandinsert.fournisseur_id;
    const dateBC = this.commandinsert.date_BC;
    const dateRC = this.commandinsert.date_lvraison_prevue;
    const numeroDA = this.commandinsert.numero_DA;
    const datebc = this.commandinsert.date_BC;
    const datelivprevu = this.commandinsert.date_lvraison_prevue;
    if (datebc < datelivprevu) {
      console.log('Cest inferieur');
    }
    console.log(idfournisseur);
    // @ts-ignore
    if (idfournisseur != null && dateBC  != null  && dateRC  != null  && numeroDA  != null  && datebc <= datelivprevu) {
      this.displayDetails = true;
    } else {
      this.displayDetails = false;
      this.showToasterCommandeErreurModifier();
    }
  }

  supprimerLaLigneDetail(detailCommande) {
    const index  =  this.commandeService.selectedCommande.articles.indexOf(detailCommande);
    console.log(index);
    if (index !== -1) {
      this.commandeService.selectedCommande.articles.splice(index, 1);
      this.commandeService.selectedCommande.montant_ttc = this.commandeService.totalTTC(
        this.commandeService.totalSum(this.commandeService.selectedCommande.articles),
        this.commandeService.totalTVA(this.commandinsert.articles),
        this.commandeService.totalBIC(this.commandinsert.articles)
      );
      this.articleService.selectedArticle = null;
      this.quantite = null;
      this.prixUnitaire = null;
      this.prix_referent = null;
      this.unite = null;
    }
  }


  showToasterCommandeModifier() {
    this.notifyService.showSuccessWithTimeout('Commande modifiée avec succès!!', 'Notification', 5000);
  }

  showToasterCommandeErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }

  showToasterCommandeErreurAjoutrticles() {
    this.notifyService.showErrorWithTimeout('Echec: Veuillez ajouter des articles  !!', 'Notification', 5000);
  }


  modifierCommandeChantier() {

    const totalarticles = this.commandeService.selectedCommande.articles.length;

    if (totalarticles !== 0) {


      const idfournisseur = this.commandeService.selectedCommande.fournisseur_id;
      console.log('le fournisseur 1 : ' + idfournisseur);
      const dateBC = this.commandeService.selectedCommande.date_BC;
      const dateRC = this.commandeService.selectedCommande.date_lvraison_prevue;
      const numeroDA = this.commandeService.selectedCommande.numero_DA;
      console.log(idfournisseur);
      console.log(numeroDA);
      if (idfournisseur != null && dateBC  != null  && dateRC  != null  && numeroDA  !== '' ) {
        const idcom = window.localStorage.getItem('IDcommande');
        this.commandinsert.date_BC = this.commandeService.selectedCommande.date_BC;
        this.commandinsert.numero_DA = this.commandeService.selectedCommande.numero_DA;
        this.commandinsert.date_lvraison_prevue = this.commandeService.selectedCommande.date_lvraison_prevue;
        this.commandinsert.mode_paiement = this.commandeService.selectedCommande.mode_paiement;
        this.commandinsert.fournisseur_id = this.commandeService.selectedCommande.fournisseur_id;
        this.commandinsert.articles =  this.commandeService.selectedCommande.articles;
        this.commandinsert.is_prix_ttc =  this.commandeService.selectedCommande.is_prix_ttc;
        this.commandinsert.modepaiment_id =  this.commandeService.selectedCommande.modepaiment_id;
        this.commandinsert.personneacontacter =  this.commandeService.selectedCommande.personneacontacter;
        // this.commandinsert.is_bic =  this.commandeService.selectedCommande.is_bic;
        // this.commandinsert.valueofbic =  this.commandeService.selectedCommande.valueofbic;
        this.commandinsert.montant_ttc = this.commandeService.totalTTC(
          // tslint:disable-next-line:max-line-length
          this.commandeService.totalSum(this.commandeService.selectedCommande.articles), this.commandeService.totalTVA(this.commandinsert.articles), this.commandeService.totalBIC(this.commandinsert.articles)
        );
        this.commandinsert.montant_tva = this.commandeService.totalTVA(this.commandinsert.articles);
        this.commandinsert.montant_bic = this.commandeService.totalBIC(this.commandinsert.articles);
        console.log('le fournisseur 2 : ' + this.commandinsert.fournisseur_id);
        this.commandeService.updateChantierCommande(
          +idcom,
          this.commandinsert
        ).subscribe((commande: Commande) => {
          this.router.navigate(['/start/commandes']);
          this.showToasterCommandeModifier();
        });
        console.log(this.commandinsert);
      } else {
        this.showToasterCommandeErreurModifier();
      }
    } else {
        this.showToasterCommandeErreurAjoutrticles();
    }


  }

}
