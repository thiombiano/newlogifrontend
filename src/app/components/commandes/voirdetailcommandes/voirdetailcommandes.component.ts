import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CommandeService} from '../shared/commande.service';
import {Article} from '../../article/Shared/article';
import {CommandeInsert} from '../shared/commandInsert';
import {environment} from "../../../../environments/environment";

@Component({
  selector: 'app-voirdetailcommandes',
  templateUrl: './voirdetailcommandes.component.html',
  styleUrls: ['./voirdetailcommandes.component.css']
})
export class VoirdetailcommandesComponent implements OnInit {
  firstlevel: string;
  secondlevel: string;
  istherebic = false;
  articlewithbic: any [];
  public isTotalReceptionne = false;
  public roles: { A: string; T: string; L: string; M: string;  C: string; SA: string; CSR: string; CDC: string};
  constructor( private router: Router, private activatedRoute: ActivatedRoute,
               public commandeService: CommandeService) { }

  ngOnInit() {
    this.roles = environment.roles;
    const idcom =   window.localStorage.getItem('IDVcommande');
    console.log('id:' + idcom);
    this.firstlevel = 'Détail de la commande';
    this.secondlevel = 'Commande';
    this.commandeService.getSelectedCommande(+idcom);
    // @ts-ignore
    this.displayArticleWithBIC(idcom);
    console.log(this.commandeService.selectedCommande);
  }

  displayArticleWithBIC(id: number) {
    this.commandeService.getCommande(+id).subscribe((data) => {
      if (data) {
        console.log(data);
        console.log('les articles : ' + data.data.articles.length);
        // tslint:disable-next-line:prefer-for-of
        if (data.data.articles) {
          // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < data.data.articles.length; i++) {
          if (data.data.articles[i].is_bic) {
            this.istherebic = true;
            console.log('Boolen : ' + data.data.articles[i].is_bic);
            console.log('Valeur BIC : ' + data.data.articles[i].valueofbic);
            break;
          }
        }

          // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < data.data.articles.length; i++) {
            if (data.data.articles[i].reliquat !== 0 ) {
              this.isTotalReceptionne = true;
              break;
            }
          }

        }
      }
    });
  }

  get CommandeSelectionne() {
    return this.commandeService.selectedCommande;
  }


  effectuerreception(id) {
    window.localStorage.removeItem('IDCom');
    window.localStorage.setItem('IDCom', id);
    this.router.navigate(['/start/effectuer-reception']);
  }

  printComponent(cmpName) {
    const printContents = document.getElementById(cmpName).innerHTML;
    const originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
  }

}
