import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoirdetailcommandesComponent } from './voirdetailcommandes.component';

describe('VoirdetailcommandesComponent', () => {
  let component: VoirdetailcommandesComponent;
  let fixture: ComponentFixture<VoirdetailcommandesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoirdetailcommandesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoirdetailcommandesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
