import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CommandeService} from '../shared/commande.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';
import {Article} from '../../article/Shared/article';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {Commande} from '../shared/commande';
import {MatDialog} from '@angular/material';
import {ConfirmationDialogComponent} from '../../confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-annulercommande',
  templateUrl: './annulercommande.component.html',
  styleUrls: ['./annulercommande.component.css']
})
export class AnnulercommandeComponent implements OnInit {
  firstlevel: string;
  secondlevel: string;
  submitted = false;
  editForm: FormGroup;
  // tslint:disable-next-line:max-line-length
  constructor(public dialog: MatDialog, private notifyService: NotificationService, private toastr: ToastrService, private router: Router, private activatedRoute: ActivatedRoute,
               // tslint:disable-next-line:variable-name
              public commandeService: CommandeService, private _formbuilder: FormBuilder) { }

  ngOnInit() {
    const idcom =   window.localStorage.getItem('IDAcommande');
    console.log('id:' + idcom);
    this.firstlevel = 'Annulation de la commande';
    this.secondlevel = 'Commande';
    this.commandeService.getSelectedCommande(+idcom);
    this.editForm = this._formbuilder.group({
      canceledcommentaire: ['', Validators.required]
    });
  }


  showToasterAnnulerBonModifier() {
    this.notifyService.showSuccessWithTimeout('Bon de commande annuler avec succès !!', 'Notification', 5000);
  }

  showToasterAnnulerBonErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }

  onSubmit() {


    this.submitted = true;
    // stop here if form is invalid
    if (this.editForm.invalid) {
      console.log('error form');
      this.showToasterAnnulerBonErreurModifier();
    } else {
      const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        width: '350px',
        data: 'êtes vous sûr de vouloir annuler ce bon de commande?'
      });
      const annulerBonId = window.localStorage.getItem('IDAcommande');
      console.log(this.editForm.value);

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.commandeService.updateAnnulerChantierCommande(+annulerBonId, this.editForm.value)
            .pipe(first())
            .subscribe((commande: Commande) => {
              // @ts-ignore
              console.log(commande);
              this.showToasterAnnulerBonModifier();
              this.router.navigate(['start/commandes']);
            });
        }
      });
    }


  }

}
