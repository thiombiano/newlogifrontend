import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {DatePipe, formatDate} from '@angular/common';
import {FournisseurService} from '../../fournisseur/shared/fournisseur.service';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {ArticleService} from '../../article/Shared/article.service';
import {CommandeService} from '../shared/commande.service';
import {Article} from '../../article/Shared/article';
import {Commande, DetailsCommande} from '../shared/commande';
import {Router} from '@angular/router';
import {ChantierService} from '../../chantier/shared/chantier.service';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {CommandeInsert} from '../shared/commandInsert';
import {Unitemesure} from '../../article/Shared/unitemesure';
import {MatStepper} from '@angular/material';
import {Modepaiement} from '../../modepaiement/shared/modepaiement';
import {ModepaiementService} from '../../modepaiement/shared/modepaiement.service';

@Component({
  selector: 'app-ajoutercommande',
  templateUrl: './ajoutercommande.component.html',
  styleUrls: ['./ajoutercommande.component.css'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
  ]
})
export class AjoutercommandeComponent implements OnInit {
  displayDetails = false;
  unitemesure: Unitemesure[];
  modepaiements: Modepaiement[];
  firstlevel: string;
  // @ts-ignore
  // @ts-ignore
  commandinsert: CommandeInsert = new CommandeInsert();
  secondlevel: string;
  addForm: FormGroup;
  bLigneDetailAjoutee = false;
  listDetailsCommandes: any;
  quantite: number;
  prixUnitaire: number;
  thedate: Date;
  submitted = false;
  // tslint:disable-next-line:variable-name
  date_BC: Date;
  total: number;
  unite: string;
  today: string;
  modpaiement: string;
  // statebic = false;
  currentdate =  formatDate(new Date(), 'dd/MM/yyyy:HH:mm:ss', 'en');
  // bicvalue = ['1', '2'];
  // tslint:disable-next-line:variable-name
  prix_referent: number;
  // tslint:disable-next-line:variable-name max-line-length
  constructor(public ApiModePaiment: ModepaiementService, private dateAdapter: DateAdapter<Date>, private notifyService: NotificationService, private toastr: ToastrService, private chantierService: ChantierService, private router: Router, public commandeService: CommandeService, public articleService: ArticleService, private datePipe: DatePipe, private _adapter: DateAdapter<any>, private _formBuilder: FormBuilder, public fournisseurService: FournisseurService) {
    this.dateAdapter.setLocale('fr');
  }

  // GetIsBIC(event) {
  //   console.log( event);
  //   const bicState = event.checked;
  //   console.log('the value : ' + bicState);
  //   if (bicState === true) {
  //     this.statebic = true;
  //   } else {
  //     this.statebic = false;
  //   }
  // }

  get f() { return this.addForm.controls; }

  ngOnInit() {
    this._adapter.setLocale('fr');
    this.firstlevel = 'Bon de commande';
    this.secondlevel = 'Commande';
    this.articleService.getArticleList();
    this.fournisseurService.getFournisseurNonBlackLister();
    // this.resetForm();

    // @ts-ignore
    // @ts-ignore
    this.commandinsert = {
      numero_BC: null,
      modpaiement: null,
      numero_DA: null,
      date_BC: null,
      mode_paiement: null,
      statut_BC: null,
      personneacontacter: null,
      chantier_id: null,
      fournisseur_id: null,
      // is_bic: false,
      // valueofbic: null,
      fournisseur: null,
      modepaiment_id: null,
      adresse_fournisseur: null,
      telephone_fournisseur: null,
      is_prix_ttc: true,
      adresse_chantier: null,
      montant_ttc: null,
      montant_tva: null,
      montant_bic: null,
      chantier : null,
      date_lvraison_prevue : null,
      dateLivraisonEffectif : null,
      articles: new Array<DetailsCommande>(),
      id: null
    };

    this.ApiModePaiment.getAllModePaiementList().subscribe(
      data => {
        this.modepaiements = data;
      },
      err => console.error(err),
      () => console.log('Wiki Items Geladen')
    );

  }

  goBack(stepper: MatStepper) {
    stepper.previous();
  }


  ajouterLesDetails(stepper: MatStepper) {
    this.articleService.getArticleList();

    const idfournisseur = this.commandinsert.fournisseur_id;
    const dateBC = this.commandinsert.date_BC;
    const dateRC = this.commandinsert.date_lvraison_prevue;
    const numeroDA = this.commandinsert.numero_DA;
    const datebc = this.commandinsert.date_BC;
    const datelivprevu = this.commandinsert.date_lvraison_prevue;
    // const valueofbic = this.commandinsert.valueofbic;
    // const isBic = this.commandinsert.is_bic;
    if (datebc < datelivprevu) {
      console.log('Cest inferieur');
    }

      // @ts-ignore
    if (idfournisseur != null && dateBC  != null  && dateRC  != null  && numeroDA  != null  && datebc <= datelivprevu) {
        this.displayDetails = true;
        stepper.next();
      } else {
        this.displayDetails = false;
        this.showToasterCommandeErreurModifier();
      }

  }

  resetForm(form?: NgForm) {
    if (form != null) {
      form.resetForm();
    }
    this.commandeService.formData = {
      numero_BC: null,
      numero_DA: null,
      date_BC: null,
      statut_BC: null,
      personneacontacter: null,
      chantier_id: null,
      fournisseur_id: null,
      fournisseur: null,
      modepaiment_id: null,
      adresse_fournisseur: null,
      adresse_chantier: null,
      telephone_fournisseur: null,
      date_lvraison_prevue: null,
      montant_ttc: null,
      is_prix_ttc: true,
      articles: new Array<DetailsCommande>(),
      id: null
    };

    // @ts-ignore
    // @ts-ignore
    // @ts-ignore
    this.articleService.selectedArticle = {
      libelle_article: null,
      reference: null,
      description: null,
      prix_referent: null,
      unite_mesure_id: null,
      unite: null,
      id: null
    };
  }

  onChantierChange() {
    console.log(
      'Selection: ' + this.chantierService.selectedChantier.code_chantier
    );
    this.commandeService.getChantierCommandesList(
      this.chantierService.selectedChantier.id
    );
  }

  showToasterCommandeEnregistrer() {
    this.notifyService.showSuccessWithTimeout('Commande enregistré avec succès !!', 'Notification', 5000);
  }

  showToasterCommandeErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }

  showToasterCommandeErreurAjoutrticles() {
    this.notifyService.showErrorWithTimeout('Echec: Veuillez ajouter des articles  !!', 'Notification', 5000);
  }


  enregistrerCommandeChantier() {

    const totalarticles = this.commandinsert.articles.length;

    if (totalarticles !== 0) {
      const idfournisseur = this.commandinsert.fournisseur_id;
      const dateBC = this.commandinsert.date_BC;
      const dateRC = this.commandinsert.date_lvraison_prevue;
      const numeroDA = this.commandinsert.numero_DA;
      console.log(idfournisseur);
      // @ts-ignore
      if (idfournisseur != null && dateBC  != null  && dateRC  != null  && numeroDA  != null ) {
        const idchantier = window.localStorage.getItem('Idchantier');
        const theid = +idchantier;
        this.commandinsert.montant_ttc = this.commandeService.totalTTC(
          this.commandeService.totalSum(this.commandinsert.articles),
          this.commandeService.totalTVA(this.commandinsert.articles),
          this.commandeService.totalBIC(this.commandinsert.articles)
        );
        this.commandinsert.montant_tva = this.commandeService.totalTVA(this.commandinsert.articles);
        this.commandinsert.montant_bic = this.commandeService.totalBIC(this.commandinsert.articles);
        this.commandeService.addChantierCommande(
          theid,
          this.commandinsert
        ).subscribe((commande: Commande) => {
          this.router.navigate(['/start/commandes']);
          this.showToasterCommandeAjoute();
        });
        console.log(this.commandinsert);
      } else {
        this.showToasterCommandeErreurModifier();
      }
    } else{
      this.showToasterCommandeErreurAjoutrticles();
    }

  }

  showToasterCommandeAjoute() {
    this.notifyService.showSuccessWithTimeout('Commande ajoutée avec succès!!', 'Notification', 5000);
  }

  selectionnerArticle() {
    console.log(this.articleService.selectedArticle);
    // this.articleService.getOneArticle(this.articleService.selectedArticle.id).subscribe(data => {
      // this.unite = this.articleService.selectedArticle.unite;
      // console.log(data);
      // console.log(data.data.unite);
    // });
    // @ts-ignore
    this.unitemesure = this.articleService.selectedArticle.unite_mesure;
    this.unite  =   this.unitemesure['code'];
    this.prix_referent = this.articleService.selectedArticle.prix_referent;
  }

  ajouterLaLigneDetail() {

    // @ts-ignore
    // @ts-ignore

    if (this.quantite > 0 && this.prixUnitaire > 0) {
      this.bLigneDetailAjoutee = true;
      this.commandinsert.articles.push({
        article_id: this.articleService.selectedArticle.id,
        article: this.articleService.selectedArticle.libelle_article,
        quantite: this.quantite,
        unite: this.articleService.selectedArticle.unite,
        prix_unitaire: this.prixUnitaire,
        prix_referent: this.prixUnitaire,
        is_bic: this.articleService.selectedArticle.is_bic,
        is_tva: this.articleService.selectedArticle.is_tva,
        valueofbic: this.articleService.selectedArticle.valueofbic,
        quantite_receptionnee: this.quantite,
        reliquat: 0,
        commentaires: ''
      });
      console.log(this.commandinsert);
      this.commandinsert.montant_ttc = this.commandeService.totalTTC(
        // tslint:disable-next-line:max-line-length
        this.commandeService.totalSum(this.commandinsert.articles), this.commandeService.totalTVA(this.commandinsert.articles), this.commandeService.totalBIC(this.commandinsert.articles)
      );

      this.articleService.selectedArticle = null;
      this.quantite = null;
      this.prixUnitaire = null;
      this.prix_referent = null;
      this.unite = null;
      console.log('Montant TTC' + this.commandinsert.montant_ttc);

    } else {
      this.showToasterCommandeErreurModifier();
      console.log('erreur');
      this.bLigneDetailAjoutee = false;
    }
  }

  // tslint:disable-next-line:no-shadowed-variable variable-name
  supprimerLaLigneDetail(detailCommande) {
    const index  =  this.commandinsert.articles.indexOf(detailCommande);
    console.log(index);
    if (index !== -1) {
      this.commandinsert.articles.splice(index, 1);
      // @ts-ignore
      this.commandinsert.montant_ttc = this.commandeService.totalTTC(
        this.commandeService.totalSum(this.commandinsert.articles),
        this.commandeService.totalTVA(this.commandinsert.articles),
        this.commandeService.totalBIC(this.commandinsert.articles)
      );
      this.articleService.selectedArticle = null;
      this.quantite = null;
      this.prixUnitaire = null;
      this.prix_referent = null;
      this.unite = null;
    }
  }


  //   ngAfterContentChecked()  {
  //     this.total = this.commandeService.formData.articles.reduce(funtion(runningValue: number, detCom: DetailsCommande) => {
  //       runningValue = runningValue + (detCom.prixUnitaire * detCom.quantite);
  //     }, 0);
  // }

  // totalSum(data) {
  //   let total = 0;
  //   data.forEach(detCom => {
  //     total += detCom.prixUnitaire * detCom.quantite;
  //   });
  //   return total;
  // }

  // totalTVA(data) {
  //   return (data * 18) / 100;
  // }

  // totalTTC(data) {
  //   return data + this.totalTVA(data);
  // }
}

