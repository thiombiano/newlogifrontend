import {ChangeDetectorRef, Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {MdbTableDirective, MdbTablePaginationComponent} from 'angular-bootstrap-md';
import {NotificationService} from '../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {RoleService} from './shared/role.service';
import {Role} from './shared/role.model';
import {Permission} from '../permission/shared/permission.model';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
export class RoleComponent implements OnInit {
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, private toastr: ToastrService, private cdRef: ChangeDetectorRef, public ApiRole: RoleService, private router: Router) { }
  headElements = ['N°', 'Role', 'Permission', 'Description', 'Action'];
  searchText = '';
  roles: Role[];
  totalrole = 0;
  elements: any = [];
  previous: string;
  firstlevel: string;
  secondlevel: string;
  popoverTitle = 'Suppression de ce role';
  popoverMessage = 'Voulez vous vraiment supprimer ce role';
  cancelClicked = false;
  ngOnInit() {
    this.loadAllRole();
  }

  loadAllRole(): void {
    this.ApiRole.getAllRoleList().subscribe(
      data => {
        this.roles = data.data;
        console.log(this.roles);
        this.firstlevel = 'Tableau de bord';
        this.secondlevel = 'Roles';
        this.totalrole = this.roles.length;
        this.mdbTable.setDataSource(this.roles);
        this.elements = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();
      },
      err => console.error(err),
      () => console.log('liste des roles obtenus')
    );
  }

  @HostListener('input') oninput() {
    this.searchItems();
  }

  searchItems() {
    const prev = this.roles;

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.elements = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(15);

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }



  editRole(role: Role): void  {
    window.localStorage.removeItem('editRoleId');
    window.localStorage.setItem('editRoleId', role.id.toString());
    this.router.navigate(['start/modifier-role']);
  }

  showToasterRoleSupprimer() {
    this.notifyService.showSuccessWithTimeout('Role supprimé avec succès !!', 'Notification', 5000);
  }

  deleteRole(role: Role) {
    this.ApiRole.deleteRole(role).subscribe((data) => {
      this.roles = this.roles.filter(s => s !== role);     this.loadAllRole(); }
    );
    this.showToasterRoleSupprimer();
  }


}
