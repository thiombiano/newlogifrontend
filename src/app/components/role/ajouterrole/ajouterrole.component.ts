import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Role} from '../shared/role.model';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {RoleService} from '../shared/role.service';
import {Permission} from '../../permission/shared/permission.model';
import {Unitemesure} from '../../article/Shared/unitemesure';
import {PermissionService} from '../../permission/shared/permission.service';

@Component({
  selector: 'app-ajouterrole',
  templateUrl: './ajouterrole.component.html',
  styleUrls: ['./ajouterrole.component.css']
})
export class AjouterroleComponent implements OnInit {
  firstlevel: string;
  secondlevel: string;
  addForm: FormGroup;
  submitted = false;
  role: Role;
  permissions: Permission[];
  toppings: FormControl;
  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, public ApiPermission: PermissionService,  private toastr: ToastrService, private cdRef: ChangeDetectorRef, public ApiRole: RoleService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.ApiPermission.getAllPermissionList().subscribe((permissions: Permission[]) => {
      this.permissions = permissions;
    });
    this.toppings = new FormControl();
    this.firstlevel = 'Role';
    this.secondlevel = 'Ajouter role';
    this.addForm = this.formBuilder.group({
      libellerole: ['', Validators.required],
      descriptionrole: [''],
      permissions: ['', Validators.required],
    });
  }

  showToasterRoleEnregistrer() {
    this.notifyService.showSuccessWithTimeout('Role enregistré avec succès !!', 'Notification', 5000);
  }

  showToasterRoleErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }


  onSubmit() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.addForm.invalid) {
      this.showToasterRoleErreurModifier();
      return;
    }
    console.log(this.addForm.value);
    this.ApiRole.addRole(this.addForm.value).subscribe((role: Role) => {
      this.router.navigate(['start/roles']);
      this.showToasterRoleEnregistrer();
    });

  }

}
