import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {NotificationService} from '../../../utility/notification.service';
import {PermissionService} from '../../permission/shared/permission.service';
import {ToastrService} from 'ngx-toastr';
import {RoleService} from '../shared/role.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';
import {Permission} from '../../permission/shared/permission.model';
import {Role} from '../shared/role.model';

@Component({
  selector: 'app-modifierrole',
  templateUrl: './modifierrole.component.html',
  styleUrls: ['./modifierrole.component.css']
})
export class ModifierroleComponent implements OnInit {
  firstlevel: string;
  secondlevel: string;
  editForm: FormGroup;
  lespermissions: Permission[];
  permissions: Permission[];
  submitted = false;
  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, public ApiPermission: PermissionService,  private toastr: ToastrService, private cdRef: ChangeDetectorRef, public ApiRole: RoleService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.ApiPermission.getAllPermissionList().subscribe((permissions: Permission[]) => {
      this.lespermissions = permissions;
    });
    this.firstlevel = 'Role';
    this.secondlevel = 'Modifier role';
    const roleId = window.localStorage.getItem('editRoleId');
    if (!roleId) {
      alert('Invalid action.');
      this.router.navigate(['start/roles']);
      return;
    }
    this.editForm = this.formBuilder.group({
      id: [],
      libellerole: ['', Validators.required],
      descriptionrole: [''],
      permissions: ['', Validators.required],
    });

    this.ApiRole.getRole(+roleId).subscribe( (data) => {
      console.log('lespermissions pour le role 1 : ' +   this.lespermissions);
      this.editForm.get('id').setValue(data.data[0].id);
      this.editForm.get('libellerole').setValue(data.data[0].libellerole);
      this.editForm.get('descriptionrole').setValue(data.data[0].descriptionrole);
      this.editForm.get('permissions').setValue(data.data[0].permissions);
    });
  }

  compareFn(permission1: Permission, permission2: Permission) {
    return permission1 && permission2 ? permission1.id === permission2.id : permission1 === permission2;
  }

  showToasterRoleEnregistrer() {
    this.notifyService.showSuccessWithTimeout('Role mise à jour avec succès !!', 'Notification', 5000);
  }

  showToasterRoleErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }

  onSubmit() {

    this.submitted = true;
    console.log(this.editForm.value);
    // stop here if form is invalid
    if (this.editForm.invalid) {
      console.log('error form');
      this.showToasterRoleErreurModifier();
      return;
    }
    const roleId = window.localStorage.getItem('editRoleId');
    console.log(this.editForm.value);
    this.ApiRole.updateRole(+roleId, this.editForm.value)
      .pipe(first())
      .subscribe((role: Role) => {
        // @ts-ignore
        console.log(role);
        this.showToasterRoleEnregistrer();
        this.router.navigate(['start/roles']);
      });
  }

}
