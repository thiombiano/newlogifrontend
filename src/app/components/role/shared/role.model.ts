import {Permission} from '../../permission/shared/permission.model';

export class Role {
  id: number;
  libellerole: string;
  descriptionrole: string;
  permissions: Permission[];
}
