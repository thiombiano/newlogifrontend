import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {Role} from './role.model';

@Injectable({
  providedIn: 'root'
})
export class RoleService {


  constructor(private httpclient: HttpClient) { }

  getAllRoleList() {
    return  this.httpclient.get<Role[]>(environment.apiUrl + '/roles')
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Its a Trap!'))
      );
  }


  addRole(role: Role) {
    return  this.httpclient.post<Role>(environment.apiUrl + '/roles', role);
  }

  deleteRole(role: Role) {
    // @ts-ignore
    return this.httpclient.delete<Role>(environment.apiUrl + '/roles/' + role.id);
  }

  updateRole(id, value: any) {
    return this.httpclient.put<Role>(environment.apiUrl + '/roles/' + id, value );
  }


  getRole(id: number) {
    return  this.httpclient.get<Role>(environment.apiUrl + '/roles/' + id)
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Its a Trap!'))
      );
  }
}
