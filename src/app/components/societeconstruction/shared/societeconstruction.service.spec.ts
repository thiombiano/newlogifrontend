import { TestBed } from '@angular/core/testing';

import { SocieteconstructionService } from './societeconstruction.service';

describe('SocieteconstructionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SocieteconstructionService = TestBed.get(SocieteconstructionService);
    expect(service).toBeTruthy();
  });
});
