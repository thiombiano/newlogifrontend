import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Typemaison} from '../../typemaison/shared/Typemaison.model';
import {environment} from '../../../../environments/environment';
import {catchError, map} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import {Societeconstruction} from './societeconstruction.model';
import {Etape} from '../../etapeconstruction/shared/etape.model';

@Injectable({
  providedIn: 'root'
})
export class SocieteconstructionService {

  constructor(private httpclient: HttpClient) { }

  getAllSocieteList() {
    return  this.httpclient.get<Societeconstruction[]>(environment.apiUrl + '/societeconstructives')
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Impossible dobtenir la liste des sociétés'))
      );
  }


  updateSociete(id: number, value: any): Observable<Societeconstruction> {
    return this.httpclient
      .put<Societeconstruction>(environment.apiUrl + '/societeconstructives/' + id, value);
  }

  getSociete(id: number) {
    return  this.httpclient.get<Societeconstruction>(environment.apiUrl + '/societeconstructives/' + id);
  }


  addSociete(societe: Societeconstruction) {
    return  this.httpclient.post<Societeconstruction>(environment.apiUrl + '/societeconstructives', societe);
  }

  deleteSociete(societe: Societeconstruction) {
    // @ts-ignore
    return this.httpclient.delete<Societeconstruction>(environment.apiUrl + '/societeconstructives/' + societe.id);
  }

}
