import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutersocieteComponent } from './ajoutersociete.component';

describe('AjoutersocieteComponent', () => {
  let component: AjoutersocieteComponent;
  let fixture: ComponentFixture<AjoutersocieteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjoutersocieteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutersocieteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
