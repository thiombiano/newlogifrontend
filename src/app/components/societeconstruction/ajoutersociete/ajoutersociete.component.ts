import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Etape} from '../../etapeconstruction/shared/etape.model';
import {Societeconstruction} from '../shared/societeconstruction.model';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {EtapeService} from '../../etapeconstruction/shared/etape.service';
import {Router} from '@angular/router';
import {SocieteconstructionService} from '../shared/societeconstruction.service';

@Component({
  selector: 'app-ajoutersociete',
  templateUrl: './ajoutersociete.component.html',
  styleUrls: ['./ajoutersociete.component.css']
})
export class AjoutersocieteComponent implements OnInit {
  firstlevel: string;
  secondlevel: string;
  addForm: FormGroup;
  submitted = false;
  societe: Societeconstruction;
  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, private toastr: ToastrService, private cdRef: ChangeDetectorRef, public ApiSociete: SocieteconstructionService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.firstlevel = 'Société de construction';
    this.secondlevel = 'Ajouter une société de construction';
    this.addForm = this.formBuilder.group({
      libellesociete: ['', Validators.required],
      description: [''],
    });
  }


  showToasterSocieteEnregistrer() {
    this.notifyService.showSuccessWithTimeout('Société enregistrée avec succès !!', 'Notification', 5000);
  }

  showToasterSocieteErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }

  onSubmit() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.addForm.invalid) {
      this.showToasterSocieteErreurModifier();
      return;
    }
    console.log(this.addForm.value);

    this.ApiSociete.addSociete(this.addForm.value).subscribe((societe: Societeconstruction) => {
      this.router.navigate(['start/societes']);
      this.showToasterSocieteEnregistrer();
    });

  }

}
