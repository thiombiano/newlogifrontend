import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocieteconstructionComponent } from './societeconstruction.component';

describe('SocieteconstructionComponent', () => {
  let component: SocieteconstructionComponent;
  let fixture: ComponentFixture<SocieteconstructionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocieteconstructionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocieteconstructionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
