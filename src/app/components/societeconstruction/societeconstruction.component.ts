import {ChangeDetectorRef, Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {MdbTableDirective, MdbTablePaginationComponent} from 'angular-bootstrap-md';
import {NotificationService} from '../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {SocieteconstructionService} from './shared/societeconstruction.service';
import {Societeconstruction} from './shared/societeconstruction.model';
import {Etape} from '../etapeconstruction/shared/etape.model';

@Component({
  selector: 'app-societeconstruction',
  templateUrl: './societeconstruction.component.html',
  styleUrls: ['./societeconstruction.component.css']
})
export class SocieteconstructionComponent implements OnInit {
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  headElements = ['N°',  'Société', 'Description', 'Action'];
  searchText = '';
  totalsociete = 0;
  elements: any = [];
  societes: Societeconstruction[];
  previous: string;
  firstlevel: string;
  secondlevel: string;
  popoverTitle = 'Suppression de cette société';
  popoverMessage = 'Voulez vous vraiment supprimer cette société';
  cancelClicked = false;
  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, private toastr: ToastrService, private cdRef: ChangeDetectorRef, private ApiSociete: SocieteconstructionService, private router: Router) { }

  ngOnInit() {
    this.loadAllSociete();
  }

  loadAllSociete(): void {
    this.ApiSociete.getAllSocieteList().subscribe(
      data => {
        this.societes = data.data;
        console.log(this.societes);
        this.firstlevel = 'Tableau de bord';
        this.secondlevel = 'Sociétés';
        this.totalsociete = this.societes.length;
        this.mdbTable.setDataSource(this.societes);
        this.elements = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();
      },
      err => console.error(err),
      () => console.log('liste des sociétés obtenue')
    );
  }


  @HostListener('input') oninput() {
    this.searchItems();
  }


  searchItems() {
    const prev = this.societes;

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.elements = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(15);

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }


  editSociete(societe: Societeconstruction): void  {
    window.localStorage.removeItem('editSocieteId');
    window.localStorage.setItem('editSocieteId', societe.id.toString());
    this.router.navigate(['start/modifier-societe']);
  }

  showToasterSocieteSupprimer() {
    this.notifyService.showSuccessWithTimeout('Société supprimée avec succès !!', 'Notification', 5000);
  }

  deleteSociete(societe: Societeconstruction) {
    this.ApiSociete.deleteSociete(societe).subscribe((data) => {
      this.societes = this.societes.filter(s => s !== societe);     this.loadAllSociete(); }
    );
    this.showToasterSocieteSupprimer();
  }


}
