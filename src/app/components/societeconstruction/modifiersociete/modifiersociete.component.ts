import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {SocieteconstructionService} from '../shared/societeconstruction.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Societeconstruction} from '../shared/societeconstruction.model';
import {first} from 'rxjs/operators';
import {Etape} from '../../etapeconstruction/shared/etape.model';

@Component({
  selector: 'app-modifiersociete',
  templateUrl: './modifiersociete.component.html',
  styleUrls: ['./modifiersociete.component.css']
})
export class ModifiersocieteComponent implements OnInit {
  firstlevel: string;
  secondlevel: string;
  editForm: FormGroup;
  submitted = false;
  societe: Societeconstruction;
  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, private toastr: ToastrService, private cdRef: ChangeDetectorRef, public ApiSociete: SocieteconstructionService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    const societeId = window.localStorage.getItem('editSocieteId');
    if (!societeId) {
      alert('Invalid action.');
      this.router.navigate(['start/societes']);
      return;
    }

    this.firstlevel = 'Société';
    this.secondlevel = 'Modifier une société de construction';

    this.editForm = this.formBuilder.group({
      id: [''],
      libellesociete: ['', Validators.required],
      description: [''],
    });

    this.ApiSociete.getSociete(+societeId).subscribe( (data) => {
      console.log(data);
      this.editForm.setValue(data);
    });
  }


  showToasterSocieteModifier() {
    this.notifyService.showSuccessWithTimeout('Société modifiée avec succès !!', 'Notification', 5000);
  }

  showToasterSocieteErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }

  onSubmit() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.editForm.invalid) {
      console.log('error form');
      this.showToasterSocieteErreurModifier();
    }
    const societeId = window.localStorage.getItem('editSocieteId');
    console.log(this.editForm.value);
    this.ApiSociete.updateSociete(+societeId, this.editForm.value)
      .pipe(first())
      .subscribe((societe: Societeconstruction) => {
        // @ts-ignore
        console.log(societe);
        this.showToasterSocieteModifier();
        this.router.navigate(['start/societes']);
      });
  }

}
