import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TypecontratService {

  constructor(private http: HttpClient) { }

  getAllTypeContrat() {
    return  this.http.get(environment.apiUrl + '/typecontrats')
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Impossible dobtenir les infos sur le type de contrat!'))
      );
  }
}
