import { TestBed } from '@angular/core/testing';

import { TypecontratService } from './typecontrat.service';

describe('TypecontratService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TypecontratService = TestBed.get(TypecontratService);
    expect(service).toBeTruthy();
  });
});
