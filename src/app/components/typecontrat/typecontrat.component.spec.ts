import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypecontratComponent } from './typecontrat.component';

describe('TypecontratComponent', () => {
  let component: TypecontratComponent;
  let fixture: ComponentFixture<TypecontratComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypecontratComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypecontratComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
