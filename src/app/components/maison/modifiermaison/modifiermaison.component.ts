import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';
import {Lot} from '../../lot/shared/lot.model';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {LotService} from '../../lot/shared/lot.service';
import {MaisonService} from '../shared/maison.service';
import {Maison} from '../shared/maison.model';
import {SectionService} from '../../section/shared/section.service';

@Component({
  selector: 'app-modifiermaison',
  templateUrl: './modifiermaison.component.html',
  styleUrls: ['./modifiermaison.component.css']
})
export class ModifiermaisonComponent  {
  @Output() passEntry: EventEmitter<any> = new EventEmitter();
  constructor(public ApiSection: SectionService, public dialogRef: MatDialogRef<ModifiermaisonComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public ApiMaison: MaisonService) { }

  public lotofsectionsprime: Lot[];
  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Champ requis' : '';
  }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  GetLotBySectionPrime(value) {
    console.log(value);
    window.localStorage.removeItem('sectionid');
    const permArray = [];
    window.localStorage.setItem('sectionid', value);
    this.ApiSection.getOneSectionOfLotPRIME(value).subscribe((data) =>  {
      this.data['getalllots'] = data;
    });
  }

  stopEdit(): void {
    console.log('les données dans le modal :' + this.data);
    const idmaison = this.data.id;
    // @ts-ignore
    this.ApiMaison.updateMaison(+idmaison , this.data)
      .pipe(first())
      .subscribe((maison: Maison) => {
        console.log(maison);
        this.passEntry.emit(maison);
      });
  }
}
