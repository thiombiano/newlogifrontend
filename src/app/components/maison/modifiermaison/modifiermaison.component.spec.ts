import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifiermaisonComponent } from './modifiermaison.component';

describe('ModifiermaisonComponent', () => {
  let component: ModifiermaisonComponent;
  let fixture: ComponentFixture<ModifiermaisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifiermaisonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifiermaisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
