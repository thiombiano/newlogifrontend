import {Societeconstruction} from '../../societeconstruction/shared/societeconstruction.model';
import {Employe} from '../../employe/shared/employe.model';
import {Typemaison} from '../../typemaison/shared/Typemaison.model';

export class Maison {
  id: number;
  libellemaison: string;
  codevirtuel: string;
  superficie: string;
  client: string;
  description: string;
  type_maison_id: number;
  lot_id: number;
  technicien_id: number;
  controleur_id: number;
  is_finished: boolean;
  societe: Societeconstruction[];
  technicien: Employe[];
  typemaison: Typemaison[];
  societe_id: number;
  is_actif: boolean;
}
