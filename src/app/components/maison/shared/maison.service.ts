import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Lot} from '../../lot/shared/lot.model';
import {environment} from '../../../../environments/environment';
import {Maison} from './maison.model';
import {TabdisplaytodashModel} from '../../chantier/manageprestataire/shared/tabdisplaytodash.model';
import {Etape} from '../../etapeconstruction/shared/etape.model';
import {Fonction} from '../../fonctions/shared/fonction';
import {catchError, map} from "rxjs/operators";
import {throwError} from "rxjs";

@Injectable()
export class MaisonService {
  public selectedOneMaison: Maison;

  constructor(private httpclient: HttpClient) { }

  addMaison(maison: Maison) {
    return  this.httpclient.post<Maison>(environment.apiUrl +  '/maisons', maison);
  }

  deleteMaison(id: number) {
    // @ts-ignore
    return this.httpclient.delete<Maison>(environment.apiUrl +  '/maisons/' + id);
  }
  getMaisonForBesoin(id: number) {
    return  this.httpclient.get<Maison>(environment.apiUrl + '/maisons/' + id)
      .toPromise()
      .then(x => {
        this.selectedOneMaison = x;
      });
  }


  updateMaison(id: number, value: any ) {
    return this.httpclient.put<Maison>(environment.apiUrl + '/maisons/' + id, value );
  }

  getMaisonById(id: number) {
    return this.httpclient.get(environment.apiUrl + '/maisonbyid/' + id);
  }

  getMaisonByIdInBesoin(id: number) {
    return this.httpclient.get(environment.apiUrl + '/maisons/' + id);
  }
}
