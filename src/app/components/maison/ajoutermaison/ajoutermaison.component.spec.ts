import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutermaisonComponent } from './ajoutermaison.component';

describe('AjoutermaisonComponent', () => {
  let component: AjoutermaisonComponent;
  let fixture: ComponentFixture<AjoutermaisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjoutermaisonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutermaisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
