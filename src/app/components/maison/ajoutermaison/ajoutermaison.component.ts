import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {Lot} from '../../lot/shared/lot.model';
import {SectionService} from '../../section/shared/section.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Section} from '../../section/shared/section.model';
import {LotService} from '../../lot/shared/lot.service';
import {Maison} from '../shared/maison.model';
import {MaisonService} from '../shared/maison.service';
import {EtapeService} from '../../etapeconstruction/shared/etape.service';
import {Etape} from '../../etapeconstruction/shared/etape.model';
import {DatePipe, formatDate} from '@angular/common';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {EtapeencoursService} from '../../manageetapeencoursformaison/shared/etapeencours.service';
import {EtapeencoursModel} from '../../manageetapeencoursformaison/shared/etapeencours.model';

@Component({
  selector: 'app-ajoutermaison',
  templateUrl: './ajoutermaison.component.html',
  styleUrls: ['./ajoutermaison.component.css'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
  ]
})
export class AjoutermaisonComponent  {
  public lotofsectionsprime: Lot[];
  etapes: Etape[];
  idofetapeavancement: number;
  // tslint:disable-next-line:max-line-length
  constructor(public apiEtapeEnCour: EtapeencoursService, public ApiEtape: EtapeService, public ApiSection: SectionService, public dialogRef: MatDialogRef<AjoutermaisonComponent>, private dateAdapter: DateAdapter<Date>, private datePipe: DatePipe, private _adapter: DateAdapter<any>,
              @Inject(MAT_DIALOG_DATA) public data: Maison,
              public ApiMaison: MaisonService) { this._adapter.setLocale('fr'); }
  thedate: Date;
  dateouvertureetape: Date;
  @Output() passEntry: EventEmitter<any> = new EventEmitter();

  formControl = new FormControl('', [
    Validators.required
  ]);


  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Le champ est requis' : '';
  }

  GetLotBySectionPrime(value) {
    console.log(value);
    window.localStorage.removeItem('sectionid');
    const permArray = [];
    window.localStorage.setItem('sectionid', value);
    this.ApiSection.getOneSectionOfLotPRIME(value).subscribe((data) =>  {
      this.lotofsectionsprime = data;
    });
  }


  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {

    console.log('les données dans le modal :' + this.data.type_maison_id);
    this.ApiMaison.addMaison(this.data).subscribe((maison: Maison) => {
      this.passEntry.emit(maison);
      // console.log('les données de la maisons : ' +  maison);
      // const formData = new FormData();
      // @ts-ignore
      // formData.append('etape_construction_id', this.idofetapeavancement);
      // @ts-ignore
      // formData.append('maison_id', maison);
      // @ts-ignore
      // formData.append('dateouvertureetape', this.dateouvertureetape);
      // this.apiEtapeEnCour.addEtapeEncoursForMaison(formData).subscribe((dataetapencours) => {
      //   console.log(dataetapencours);
      // });
    });


  }

  GetEtapeByTypeMaison(value) {
    this.ApiEtape.getAllEtapeListByTypeMaison(+value).subscribe((data) => {
      this.etapes = data;
    });
  }
}
