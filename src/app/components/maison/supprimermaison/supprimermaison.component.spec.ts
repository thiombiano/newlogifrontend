import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupprimermaisonComponent } from './supprimermaison.component';

describe('SupprimermaisonComponent', () => {
  let component: SupprimermaisonComponent;
  let fixture: ComponentFixture<SupprimermaisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupprimermaisonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupprimermaisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
