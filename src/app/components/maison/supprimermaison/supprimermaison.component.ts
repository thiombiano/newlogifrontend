import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {Lot} from '../../lot/shared/lot.model';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {LotService} from '../../lot/shared/lot.service';
import {MaisonService} from '../shared/maison.service';
import {Maison} from '../shared/maison.model';

@Component({
  selector: 'app-supprimermaison',
  templateUrl: './supprimermaison.component.html',
  styleUrls: ['./supprimermaison.component.css']
})
export class SupprimermaisonComponent {
  @Output() passEntry: EventEmitter<any> = new EventEmitter();
  constructor(public dialogRef: MatDialogRef<SupprimermaisonComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public ApiMaison: MaisonService) { }


  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    if (this.data.id) {
      this.ApiMaison.deleteMaison(this.data.id).subscribe((maison: Maison) => {
        console.log('la maison : ' + maison);
        this.passEntry.emit(maison);
      });
    }
  }
}
