import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoirbesoinComponent } from './voirbesoin.component';

describe('VoirbesoinComponent', () => {
  let component: VoirbesoinComponent;
  let fixture: ComponentFixture<VoirbesoinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoirbesoinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoirbesoinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
