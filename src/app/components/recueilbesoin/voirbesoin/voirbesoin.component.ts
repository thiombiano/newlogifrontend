import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {RecueilbesoinService} from '../shared/recueilbesoin.service';
import {ChantierService} from '../../chantier/shared/chantier.service';
import {Recueilbesoin} from '../shared/recueilbesoin.model';

@Component({
  selector: 'app-voirbesoin',
  templateUrl: './voirbesoin.component.html',
  styleUrls: ['./voirbesoin.component.css']
})
export class VoirbesoinComponent implements OnInit {
  firstlevel: string;
  secondlevel: string;
  recueilbesoin: Recueilbesoin;
  // tslint:disable-next-line:max-line-length
  constructor(private router: Router, private cdRef: ChangeDetectorRef, public ApiRecueil: RecueilbesoinService, public chantierService: ChantierService) { }

  ngOnInit() {
    const idrecueil =   window.localStorage.getItem('IDVrecueil');
    console.log('id:' + idrecueil);
    this.firstlevel = 'Détail du besoin';
    this.secondlevel = 'Recueil besoin';
    this.ApiRecueil.getSelectedRecueil(+idrecueil).subscribe((data) => {
      this.recueilbesoin = data.data;
      console.log('all data : ' + data.data.numeroRB);
    });
  }
}
