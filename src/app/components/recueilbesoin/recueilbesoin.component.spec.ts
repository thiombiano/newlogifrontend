import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecueilbesoinComponent } from './recueilbesoin.component';

describe('RecueilbesoinComponent', () => {
  let component: RecueilbesoinComponent;
  let fixture: ComponentFixture<RecueilbesoinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecueilbesoinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecueilbesoinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
