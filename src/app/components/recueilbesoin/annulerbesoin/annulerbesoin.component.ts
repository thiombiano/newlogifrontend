import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Recueilbesoin} from '../shared/recueilbesoin.model';
import {Router} from '@angular/router';
import {RecueilbesoinService} from '../shared/recueilbesoin.service';
import {ChantierService} from '../../chantier/shared/chantier.service';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ConfirmationDialogComponent} from '../../confirmation-dialog/confirmation-dialog.component';
import {first} from 'rxjs/operators';
import {Commande} from '../../commandes/shared/commande';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'app-annulerbesoin',
  templateUrl: './annulerbesoin.component.html',
  styleUrls: ['./annulerbesoin.component.css']
})
export class AnnulerbesoinComponent implements OnInit {
  firstlevel: string;
  secondlevel: string;
  recueilbesoin: Recueilbesoin;
  submitted = false;
  editForm: FormGroup;
  // tslint:disable-next-line:max-line-length
  constructor(public dialog: MatDialog, private notifyService: NotificationService, private _formbuilder: FormBuilder, private toastr: ToastrService, private router: Router, private cdRef: ChangeDetectorRef, public ApiRecueil: RecueilbesoinService, public chantierService: ChantierService) { }


  ngOnInit() {
    const idrecueil =   window.localStorage.getItem('IDvrecueil');
    console.log('id:' + idrecueil);
    this.firstlevel = 'Annulation du besoin';
    this.secondlevel = 'Recueil besoin';
    this.ApiRecueil.getSelectedRecueil(+idrecueil).subscribe((data) => {
      this.recueilbesoin = data.data;
      console.log('all data : ' + data.data.numeroRB);
    });
    this.editForm = this._formbuilder.group({
      canceledcommentaire: ['', Validators.required]
    });
  }


  showToasterAnnulerBesoinModifier() {
    this.notifyService.showSuccessWithTimeout('Besoin annuler avec succès !!', 'Notification', 5000);
  }

  showToasterAnnulerBesoinErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }

  onSubmit() {


    this.submitted = true;
    // stop here if form is invalid
    if (this.editForm.invalid) {
      console.log('error form');
      this.showToasterAnnulerBesoinErreurModifier();
    } else {
      const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        width: '350px',
        data: 'êtes vous sûr de vouloir annuler ce recueil de besoin?'
      });
      const idrecueil =   window.localStorage.getItem('IDvrecueil');
      console.log(this.editForm.value);

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.ApiRecueil.updateAnnulerChantierRecueil(+idrecueil, this.editForm.value)
            .pipe(first())
            .subscribe((recueil: Recueilbesoin) => {
              // @ts-ignore
              console.log(recueil);
              this.showToasterAnnulerBesoinModifier();
              this.router.navigate(['start/recueils']);
            });
        }
      });
    }


  }

}
