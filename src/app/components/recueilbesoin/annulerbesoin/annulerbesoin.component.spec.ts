import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnulerbesoinComponent } from './annulerbesoin.component';

describe('AnnulerbesoinComponent', () => {
  let component: AnnulerbesoinComponent;
  let fixture: ComponentFixture<AnnulerbesoinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnnulerbesoinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnulerbesoinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
