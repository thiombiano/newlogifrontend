import {ChangeDetectorRef, Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {MdbTableDirective, MdbTablePaginationComponent} from 'angular-bootstrap-md';
import {Commande} from '../commandes/shared/commande';
import {Recueilbesoin} from './shared/recueilbesoin.model';
import {Router} from '@angular/router';
import {ChantierService} from '../chantier/shared/chantier.service';
import {RecueilbesoinService} from './shared/recueilbesoin.service';
import {environment} from '../../../environments/environment';
import {NgxSpinnerService} from "ngx-spinner";

@Component({
  selector: 'app-recueilbesoin',
  templateUrl: './recueilbesoin.component.html',
  styleUrls: ['./recueilbesoin.component.css']
})
export class RecueilbesoinComponent implements OnInit {
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  popoverTitle = 'Validation du besion';
  popoverMessage = 'Voulez vous vraiment valider cet besoin';
  cancelClicked = false;
  elements: any = [];
  display = 'none';
  curDate = new Date();
  recueils: Recueilbesoin[];
  previous: string;
  searchText = '';
  totalrecueil: number;
  firstlevel: string;
  secondlevel: string;
  headElements = ['N°', 'Date', 'Numéro RB' ,  'Date Livraison cible', 'Actions'];
  public roles: { A: string; T: string; L: string; M: string;  C: string; SA: string; CSR: string; CDC: string};
  // tslint:disable-next-line:max-line-length
  constructor(public SpinnerService: NgxSpinnerService, private router: Router, private cdRef: ChangeDetectorRef, public ApiRecueil: RecueilbesoinService, public chantierService: ChantierService) { }

  ngOnInit() {
    this.roles = environment.roles;
    const id =  window.localStorage.getItem('Idchantier');
    this.getListeRecueil(+id);
  }

  voirdetailrecueil(id) {
    window.localStorage.removeItem('IDVrecueil');
    window.localStorage.setItem('IDVrecueil', id);
    this.router.navigate(['/start/voir-recueil']);
  }

  ModifierRecueil(id) {
    window.localStorage.removeItem('IDrecueil');
    window.localStorage.setItem('IDrecueil', id);
    // this.commandeService.getSelectedCommande(+id);
    this.router.navigate(['/start/modifier-recueil']);
  }

  AnnulerBesoin(id) {
    window.localStorage.removeItem('IDvrecueil');
    window.localStorage.setItem('IDvrecueil', id);
    // this.commandeService.getSelectedCommande(+id);
    this.router.navigate(['/start/annuler-recueil']);
  }

  searchItems() {
    const prev = this.recueils;

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.elements = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(15);
    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

  @HostListener('input') oninput() {
    this.searchItems();
  }

  getListeRecueil(id) {
    this.ApiRecueil.getAllRECUEILList(id).subscribe(
      data => {
        this.recueils = data;
        this.firstlevel = 'Tableau de bord';
        this.secondlevel = 'Recueils';
        this.totalrecueil =  this.recueils.length;
        this.mdbTable.setDataSource(this.recueils);
        this.elements = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();
      },
      err => console.error(err),
      () => console.log('liste recueil obtenue')
    );
  }

  validateBesoin(el) {
    this.SpinnerService.show();
    this.ApiRecueil.updateValiderrRecueilBesoinChantie(el.id).subscribe(() => {
      const id =  window.localStorage.getItem('Idchantier');
      this.getListeRecueil(+id);
      this.SpinnerService.hide();
    });
  }
}
