import {DetailsCommande} from '../../commandes/shared/commande';

export class Recueilbesoin {
  constructor(
    public numeroRB: string,
    public statutRB: string,
    public dateRB: Date,
    public destinations: string,
    public chantier_id: number,
    public dateLivraisonCible: Date,
    public articles: DetailsBesoin[],
    public isVisaDemandeur: boolean,
    public isVisaConducteurChantier: boolean,
    public isVisaResponsableStock: boolean,
    public isVisaDirecteurTechnique: boolean,
    public isVisaResponsableAchat: boolean,
    public isValidate: boolean,
    public chantier?: string,
    public id?: number
) {}
}


export class DetailsBesoin {
  constructor(
    public article_id: number,
    public unite: string,
    public quantiteDemandee: number,
    public article?: string,
    public reference?: string
  ) {}
}

