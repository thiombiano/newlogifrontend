import { Injectable } from '@angular/core';
import {environment} from '../../../../environments/environment';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Commande, DetailsCommande} from '../../commandes/shared/commande';
import {DetailsBesoin, Recueilbesoin} from './recueilbesoin.model';
import {CommandeInsert} from '../../commandes/shared/commandInsert';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Access-Control-Allow-Origin': '*'
    // 'Authorization': 'Bearer '+ this.token
  })
};
@Injectable()
export class RecueilbesoinService {
  // tslint:disable-next-line:max-line-length
  formData: {numeroRB: null, statutRB: null, dateRB: null, isVisaDemandeur: null, destinations: null, isVisaConducteurChantier: null, isVisaResponsableStock: null,  isVisaDirecteurTechnique: null, chantier_id: null, isVisaResponsableAchat: null, dateLivraisonCible: null,  id: null, articles: DetailsBesoin[] };
  constructor(public http: HttpClient) { }
  selectedRecueil: Recueilbesoin;
  getAllRECUEILList(idChantier: number) {
    return this.http.get(environment.apiUrl + '/chantiers/' + idChantier + '/recueilbesoins')
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Impossible dobtenir la liste de toutes les recueils de besoin!'))
      );
  }

  getSelectedRecueil(id: number) {
   return  this.http.get(environment.apiUrl + '/recueils/' + id) .pipe(map((data: any) => data ),
     catchError(error => throwError('Impossible dobtenir la liste de toutes les commandes!'))
   );
  }

  addChantierRecueil(idChantier: number, recueil: Recueilbesoin) {
    return this.http
      .post<Recueilbesoin>(
        environment.apiUrl + '/chantiers/' + idChantier + '/recueilbesoins',
        recueil,
        httpOptions
      );
  }

  updateAnnulerChantierRecueil(idRecueil, value: any) {
    return this.http
      .post<Recueilbesoin>(
        environment.apiUrl + '/annulationrecueilbesoin/' + idRecueil , value
      );
  }



  updateValiderrRecueilBesoinChantie(idRecueil) {
    return this.http.get<Recueilbesoin>(environment.apiUrl + '/validerrecueilbesoin/' + idRecueil);
  }

  updateChantierRecueil(idRecueil, value: any) {
    const id =  window.localStorage.getItem('Idchantier');
    return this.http
      .put<Recueilbesoin>(
        environment.apiUrl + '/chantiers/' + idRecueil + '/recueilbesoins/' + id , value
      );
  }

}


interface RecueilApiResponseOne {
  data: Recueilbesoin;
}
