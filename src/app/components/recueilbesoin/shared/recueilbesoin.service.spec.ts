import { TestBed } from '@angular/core/testing';

import { RecueilbesoinService } from './recueilbesoin.service';

describe('RecueilbesoinService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RecueilbesoinService = TestBed.get(RecueilbesoinService);
    expect(service).toBeTruthy();
  });
});
