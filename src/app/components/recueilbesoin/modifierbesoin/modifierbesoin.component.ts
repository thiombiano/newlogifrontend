import { Component, OnInit } from '@angular/core';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatStepper} from '@angular/material';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {ChantierService} from '../../chantier/shared/chantier.service';
import {Router} from '@angular/router';
import {RecueilbesoinService} from '../shared/recueilbesoin.service';
import {ArticleService} from '../../article/Shared/article.service';
import {DatePipe, formatDate} from '@angular/common';
import {FormBuilder, NgForm} from '@angular/forms';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DetailsBesoin, Recueilbesoin} from '../shared/recueilbesoin.model';
import {Unitemesure} from '../../article/Shared/unitemesure';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-modifierbesoin',
  templateUrl: './modifierbesoin.component.html',
  styleUrls: ['./modifierbesoin.component.css'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
  ]
})
export class ModifierbesoinComponent implements OnInit {
  firstlevel: string;
  recueilbesoin: Recueilbesoin;
  currentdate =  formatDate(new Date(), 'dd/MM/yyyy:HH:mm:ss', 'en');
  displayDetails = false;
  public unite: any;
  unitemesure: Unitemesure[];
  public quantite: any;
  // @ts-ignore
  recueilinsert: Recueilbesoin = new Recueilbesoin();
  bLigneDetailAjoutee = true;
  thedate: Date;
  secondlevel: string;
  // tslint:disable-next-line:max-line-length
  constructor(public SpinnerService: NgxSpinnerService, private dateAdapter: DateAdapter<Date>, private notifyService: NotificationService, private toastr: ToastrService, public chantierService: ChantierService, private router: Router, public ApiRecueil: RecueilbesoinService, public articleService: ArticleService, private datePipe: DatePipe, private _adapter: DateAdapter<any>, private _formBuilder: FormBuilder) { }

  getAlldataSelected() {
    if (this.ApiRecueil.selectedRecueil) {
      this.recueilinsert = {
        numeroRB: null,
        statutRB: null,
        dateRB: null,
        isVisaDemandeur: null,
        destinations: null,
        isVisaConducteurChantier: null,
        isVisaResponsableStock: null,
        isVisaDirecteurTechnique: null,
        chantier_id: null,
        isVisaResponsableAchat: null,
        isValidate: null,
        dateLivraisonCible: null,
        articles: new Array<DetailsBesoin>(),
        id: null
      };

      for (const articleDemande of this.ApiRecueil.selectedRecueil.articles) {
        this.recueilinsert.articles.push({
          article_id: articleDemande.article_id,
          article: articleDemande.article,
          unite: articleDemande.unite,
          quantiteDemandee: articleDemande.quantiteDemandee,
        });
      }
    }
  }

  ngOnInit() {
    this._adapter.setLocale('fr');
    this.firstlevel = 'Recueil de besoin';
    this.secondlevel = 'Mis à jour Recueil Besoin';
    this.articleService.getArticleList();
    const idrecueil = window.localStorage.getItem('IDrecueil');
    this.ApiRecueil.getSelectedRecueil(+idrecueil).subscribe((data) => {
      this.recueilbesoin = data.data;
      console.log('all data : ' + data.data.numeroRB);
    });
  }

  goBack(stepper: MatStepper) {
    stepper.previous();
  }


  ajouterLesDetails(stepper: MatStepper) {
    this.articleService.getArticleList();

    const dateRB = this.recueilbesoin.dateRB;
    const dateLivraisonCible = this.recueilbesoin.dateLivraisonCible;

    // @ts-ignore
    if (dateRB  != null  && dateLivraisonCible  != null  && dateRB <= dateLivraisonCible) {
      this.displayDetails = true;
      stepper.next();
    } else {
      this.displayDetails = false;
      this.showToasterRecueilErreurModifier();
    }

  }

  resetForm(form?: NgForm) {
    if (form != null) {
      form.resetForm();
    }
    this.ApiRecueil.formData = {
      numeroRB: null,
      statutRB: null,
      dateRB: null,
      destinations: null,
      isVisaDemandeur: null,
      isVisaConducteurChantier: null,
      isVisaResponsableStock: null,
      isVisaDirecteurTechnique: null,
      chantier_id: null,
      isVisaResponsableAchat: null,
      dateLivraisonCible: null,
      articles: new Array<DetailsBesoin>(),
      id: null
    };

    // @ts-ignore
    // @ts-ignore
    // @ts-ignore
    this.articleService.selectedArticle = {
      libelle_article: null,
      reference: null,
      description: null,
      prix_referent: null,
      unite_mesure_id: null,
      unite: null,
      id: null
    };
  }

  showToasterRecueilAjoute() {
    this.notifyService.showSuccessWithTimeout('Besoin ajoutée avec succès!!', 'Notification', 5000);
  }

  selectionnerArticle() {
    console.log(this.articleService.selectedArticle);
    // @ts-ignore
    this.unitemesure = this.articleService.selectedArticle.unite_mesure;
    this.unite  =   this.unitemesure['code'];
  }

  showToasterRecueilErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }

  ajouterLaLigneDetail() {

    this.bLigneDetailAjoutee = true;
    this.recueilbesoin.articles.push({
      article_id: this.articleService.selectedArticle.id,
      article: this.articleService.selectedArticle.libelle_article,
      unite: this.articleService.selectedArticle.unite,
      quantiteDemandee: this.quantite,
    });
    console.log(this.recueilinsert);


    this.articleService.selectedArticle = null;
    this.quantite = null;
    this.unite = null;

  }

  supprimerLaLigneDetail(detailRecueil) {
    const index  =  this.recueilbesoin.articles.indexOf(detailRecueil);
    console.log(index);
    if (index !== -1) {
      this.recueilbesoin.articles.splice(index, 1);

      this.articleService.selectedArticle = null;
      this.quantite = null;
      this.unite = null;
    }
  }


  MiseAjourRecueilChantier() {

    const totalrecueil = this.recueilbesoin.articles.length;

    if (totalrecueil !== 0) {
      const date_RB = this.recueilbesoin.dateRB;
      const dateLivraisonCible = this.recueilbesoin.dateLivraisonCible;
      const numeroRB = this.recueilbesoin.numeroRB;
      const idrecueil = window.localStorage.getItem('IDrecueil');
      // @ts-ignore
      if (date_RB  != null  && dateLivraisonCible  != null ) {
        const idchantier = window.localStorage.getItem('Idchantier');
        const theid = +idchantier;
        this.SpinnerService.show();
        this.ApiRecueil.updateChantierRecueil(
          idrecueil,
          this.recueilbesoin
        ).subscribe((recueil: Recueilbesoin) => {
          this.router.navigate(['/start/recueils']);
          this.SpinnerService.hide();
          this.showToasterRecueilAjoute();
        });
        console.log(this.recueilbesoin);
      } else {
        this.showToasterRecueilErreurModifier();
      }
    } else{
      this.showToasterRecueilErreurModifier();
    }

  }

}
