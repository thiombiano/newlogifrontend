import { Component, OnInit } from '@angular/core';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {ChantierService} from '../../chantier/shared/chantier.service';
import {Router} from '@angular/router';
import {CommandeService} from '../../commandes/shared/commande.service';
import {ArticleService} from '../../article/Shared/article.service';
import {DatePipe, formatDate} from '@angular/common';
import {FormBuilder, FormGroup, NgForm} from '@angular/forms';
import {RecueilbesoinService} from '../shared/recueilbesoin.service';
import {CommandeInsert} from '../../commandes/shared/commandInsert';
import {DetailsBesoin, Recueilbesoin} from '../shared/recueilbesoin.model';
import {Commande, DetailsCommande} from '../../commandes/shared/commande';
import {MatStepper} from '@angular/material';
import {Unitemesure} from '../../article/Shared/unitemesure';
import {NgxSpinnerService} from 'ngx-spinner';
@Component({
  selector: 'app-ajouterbesoin',
  templateUrl: './ajouterbesoin.component.html',
  styleUrls: ['./ajouterbesoin.component.css'],
  providers: [
      {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
      {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
    ]
})
export class AjouterbesoinComponent implements OnInit {
  currentdate =  formatDate(new Date(), 'dd/MM/yyyy:HH:mm:ss', 'en');
  displayDetails = false;
  secondlevel: string;
  date_RB: Date;
  unitemesure: Unitemesure[];
  today: string;
  firstlevel: string;
  addForm: FormGroup;
  public unite: any;
  public quantite: any;
  thedate: Date;
  get f() { return this.addForm.controls; }
  bLigneDetailAjoutee = false;
  // @ts-ignore
  recueilinsert: Recueilbesoin = new Recueilbesoin();
  // tslint:disable-next-line:max-line-length
  constructor(private SpinnerService: NgxSpinnerService, private dateAdapter: DateAdapter<Date>, private notifyService: NotificationService, private toastr: ToastrService, private chantierService: ChantierService, private router: Router, public ApiRecueil: RecueilbesoinService, public articleService: ArticleService, private datePipe: DatePipe, private _adapter: DateAdapter<any>, private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this._adapter.setLocale('fr');
    this.firstlevel = 'Recueil de besoin';
    this.secondlevel = 'Recueil';
    this.articleService.getArticleList();

    this.recueilinsert = {
      numeroRB: null,
      statutRB: null,
      dateRB: null,
      isVisaDemandeur: null,
      destinations: null,
      isVisaConducteurChantier: null,
      isVisaResponsableStock: null,
      isVisaDirecteurTechnique: null,
      chantier_id: null,
      isVisaResponsableAchat: null,
      isValidate: null,
      dateLivraisonCible: null,
      articles: new Array<DetailsBesoin>(),
      id: null
    };
  }

  goBack(stepper: MatStepper) {
    stepper.previous();
  }


  ajouterLesDetails(stepper: MatStepper) {
    this.articleService.getArticleList();

    const dateRB = this.recueilinsert.dateRB;
    const dateLivraisonCible = this.recueilinsert.dateLivraisonCible;

    // @ts-ignore
    if (dateRB  != null  && dateLivraisonCible  != null  && dateRB <= dateLivraisonCible) {
      this.displayDetails = true;
      stepper.next();
    } else {
      this.displayDetails = false;
      this.showToasterRecueilErreurModifier();
    }

  }

  resetForm(form?: NgForm) {
    if (form != null) {
      form.resetForm();
    }
    this.ApiRecueil.formData = {
      numeroRB: null,
      statutRB: null,
      dateRB: null,
      destinations: null,
      isVisaDemandeur: null,
      isVisaConducteurChantier: null,
      isVisaResponsableStock: null,
      isVisaDirecteurTechnique: null,
      chantier_id: null,
      isVisaResponsableAchat: null,
      dateLivraisonCible: null,
      articles: new Array<DetailsBesoin>(),
      id: null
    };

    // @ts-ignore
    // @ts-ignore
    // @ts-ignore
    this.articleService.selectedArticle = {
      libelle_article: null,
      reference: null,
      description: null,
      prix_referent: null,
      unite_mesure_id: null,
      unite: null,
      id: null
    };
  }

  showToasterRecueilAjoute() {
    this.notifyService.showSuccessWithTimeout('Besoin ajoutée avec succès!!', 'Notification', 5000);
  }

  selectionnerArticle() {
    console.log(this.articleService.selectedArticle);
    // @ts-ignore
    this.unitemesure = this.articleService.selectedArticle.unite_mesure;
    this.unite  =   this.unitemesure['code'];
  }

  showToasterRecueilErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }

  ajouterLaLigneDetail() {

      this.bLigneDetailAjoutee = true;
      this.recueilinsert.articles.push({
        article_id: this.articleService.selectedArticle.id,
        article: this.articleService.selectedArticle.libelle_article,
        unite: this.articleService.selectedArticle.unite,
        quantiteDemandee: this.quantite,
      });
      console.log(this.recueilinsert);


      this.articleService.selectedArticle = null;
      this.quantite = null;
      this.unite = null;

  }

  supprimerLaLigneDetail(detailRecueil) {
    const index  =  this.recueilinsert.articles.indexOf(detailRecueil);
    console.log(index);
    if (index !== -1) {
      this.recueilinsert.articles.splice(index, 1);

      this.articleService.selectedArticle = null;
      this.quantite = null;
      this.unite = null;
    }
  }


  enregistrerRecueilChantier() {

    const totalrecueil = this.recueilinsert.articles.length;

    if (totalrecueil !== 0) {
      const date_RB = this.recueilinsert.dateRB;
      const dateLivraisonCible = this.recueilinsert.dateLivraisonCible;
      const numeroRB = this.recueilinsert.numeroRB;
      // @ts-ignore
      if (date_RB  != null  && dateLivraisonCible  != null ) {
        const idchantier = window.localStorage.getItem('Idchantier');
        const theid = +idchantier;
        this.SpinnerService.show();
        this.ApiRecueil.addChantierRecueil(
          theid,
          this.recueilinsert
        ).subscribe((recueil: Recueilbesoin) => {
          this.router.navigate(['/start/recueils']);
          this.SpinnerService.hide();
          this.showToasterRecueilAjoute();
        });
        console.log(this.recueilinsert);
      } else {
        this.showToasterRecueilErreurModifier();
      }
    } else{
      this.showToasterRecueilErreurModifier();
    }

  }

}

