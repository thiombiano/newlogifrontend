import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupprimersectionComponent } from './supprimersection.component';

describe('SupprimersectionComponent', () => {
  let component: SupprimersectionComponent;
  let fixture: ComponentFixture<SupprimersectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupprimersectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupprimersectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
