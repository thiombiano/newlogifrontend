import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Section} from '../shared/section.model';
import {SectionService} from '../shared/section.service';

@Component({
  selector: 'app-supprimersection',
  templateUrl: './supprimersection.component.html',
  styleUrls: ['./supprimersection.component.css']
})
export class SupprimersectionComponent {
  @Output() passEntry: EventEmitter<any> = new EventEmitter();
  constructor(public dialogRef: MatDialogRef<SupprimersectionComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public ApiSection: SectionService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    if(this.data.id) {
    this.ApiSection.deleteSection(this.data.id).subscribe((section: Section) => {
      console.log(section);
      this.passEntry.emit(section);
    });
    }
  }

}
