import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormControl, Validators} from '@angular/forms';
import {Section} from '../shared/section.model';
import {SectionService} from '../shared/section.service';

@Component({
  selector: 'app-ajoutersection',
  templateUrl: './ajoutersection.component.html',
  styleUrls: ['./ajoutersection.component.css']
})
export class AjoutersectionComponent  {
  @Output() passEntry: EventEmitter<any> = new EventEmitter();
  constructor(public dialogRef: MatDialogRef<AjoutersectionComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Section,
              public ApiSection: SectionService) { }


  formControl = new FormControl('', [
    Validators.required
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Le champ est requis' : '';
  }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    console.log('les données dans le modal :' + this.data);
    const ChantierId = window.localStorage.getItem('voirChantierId');
    this.data.chantier_id = +ChantierId;
    this.ApiSection.addSection(this.data).subscribe((section: Section) => {
      this.passEntry.emit(section);
      console.log(section);
    });
  }

}
