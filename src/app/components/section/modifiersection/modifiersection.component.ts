import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {SectionService} from '../shared/section.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Section} from '../shared/section.model';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-modifiersection',
  templateUrl: './modifiersection.component.html',
  styleUrls: ['./modifiersection.component.css']
})
export class ModifiersectionComponent {
  @Output() passEntry: EventEmitter<any> = new EventEmitter();
  constructor(public dialogRef: MatDialogRef<ModifiersectionComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public ApiSection: SectionService) { }

  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Champ requis' : '';
  }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  stopEdit(): void {
    console.log('les données dans le modal :' + this.data);
    const ChantierId = window.localStorage.getItem('voirChantierId');
    this.data.chantier_id = +ChantierId;
    const idsection = this.data.id;
    // @ts-ignore
    this.ApiSection.updateSection(+idsection , this.data)
      .pipe(first())
      .subscribe((section: Section) => {
      console.log(section);
      this.passEntry.emit(section);
    });
  }

}
