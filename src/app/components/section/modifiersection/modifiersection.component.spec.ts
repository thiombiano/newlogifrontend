import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifiersectionComponent } from './modifiersection.component';

describe('ModifiersectionComponent', () => {
  let component: ModifiersectionComponent;
  let fixture: ComponentFixture<ModifiersectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifiersectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifiersectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
