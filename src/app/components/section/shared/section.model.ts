import {Lot} from '../../lot/shared/lot.model';

export class Section {
  id: number;
  libellesection: string;
  is_actif: boolean;
  chantier_id: number;
  lots: Lot[];
}
