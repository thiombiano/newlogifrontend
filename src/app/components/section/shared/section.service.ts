import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {catchError, map} from 'rxjs/operators';
import {BehaviorSubject, throwError} from 'rxjs';
import {Section} from './section.model';

@Injectable()
export class SectionService {
  dataChange: BehaviorSubject<Section[]> = new BehaviorSubject<Section[]>([]);
  constructor(private httpclient: HttpClient) { }
  dialogData: any;
  getSectionList(idChantier: number) {
    return  this.httpclient.get(environment.apiUrl + '/chantiers/' + idChantier + '/sections')
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Its a Trap!'))
      );
  }

  getAllSection(idChantier: number)  {
   return  this.httpclient.get<Section[]>(environment.apiUrl + '/chantiers/' + idChantier + '/sections').subscribe(data => {
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        console.log (error.name + ' ' + error.message);
      });
  }

  addSection(section: Section) {
    const idchantier =  window.localStorage.getItem('voirChantierId');
    return  this.httpclient.post<Section>(environment.apiUrl + '/chantiers/' + idchantier + '/sections', section);
  }

  deleteSection(id: number) {
    const idchantier =  window.localStorage.getItem('voirChantierId');
    // @ts-ignore
    return this.httpclient.delete<Section>(environment.apiUrl + '/chantiers/' + id + '/sections/' + idchantier);
  }

  getOneSectionOfLot(idChantier: number, idsection: number) {
    return  this.httpclient.get(environment.apiUrl + '/chantiers/' + idChantier + '/sections/' + idsection)
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Its a Trap!'))
      );
  }

  getOneSectionOfLotPRIME(idsection: number) {
    return  this.httpclient.get(environment.apiUrl +  '/lotsbysections/' + idsection)
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Its a Trap!'))
      );
  }

  updateSection(id, value: any ) {
    const idchantier =  window.localStorage.getItem('voirChantierId');
    return this.httpclient.put<Section>(environment.apiUrl + '/chantiers/' + id  + '/sections/' + idchantier, value );
  }

  get data(): Section[] {
    return this.dataChange.value;
  }

}
