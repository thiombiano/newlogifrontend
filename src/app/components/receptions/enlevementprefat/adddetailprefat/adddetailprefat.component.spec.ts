import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdddetailprefatComponent } from './adddetailprefat.component';

describe('AdddetailprefatComponent', () => {
  let component: AdddetailprefatComponent;
  let fixture: ComponentFixture<AdddetailprefatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdddetailprefatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdddetailprefatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
