import {Component, Inject, OnInit} from '@angular/core';
import {NotificationService} from '../../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {StocksService} from '../../../stocks/shared/stocks.service';
import {FonctionsService} from '../../../fonctions/shared/fonctions.service';
import {ArticleService} from '../../../article/Shared/article.service';
import {EtapeService} from '../../../etapeconstruction/shared/etape.service';
import {BesoinjournalierService} from '../../../besoinjournalier/shared/besoinjournalier.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {BesoinJItem} from '../../../besoinjournalier/shared/besoinjournalier.model';
import {MaisonService} from '../../../maison/shared/maison.service';
import {FormControl, Validators} from '@angular/forms';
import {DetailArticleprefat, Prefat} from '../../shared/articletoprefat.interface';
import {Etape} from '../../../etapeconstruction/shared/etape.model';
import {Fonction} from '../../../fonctions/shared/fonction';
import {Personnel} from '../../../personnels/shared/personnel';
import {Detailenlevement} from '../../../enlevement/shared/detailenlevement';


@Component({
  selector: 'app-adddetailprefat',
  templateUrl: './adddetailprefat.component.html',
  styleUrls: ['./adddetailprefat.component.css']
})
export class AdddetailprefatComponent  {

  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, private toastr: ToastrService, public ApiStock: StocksService, public fonctionApi: FonctionsService, public articleService: ArticleService,  public ApiEtape: EtapeService, public ApiBesoinJournalier: BesoinjournalierService, public dialogRef: MatDialogRef<AdddetailprefatComponent>,
              @Inject(MAT_DIALOG_DATA) public data: BesoinJItem,
              public ApiMaison: MaisonService) { }

  quantiteCommande: number;
  etape: string;
  etapes: Etape[];
  maison: string;
  article: string ;
  leprestaire: string;
  libelleetape: string;
  fonctions: Fonction[];
  prestataire: Personnel;
  thebesoinofprefat: Detailenlevement;
  formControl = new FormControl('', [
    Validators.required
  ]);

  getFonctionByPrefat(value) {
    window.localStorage.removeItem('idprefat');
    window.localStorage.removeItem('libelleprefat');
    window.localStorage.setItem('libelleprefat', value.libelleprefat);
    window.localStorage.setItem('idprefat', value.id);
    this.fonctionApi.getFonctionByPrefat(value.id).subscribe((fonction: Fonction[]) => {
      this.fonctions = fonction;
    });
  }


  setSelectedArticle(value) {
    window.localStorage.removeItem('idarticle');
    window.localStorage.setItem('idarticle', value.article_id);
    this.articleService.getArticleForBesoin(+value.article_id);
    this.quantiteCommande = value.quantite_receptionnee;
    console.log(value);
  }

  setSelectedFonction(value) {
    this.fonctionApi.getFonctionForBesoin(value);
  }

  setPrestaire(value: any) {
    const idprefat = window.localStorage.getItem('idprefat');
    this.setSelectedFonction(value);
    this.fonctionApi.getPrestataireByPrefatAndByFonction(+idprefat, value ).subscribe((prestataire: Personnel[]) => {
      this.prestataire = prestataire[0];
      this.leprestaire = prestataire[0].prenom + ' ' + prestataire[0].nom;
      console.log('le prestataire est : ' + JSON.stringify(this.prestataire)  );
    });
  }

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Le champ est requis' : '';
  }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  showToasterEnlevementErreurQuantite() {
    this.notifyService.showErrorWithTimeout('Erreur: Quantité indisponible !!', 'Notification', 5000);
  }



  public confirmAdd(): void {
    let libelleprefats =    window.localStorage.getItem('libelleprefat');
    let idprefat =     window.localStorage.getItem('idprefat');
    let idARTICLE =     window.localStorage.getItem('idarticle');
    // tslint:disable-next-line:label-position
    // @ts-ignore
    this.thebesoinofprefat = ({
      prefat_id: +idprefat,
      article_id: +idARTICLE,
      quantiteBesoin: this.data.quantiteBesoin,
      commentaires: '',
      article: this.articleService.selectedOneArticle.libelle_article,
      fonction_id: this.data.fonction_id,
      libelleprefat: libelleprefats,
      libellefonction: this.fonctionApi.selectedOneFonction.libelleFonction
    });
    console.log('ALL DATA MUST EMITTED : ' + JSON.stringify(this.thebesoinofprefat) );
  }
}
