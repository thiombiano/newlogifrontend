import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnlevementprefatComponent } from './enlevementprefat.component';

describe('EnlevementprefatComponent', () => {
  let component: EnlevementprefatComponent;
  let fixture: ComponentFixture<EnlevementprefatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnlevementprefatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnlevementprefatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
