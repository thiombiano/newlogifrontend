import {ChangeDetectorRef, Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {ChantierService} from '../chantier/shared/chantier.service';
import {StocksService} from '../stocks/shared/stocks.service';
import {Router} from '@angular/router';
import {Reception} from './shared/reception';
import {ReceptionService} from './shared/reception.service';
import {MdbTableDirective, MdbTablePaginationComponent} from 'angular-bootstrap-md';
import {Article} from '../article/Shared/article';
class typereception {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-receptions',
  templateUrl: './receptions.component.html',
  styleUrls: ['./receptions.component.css']
})
export class ReceptionsComponent implements OnInit {
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  elements: any = [];
  previous: string;
  firstlevel: string;
  searchText = '';
  receptions: Reception[];
  listereception: Reception[];
  secondlevel: string;
  totalreception: number;
  headElements = ['N°', 'Date', 'Numéro RC' , 'Fournisseur', 'Numéro BC', 'Action'];
  typereceptions: typereception[] = [
    {value: 'Reception totale', viewValue: 'Réception totale'},
    {value: 'Reception partielle', viewValue: 'Réception partielle'},
    {value: 'Reception partielle finale', viewValue: 'Réception partielle finale'},
  ];
  popoverTitle = 'Valider cette réception partielle finale';
  popoverMessage = 'Voulez vous vraiment valider cette réception partielle finale';
  cancelClicked = false;
  // tslint:disable-next-line:max-line-length
  public selectedtypereception: string;
  constructor(private router: Router, private cdRef: ChangeDetectorRef, private receptionService: ReceptionService) { }

  ngOnInit() {
    const id =  window.localStorage.getItem('Idchantier');
    this.firstlevel = 'Tableau de bord';
    this.secondlevel = 'Receptions';
    this.selectedtypereception = 'Toutes les réceptions';
    this.getListeReception(+id);
  }

  getListeReceptionParChantier() {
    this.firstlevel = 'Tableau de bord';
    this.secondlevel = 'Réceptions';
    const idchantier = window.localStorage.getItem('Idchantier');
    const theid = +idchantier;
    console.log(idchantier);
    // @ts-ignore
    this.listereception = this.receptionService.getChantierReceptionList(theid);
    console.log(this.listereception);
    this.mdbTable.setDataSource(this.listereception);
    this.elements = this.mdbTable.getDataSource();
    this.previous = this.mdbTable.getDataSource();
  }


  voirdetailrreception(id) {
    window.localStorage.removeItem('IDreception');
    window.localStorage.setItem('IDreception', id);
    this.router.navigate(['/start/voir-detailreception']);
  }


  getListeReception(id: number)  {
    this.receptionService.getChantierReceptionList(id).subscribe(
      data => {
        console.log(data);
        this.receptions = data;
        this.totalreception = this.receptions.length;
        this.mdbTable.setDataSource(this.receptions);
        this.elements = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();
        console.log(this.receptions); // <-- here instead
      },
      err => console.error(err),
      () => console.log('Liste des réceptions obtenue')
    );
  }

  getListeReceptionTotale(id: number)  {
    this.receptionService.getChantierReceptionTotaleList(id).subscribe(
      data => {
        console.log(data);
        this.receptions = data;
        this.totalreception = this.receptions.length;
        this.mdbTable.setDataSource(this.receptions);
        this.elements = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();
        console.log(this.receptions); // <-- here instead
      },
      err => console.error(err),
      () => console.log('Wiki Items Geladen')
    );
  }

  getListeReceptionPartielle(id: number)  {
    this.receptionService.getChantierReceptionPartielleList(id).subscribe(
      data => {
        console.log(data);
        this.receptions = data;
        this.totalreception = this.receptions.length;
        this.mdbTable.setDataSource(this.receptions);
        this.elements = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();
        console.log(this.receptions); // <-- here instead
      },
      err => console.error(err),
      () => console.log('Wiki Items Geladen')
    );
  }

  getListeReceptionPartielleFinale(id: number)  {
    this.receptionService.getChantierReceptionPartielleFinaleList(id).subscribe(
      data => {
        this.receptions = data;
        this.totalreception = this.receptions.length;
        this.mdbTable.setDataSource(this.receptions);
        this.elements = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();
        console.log(this.receptions); // <-- here instead
      },
      err => console.error(err),
      () => console.log('Wiki Items Geladen')
    );
  }


  searchItems() {
    const prev = this.receptions;

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.elements = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(15);

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

  @HostListener('input') oninput() {
    this.searchItems();
  }


  GetReceptionState(value) {
    const id =  window.localStorage.getItem('Idchantier');
    if (value === 'Reception totale') {
      this.getListeReceptionTotale(+id);
    } else if (value === 'Reception partielle') {
      this.getListeReceptionPartielle(+id); } else if (value === 'Reception partielle finale') {
      this.getListeReceptionPartielleFinale(+id);
    } else {
      this.getListeReception(+id);
    }
  }

  UpdateForReceptionPartielleFinale(el) {
    const id =  window.localStorage.getItem('Idchantier');
    this.receptionService.updateForRecepetionPartielle(el.id).subscribe((reception: Reception) => {
      console.log(reception);
      this.getListeReception(+id);
      this.selectedtypereception = 'Toutes les réceptions';
      this.router.navigate(['/start/receptions']);
    });
  }
}
