export class Reception {
 numero_RC: string;
 date_RC: Date;
 statut_RC: number;
 commande_id: number;
 fournisseur_id: number;
 fournisseur: string;
 livreur: string;
 immatriculation: string;
 telephone_livreur: string;
 chantier_id: number;
 isValidateForReceptionPFinalle: boolean;
 typedereception: string;
 canceledcommentaire: string;
 isCanceled: boolean;
 numero_piece_fournisseur: string;
 articles: DetailsReception[];
 dateLivraisonEffectif?: Date;
 id?: number;
 commentaires: string;
}

export class DetailsReception {
  unite: string;
  quantite: number;
  article_id: number;
  quantite_receptionnee: number;
  commande_id: number;
  commentaires?: string;
  article?: string;
  reference?: string;
}
