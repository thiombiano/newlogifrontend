export class Prefat {
  id: number;
  articles: DetailArticleprefat[];
}

export class DetailArticleprefat {
  quantite: number;
  article_id: number;
  article: string;
}
