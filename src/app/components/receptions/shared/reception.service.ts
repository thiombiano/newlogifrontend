import { Injectable } from '@angular/core';
import {Reception} from './reception';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Article} from '../../article/Shared/article';
import {throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {Router} from '@angular/router';
import {CommandeService} from '../../commandes/shared/commande.service';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {NgxSpinnerService} from 'ngx-spinner';
import {Commande} from '../../commandes/shared/commande';

@Injectable({
  providedIn: 'root'
})
export class ReceptionService {
  selectedReception: Reception;
  listReceptions: Reception[];
  // tslint:disable-next-line:max-line-length
  constructor(private SpinnerService: NgxSpinnerService, private notifyService: NotificationService, private toastr: ToastrService, private router: Router, private http: HttpClient, private commandeService: CommandeService) { }

  // getChantierReceptionList(idChantier: number) {
  //   return  this.http
  //     .get(environment.apiUrl + '/chantiers/' + idChantier + '/receptions')
  //     .toPromise()
  //     .then((x: CommandeApiResponse) => {
  //       this.listReceptions = x.data;
  //     });
  // }


  getChantierReceptionList(idChantier: number) {
    return  this.http
      .get(environment.apiUrl + '/chantiers/' + idChantier + '/receptions')
      .pipe(map((data: any) => data.data ),
        catchError(error => throwError('Its a Trap!'))
      );
  }


  getChantierReceptionPartielleList(idChantier: number) {
    return  this.http
      .get(environment.apiUrl + '/chantiers/' + idChantier + '/receptionpartielle')
      .pipe(map((data: any) => data.data ),
        catchError(error => throwError('Its a Trap!'))
      );
  }

  getChantierReceptionTotaleList(idChantier: number) {
    return  this.http
      .get(environment.apiUrl + '/chantiers/' + idChantier + '/receptiontotale')
      .pipe(map((data: any) => data.data ),
        catchError(error => throwError('Its a Trap!'))
      );
  }

  getChantierReceptionPartielleFinaleList(idChantier: number) {
    return  this.http
      .get(environment.apiUrl + '/chantiers/' + idChantier + '/receptionpartiellefinale')
      .pipe(map((data: any) => data.data ),
        catchError(error => throwError('Its a Trap!'))
      );
  }


  updateForRecepetionPartielle(idReception: number) {
    // @ts-ignore
    return this.http.post<Reception>(environment.apiUrl + '/miseajourrecep/' + idReception);
  }

  getIsCommandereception(idcommande) {
    return this.http.get(environment.apiUrl + '/commandes/' + idcommande + '/receptions');
  }

  addCommandeReception(idCcommande: number, reception: Reception) {
    console.log('Enregistrement RC service: ' + JSON.stringify(reception) );
    const id = window.localStorage.getItem('Idchantier');

    return this.http
      .post<Reception>(
        environment.apiUrl + '/commandes/' + idCcommande + '/receptions',
        reception
      );
      // tslint:disable-next-line:max-line-length no-unused-expression
      // .subscribe(e => {console.log(e); this.router.navigate(['/start/receptions']); this.SpinnerService.hide(); this.showToasterReceptionAjoute(); } );
  }

  showToasterReceptionAjoute() {
    this.notifyService.showSuccessWithTimeout('Réception effectuée avec succès!!', 'Notification', 5000);
  }

  getSelectedReception(id: number) {
    this.http
      .get(environment.apiUrl + '/receptions/' + id)
      .toPromise()
      .then((x: ReceptionApiResponseOne) => {
        this.selectedReception = x.data;
        console.log('Selected Recept: ' + this.selectedReception);
        this.commandeService.getSelectedCommande(
          this.selectedReception.commande_id
        );
      });
  }

  getAllReceptionList() {
    return  this.http.get(environment.apiUrl + '/receptions')
      .pipe(map((data: any) => data.data ),
      catchError(error => throwError('Its a Trap!'))
    );
  }


}


interface CommandeApiResponse {
  data: Reception[];
}

interface ReceptionApiResponseOne {
  data: Reception;
}

