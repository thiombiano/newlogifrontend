import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnlevementmaisonComponent } from './enlevementmaison.component';

describe('EnlevementmaisonComponent', () => {
  let component: EnlevementmaisonComponent;
  let fixture: ComponentFixture<EnlevementmaisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnlevementmaisonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnlevementmaisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
