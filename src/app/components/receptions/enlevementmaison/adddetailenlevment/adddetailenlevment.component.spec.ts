import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdddetailenlevmentComponent } from './adddetailenlevment.component';

describe('AdddetailenlevmentComponent', () => {
  let component: AdddetailenlevmentComponent;
  let fixture: ComponentFixture<AdddetailenlevmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdddetailenlevmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdddetailenlevmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
