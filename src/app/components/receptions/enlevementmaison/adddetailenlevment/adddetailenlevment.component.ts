import {Component, Inject, OnInit} from '@angular/core';
import {NotificationService} from '../../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {StocksService} from '../../../stocks/shared/stocks.service';
import {FonctionsService} from '../../../fonctions/shared/fonctions.service';
import {ArticleService} from '../../../article/Shared/article.service';
import {EtapeService} from '../../../etapeconstruction/shared/etape.service';
import {BesoinjournalierService} from '../../../besoinjournalier/shared/besoinjournalier.service';
import {BesoinJItem} from '../../../besoinjournalier/shared/besoinjournalier.model';
import {MaisonService} from '../../../maison/shared/maison.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Etape} from '../../../etapeconstruction/shared/etape.model';
import {Fonction} from '../../../fonctions/shared/fonction';
import {Personnel} from '../../../personnels/shared/personnel';
import {FormControl, Validators} from '@angular/forms';
import {Detailenlevement} from '../../../enlevement/shared/detailenlevement';
import {Maison} from '../../../maison/shared/maison.model';
import {Lot} from '../../../lot/shared/lot.model';
import {LotService} from '../../../lot/shared/lot.service';
import {SectionService} from '../../../section/shared/section.service';

@Component({
  selector: 'app-adddetailenlevment',
  templateUrl: './adddetailenlevment.component.html',
  styleUrls: ['./adddetailenlevment.component.css']
})
export class AdddetailenlevmentComponent {

  // tslint:disable-next-line:max-line-length
  constructor(public ApiLot: LotService, public ApiSection: SectionService, private notifyService: NotificationService, private toastr: ToastrService, public ApiStock: StocksService, public fonctionApi: FonctionsService, public articleService: ArticleService,  public ApiEtape: EtapeService, public ApiBesoinJournalier: BesoinjournalierService, public dialogRef: MatDialogRef<AdddetailenlevmentComponent>,
              @Inject(MAT_DIALOG_DATA) public data: BesoinJItem,
              public ApiMaison: MaisonService) { }

  public isNull: string;
  sectionselectedinmaison: number;
  sectionselected: number;
  maisons: Maison[];
  lotofsectionsprime: Lot[];
  etape: string;
  etapes: Etape[];
  maison: string;
  article: string ;
  leprestaire: string;
  libelleetape: string;
  fonctions: Fonction[];
  prestataire: Personnel;
  thebesoin: Detailenlevement;
  formControl = new FormControl('', [Validators.required]
    );
  quantiteCommande: number;

  getFonctionByMaison(value) {
    this.fonctionApi.getFonctionByMaison(value).subscribe((fonction: Fonction[]) => {
      this.fonctions = fonction;
    });
  }

  getEtapeByMaison(value) {
    this.ApiEtape.getAllEtapeListByIdMaison(+value).subscribe((data) => {
      this.etapes = data;
    });
    window.localStorage.removeItem('idmaison');
    window.localStorage.setItem('idmaison', value);
    this.ApiMaison.getMaisonForBesoin(value);
    this.leprestaire = null;
    this.etapes = null;
    this.fonctions = null;
    this.getFonctionByMaison(value);

  }

  submit() {
    // emppty stuff
  }

  setSelectedArticle(value) {
    window.localStorage.removeItem('idarticle');
    window.localStorage.setItem('idarticle', value.article_id);
    this.articleService.getArticleForBesoin(+value.article_id);
    this.quantiteCommande = value.quantite_receptionnee;
    console.log(value);
  }

  setSelectedFonction(value) {
    this.fonctionApi.getFonctionForBesoin(value);
  }

  setSelectedEtape(value) {
    this.ApiEtape.getEtapeForBesoin(value);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  showToasterEnlevementErreurQuantite() {
    this.notifyService.showErrorWithTimeout('Erreur: Veuillez vérifier les quantités !!', 'Notification', 5000);
  }

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Le champ est requis' : '';
  }

  setPrestaire(value: any) {
    const idmaison = window.localStorage.getItem('idmaison');
    this.setSelectedFonction(value);
    this.fonctionApi.getPrestataireByMaisonAndByFonction(+idmaison, value ).subscribe((prestataire: Personnel[]) => {
      this.prestataire = prestataire[0];
      this.leprestaire = prestataire[0].prenom + ' ' + prestataire[0].nom;
      console.log('le prestataire est : ' + JSON.stringify(this.prestataire)  );
    });
  }


  GetLotBySectionPrime(value) {
    console.log(value);
    window.localStorage.removeItem('sectionid');
    this.lotofsectionsprime = undefined;
    window.localStorage.setItem('sectionid', value);
    this.sectionselected = value;
    this.sectionselectedinmaison = value;
    this.ApiSection.getOneSectionOfLotPRIME(value).subscribe((datalot) =>  {
      this.lotofsectionsprime = datalot;
    });
    this.isNull = null;
    this.ApiLot.getMaisonBySection(+value).subscribe((data) =>  {
      if (data.data.length !== 0) {
        console.log(data.data);
        this.maisons = data.data[0].maisons;
        console.log(this.maisons);
      } else {
        this.maisons = [];
        console.log(this.maisons);
      }
    });
  }


  GetMaisonByLot(value) {
    console.log(value);
    window.localStorage.removeItem('lotid');
    window.localStorage.setItem('lotid', value);
    this.ApiLot.getLotByMaison(value).subscribe((data) =>  {
      this.data['getallmaisons'] = data.data[0].maisons;
      // this.maisons = data.data[0].maisons;
      console.log(this.maisons);
    });
  }

  public confirmAdd(): void {
    console.log( this.quantiteCommande);
    if (this.data.quantiteBesoin <= this.quantiteCommande) {
      const idart = window.localStorage.getItem('idarticle');
      // tslint:disable-next-line:label-position
      // @ts-ignore
      this.thebesoin = ({
        maison_id: this.data.maison_id,
        etape_construction_id: this.data.etape_construction_id,
        article_id: +idart,
        quantiteBesoin: this.data.quantiteBesoin,
        commentaires: '',
        article: this.articleService.selectedOneArticle.libelle_article,
        libelleetape: this.ApiEtape.selectedOneEtape.libelleetape,
        fonction_id: this.data.fonction_id,
        libellemaison: this.ApiMaison.selectedOneMaison[0].libellemaison,
        libellefonction: this.fonctionApi.selectedOneFonction.libelleFonction
      });
      // console.log('Voici votre maison : ' + JSON.stringify(this.fonctionApi.selectedOneFonction[0].libelleFonction));
      console.log('ALL DATA MUST EMITTED : ' + JSON.stringify(this.thebesoin) );
    } else {
      this.showToasterEnlevementErreurQuantite();
    }
  }
}
