import { Component, OnInit } from '@angular/core';
import {StocksService} from '../../stocks/shared/stocks.service';
import {FonctionsService} from '../../fonctions/shared/fonctions.service';
import {MaisonService} from '../../maison/shared/maison.service';
import {EtapeService} from '../../etapeconstruction/shared/etape.service';
import {ArticleService} from '../../article/Shared/article.service';
import {LotService} from '../../lot/shared/lot.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {ChantierService} from '../../chantier/shared/chantier.service';
import {Router} from '@angular/router';
import {BesoinjournalierService} from '../../besoinjournalier/shared/besoinjournalier.service';
import {DatePipe} from '@angular/common';
import {FormBuilder} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {DateAdapter} from '@angular/material/core';

@Component({
  selector: 'app-enlevementmaison',
  templateUrl: './enlevementmaison.component.html',
  styleUrls: ['./enlevementmaison.component.css']
})
export class EnlevementmaisonComponent implements OnInit {
  headElement = ['N°', 'Fonction', 'Maison', 'Article', 'Quantité', 'Etape avancement', 'Action'];
  // tslint:disable-next-line:max-line-length
  constructor(public ApiStock: StocksService, public fonctionApi: FonctionsService, public ApiMaison: MaisonService, public ApiEtape: EtapeService, public ApiArticle: ArticleService, public dialog: MatDialog, public ApiLot: LotService, private SpinnerService: NgxSpinnerService, private dateAdapter: DateAdapter<Date>, private notifyService: NotificationService, private toastr: ToastrService, private chantierService: ChantierService, private router: Router, public ApiBesoin: BesoinjournalierService, public articleService: ArticleService, private datePipe: DatePipe, private _adapter: DateAdapter<any>, private _formBuilder: FormBuilder) { }

  ngOnInit() {
  }

}
