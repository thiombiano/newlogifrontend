import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CommandeService} from '../../commandes/shared/commande.service';
import {ReceptionService} from '../shared/reception.service';
import {ArticleService} from '../../article/Shared/article.service';

@Component({
  selector: 'app-voirunereception',
  templateUrl: './voirunereception.component.html',
  styleUrls: ['./voirunereception.component.css']
})
export class VoirunereceptionComponent implements OnInit {

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              public commandeService: CommandeService,
              private articleService: ArticleService,
              public receptionService: ReceptionService) { }
  id: number;
  firstlevel: string;
  secondlevel: string;

  ngOnInit( ) {
    this.firstlevel = 'Reception';
    this.secondlevel = 'Détail réception';
    const idreception = window.localStorage.getItem('IDreception');
    console.log('ReceptId: ' + this.id);
    this.receptionService.getSelectedReception(+idreception);
  }


  printComponent(cmpName) {
    const printContents = document.getElementById(cmpName).innerHTML;
    const originalContents = document.body.innerHTML;

    document.body.innerHTML = printContents;

    window.print();

    document.body.innerHTML = originalContents;
  }

}
