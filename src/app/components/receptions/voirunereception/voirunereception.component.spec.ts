import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoirunereceptionComponent } from './voirunereception.component';

describe('VoirunereceptionComponent', () => {
  let component: VoirunereceptionComponent;
  let fixture: ComponentFixture<VoirunereceptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoirunereceptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoirunereceptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
