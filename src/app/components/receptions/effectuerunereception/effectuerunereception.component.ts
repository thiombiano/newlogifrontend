import { Component, OnInit } from '@angular/core';
import {DetailsReception} from '../shared/reception';
import {ActivatedRoute, Router} from '@angular/router';
import {CommandeService} from '../../commandes/shared/commande.service';
import {ReceptionService} from '../shared/reception.service';
import {ArticleService} from '../../article/Shared/article.service';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {count, first} from 'rxjs/operators';
import {NgxSpinnerService} from 'ngx-spinner';
import {AdddetailComponent} from '../../besoinjournalier/adddetail/adddetail.component';
import {Besoinjournalier} from '../../besoinjournalier/shared/besoinjournalier.model';
import {StocksService} from '../../stocks/shared/stocks.service';
import {FonctionsService} from '../../fonctions/shared/fonctions.service';
import {MaisonService} from '../../maison/shared/maison.service';
import {EtapeService} from '../../etapeconstruction/shared/etape.service';
import {MatDialog} from '@angular/material/dialog';
import {Maison} from '../../maison/shared/maison.model';
import {Article} from '../../article/Shared/article';
import {Fonction} from '../../fonctions/shared/fonction';
import {Etape} from '../../etapeconstruction/shared/etape.model';
import {Detailenlevement, Enlevement} from '../../enlevement/shared/detailenlevement';
import {LotService} from '../../lot/shared/lot.service';
import {AdddetailenlevmentComponent} from '../enlevementmaison/adddetailenlevment/adddetailenlevment.component';
import {EnlevementService} from '../../enlevement/shared/enlevement.service';
import {DetailArticleprefat, Prefat} from '../shared/articletoprefat.interface';
import {AdddetailprefatComponent} from '../enlevementprefat/adddetailprefat/adddetailprefat.component';
import {MatStepper} from '@angular/material';
import {AdddetailtoprefatComponent} from '../../besoinjournalier/adddetailtoprefat/adddetailtoprefat.component';
import {PrefatService} from '../../prefat/shared/prefat.service';
import {Commande} from '../../commandes/shared/commande';
import {Section} from "../../section/shared/section.model";
import {SectionService} from "../../section/shared/section.service";

// tslint:disable-next-line:class-name
class typereception {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-effectuerunereception',
  templateUrl: './effectuerunereception.component.html',
  styleUrls: ['./effectuerunereception.component.css']
})
export class EffectuerunereceptionComponent implements OnInit {
  typereceptions: typereception[] = [
    {value: 'Reception totale', viewValue: 'Réception totale'},
    {value: 'Reception partielle', viewValue: 'Réception partielle'},
    {value: 'Reception partielle finale', viewValue: 'Réception partielle finale'},
    {value: 'Reception non conforme', viewValue: 'Réception non conforme'}
  ];

  nextLabel = 'Suivant';
  previousLabel = 'Précédent';
  maxSize = 9;
  directionLinks = true;
  autoHide = false;
  responsive = true;
  screenReaderPaginationLabel = 'Pagination';
  screenReaderPageLabel = 'page';
  screenReaderCurrentLabel = 'Vous êtes sur cette page';
  public page: number;
  public isNull: string;
  public pageSize: number;
  p = 1;
  count = 10;
  searchText;
  isBLokForValidation: boolean;
  numeroReception: string;
  dateRC: Date;
  firstlevel: string;
  secondlevel: string;
  numeroPieceFournisseur: string;
  commentaires: string;
  thedate: Date;
  isReceptionne: boolean;
  quantiteactuel: number;
  public prefats: Prefat[];
  livreur: string;
  immatriculation: string;
  motifpasimmatriculation: string;
  telephoneLivreur: string;
  id: number;
  thearticle: string;
  sections: Section[] = null;
  article: number;
  unite: string;
  quantite: number;
  valeurreception: string;
  iscanceled: boolean;
  prixUnitaire: number;
  showModifierDetail: boolean;
  maisons: Maison[];
  articles: Article[];
  boolrecept: boolean;
  fonctions: Fonction[];
  touslesetapes: Etape[];
  libellemaison: string;
  libelleetape: string;
  libellearticle: string;
  // @ts-ignore
  enlevementdetail: Enlevement = new Detailenlevement();
  // @ts-ignore
  articletoprefat: Prefat =  new DetailArticleprefat();
  // @ts-ignore
  enlevementdetailforprefat: Enlevement = new Detailenlevement();
  aticlesLivres = new Array<DetailsReception>();
  curDate = new Date();
  detailsReceptionToUpdate: DetailsReception;
  idchantier: number;
  getreceptionstate: boolean;
  selectedtypereception: string;
  imIsIn: boolean;
  imIsNotIn: boolean;
  canceledcommentaire: string;
  toogleischeckedbydefaut: boolean;
  // tslint:disable-next-line:max-line-length
  headElement = ['N°', 'Fonction', 'Maison', 'Article', 'Qte stck théorique', 'Quantité', 'Quantité stck théorique restante', 'Etape avancement', 'Action'];
  // tslint:disable-next-line:max-line-length
  headElementArtcile = ['N°', 'Fonction', 'Préfat', 'Article', 'Qte stck théorique', 'Quantité', 'Quantité stck théorique restante', 'Action'];
  selected: any =  'receptionuniquement';
  ComIsCanceled: boolean;
  // tslint:disable-next-line:max-line-length
  IsForEnlevementToHouse: boolean;
  IsForEnlevementToPrefat: boolean;
  // tslint:disable-next-line:max-line-length
  constructor(public ApiSection: SectionService, public ApiCommande: CommandeService, public ApiPrefat: PrefatService, public ApiEnlevement: EnlevementService, public ApiLot: LotService, public dialog: MatDialog, public ApiStock: StocksService, public fonctionApi: FonctionsService, public ApiMaison: MaisonService, public ApiEtape: EtapeService, private SpinnerService: NgxSpinnerService, private notifyService: NotificationService, private toastr: ToastrService, private router: Router, private activatedRoute: ActivatedRoute, public commandeService: CommandeService, private articleService: ArticleService, private receptionService: ReceptionService) { }

  ngOnInit() {
    this.IsForEnlevementToHouse = false;
    this.isBLokForValidation = true;
    this.toogleischeckedbydefaut = true;
    this.iscanceled = false;
    this.imIsIn = true;
    this.imIsNotIn = false;
    this.ComIsCanceled = false;
    this.selectedtypereception = 'Aucune';
    this.getreceptionstate = false;
    this.secondlevel = 'Commande';
    this.firstlevel = 'Réception';
    const idcom =   window.localStorage.getItem('IDCom');
    console.log('id:' + idcom);
    this.commandeService.getSelectedCommande(+idcom);
    this.articleService.getArticleList();
    this.ChargeLaListeArticleCommande(+idcom);
    //
    // for (const articleLivre of this.commandeService.selectedCommande.articles) {
    //   if (articleLivre.reliquat === 0) {
    //     this.isReceptionne = true;
    //   } else {
    //     this.isReceptionne = false;
    //     break;
    //   }
    // }
    console.log(this.isReceptionne);

    this.enlevementdetail = {
      besoin_journalier_id: null,
      numero_En: null,
      nomtechnicien: null,
      typeenlevement: null,
      nommagasinier: null,
      date_En: null,
      statut_En: null,
      livreur_id: null,
      nomlivreur: null,
      prenomlivreur: null,
      chantier_id: null,
      telephonemagasinier: null,
      telephonetechnicien: null,
      telephonelivreur: null,
      magasinier_id: null,
      articles: new Array<Detailenlevement>(),
      id: null
    };

    this.enlevementdetailforprefat = {
      besoin_journalier_id: null,
      numero_En: null,
      nomtechnicien: null,
      typeenlevement: null,
      nommagasinier: null,
      date_En: null,
      statut_En: null,
      livreur_id: null,
      nomlivreur: null,
      prenomlivreur: null,
      chantier_id: null,
      telephonemagasinier: null,
      telephonetechnicien: null,
      telephonelivreur: null,
      magasinier_id: null,
      articles: new Array<Detailenlevement>(),
      id: null
    };


    this.articletoprefat = {
      articles : new Array<DetailArticleprefat>(),
      id: null,
    };

    const ChantierId = window.localStorage.getItem('Idchantier');
    this.getAllMaisonByChantier(+ChantierId);
    this.ListSectionOfChantier(+ChantierId);
    this.getAllPrefat(+ChantierId);
  }


  ChargeLaListeArticleCommande(idcom: number) {
    this.aticlesLivres = [];
    this.ApiCommande.getSelectedCommandeKeppFocus(idcom).subscribe((data) => {
      console.log('liste commande : ' + JSON.stringify(data.data.articles) );
      if (data.data.articles) {
        for (const articleLivre of data.data.articles) {
          console.log('quantite: ' + articleLivre.reliquat);
          // @ts-ignore
          this.aticlesLivres.push({
            article: articleLivre.article,
            unite: articleLivre.unite,
            quantite: articleLivre.quantite,
            reference: articleLivre.reference,
            article_id: articleLivre.article_id,
            quantite_receptionnee: articleLivre.reliquat,
            commande_id: +idcom,
            commentaires: ''
          });

          console.log(articleLivre.reliquat);
          // @ts-ignore

        }
      }
    });
  }

  radioChange(event) {
    const radiovalue = event.value;
    console.log(radiovalue);
    if (radiovalue === '1') {
      this.imIsIn = true;
      this.imIsNotIn = false;
    } else {
      this.imIsIn = false;
      this.imIsNotIn = true;
    }
  }

  getAllPrefat(idchantier: number) {
    this.ApiPrefat.getPrefatList(+idchantier).subscribe((prefat: Prefat[]) => {
      this.prefats = prefat;
    });
  }

  GetReceptionState(value) {
    const idcomm =   window.localStorage.getItem('IDCom');
    this.ChargeLaListeArticleCommande(+idcomm);
    console.log(value);
    if (value === 'Reception partielle' || value === 'Reception partielle définitive' || value === 'Reception partielle finale') {
      this.getreceptionstate = true;
      this.iscanceled = false;
      this.showModifierDetail = false;
    } else {
      this.getreceptionstate = false;
    }
    if (value === 'Reception totale') {
      this.iscanceled = false;
      this.showModifierDetail = false;
    }
    if (value === undefined) {
      this.selectedtypereception = 'Aucune';
      this.showModifierDetail = false;
    }
    if (value === 'Reception non conforme') {
      this.iscanceled = true;
      this.showModifierDetail = false;
    }
    this.valeurreception = value;
  }

  showModificationDetailReception(detailReception: DetailsReception) {
    this.thearticle = detailReception.article;
    this.article = detailReception.article_id;
    this.quantite = detailReception.quantite_receptionnee;
    this.quantiteactuel = detailReception.quantite_receptionnee;
    this.showModifierDetail = true;
    console.log('article: ' + detailReception.article_id);
  }

  modifierLaLigneDetail() {

   if ( this.quantite > 0 && this.quantite <= this.aticlesLivres.find(art => art.article_id === this.article).quantite) {
     this.aticlesLivres.find(
       art => art.article_id === this.article
     ).quantite_receptionnee = this.quantite;
     this.showModifierDetail = false;
    } else {
      this.showToasterErreurModifier();
    }
  }

  showToasterErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifiez les quantités !!', 'Notification', 5000);
  }

  showToasterReceptionAjoute() {
    this.notifyService.showSuccessWithTimeout('Réception effectuée avec succès!!', 'Notification', 5000);
  }

  showToasterCommandeErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Veuillez renseigner toutes les infos. !!', 'Notification', 5000);
  }

  showToasterCommandeErreurModifieIMr() {
    // tslint:disable-next-line:max-line-length
    this.notifyService.showErrorWithTimeout('Echec: Veuillez renseigner les informations dans la rubrique immatriculation !!', 'Notification', 5000);
  }

  showToasterCommandeErreurCOM() {
    // tslint:disable-next-line:max-line-length
    this.notifyService.showErrorWithTimeout('Echec: Veuillez renseigner le champ de non conformité !!', 'Notification', 5000);
  }

  showToasterReceptionPartielle() {
    // tslint:disable-next-line:max-line-length
    this.notifyService.showErrorWithTimeout('Echec: Veuillez modifier les quantités pour une réception partielle !!', 'Notification', 5000);
  }

  showToasterReceptionTotale() {
    // tslint:disable-next-line:max-line-length
    this.notifyService.showErrorWithTimeout('Echec: Veuillez enregistré cette réception en réception partielle !!', 'Notification', 5000);
  }

  showToasterRErreurQuantite() {
    // tslint:disable-next-line:max-line-length
    this.notifyService.showErrorWithTimeout('Echec: Quantité discordante, Veuillez vérifier vos saisies de quantité!!', 'Notification', 5000);
  }

  showToasterRErreurQuantiteF() {
    // tslint:disable-next-line:max-line-length
    this.notifyService.showErrorWithTimeout('Echec: ta geule!!', 'Notification', 5000);
  }

  enregistrerReception(stepper: MatStepper) {

    const thedateRC = this.dateRC;
    const thenumeroPiece = this.numeroPieceFournisseur;
    const thelivreur = this.livreur;
    const thetelephonelivreur = this.telephoneLivreur;
    this.clearEnlevementDetail();
    this.clearEnlevementDetailForPrefat();
    this.selected = 'receptionuniquement';
    // tslint:disable-next-line:prefer-for-of
    // @ts-ignore
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.aticlesLivres.length; i++) {
      // tslint:disable-next-line:max-line-length
      if (this.aticlesLivres[i].quantite_receptionnee  !== 0) {
      if (this.aticlesLivres[i].quantite === this.aticlesLivres[i].quantite_receptionnee) {
        // @ts-ignore
        this.boolrecept = true;
      } else {
        // @ts-ignore
        this.boolrecept = false;
        break;
      }
      }
    }

    if (this.immatriculation === undefined && this.motifpasimmatriculation === undefined) {
      this.showToasterCommandeErreurModifieIMr();
      return;
    }
    if (this.valeurreception === 'Reception non conforme' && this.canceledcommentaire === undefined) {
      this.showToasterCommandeErreurCOM();
      return;
    }
    if (this.boolrecept === true && this.valeurreception === 'Reception partielle') {
      this.showToasterReceptionPartielle();
      return;
    }
    if (this.boolrecept === true && this.valeurreception === 'Reception partielle finale') {
      this.showToasterReceptionPartielle();
      return;
    }

    if (this.boolrecept === false && this.valeurreception === 'Reception totale') {
      this.showToasterReceptionTotale();
      return;
    }

    // if (this.boolrecept === false && this.valeurreception === 'Reception partielle finale') {
    //   this.showToasterReceptionTotale();
    //   return;
    // }

    // @ts-ignore
    if (thenumeroPiece  != null  && thelivreur  != null  && thetelephonelivreur  != null ) {
      const idcom =   window.localStorage.getItem('IDCom');
      console.log('id:' + idcom);
      const id = window.localStorage.getItem('Idchantier');
      this.idchantier = +id;
      console.log('id:' + id);

      // tslint:disable-next-line:max-line-length
      if (this.articletoprefat.articles.length > 0  && this.IsForEnlevementToHouse === false && this.IsForEnlevementToPrefat === true && this.valeurreception !== 'Reception non conforme') {
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < this.articletoprefat.articles.length; i++) {
          // tslint:disable-next-line:prefer-for-of
          for (let j = 0; j < this.aticlesLivres.length; j++) {
            if (this.articletoprefat.articles[i].article === this.aticlesLivres[j].article) {
              if (this.articletoprefat.articles[i].quantite > this.aticlesLivres[j].quantite_receptionnee) {
                this.showToasterRErreurQuantite();
                return;
              }
            }
          }
        }
      }

      // tslint:disable-next-line:max-line-length
      if (this.enlevementdetail.articles.length > 0  && this.IsForEnlevementToHouse === true && this.IsForEnlevementToPrefat === false && this.valeurreception !== 'Reception non conforme') {
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < this.enlevementdetail.articles.length; i++) {
          // tslint:disable-next-line:prefer-for-of
          for (let j = 0; j < this.aticlesLivres.length; j++) {
            if (this.enlevementdetail.articles[i].article === this.aticlesLivres[j].article) {
              if (this.enlevementdetail.articles[i].quantiteASortir > this.aticlesLivres[j].quantite_receptionnee) {
                this.showToasterRErreurQuantite();
                return;
              }
            }
          }
        }
      }
      stepper.next();
    } else {
      this.showToasterCommandeErreurModifier();
    }



  }

  doEnlevementDirectToHome() {
    // tslint:disable-next-line:max-line-length
    if (this.enlevementdetail.articles.length > 0  && this.IsForEnlevementToHouse === true && this.IsForEnlevementToPrefat === false && this.valeurreception !== 'Reception non conforme') {
      const idchantier =  window.localStorage.getItem('Idchantier');
      const idutilisateur =  window.localStorage.getItem('idutilisateur');
      this.enlevementdetail.chantier_id = +idchantier;
      this.enlevementdetail.magasinier_id = +idutilisateur;
      this.enlevementdetail.besoin_journalier_id = null;
      this.ApiEnlevement.addChantierEnlevementDirectOnHome(+idchantier,  this.enlevementdetail).subscribe((enlevement: Enlevement) => {
        console.log(enlevement);
      });
    }
  }

  doEnlevementDirectToPrefat() {
    // tslint:disable-next-line:max-line-length
    if (this.enlevementdetailforprefat.articles.length > 0  && this.IsForEnlevementToHouse === false && this.IsForEnlevementToPrefat === true && this.valeurreception !== 'Reception non conforme') {
      const idchantier =  window.localStorage.getItem('Idchantier');
      const idutilisateur =  window.localStorage.getItem('idutilisateur');
      this.enlevementdetailforprefat.chantier_id = +idchantier;
      this.enlevementdetailforprefat.magasinier_id = +idutilisateur;
      this.enlevementdetail.besoin_journalier_id = null;
      // tslint:disable-next-line:max-line-length
      this.ApiEnlevement.addChantierEnlevementDirectOnPrefat(+idchantier,  this.enlevementdetailforprefat).subscribe((enlevement: Enlevement) => {
        console.log(enlevement);
      });
    }
  }

  // doEnlevementDirectToPrefat() {
  //   // tslint:disable-next-line:max-line-length
  // tslint:disable-next-line:max-line-length
  //   if (this.articletoprefat.articles.length > 0  && this.IsForEnlevementToHouse === false && this.IsForEnlevementToPrefat === true && this.valeurreception !== 'Reception non conforme') {
  //     const idchantier =  window.localStorage.getItem('Idchantier');
  //     const idutilisateur =  window.localStorage.getItem('idutilisateur');
  //     const formData = new FormData();
  //     formData.append('articles', JSON.stringify(this.articletoprefat));
  //     console.log('Form data : ' + formData.get('articles'));
  //     this.ApiEnlevement.addChantierEnlevementDirectOnPrefat(+idchantier,  this.articletoprefat).subscribe((data) => {
  //       console.log(data);
  //     });
  //   }
  // }



  getAllMaisonByChantier(idchantier: number) {
    this.ApiLot.getALLMaisonByChantier(+idchantier).subscribe((data) => {
      console.log(data.data);
      this.maisons = data.data;
    });
  }


  showToasterEnlevementErreurQuantite() {
    this.notifyService.showErrorWithTimeout('Erreur: Quantité inssufisante !!', 'Notification', 5000);
  }


  getLibelleOfMaisonEtapeArticle(idarticle: number, idmaison: number, idetape: number) {
    this.articleService.getArticle(+idarticle).subscribe((article: Article) => {
      this.libellearticle = article.libelle_article;
    });

    this.ApiMaison.getMaisonByIdInBesoin(+idmaison).subscribe((maison: Maison) => {
      this.libellemaison = maison.libellemaison;
    });

    this.ApiEtape.getEtape(+idetape).subscribe((etape: Etape) => {
      this.libelleetape = etape.libelleetape;
    });

  }


  ListSectionOfChantier(id: number) {
    this.ApiSection.getSectionList(+id).subscribe((section: Section[]) => {
      this.sections = section;
      console.log(section);
    });
  }

  addNewDetail() {
    console.log('les articles livrées : ' + JSON.stringify(this.aticlesLivres) );
    // tslint:disable-next-line:max-line-length
    const dialogRef = this.dialog.open(AdddetailenlevmentComponent, {
      data: {
        besoin: Enlevement,
        getallmaisons: this.maisons,
        getallsections: this.sections,
        getallarticles: this.aticlesLivres
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1 && dialogRef.componentInstance.thebesoin) {
        const idmaison = JSON.stringify(dialogRef.componentInstance.thebesoin.maison_id);
        const idetape = JSON.stringify(dialogRef.componentInstance.thebesoin.etape_construction_id);
        const idarticle = JSON.stringify(dialogRef.componentInstance.thebesoin.article_id);
        this.getLibelleOfMaisonEtapeArticle(+idarticle, +idmaison, +idetape);
        console.log('la maison : ' + this.libellemaison);
        console.log('la result : ' + JSON.stringify(dialogRef.componentInstance.thebesoin));
        const idchantier = window.localStorage.getItem('Idchantier');
        const lidarticle = dialogRef.componentInstance.thebesoin.article_id;
        const quantiteBesoin = dialogRef.componentInstance.thebesoin.quantiteBesoin;
        const article_id = dialogRef.componentInstance.thebesoin.article_id;
        const libellemaison = dialogRef.componentInstance.thebesoin.libellemaison;
        const libelleetape = dialogRef.componentInstance.thebesoin.libelleetape;
        const LquantiteBesoin = dialogRef.componentInstance.thebesoin.quantiteBesoin;
        const commentaires = '';
        const Larticle = dialogRef.componentInstance.thebesoin.article;
        const etape_construction_id = dialogRef.componentInstance.thebesoin.etape_construction_id;
        const maison_id = dialogRef.componentInstance.thebesoin.maison_id;
        const prefat_id = dialogRef.componentInstance.thebesoin.prefat_id;
        const fonction_id = dialogRef.componentInstance.thebesoin.fonction_id;
        const libellefonction = dialogRef.componentInstance.thebesoin.libellefonction;
        const libelleprefat = dialogRef.componentInstance.thebesoin.libelleprefat;
        let quantitevirtuelarticle = 0;
        let quantitevirtuelarticleTOTAL = 0;
        this.ApiStock.getQuantiteDisponible(+idchantier, +lidarticle).subscribe((data) => {
          // tslint:disable-next-line:max-line-length
          console.log('la quantité disponible pour cet article est : ' + (+data + this.aticlesLivres.find(art => art.article_id === +lidarticle).quantite_receptionnee));
          // tslint:disable-next-line:max-line-length
          quantitevirtuelarticle = (+data + this.aticlesLivres.find(art => art.article_id === +lidarticle).quantite_receptionnee);
          if (quantiteBesoin > (+data + quantitevirtuelarticle)) {
            quantitevirtuelarticleTOTAL = (+data + quantitevirtuelarticle);
            this.showToasterEnlevementErreurQuantite();
            return;
          }
          this.enlevementdetail.articles.push({
            id: 0,
            article: Larticle,
            etape_construction_id,
            article_id,
            prefat_id,
            quantiteBesoin: LquantiteBesoin,
            libelleetape,
            libellemaison,
            quantiteASortir: LquantiteBesoin,
            maison_id,
            libelleprefat,
            checked: null,
            reliquat: LquantiteBesoin,
            commentaires: null,
            fonction_id,
            libellefonction,
            nomprestataire: null,
            prenomprestataire: null,
            besoin_jitems_id: null,
            quantitevirtuel: quantitevirtuelarticle,
          });
        });
        this.isBLokForValidation = true;
      }
    });
  }


  addNewDetailToPrefat() {
    // tslint:disable-next-line:max-line-length
    const dialogRef = this.dialog.open(AdddetailprefatComponent, {
      data: {
        besoin: Enlevement,
        getallprefats: this.prefats,
        getallarticles: this.aticlesLivres
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        const idprefat = JSON.stringify(dialogRef.componentInstance.thebesoinofprefat.prefat_id);
        const idarticle = JSON.stringify(dialogRef.componentInstance.thebesoinofprefat.article_id);
        const idchantier =  window.localStorage.getItem('Idchantier');
        const lidarticle = dialogRef.componentInstance.thebesoinofprefat.article_id;
        const quantiteBesoin = dialogRef.componentInstance.thebesoinofprefat.quantiteBesoin;

        const article_id = dialogRef.componentInstance.thebesoinofprefat.article_id;
        const libelleprefat = dialogRef.componentInstance.thebesoinofprefat.libelleprefat;
        const LquantiteBesoin = dialogRef.componentInstance.thebesoinofprefat.quantiteBesoin;
        const commentaires = '';
        const Larticle = dialogRef.componentInstance.thebesoinofprefat.article;
        const prefat_id = dialogRef.componentInstance.thebesoinofprefat.prefat_id;
        const fonction_id = dialogRef.componentInstance.thebesoinofprefat.fonction_id;
        const libellefonction =  dialogRef.componentInstance.thebesoinofprefat.libellefonction;

        this.ApiStock.getQuantiteDisponible(+idchantier, +lidarticle).subscribe((data) => {
          // tslint:disable-next-line:max-line-length
          console.log('la quantité disponible pour cet article est : ' + (+data +  this.aticlesLivres.find(art => art.article_id  === +lidarticle).quantite_receptionnee));
          const quantitevirtuelarticle = (+data +  this.aticlesLivres.find(art => art.article_id  === +lidarticle).quantite_receptionnee);
          if (quantiteBesoin > (+data +  quantitevirtuelarticle) ) {
            this.showToasterEnlevementErreurQuantite();
            return;
          }
          this.enlevementdetailforprefat.articles.push({
            id: 0,
            article: Larticle,
            etape_construction_id: null,
            article_id,
            prefat_id,
            quantiteBesoin: LquantiteBesoin,
            libelleetape: null,
            libellemaison: null,
            quantiteASortir: LquantiteBesoin,
            maison_id: null,
            libelleprefat,
            checked: null,
            reliquat: LquantiteBesoin,
            commentaires: null,
            fonction_id,
            libellefonction,
            nomprestataire: null,
            prenomprestataire: null,
            besoin_jitems_id: null,
            quantitevirtuel: quantitevirtuelarticle
          });
        });
        this.isBLokForValidation = true;
      }
    });
  }

  clearEnlevementDetail() {
    this.enlevementdetail = undefined;
    this.enlevementdetail = {
      besoin_journalier_id: null,
      nomtechnicien: null,
      nommagasinier: null,
      typeenlevement: null,
      numero_En: null,
      date_En: null,
      statut_En: null,
      chantier_id: null,
      telephonemagasinier: null,
      telephonetechnicien: null,
      magasinier_id: null,
      livreur_id: null,
      nomlivreur: null,
      prenomlivreur: null,
      telephonelivreur: null,
      articles: new Array<Detailenlevement>(),
      id: null
    };
  }


  clearEnlevementDetailForPrefat() {
    this.enlevementdetailforprefat = undefined;
    this.enlevementdetailforprefat = {
      besoin_journalier_id: null,
      nomtechnicien: null,
      nommagasinier: null,
      typeenlevement: null,
      numero_En: null,
      date_En: null,
      statut_En: null,
      chantier_id: null,
      telephonemagasinier: null,
      telephonetechnicien: null,
      magasinier_id: null,
      livreur_id: null,
      nomlivreur: null,
      prenomlivreur: null,
      telephonelivreur: null,
      articles: new Array<Detailenlevement>(),
      id: null
    };
  }

  supprimerLaLigneDetail(lignebesoin) {
    const index  =  this.enlevementdetail.articles.indexOf(lignebesoin);
    if (index !== -1) {
      this.enlevementdetail.articles.splice(index, 1);
    }

    if (this.enlevementdetail.articles.length === 0) {
      this.isBLokForValidation = false;
    }
  }

  getToogleForReception(event) {
    console.log(event.value);
    this.clearEnlevementDetail();
    this.clearEnlevementDetailForPrefat();
    if (event.value === 'enlevementversmaison') {
      this.IsForEnlevementToHouse = true;
      this.isBLokForValidation = false;
      this.IsForEnlevementToPrefat = false;
    } else if (event.value === 'enlevementverslaprefat') {
      this.IsForEnlevementToPrefat = true;
      this.IsForEnlevementToHouse = false;
      this.isBLokForValidation = false;
    } else {
      this.clearEnlevementDetail();
      this.clearEnlevementDetailForPrefat();
      this.isBLokForValidation = true;
      this.IsForEnlevementToHouse = false;
      this.IsForEnlevementToPrefat = false;
    }
  }

  supprimerLaLigneDetailPrefat(ligneprefat) {
    const index  =  this.articletoprefat.articles.indexOf(ligneprefat);
    if (index !== -1) {
      this.articletoprefat.articles.splice(index, 1);
    }

    if (this.articletoprefat.articles.length === 0) {
      this.isBLokForValidation = false;
    }
  }

  EnregistrerLeBondeReception() {
    const thedateRC = this.dateRC;
    const thenumeroPiece = this.numeroPieceFournisseur;
    const thelivreur = this.livreur;
    const thetelephonelivreur = this.telephoneLivreur;
    // tslint:disable-next-line:prefer-for-of
    // @ts-ignore
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.aticlesLivres.length; i++) {
      // tslint:disable-next-line:max-line-length
      if (this.aticlesLivres[i].quantite_receptionnee  !== 0) {
        if (this.aticlesLivres[i].quantite === this.aticlesLivres[i].quantite_receptionnee) {
          // @ts-ignore
          this.boolrecept = true;
        } else {
          // @ts-ignore
          this.boolrecept = false;
          break;
        }
      }
    }

    if (this.immatriculation === undefined && this.motifpasimmatriculation === undefined) {
      this.showToasterCommandeErreurModifieIMr();
      return;
    }
    if (this.valeurreception === 'Reception non conforme' && this.canceledcommentaire === undefined) {
      this.showToasterCommandeErreurCOM();
      return;
    }
    if (this.boolrecept === true && this.valeurreception === 'Reception partielle') {
      this.showToasterReceptionPartielle();
      return;
    }
    if (this.boolrecept === true && this.valeurreception === 'Reception partielle finale') {
      this.showToasterReceptionPartielle();
      return;
    }

    if (this.boolrecept === false && this.valeurreception === 'Reception totale') {
      this.showToasterReceptionTotale();
      return;
    }

    // if (this.boolrecept === false && this.valeurreception === 'Reception partielle finale') {
    //   this.showToasterReceptionTotale();
    //   return;
    // }

    // @ts-ignore
    if (thenumeroPiece  != null  && thelivreur  != null  && thetelephonelivreur  != null ) {
      const idcom =   window.localStorage.getItem('IDCom');
      console.log('id:' + idcom);
      const id = window.localStorage.getItem('Idchantier');
      this.idchantier = +id;
      console.log('id:' + id);

      // tslint:disable-next-line:max-line-length
      if (this.enlevementdetailforprefat.articles.length > 0  && this.IsForEnlevementToHouse === false && this.IsForEnlevementToPrefat === true && this.valeurreception !== 'Reception non conforme') {
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < this.enlevementdetailforprefat.articles.length; i++) {
          // tslint:disable-next-line:prefer-for-of
          for (let j = 0; j < this.aticlesLivres.length; j++) {
            if (this.enlevementdetailforprefat.articles[i].article === this.aticlesLivres[j].article) {
              if (this.enlevementdetailforprefat.articles[i].quantiteASortir > this.aticlesLivres[j].quantite_receptionnee) {
                this.showToasterRErreurQuantite();
                return;
              }
            }
          }
        }
      }

      // tslint:disable-next-line:max-line-length
      if (this.enlevementdetail.articles.length > 0  && this.IsForEnlevementToHouse === true && this.IsForEnlevementToPrefat === false && this.valeurreception !== 'Reception non conforme') {
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < this.enlevementdetail.articles.length; i++) {
          // tslint:disable-next-line:prefer-for-of
          for (let j = 0; j < this.aticlesLivres.length; j++) {
            if (this.enlevementdetail.articles[i].article === this.aticlesLivres[j].article) {
              if (this.enlevementdetail.articles[i].quantiteASortir > this.aticlesLivres[j].quantite_receptionnee) {
                this.showToasterRErreurQuantite();
                return;
              }
            }
          }
        }
      }
      this.SpinnerService.show();
      this.receptionService.addCommandeReception(+idcom, {
        chantier_id: this.idchantier,
        numero_RC: this.numeroReception,
        date_RC: this.dateRC,
        commentaires: this.commentaires,
        statut_RC: 1,
        numero_piece_fournisseur: this.numeroPieceFournisseur,
        fournisseur: this.commandeService.selectedCommande.fournisseur,
        livreur: this.livreur,
        typedereception: this.valeurreception,
        canceledcommentaire: this.canceledcommentaire,
        isCanceled: this.iscanceled,
        immatriculation: this.immatriculation,
        telephone_livreur: this.telephoneLivreur,
        commande_id: this.commandeService.selectedCommande.id,
        fournisseur_id: this.commandeService.selectedCommande.fournisseur_id,
        articles: this.aticlesLivres,
        isValidateForReceptionPFinalle: false
      }).subscribe(e => {
        console.log(e);
        this.doEnlevementDirectToHome();
        this.doEnlevementDirectToPrefat();
        this.router.navigate(['/start/receptions']);
        this.SpinnerService.hide();
        this.showToasterReceptionAjoute(); });
    } else {
      this.showToasterCommandeErreurModifier();
    }
  }

  goBack(stepper: MatStepper) {
    this.clearEnlevementDetail();
    this.clearEnlevementDetailForPrefat();
    this.selected = undefined;
    this.IsForEnlevementToHouse = false;
    this.IsForEnlevementToPrefat = false;
    stepper.previous();
  }

  showToasterAnnulerBonModifier() {
    this.notifyService.showSuccessWithTimeout('Bon de commande annuler avec succès !!', 'Notification', 5000);
  }

  AnnulerBondeReception(idcommande: number) {
    if (this.valeurreception === 'Reception non conforme' && this.canceledcommentaire === undefined) {
      this.showToasterCommandeErreurCOM();
      return;
    }
    const formData = new FormData();
    formData.append('canceledcommentaire', this.canceledcommentaire);
    this.commandeService.updateAnnulerChantierCommande(+idcommande, formData)
      .pipe(first())
      .subscribe((commande: Commande) => {
        // @ts-ignore
        console.log(commande);
        this.showToasterAnnulerBonModifier();
        this.router.navigate(['start/commandes']);
      });
  }
}
