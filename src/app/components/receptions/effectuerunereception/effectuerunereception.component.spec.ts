import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EffectuerunereceptionComponent } from './effectuerunereception.component';

describe('EffectuerunereceptionComponent', () => {
  let component: EffectuerunereceptionComponent;
  let fixture: ComponentFixture<EffectuerunereceptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EffectuerunereceptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EffectuerunereceptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
