import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {ArticleService} from '../../article/Shared/article.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PermissionService} from '../shared/permission.service';
import {Permission} from '../shared/permission.model';
import {Article} from '../../article/Shared/article';

@Component({
  selector: 'app-ajouterpermission',
  templateUrl: './ajouterpermission.component.html',
  styleUrls: ['./ajouterpermission.component.css']
})
export class AjouterpermissionComponent implements OnInit {

  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, private toastr: ToastrService, private cdRef: ChangeDetectorRef, public ApiPermission: PermissionService, private router: Router, private formBuilder: FormBuilder) { }
  firstlevel: string;
  secondlevel: string;
  addForm: FormGroup;
  submitted = false;
  permission: Permission;
  ngOnInit() {
    this.firstlevel = 'Permission';
    this.secondlevel = 'Ajouter permission';
    this.addForm = this.formBuilder.group({
      libellepermission: ['', Validators.required],
      descriptionpermission: [''],
    });
  }

  showToasterPermissionEnregistrer() {
    this.notifyService.showSuccessWithTimeout('Permission enregistrée avec succès !!', 'Notification', 5000);
  }

  showToasterPermissionErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }

  onSubmit() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.addForm.invalid) {
      this.showToasterPermissionErreurModifier();
      return;
    }

    this.ApiPermission.addPermission(this.addForm.value).subscribe((permission: Permission) => {
      this.router.navigate(['start/permissions']);
      this.showToasterPermissionEnregistrer();
    });

  }

}
