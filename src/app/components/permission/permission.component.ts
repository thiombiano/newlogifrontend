import {ChangeDetectorRef, Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {MdbTableDirective, MdbTablePaginationComponent} from 'angular-bootstrap-md';
import {Permission} from './shared/permission.model';
import {NotificationService} from '../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {PermissionService} from './shared/permission.service';


@Component({
  selector: 'app-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.css']
})
export class PermissionComponent implements OnInit {
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  permissions: Permission[];
  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, private toastr: ToastrService, private cdRef: ChangeDetectorRef, public ApiPermission: PermissionService, private router: Router) { }
  headElements = ['N°', 'Permission', 'Description', 'Action'];
  searchText = '';
  totalpermission = 0;
  elements: any = [];
  previous: string;
  firstlevel: string;
  secondlevel: string;
  popoverTitle = 'Suppression de cette permission';
  popoverMessage = 'Voulez vous vraiment supprimer cette permission';
  cancelClicked = false;
  ngOnInit() {
    this.loadAllPermission();
  }

  loadAllPermission(): void {
    this.ApiPermission.getAllPermissionList().subscribe(
      data => {
        this.permissions = data;
        console.log(this.permissions);
        this.firstlevel = 'Tableau de bord';
        this.secondlevel = 'Permissions';
        this.totalpermission = this.permissions.length;
        this.mdbTable.setDataSource(this.permissions);
        this.elements = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();
      },
      err => console.error(err),
      () => console.log('liste des permissions obtenue')
    );
  }

  @HostListener('input') oninput() {
    this.searchItems();
  }

  searchItems() {
    const prev = this.permissions;

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.elements = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(15);

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }



  editPermission(permission: Permission): void  {
    window.localStorage.removeItem('editPermissionId');
    window.localStorage.setItem('editPermissionId', permission.id.toString());
    this.router.navigate(['start/modifier-permission']);
  }

  showToasterPermissionSupprimer() {
    this.notifyService.showSuccessWithTimeout('Permission supprimée avec succès !!', 'Notification', 5000);
  }

  deletePermission(permission: Permission) {
    this.ApiPermission.deletePermission(permission).subscribe((data) => {
      this.permissions = this.permissions.filter(s => s !== permission);     this.loadAllPermission(); }
    );
    this.showToasterPermissionSupprimer();
  }


}
