import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifierpermissionComponent } from './modifierpermission.component';

describe('ModifierpermissionComponent', () => {
  let component: ModifierpermissionComponent;
  let fixture: ComponentFixture<ModifierpermissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifierpermissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifierpermissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
