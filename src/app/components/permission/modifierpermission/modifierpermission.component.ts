import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {PermissionService} from '../shared/permission.service';
import {Router} from '@angular/router';
import {first} from 'rxjs/operators';
import {Permission} from '../shared/permission.model';

@Component({
  selector: 'app-modifierpermission',
  templateUrl: './modifierpermission.component.html',
  styleUrls: ['./modifierpermission.component.css']
})
export class ModifierpermissionComponent implements OnInit {
  firstlevel: string;
  secondlevel: string;
  editForm: FormGroup;
  submitted = false;
  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, private toastr: ToastrService, private cdRef: ChangeDetectorRef, public ApiPermission: PermissionService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {

    const permissionId = window.localStorage.getItem('editPermissionId');
    if (!permissionId) {
      alert('Invalid action.');
      this.router.navigate(['start/permissions']);
      return;
    }
    this.firstlevel = 'Permission';
    this.secondlevel = 'Modifier permission';
    this.editForm = this.formBuilder.group({
      id: [],
      libellepermission: ['', Validators.required],
      descriptionpermission: [''],
    });

    this.ApiPermission.getPermission(+permissionId).subscribe( (data) => {
      console.log(data);
      this.editForm.setValue(data);
    });
  }

  showToasterPermissionEnregistrer() {
    this.notifyService.showSuccessWithTimeout('Permission mise à jour avec succès !!', 'Notification', 5000);
  }

  showToasterPermissionErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }

  onSubmit() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.editForm.invalid) {
      console.log('error form');
    }
    const permissionId = window.localStorage.getItem('editPermissionId');
    console.log(this.editForm.value);
    this.ApiPermission.updatePermission(+permissionId, this.editForm.value)
      .pipe(first())
      .subscribe((permission: Permission) => {
        // @ts-ignore
        console.log(permission);
        this.showToasterPermissionEnregistrer();
        this.router.navigate(['start/permissions']);
      });
  }

}
