import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {Permission} from './permission.model';
import {Article} from '../../article/Shared/article';
import {Fournisseur} from '../../fournisseur/shared/fournisseur';

@Injectable({
  providedIn: 'root'
})
export class PermissionService {

  constructor(private httpclient: HttpClient) { }

  getAllPermissionList() {
    return  this.httpclient.get<Permission[]>(environment.apiUrl + '/permissions')
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Its a Trap!'))
      );
  }


  addPermission(permission: Permission) {
    return  this.httpclient.post<Permission>(environment.apiUrl + '/permissions', permission);
  }

  deletePermission(permission: Permission) {
    // @ts-ignore
    return this.httpclient.delete<Permission>(environment.apiUrl + '/permissions/' + permission.id);
  }

  updatePermission(id, value: any) {
    return this.httpclient.put<Permission>(environment.apiUrl + '/permissions/' + id, value );
  }


  getPermission(id: number) {
    return  this.httpclient.get<Permission>(environment.apiUrl + '/permissions/' + id);
  }

}
