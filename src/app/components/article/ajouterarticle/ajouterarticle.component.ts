import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Unitemesure} from '../Shared/unitemesure';
import {ArticleService} from '../Shared/article.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Fournisseur} from '../../fournisseur/shared/fournisseur';
import {Article} from '../Shared/article';
import {ToastrService} from 'ngx-toastr';
import {NotificationService} from '../../../utility/notification.service';

@Component({
  selector: 'app-ajouterarticle',
  templateUrl: './ajouterarticle.component.html',
  styleUrls: ['./ajouterarticle.component.css']
})
export class AjouterarticleComponent implements OnInit {

  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, private toastr: ToastrService, private cdRef: ChangeDetectorRef, public ApiArticle: ArticleService, private router: Router, private formBuilder: FormBuilder) { }
  unitemesure: Unitemesure[];
  firstlevel: string;
  secondlevel: string;
  addForm: FormGroup;
  submitted = false;
  bicvalue = ['1', '2', '0.2'];
  article: Article;
  statebic = false;

  GetIsBIC(event) {
    console.log( event);
    const bicState = event.checked;
    console.log('the value : ' + bicState);
    if (bicState === true) {
      this.statebic = true;
    } else {
      this.statebic = false;
    }
  }

  ngOnInit() {
    this.ApiArticle.getAllUniteMesureList().subscribe((unitemesures: Unitemesure[]) => {
      this.unitemesure = unitemesures;
    });

    this.firstlevel = 'Article';
    this.secondlevel = 'Ajouter article';
    this.addForm = this.formBuilder.group({
      id: [''],
      reference: ['', Validators.required],
      libelle_article: ['', Validators.required],
      description: [''],
      prix_referent: ['',  [Validators.pattern('^[0-9]*$')]],
      unite_mesure_id: ['', Validators.required],
      is_tva: [false],
      is_bic: [false],
      valueofbic: ['']
    });
  }

  showToasterArticleEnregistrer() {
    this.notifyService.showSuccessWithTimeout('Article enregistré avec succès !!', 'Notification', 5000);
  }

  showToasterArticleErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }

  onSubmit() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.addForm.invalid) {
      this.showToasterArticleErreurModifier();
      return;
    }
    console.log(this.addForm.value);
    const idutilisateur =  window.localStorage.getItem('idutilisateur');
    this.addForm.get('id').setValue(idutilisateur);
    if (this.addForm.get('is_bic').value === true &&  this.addForm.get('valueofbic').value !== '') {
    this.ApiArticle.addArticle(this.addForm.value).subscribe((article: Article) => {
      this.router.navigate(['start/articles']);
      this.showToasterArticleEnregistrer();
    });
    } else if (this.addForm.get('is_bic').value === false &&  this.addForm.get('valueofbic').value === '') {
      this.ApiArticle.addArticle(this.addForm.value).subscribe((article: Article) => {
        this.router.navigate(['start/articles']);
        this.showToasterArticleEnregistrer();
      });
    } else {
      this.showToasterArticleErreurModifier();
    }

  }

}
