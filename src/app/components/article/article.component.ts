import {ChangeDetectorRef, Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {MdbTableDirective, MdbTablePaginationComponent} from 'angular-bootstrap-md';
import {Article} from './Shared/article';
import {ArticleService} from './Shared/article.service';
import {Unitemesure} from './Shared/unitemesure';
import {NotificationService} from '../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  articles: Article[];
  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, private toastr: ToastrService, private cdRef: ChangeDetectorRef, public ApiArticle: ArticleService, private router: Router) { }
  headElements = ['N°', 'Reférence', 'Article' , 'Prix', 'Mesure', 'Description', 'Action'];
  public roles: { A: string; T: string; L: string; M: string;  C: string; SA: string; CSR: string; CDC: string};
  searchText = '';
  totalarticle = 0;
  elements: any = [];
  previous: string;
  firstlevel: string;
  secondlevel: string;
  popoverTitle = 'Suppression de cet article';
  popoverMessage = 'Voulez vous vraiment supprimer cet article';
  cancelClicked = false;
  ngOnInit() {
    this.roles = environment.roles;
    this.loadAllArticles();
    // tslint:disable-next-line:only-arrow-functions
    // window.addEventListener('beforeunload', function(e) {
    //   const confirmationMessage = '\o/';
    //   console.log('cond');
    //   e.returnValue = confirmationMessage;     // Gecko, Trident, Chrome 34+
    //   return confirmationMessage; // Gecko, WebKit, Chrome <34
    // });
  }


/*   loadAllArticles() {
    this.ApiArticle.getAllArticleList().subscribe((articles: Article[]) => {
      this.articles = articles;
      console.log(articles);
      this.firstlevel = 'Tableau de bord';
      this.secondlevel = 'Articles';
      this.totalarticle = articles.length;
      this.mdbTable.setDataSource(articles);
      this.elements = this.mdbTable.getDataSource();
      this.previous = this.mdbTable.getDataSource();
    });
  } */

  loadAllArticles(): void {
    this.ApiArticle.getAllArticleList().subscribe(
      data => {
        this.articles = data;
        console.log(this.articles);
        this.firstlevel = 'Tableau de bord';
        this.secondlevel = 'Articles';
        this.totalarticle = this.articles.length;
        this.mdbTable.setDataSource(this.articles);
        this.elements = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();
      },
      err => console.error(err),
      () => console.log('Liste des articles obtenue')
    );
  }

  @HostListener('input') oninput() {
    this.searchItems();
  }

  get ListArticle() {
    return this.ApiArticle.getArticleList();
  }

  searchItems() {
    const prev = this.articles;

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.elements = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(15);

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

  editArticle(article: Article): void  {
    window.localStorage.removeItem('editArticleId');
    window.localStorage.setItem('editArticleId', article.id.toString());
    this.router.navigate(['start/modifier-article']);
  }

  showToasterArticleSupprimer() {
    this.notifyService.showSuccessWithTimeout('Article supprimé avec succès !!', 'Notification', 5000);
  }

  deleteArticle(article: Article) {
    const idutilisateur =  window.localStorage.getItem('idutilisateur');
    const formdata = new FormData();
    // @ts-ignore
    formdata.append('idutilisateur', +idutilisateur);
    formdata.append('_method', 'DELETE');
    console.log('id user : ' + idutilisateur);
    this.ApiArticle.deleteArticle(article, formdata).subscribe((data) => {
      this.articles = this.articles.filter(s => s !== article);     this.loadAllArticles(); }
      );

    this.showToasterArticleSupprimer();
  }

}
