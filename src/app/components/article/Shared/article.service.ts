import { Injectable } from '@angular/core';
import {Fournisseur} from '../../fournisseur/shared/fournisseur';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Article} from './article';
import {Unitemesure} from './unitemesure';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {HttpErrorHandler, HandleError} from '../../../services/http-error-handler.service';

@Injectable()
export class ArticleService {
  private handleError: HandleError;
  constructor(private http: HttpClient ) {
    // this.handleError = httpErrorHandler.createHandleError('ArticleService');
  }

  listArticles: Article[];
  selectedArticle: Article;
  selectedOneArticle: Article;
  getAllArticleList() {
    return  this.http.get<Article[]>(environment.apiUrl + '/articles')
    .pipe(map((data: any) => data ),
        catchError(error => throwError('Its a Trap!'))
      );
  }

  // @ts-ignore
  getLArticle(id: number) {
    this.http
      .get<Article>(environment.apiUrl + '/articles/' + id)
      .toPromise()
      .then(x => {
        this.selectedArticle = x;
      });
  }


  getAllUniteMesureList() {
    return  this.http.get<Unitemesure[]>(environment.apiUrl + '/unitemesures');
  }

  getArticleList() {
    this.http
      .get<Article[]>(environment.apiUrl + '/articles')
      .toPromise()
      .then(x => {
        this.listArticles = x;
        console.log('data: ' + this.listArticles);
      });
  }

  updateArticles(id: number, value: any): Observable<Article> {
    return this.http
      .put<Article>(environment.apiUrl + '/articles/' + id, value);
  }

  getArticle(id: number) {
    return  this.http.get<Article>(environment.apiUrl + '/articles/' + id);
  }

  getArticleForBesoin(id: number) {
    return  this.http.get<Article>(environment.apiUrl + '/articles/' + id)
      .toPromise()
      .then(x => {
        this.selectedOneArticle = x;
      });
  }

  getOneArticle(id: number) {
    return  this.http.get(environment.apiUrl + '/articles/' + id)
      .pipe(map((data: any) => data ),
      catchError(error => throwError('Its a Trap!'))
    );
  }

  addArticle(article: Article) {
    return  this.http.post<Article>(environment.apiUrl + '/articles', article);
  }

  deleteArticle(article: Article, value: any) {
    // @ts-ignore
    return this.http.post<Article>(environment.apiUrl + '/articles/' + article.id, value);
  }

}
