import {Unitemesure} from './unitemesure';

export class Article {

  constructor(
    public libelle_article: string,
    public reference: string,
    public description: string,
    public unite_mesure_id: number,
    public prix_referent: number,
    public unite: string,
    public valueofbic: any,
    public is_bic: boolean,
    public is_tva: boolean,
    public  unite_mesure: Unitemesure[],
    public id?: number
   ) {}

  // id: number;
  // libelle_article: string;
  // reference: string;
  // description: string;
  // prix_referent: number;
  // unite_mesure_id: number;
  // unite: string;
}



