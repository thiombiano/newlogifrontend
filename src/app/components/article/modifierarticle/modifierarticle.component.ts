import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ArticleService} from '../Shared/article.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Unitemesure} from '../Shared/unitemesure';
import {Article} from '../Shared/article';
import {first} from 'rxjs/operators';
import {Fournisseur} from '../../fournisseur/shared/fournisseur';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-modifierarticle',
  templateUrl: './modifierarticle.component.html',
  styleUrls: ['./modifierarticle.component.css']
})
export class ModifierarticleComponent implements OnInit {
  private getbicisfalse: boolean;

  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, private toastr: ToastrService, private cdRef: ChangeDetectorRef, private ApiArticle: ArticleService, private router: Router, private formBuilder: FormBuilder) { }
  unitemesure: Unitemesure[];
  firstlevel: string;
  secondlevel: string;
  editForm: FormGroup;
  submitted = false;
  article: Article;
  bicvalue = ['1', '2', '0.2'];
  statebic = false;
  private getbicistrue: boolean;

  GetIsBIC(event) {
    console.log( event);
    const bicState = event.checked;
    console.log('the value : ' + bicState);
    if (bicState === true) {
      this.statebic = true;
    } else {
      this.statebic = false;
      console.log(this.editForm.get('valueofbic').value);
    }
  }

  ngOnInit() {

    this.ApiArticle.getAllUniteMesureList().subscribe((unitemesures: Unitemesure[]) => {
      this.unitemesure = unitemesures;
    });

    const articleId = window.localStorage.getItem('editArticleId');
    if (!articleId) {
      alert('Invalid action.');
      this.router.navigate(['start/articles']);
      return;
    }

    console.log('ID ARTICLE :  ' + articleId);

    this.firstlevel = 'Article';
    this.secondlevel = 'Modifier article';


    this.editForm = this.formBuilder.group({
      id: [''],
      reference: ['', Validators.required],
      libelle_article: ['', Validators.required],
      description: [''],
      prix_referent: ['',  [Validators.pattern('^[0-9]*$')]],
      unite_mesure_id: ['', Validators.required],
      is_tva: [''],
      is_bic: [''],
      valueofbic: ['']
    });

    this.ApiArticle.getArticle(+articleId).subscribe( (data) => {
      console.log(data);
      if (data.is_bic) {
        this.statebic = true;
      }
      this.editForm.setValue(data);
    });
  }

  showToasterArticleModifier() {
    this.notifyService.showSuccessWithTimeout('Article modifié avec succès !!', 'Notification', 5000);
  }

  showToasterArticleErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }

  get f() { return this.editForm.controls; }

  onSubmit() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.editForm.invalid) {
      console.log('error form');
      this.showToasterArticleErreurModifier();
      return;
    }


    console.log('Etat BIC : ' + this.statebic);

    if (!this.statebic) {
      // @ts-ignore
      this.editForm.get('valueofbic').setValue(null);
      console.log('valueofbic : ' +  this.editForm.get('valueofbic').value );
    }
    // @ts-ignore

    const articleId = window.localStorage.getItem('editArticleId');
    console.log(this.editForm.value);

    console.log('salam : ' + this.editForm.get('is_bic').value);
    console.log('salam valie: ' + this.editForm.get('valueofbic').value);

    if (this.editForm.get('is_bic').value === true) {
      this.getbicistrue = true;
    } else if (this.editForm.get('is_bic').value === 1) {
      this.getbicistrue = true;
    }

    if (this.editForm.get('is_bic').value === false) {
      this.getbicisfalse = false;
    } else if (this.editForm.get('is_bic').value === 0) {
      this.getbicisfalse = false;
    }
    // const idutilisateur =  window.localStorage.getItem('idutilisateur');
    // this.editForm.get('idutilisateur').setValue(idutilisateur);

    if ( this.getbicisfalse === false && this.editForm.get('valueofbic').value === null) {
    this.ApiArticle.updateArticles(+articleId, this.editForm.value)
        .pipe(first())
        .subscribe((article: Article) => {
          // @ts-ignore
          console.log(article);
          this.showToasterArticleModifier();
          this.router.navigate(['start/articles']);
        });
    console.log('false ande null');
    } else if ( this.getbicistrue === true  && this.editForm.get('valueofbic').value !== null) {
      this.ApiArticle.updateArticles(+articleId, this.editForm.value)
        .pipe(first())
        .subscribe((article: Article) => {
          // @ts-ignore
          console.log(article);
          this.showToasterArticleModifier();
          this.router.navigate(['start/articles']);
        });
      console.log('true and different null 1');
    }  else {
      this.showToasterArticleErreurModifier();
    }
  }
}
