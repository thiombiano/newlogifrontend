import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypechantierComponent } from './typechantier.component';

describe('TypechantierComponent', () => {
  let component: TypechantierComponent;
  let fixture: ComponentFixture<TypechantierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypechantierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypechantierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
