import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifiertypechantierComponent } from './modifiertypechantier.component';

describe('ModifiertypechantierComponent', () => {
  let component: ModifiertypechantierComponent;
  let fixture: ComponentFixture<ModifiertypechantierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifiertypechantierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifiertypechantierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
