import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {TypechantierService} from '../shared/typechantier.service';
import {Router} from '@angular/router';
import {first} from 'rxjs/operators';
import {Modepaiement} from '../../modepaiement/shared/modepaiement';
import {Typechantier} from '../shared/Typechantier.model';

@Component({
  selector: 'app-modifiertypechantier',
  templateUrl: './modifiertypechantier.component.html',
  styleUrls: ['./modifiertypechantier.component.css']
})
export class ModifiertypechantierComponent implements OnInit {
  firstlevel: string;
  secondlevel: string;
  editForm: FormGroup;
  submitted = false;
  typechantier: Typechantier;
  // tslint:disable-next-line:max-line-length
  constructor(private formBuilder: FormBuilder,  private notifyService: NotificationService, private toastr: ToastrService, private cdRef: ChangeDetectorRef, private ApiTypeChantier: TypechantierService, private router: Router) { }

  ngOnInit() {

    const typedechantierId = window.localStorage.getItem('editTypeChantierId');
    if (!typedechantierId) {
      alert('Invalid action.');
      this.router.navigate(['start/typechantiers']);
      return;
    }
    this.firstlevel = 'Type de chantier';
    this.secondlevel = 'Modifier un type de chantier';
    this.editForm = this.formBuilder.group({
      id: [''],
      code_type_chantier: ['', Validators.required],
      libelle_type_chantier: ['', Validators.required],
    });

    this.ApiTypeChantier.getTypechantier(+typedechantierId).subscribe( (data) => {
      console.log(data);
      this.editForm.setValue(data);
    });
  }

  showToasterTypeChantierModifier() {
    this.notifyService.showSuccessWithTimeout('Type de chantier modifié avec succès !!', 'Notification', 5000);
  }

  showToasterTypeChantierErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.editForm.invalid) {
      console.log('error form');
      this.showToasterTypeChantierErreurModifier();
    }
    // const idutilisateur =  window.localStorage.getItem('idutilisateur');
    // this.editForm.get('idutilisateur').setValue(idutilisateur);
    const typechantierId = window.localStorage.getItem('editTypeChantierId');
    const formData = new FormData();
    const idutilisateur =  window.localStorage.getItem('idutilisateur');
    formData.append('code_type_chantier', this.editForm.get('code_type_chantier').value);
    formData.append('libelle_type_chantier', this.editForm.get('libelle_type_chantier').value);
    formData.append('idutilisateur', idutilisateur);
    formData.append('_method', 'PUT');
    this.ApiTypeChantier.updateTypechantier(formData, +typechantierId)
      .pipe(first())
      .subscribe((typechantier: Typechantier) => {
        // @ts-ignore
        console.log(typechantier);
        this.showToasterTypeChantierModifier();
        this.router.navigate(['start/typechantiers']);
      });
  }
}
