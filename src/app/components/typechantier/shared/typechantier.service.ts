import { Injectable } from '@angular/core';
import {environment} from '../../../../environments/environment';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Typechantier} from './Typechantier.model';

@Injectable({
  providedIn: 'root'
})
export class TypechantierService {

  constructor(private httpclient: HttpClient) { }


  getAllTypeChantierList() {
    return  this.httpclient.get<Typechantier[]>(environment.apiUrl + '/typechantiers')
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Its a Trap!'))
      );
  }


  addTypechantier(value: any) {
    return  this.httpclient.post<Typechantier>(environment.apiUrl + '/typechantiers', value);
  }

  deleteTypechantier(typechantier: Typechantier , value: any) {
    console.log(typechantier.id);
    return this.httpclient.post<Typechantier>(environment.apiUrl + '/typechantiers/' + typechantier.id, value);
  }

  updateTypechantier(value: any, id) {
    return this.httpclient.post<Typechantier>(environment.apiUrl + '/typechantiers/' + id, value);
  }


  getTypechantier(id: number) {
    return  this.httpclient.get<Typechantier>(environment.apiUrl + '/typechantiers/' + id)
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Its a Trap!'))
      );
  }
}
