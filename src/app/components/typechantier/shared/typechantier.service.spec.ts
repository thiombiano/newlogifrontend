import { TestBed } from '@angular/core/testing';

import { TypechantierService } from './typechantier.service';

describe('TypechantierService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TypechantierService = TestBed.get(TypechantierService);
    expect(service).toBeTruthy();
  });
});
