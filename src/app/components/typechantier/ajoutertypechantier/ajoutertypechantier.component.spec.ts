import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutertypechantierComponent } from './ajoutertypechantier.component';

describe('AjoutertypechantierComponent', () => {
  let component: AjoutertypechantierComponent;
  let fixture: ComponentFixture<AjoutertypechantierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjoutertypechantierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutertypechantierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
