import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {TypechantierService} from '../shared/typechantier.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Modepaiement} from '../../modepaiement/shared/modepaiement';
import {Typechantier} from '../shared/Typechantier.model';

@Component({
  selector: 'app-ajoutertypechantier',
  templateUrl: './ajoutertypechantier.component.html',
  styleUrls: ['./ajoutertypechantier.component.css']
})
export class AjoutertypechantierComponent implements OnInit {
  firstlevel: string;
  secondlevel: string;
  addForm: FormGroup;
  submitted = false;
  // tslint:disable-next-line:max-line-length
  constructor(private formBuilder: FormBuilder,  private notifyService: NotificationService, private toastr: ToastrService, private cdRef: ChangeDetectorRef, private ApiTypeChantier: TypechantierService, private router: Router) { }

  ngOnInit() {
    this.firstlevel = 'Type de chantier';
    this.secondlevel = 'Ajouter un type de chantier';
    this.addForm = this.formBuilder.group({
      id: [''],
      code_type_chantier: ['', Validators.required],
      libelle_type_chantier: ['', Validators.required],
    });
  }

  showToasterTypeChantierEnregistrer() {
    this.notifyService.showSuccessWithTimeout('Type de Chantier enregistré avec succès !!', 'Notification', 5000);
  }

  showToasterTypeChantierErreurAjout() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }

  onSubmit() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.addForm.invalid) {
      this.showToasterTypeChantierErreurAjout();
      return;
    }
    console.log(this.addForm.value);
    const formData = new FormData();
    const idutilisateur =  window.localStorage.getItem('idutilisateur');
    formData.append('code_type_chantier', this.addForm.get('code_type_chantier').value);
    formData.append('libelle_type_chantier', this.addForm.get('libelle_type_chantier').value);
    formData.append('idutilisateur', idutilisateur);
    this.ApiTypeChantier.addTypechantier(formData).subscribe((typechantier: Typechantier) => {
      this.router.navigate(['start/typechantiers']);
      this.showToasterTypeChantierEnregistrer();
    });

  }

}
