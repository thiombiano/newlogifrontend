import {ChangeDetectorRef, Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {MdbTableDirective, MdbTablePaginationComponent} from 'angular-bootstrap-md';
import {Typechantier} from './shared/Typechantier.model';
import {NotificationService} from '../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {TypechantierService} from './shared/typechantier.service';


@Component({
  selector: 'app-typechantier',
  templateUrl: './typechantier.component.html',
  styleUrls: ['./typechantier.component.css']
})
export class TypechantierComponent implements OnInit {
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  elements: any = [];
  headElements = ['N°',  'Code type chantier', 'Libelle type chantier' , 'Action'];
  typechantiers: Typechantier[];
  previous: string;
  searchText = '';
  totaltypechantier = 0;
  firstlevel: string;
  secondlevel: string;
  popoverTitle = 'Suppression de ce type de chantier';
  popoverMessage = 'Voulez vous vraiment supprimer ce type de chantier';
  cancelClicked = false;
  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, private toastr: ToastrService, private cdRef: ChangeDetectorRef, private ApiTypeChantier: TypechantierService, private router: Router) { }

  ngOnInit() {
    this.loadAllTypeChantier();
  }

  loadAllTypeChantier(): void {
    this.ApiTypeChantier.getAllTypeChantierList().subscribe(
      data => {
        this.typechantiers = data;
        console.log(this.typechantiers);
        this.firstlevel = 'Tableau de bord';
        this.secondlevel = 'Type de chantier';
        this.totaltypechantier = this.typechantiers.length;
        this.mdbTable.setDataSource(this.typechantiers);
        this.elements = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();
      },
      err => console.error(err),
      () => console.log('Liste des types de chantiers obtenu')
    );
  }

  @HostListener('input') oninput() {
    this.searchItems();
  }

  searchItems() {
    const prev = this.typechantiers;

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.elements = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(6);
    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

  editTypeChantier(typechantier: Typechantier): void  {
    window.localStorage.removeItem('editTypeChantierId');
    window.localStorage.setItem('editTypeChantierId', typechantier.id.toString());
    this.router.navigate(['start/modifier-typechantier']);
  }

  showToasterTypeChantierSupprimer() {
    this.notifyService.showSuccessWithTimeout('Info type chantier supprimer avec succès !!', 'Notification', 5000);
  }

  deleteTypeChantier(typechantier: Typechantier) {
    const idutilisateur =  window.localStorage.getItem('idutilisateur');
    const formdata = new FormData();
    // @ts-ignore
    formdata.append('idutilisateur', +idutilisateur);
    formdata.append('_method', 'DELETE');
    this.ApiTypeChantier.deleteTypechantier(typechantier, formdata).subscribe((data) => {
      this.typechantiers = this.typechantiers.filter(s => s !== typechantier);
      this.loadAllTypeChantier();
      this.showToasterTypeChantierSupprimer(); });
  }

}
