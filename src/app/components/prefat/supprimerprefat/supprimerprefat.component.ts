import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {Section} from '../../section/shared/section.model';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {SectionService} from '../../section/shared/section.service';
import {PrefatService} from '../shared/prefat.service';
import {Prefat} from '../shared/prefat.model';

@Component({
  selector: 'app-supprimerprefat',
  templateUrl: './supprimerprefat.component.html',
  styleUrls: ['./supprimerprefat.component.css']
})
export class SupprimerprefatComponent {
  @Output() passEntry: EventEmitter<any> = new EventEmitter();
  constructor(public dialogRef: MatDialogRef<SupprimerprefatComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public ApiPrefat: PrefatService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    if(this.data.id) {
      this.ApiPrefat.deletePrefat(this.data.id).subscribe((prefat: Prefat) => {
        console.log(prefat);
        this.passEntry.emit(prefat);
      });
    }
  }

}
