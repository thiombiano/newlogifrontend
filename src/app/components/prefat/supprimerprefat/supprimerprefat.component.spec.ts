import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupprimerprefatComponent } from './supprimerprefat.component';

describe('SupprimerprefatComponent', () => {
  let component: SupprimerprefatComponent;
  let fixture: ComponentFixture<SupprimerprefatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupprimerprefatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupprimerprefatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
