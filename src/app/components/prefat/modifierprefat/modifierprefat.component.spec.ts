import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifierprefatComponent } from './modifierprefat.component';

describe('ModifierprefatComponent', () => {
  let component: ModifierprefatComponent;
  let fixture: ComponentFixture<ModifierprefatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifierprefatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifierprefatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
