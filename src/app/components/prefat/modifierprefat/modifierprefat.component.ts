import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Prefat} from '../shared/prefat.model';
import {PrefatService} from '../shared/prefat.service';
import {FormControl, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';
import {Section} from '../../section/shared/section.model';

@Component({
  selector: 'app-modifierprefat',
  templateUrl: './modifierprefat.component.html',
  styleUrls: ['./modifierprefat.component.css']
})
export class ModifierprefatComponent {
  @Output() passEntry: EventEmitter<any> = new EventEmitter();
  constructor(public dialogRef: MatDialogRef<ModifierprefatComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Prefat,
              public ApiPrefat: PrefatService) { }

  formControl = new FormControl('', [
    Validators.required
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Le champ est requis' : '';
  }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  stopEdit(): void {
    console.log('les données dans le modal :' + this.data);
    const ChantierId = window.localStorage.getItem('voirChantierId');
    this.data.chantier_id = +ChantierId;
    const idprefat = this.data.id;
    // @ts-ignore
    this.ApiPrefat.updatePrefat(+idprefat , this.data)
      .pipe(first())
      .subscribe((prefat: Prefat) => {
        console.log(prefat);
        this.passEntry.emit(prefat);
      });
  }

}
