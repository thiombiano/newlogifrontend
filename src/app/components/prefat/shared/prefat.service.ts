import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {Section} from '../../section/shared/section.model';
import {Prefat} from './prefat.model';
import {Maison} from '../../maison/shared/maison.model';

@Injectable({
  providedIn: 'root'
})
export class PrefatService {
  public selectedOnePrefat: Prefat;
  constructor(public http: HttpClient) { }

  getPrefatList(idChantier: number) {
    return  this.http.get(environment.apiUrl + '/chantiers/' + idChantier + '/prefats')
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Impossible d\'obtenir les prefats!'))
      );
  }


  getPrefatForBesoin(id: number) {
    const idchantier =  window.localStorage.getItem('voirChantierId');
    return  this.http.get<Prefat>(environment.apiUrl + '/chantiers/' + idchantier + '/prefats/' + id)
      .toPromise()
      .then(x => {
        this.selectedOnePrefat = x;
      });
  }

  addPrefat(prefat: Prefat) {
    const idchantier =  window.localStorage.getItem('voirChantierId');
    return  this.http.post<Prefat>(environment.apiUrl + '/chantiers/' + idchantier + '/prefats', prefat);
  }

  deletePrefat(id: number) {
    const idchantier =  window.localStorage.getItem('voirChantierId');
    // @ts-ignore
    return this.http.delete<Prefat>(environment.apiUrl + '/chantiers/' + id + '/prefats/' + idchantier);
  }

  updatePrefat(id, value: any ) {
    const idchantier =  window.localStorage.getItem('voirChantierId');
    return this.http.put<Prefat>(environment.apiUrl + '/chantiers/' + id  + '/prefats/' + idchantier, value );
  }

  getPrefatById(id: number) {
    return this.http.get(environment.apiUrl + '/prefatbyid/' + id);
  }


}
