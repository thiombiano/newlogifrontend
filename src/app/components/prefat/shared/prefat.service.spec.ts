import { TestBed } from '@angular/core/testing';

import { PrefatService } from './prefat.service';

describe('PrefatService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PrefatService = TestBed.get(PrefatService);
    expect(service).toBeTruthy();
  });
});
