import {Employe} from '../../employe/shared/employe.model';

export class Prefat {
  id: number;
  libelleprefat: string;
  is_actif: boolean;
  chantier_id: number;
  magasinier_id: number;
  technicien_id: number;
  technicien: Employe[];
  magasinier: Employe[];
}
