import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrefatComponent } from './prefat.component';

describe('PrefatComponent', () => {
  let component: PrefatComponent;
  let fixture: ComponentFixture<PrefatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrefatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrefatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
