import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Section} from '../../section/shared/section.model';
import {SectionService} from '../../section/shared/section.service';
import {Prefat} from '../shared/prefat.model';
import {PrefatService} from '../shared/prefat.service';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-ajouterprefat',
  templateUrl: './ajouterprefat.component.html',
  styleUrls: ['./ajouterprefat.component.css']
})
export class AjouterprefatComponent  {
  @Output() passEntry: EventEmitter<any> = new EventEmitter();

  constructor(public dialogRef: MatDialogRef<AjouterprefatComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Prefat,
              public ApiPrefat: PrefatService) { }

  formControl = new FormControl('', [
    Validators.required
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Le champ est requis' : '';
  }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    console.log('les données dans le modal :' + this.data);
    const ChantierId = window.localStorage.getItem('voirChantierId');
    this.data.chantier_id = +ChantierId;
    this.ApiPrefat.addPrefat(this.data).subscribe((prefat: Prefat) => {
      this.passEntry.emit(prefat);
      console.log(prefat);
    });
  }

}
