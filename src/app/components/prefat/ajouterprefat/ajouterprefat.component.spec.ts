import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjouterprefatComponent } from './ajouterprefat.component';

describe('AjouterprefatComponent', () => {
  let component: AjouterprefatComponent;
  let fixture: ComponentFixture<AjouterprefatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjouterprefatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjouterprefatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
