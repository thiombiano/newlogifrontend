import {ChangeDetectorRef, Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {MdbTableDirective, MdbTablePaginationComponent} from 'angular-bootstrap-md';
import {NotificationService} from '../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {PersonnelsService} from '../personnels/shared/personnels.service';
import {Router} from '@angular/router';
import {EmployeService} from './shared/employe.service';
import {Personnel} from '../personnels/shared/personnel';
import {Employe} from './shared/employe.model';

@Component({
  selector: 'app-employe',
  templateUrl: './employe.component.html',
  styleUrls: ['./employe.component.css']
})
export class EmployeComponent implements OnInit {
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  headElements = ['N°',  'Nom', 'Prenom' , 'Nom de connexion', 'Roles'  , 'Actif' , 'Email', 'Action'];
  searchText = '';
  employes: Employe[];
  totalemploye = 0;
  elements: any = [];
  previous: string;
  firstlevel: string;
  secondlevel: string;
  popoverTitle = 'Suppression de cet utilisateur';
  popoverMessage = 'Voulez vous vraiment supprimer cet utilisateur';
  cancelClicked = false;
  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, private toastr: ToastrService, private cdRef: ChangeDetectorRef, private ApiEmploye: EmployeService, private router: Router) { }

  ngOnInit() {
    this.loadAllEmploye();
  }

  @HostListener('input') oninput() {
    this.searchItems();
  }

  loadAllEmploye(): void {
    this.ApiEmploye.getAllEmployeList().subscribe(
      data => {
        this.employes = data.data;
        console.log(this.employes);
        this.firstlevel = 'Tableau de bord';
        this.secondlevel = 'Utilisateurs';
        this.totalemploye = this.employes.length;
        this.mdbTable.setDataSource(this.employes);
        this.elements = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();
      },
      err => console.error(err),
      () => console.log('liste des employes obtenue')
    );
  }

  searchItems() {
    const prev = this.employes;

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.elements = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(6);

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

  editEmploye(employe: Personnel): void  {
    window.localStorage.removeItem('editEmployeId');
    window.localStorage.setItem('editEmployeId', employe.id.toString());
    this.ApiEmploye.getEmploye(employe.id);
    this.router.navigate(['start/modifier-employe']);
  }

  showToasterEmployeSupprimer() {
    this.notifyService.showSuccessWithTimeout('Employe supprimé avec succès !!', 'Notification', 5000);
  }

  deleteEmploye(employe: Employe) {
    this.ApiEmploye.deleteEmploye(employe).subscribe((data) => {
      this.employes = this.employes.filter(s => s !== employe);
      this.loadAllEmploye();
      this.showToasterEmployeSupprimer(); });
  }


}
