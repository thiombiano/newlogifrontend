import {Role} from '../../role/shared/role.model';

export class Employe {
  id: number;
  name: string;
  login: string;
  email: string;
  telemploye: string;
  codeemploye: string;
  prenomemploye: string;
  password: string;
  isActif: boolean;
  roles: Role[];
}
