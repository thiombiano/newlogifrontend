import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {Employe} from './employe.model';

@Injectable({
  providedIn: 'root'
})

export class EmployeService {
  private headers: HttpHeaders;
  constructor(private httpclient: HttpClient) { }

  getAllEmployeList() {
    return  this.httpclient.get<Employe[]>(environment.apiUrl + '/users')
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Impossible dobtenir les utilisateurs!'))
      );
  }

  getAllTechnicienList() {
    return  this.httpclient.get<Employe[]>(environment.apiUrl + '/userstechniciens')
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Impossible dobtenir les techniciens!'))
      );
  }

  getAllMagasinierList() {
    return  this.httpclient.get<Employe[]>(environment.apiUrl + '/usersmagasiniers')
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Impossible dobtenir les magasiniers!'))
      );
  }

  getAllControleurList() {
    return  this.httpclient.get<Employe[]>(environment.apiUrl + '/userscontroleurs')
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Impossible dobtenir les controleurs!'))
      );
  }



  addEmploye(employe: Employe) {
    this.headers = new HttpHeaders();
    this.headers.append('Content-Type', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    return  this.httpclient.post<Employe>(environment.apiUrl + '/users', employe, {headers: this.headers});
  }

  deleteEmploye(employe: Employe) {
    // @ts-ignore
    return this.httpclient.delete<Employe>(environment.apiUrl + '/users/' + employe.id);
  }

  updateEmploye(id, value: any) {
    this.headers = new HttpHeaders();
    this.headers.append('Content-Type', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    // @ts-ignore
    return this.httpclient.post<Employe>(environment.apiUrl + '/users/' + id, value, {headers: this.headers} );
  }


  getEmploye(id: number) {
    return  this.httpclient.get<Employe>(environment.apiUrl + '/users/' + id)
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Its a Trap!'))
      );
  }
}
