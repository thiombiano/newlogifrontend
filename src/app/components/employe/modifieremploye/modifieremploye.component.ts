import { Component, OnInit } from '@angular/core';
import {RoleService} from '../../role/shared/role.service';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {EmployeService} from '../shared/employe.service';
import {FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Fonction} from '../../fonctions/shared/fonction';
import {Employe} from '../shared/employe.model';
import {Role} from '../../role/shared/role.model';
import {ErrorStateMatcher} from '@angular/material';
import {first} from 'rxjs/operators';
import {Permission} from '../../permission/shared/permission.model';
import {environment} from '../../../../environments/environment';
import {NgxPermissionsService, NgxRolesService} from "ngx-permissions";

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
    const invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);

    return (invalidCtrl || invalidParent);
  }
}

@Component({
  selector: 'app-modifieremploye',
  templateUrl: './modifieremploye.component.html',
  styleUrls: ['./modifieremploye.component.css']
})
export class ModifieremployeComponent implements OnInit {
  matcher = new MyErrorStateMatcher();
  firstlevel: string;
  secondlevel: string;
  fonctions: Fonction[];
  editForm: FormGroup;
  submitted = false;
  employe: Employe;
  roles: Role[];
  public imagePath;
  imgURL: any;
  public message: string;
  selectedFile: File;
  public path: string;
  userimage: string;
  preview(files) {
    if (files.length === 0) {
      return;
    }

    // if (event.target.files.length > 0) {
    //   const file = event.target.files[0];
    //   this.addForm.get('avatar').setValue(file);
    // }

    const mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = 'Only images are supported.';
      return;
    }

    const reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    console.log('le o : ' + files[0]);
    // tslint:disable-next-line:variable-name
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    };
    console.log('Le ficher est : ' +   this.imgURL);
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    // tslint:disable-next-line:variable-name
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    };
    console.log('Le ficher est : ' +   this.selectedFile.name);
  }
  // tslint:disable-next-line:max-line-length
  constructor(private RolesService: NgxRolesService, private permissionsService: NgxPermissionsService, private apiRole: RoleService, private notifyService: NotificationService, private toastr: ToastrService, public ApiEmploye: EmployeService, private formBuilder: FormBuilder,  private router: Router) { }

  ngOnInit() {
    this.path = environment.apiImage;
    this.apiRole.getAllRoleList().subscribe((data) => {
      this.roles = data.data;
    });
    const UtilisateurId = window.localStorage.getItem('editEmployeId');
    this.firstlevel = 'Utilisateur';
    this.secondlevel = 'Modifier info utilisation';
    this.editForm = this.formBuilder.group({
      id: [''],
      codeemploye: ['', Validators.required],
      name: ['', Validators.required],
      prenomemploye: ['', Validators.required],
      telemploye: [''],
      email: ['', Validators.email],
      login: ['', Validators.required],
      roles: ['', Validators.required],
      password: [],
      isActif : [],
      confirmPassword: [''],
      avatar: ['']
    }, {validator: this.checkPasswords });


    this.ApiEmploye.getEmploye(+UtilisateurId).subscribe( (data) => {
      console.log('lesroless pour le role 1 : ' +   this.roles);
      this.editForm.get('id').setValue(data.data[0].id);
      this.editForm.get('codeemploye').setValue(data.data[0].codeemploye);
      this.editForm.get('name').setValue(data.data[0].name);
      this.editForm.get('prenomemploye').setValue(data.data[0].prenomemploye);
      this.editForm.get('email').setValue(data.data[0].email);
      this.editForm.get('login').setValue(data.data[0].login);
      this.editForm.get('telemploye').setValue(data.data[0].telemploye);
      this.editForm.get('password').setValue(data.data[0].password);
      this.editForm.get('confirmPassword').setValue(data.data[0].password);
      this.editForm.get('roles').setValue(data.data[0].roles);
      this.editForm.get('isActif').setValue(data.data[0].isActif);
      this.userimage = data.data[0].avatar;
      this.imgURL = this.path + '/' + this.userimage;
    });
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    const pass = group.controls.password.value;
    const confirmPass = group.controls.confirmPassword.value;

    return pass === confirmPass ? null : { notSame: true };
  }

  showToasterEmployeEnregistrer() {
    this.notifyService.showSuccessWithTimeout('Utilisateur mis à jour avec succès !!', 'Notification', 5000);
  }

  showToasterEmployeErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }

  compareFn(permission1: Permission, permission2: Permission) {
    return permission1 && permission2 ? permission1.id === permission2.id : permission1 === permission2;
  }

  onSubmit() {

    this.submitted = true;
    console.log(this.editForm.value);
    // stop here if form is invalid
    if (this.editForm.invalid) {
      console.log('error form');
      this.showToasterEmployeErreurModifier();
      return;
    }
    const UtilisateurId = window.localStorage.getItem('editEmployeId');
    console.log(this.editForm.value);

    // @ts-ignore
    const formdata = new FormData();
    if (!this.editForm.get('password').value) {
      formdata.append('password', '');
      console.log(this.editForm.get('password').value);
    } else {
      formdata.append('password', this.editForm.get('password').value );
    }

    formdata.append('codeemploye', this.editForm.get('codeemploye').value );
    formdata.append('name', this.editForm.get('name').value );
    formdata.append('prenomemploye', this.editForm.get('prenomemploye').value );
    formdata.append('telemploye', this.editForm.get('telemploye').value );
    formdata.append('email', this.editForm.get('email').value );
    formdata.append('login', this.editForm.get('login').value );
    formdata.append('roles', JSON.stringify(this.editForm.get('roles').value));
    if (this.editForm.get('isActif').value === true || this.editForm.get('isActif').value === 1) {
      // @ts-ignore
      formdata.append('isActif', 1 );
    } else {
      // @ts-ignore
      formdata.append('isActif', 0);
    }
    if (this.selectedFile) {
      formdata.append('avatar', this.selectedFile);
    } else {
      formdata.append('avatar', null);
    }
    console.log(this.editForm.get('roles').value);
    console.log('ALL THE ROLE  : ' + formdata.get('roles'));
    console.log('ALL THE avatar  : ' + formdata.get('avatar'));
    console.log('ALL THE codeemploye  : ' + formdata.get('codeemploye'));
    const permArray = [];
    formdata.append('_method', 'PUT');
    // @ts-ignore
    this.ApiEmploye.updateEmploye(+UtilisateurId, formdata)
      .pipe(first())
      .subscribe((data) => {
        this.showToasterEmployeEnregistrer();
        this.router.navigate(['start/employes']);
      });

  }

}
