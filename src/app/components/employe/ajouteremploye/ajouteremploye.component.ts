import { Component, OnInit } from '@angular/core';
import {Fonction} from '../../fonctions/shared/fonction';
import {FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {Employe} from '../shared/employe.model';
import {FonctionsService} from '../../fonctions/shared/fonctions.service';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {PersonnelsService} from '../../personnels/shared/personnels.service';
import {Router} from '@angular/router';
import {EmployeService} from '../shared/employe.service';
import {RoleService} from '../../role/shared/role.service';
import {Role} from '../../role/shared/role.model';
import {Personnel} from '../../personnels/shared/personnel';
import {ErrorStateMatcher} from '@angular/material';
import {environment} from '../../../../environments/environment';
import * as moment from 'moment';
import {NgxPermissionsService, NgxRolesService} from "ngx-permissions";
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
    const invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);

    return (invalidCtrl || invalidParent);
  }
}

@Component({
  selector: 'app-ajouteremploye',
  templateUrl: './ajouteremploye.component.html',
  styleUrls: ['./ajouteremploye.component.css']
})
export class AjouteremployeComponent implements OnInit {
  matcher = new MyErrorStateMatcher();
  firstlevel: string;
  secondlevel: string;
  fonctions: Fonction[];
  addForm: FormGroup;
  submitted = false;
  employe: Employe;
  roles: Role[];
  public imagePath;
  referencevalutilisateur = '';
  imgURL: any;
  public message: string;
  selectedFile: File;
  public path: string;
  preview(files) {
    if (files.length === 0) {
      return;
    }

    // if (event.target.files.length > 0) {
    //   const file = event.target.files[0];
    //   this.addForm.get('avatar').setValue(file);
    // }

    const mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = 'Only images are supported.';
      return;
    }

    const reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    console.log('le o : ' + files[0]);
    // tslint:disable-next-line:variable-name
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    };
    console.log('Le ficher est : ' +   this.imgURL);
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
    const reader = new FileReader();
    this.imagePath = event.target.files;
    reader.readAsDataURL(event.target.files[0]);
    // tslint:disable-next-line:variable-name
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    };
    console.log('Le ficher est : ' +   this.selectedFile.name);
  }
  // tslint:disable-next-line:max-line-length
  constructor( private RolesService: NgxRolesService, private permissionsService: NgxPermissionsService, private apiRole: RoleService, private notifyService: NotificationService, private toastr: ToastrService, public ApiEmploye: EmployeService, private formBuilder: FormBuilder,  private router: Router) { }

  ngOnInit() {
    this.path = environment.apiImage;
    const userimage = 'profile/defaultUser.png';
    this.imgURL = this.path + '/' + userimage;
    this.apiRole.getAllRoleList().subscribe((data) => {
      this.roles = data.data;
    });
    this.ApiEmploye.getAllEmployeList().subscribe(data => {
        this.referencevalutilisateur = moment(new Date()).format('YYYY') + '/USR/' + (data.data.length + 1);
      });
    this.firstlevel = 'Utilisateur';
    this.secondlevel = 'Ajouter utilisateur';

    this.addForm = this.formBuilder.group({
      codeemploye: ['', Validators.required],
      name: ['', Validators.required],
      prenomemploye: ['', Validators.required],
      telemploye: [''],
      email: ['', Validators.email],
      login: ['', Validators.required],
      roles: ['', Validators.required],
      isActif : [true],
      password: ['', [Validators.required]],
      confirmPassword: [''],
      avatar: ['']
    }, {validator: this.checkPasswords });
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    const pass = group.controls.password.value;
    const confirmPass = group.controls.confirmPassword.value;

    return pass === confirmPass ? null : { notSame: true };
  }

  showToasterEmployeEnregistrer() {
    this.notifyService.showSuccessWithTimeout('Utilisateur enregistré avec succès !!', 'Notification', 5000);
  }

  showToasterEmployeErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }

  onSubmit() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.addForm.invalid) {
      this.showToasterEmployeErreurModifier();
      return;
    }
    // @ts-ignore
    const formdata = new FormData();
    formdata.append('codeemploye', this.addForm.get('codeemploye').value );
    formdata.append('name', this.addForm.get('name').value );
    formdata.append('prenomemploye', this.addForm.get('prenomemploye').value );
    formdata.append('email', this.addForm.get('email').value );
    formdata.append('login', this.addForm.get('login').value );
    formdata.append('roles', JSON.stringify(this.addForm.get('roles').value));
    if (this.addForm.get('isActif').value === true) {
    // @ts-ignore
      formdata.append('isActif', 1 );
    } else {
      // @ts-ignore
      formdata.append('isActif', 0);
    }
    formdata.append('password', this.addForm.get('password').value );
    if (this.selectedFile) {
    formdata.append('avatar', this.selectedFile);
    }
    const permArray = [];
    console.log(this.addForm.get('roles').value);
    console.log(formdata.get('roles'));
    // @ts-ignore
    this.ApiEmploye.addEmploye(formdata).subscribe((data) => {
      this.showToasterEmployeEnregistrer();
      this.RolesService.flushRoles();
      this.permissionsService.flushPermissions();
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < data.data[0].roles.length; i++) {
        console.log( 'les roles : ' + data.data[0].roles[i].libellerole);
        // tslint:disable-next-line:prefer-for-of
        for (let j = 0; j < data.data[0].roles[i].permissions.length; j++) {
          permArray.push(data.data[0].roles[i].permissions[j].libellepermission);
          console.log( 'les permissions : ' + data.data[0].roles[i].permissions[j].libellepermission);
          // @ts-ignore
          this.permissionsService.addPermission(data.data[0].roles[i].permissions[j].libellepermission);
        }
        this.RolesService.addRole(data.data[0].roles[i].libellerole, permArray);
      }
      console.log(data);
      this.router.navigate(['start/employes']);
    });

  }

}
