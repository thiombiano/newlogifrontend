import { TestBed } from '@angular/core/testing';

import { ModepaiementService } from './modepaiement.service';

describe('ModepaiementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ModepaiementService = TestBed.get(ModepaiementService);
    expect(service).toBeTruthy();
  });
});
