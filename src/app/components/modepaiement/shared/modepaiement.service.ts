import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {catchError, map} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import {Modepaiement} from './modepaiement';
import {Article} from '../../article/Shared/article';


@Injectable({
  providedIn: 'root'
})
export class ModepaiementService {


  constructor(private httpclient: HttpClient) { }

  getAllModePaiementList() {
    return  this.httpclient.get<Modepaiement[]>(environment.apiUrl + '/modepaiments')
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Its a Trap!'))
      );
  }

  updateModePaiement(id: number, value: any): Observable<Modepaiement> {
    return this.httpclient
      .put<Modepaiement>(environment.apiUrl + '/modepaiments/' + id, value);
  }

  getModePaiement(id: number) {
    return  this.httpclient.get<Modepaiement>(environment.apiUrl + '/modepaiments/' + id);
  }



  addModePaiement(modepaiement: Modepaiement) {
    return  this.httpclient.post<Modepaiement>(environment.apiUrl + '/modepaiments', modepaiement);
  }

  deleteModePaiement(modepaiement: Modepaiement) {
    // @ts-ignore
    return this.httpclient.delete<Modepaiement>(environment.apiUrl + '/modepaiments/' + modepaiement.id);
  }


}
