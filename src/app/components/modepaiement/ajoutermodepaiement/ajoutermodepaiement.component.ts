import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Modepaiement} from '../shared/modepaiement';
import {Article} from '../../article/Shared/article';
import {Unitemesure} from '../../article/Shared/unitemesure';
import {ModepaiementService} from '../shared/modepaiement.service';

@Component({
  selector: 'app-ajoutermodepaiement',
  templateUrl: './ajoutermodepaiement.component.html',
  styleUrls: ['./ajoutermodepaiement.component.css']
})
export class AjoutermodepaiementComponent implements OnInit {

  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, private toastr: ToastrService, private cdRef: ChangeDetectorRef, public ApiModePaiement: ModepaiementService, private router: Router, private formBuilder: FormBuilder) { }
  firstlevel: string;
  secondlevel: string;
  addForm: FormGroup;
  submitted = false;
  modepaiement: Modepaiement;
  ngOnInit() {
    this.firstlevel = 'Mode paiement';
    this.secondlevel = 'Ajouter un mode paiement';
    this.addForm = this.formBuilder.group({
      libellemodepaiement: ['', Validators.required],
    });
  }


  showToasterModePaiementEnregistrer() {
    this.notifyService.showSuccessWithTimeout('Mode paiement enregistré avec succès !!', 'Notification', 5000);
  }

  showToasterModePaiementErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }

  onSubmit() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.addForm.invalid) {
      this.showToasterModePaiementErreurModifier();
      return;
    }
    console.log(this.addForm.value);

    this.ApiModePaiement.addModePaiement(this.addForm.value).subscribe((modepaiement: Modepaiement) => {
      this.router.navigate(['start/modepaiements']);
      this.showToasterModePaiementEnregistrer();
    });

  }

}
