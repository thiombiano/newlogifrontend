import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutermodepaiementComponent } from './ajoutermodepaiement.component';

describe('AjoutermodepaiementComponent', () => {
  let component: AjoutermodepaiementComponent;
  let fixture: ComponentFixture<AjoutermodepaiementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjoutermodepaiementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutermodepaiementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
