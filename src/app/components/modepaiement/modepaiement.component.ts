import {ChangeDetectorRef, Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {NotificationService} from '../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {ModepaiementService} from './shared/modepaiement.service';
import {Modepaiement} from './shared/modepaiement';
import {MdbTableDirective, MdbTablePaginationComponent} from 'angular-bootstrap-md';
import {Article} from '../article/Shared/article';

@Component({
  selector: 'app-modepaiement',
  templateUrl: './modepaiement.component.html',
  styleUrls: ['./modepaiement.component.css']
})
export class ModepaiementComponent implements OnInit {
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  headElements = ['N°',  'Mode paiement', 'Action'];
  searchText = '';
  totalmodepaiement = 0;
  elements: any = [];
  modepaiements: Modepaiement[];
  previous: string;
  firstlevel: string;
  secondlevel: string;
  popoverTitle = 'Suppression de cet mode de paiement';
  popoverMessage = 'Voulez vous vraiment supprimer ce mode de paiement';
  cancelClicked = false;
  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, private toastr: ToastrService, private cdRef: ChangeDetectorRef, private ApiModePaiment: ModepaiementService, private router: Router) { }

  ngOnInit() {
    this.loadAllModePaiement();
  }

  loadAllModePaiement(): void {
    this.ApiModePaiment.getAllModePaiementList().subscribe(
      data => {
        this.modepaiements = data;
        console.log(this.modepaiements);
        this.firstlevel = 'Tableau de bord';
        this.secondlevel = 'Mode paiement';
        this.totalmodepaiement = this.modepaiements.length;
        this.mdbTable.setDataSource(this.modepaiements);
        this.elements = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();
      },
      err => console.error(err),
      () => console.log('liste des modes de paiement obtenue')
    );
  }


  @HostListener('input') oninput() {
    this.searchItems();
  }


  searchItems() {
    const prev = this.modepaiements;

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.elements = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(15);

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }


  editModePaiement(modepaiement: Modepaiement): void  {
    window.localStorage.removeItem('editModePaiementId');
    window.localStorage.setItem('editModePaiementId', modepaiement.id.toString());
    this.router.navigate(['start/modifier-modepaiements']);
  }

  showToasterModePaiementSupprimer() {
    this.notifyService.showSuccessWithTimeout('Mode de paiement supprimé avec succès !!', 'Notification', 5000);
  }

  deleteModePaiement(moedepaiement: Modepaiement) {
    this.ApiModePaiment.deleteModePaiement(moedepaiement).subscribe((data) => {
      this.modepaiements = this.modepaiements.filter(s => s !== moedepaiement);     this.loadAllModePaiement(); }
    );
    this.showToasterModePaiementSupprimer();
  }

}
