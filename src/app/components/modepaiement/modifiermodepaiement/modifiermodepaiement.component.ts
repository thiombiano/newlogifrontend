import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {ModepaiementService} from '../shared/modepaiement.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';
import {Article} from '../../article/Shared/article';
import {Modepaiement} from '../shared/modepaiement';

@Component({
  selector: 'app-modifiermodepaiement',
  templateUrl: './modifiermodepaiement.component.html',
  styleUrls: ['./modifiermodepaiement.component.css']
})
export class ModifiermodepaiementComponent implements OnInit {
  firstlevel: string;
  secondlevel: string;
  editForm: FormGroup;
  submitted = false;
  article: Article;
  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, private toastr: ToastrService, private cdRef: ChangeDetectorRef, public ApiModePaiement: ModepaiementService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {

    const modepaiementId = window.localStorage.getItem('editModePaiementId');
    if (!modepaiementId) {
      alert('Invalid action.');
      this.router.navigate(['start/modepaiements']);
      return;
    }

    console.log('ID MODEPAIEMENT :  ' + modepaiementId);

    this.firstlevel = 'Mode paiement';
    this.secondlevel = 'Modifier mode paiement';


    this.editForm = this.formBuilder.group({
      id: [''],
      libellemodepaiement: ['', Validators.required],
    });

    this.ApiModePaiement.getModePaiement(+modepaiementId).subscribe( (data) => {
      console.log(data);
      this.editForm.setValue(data);
    });

  }

  showToasterModePaiementModifier() {
    this.notifyService.showSuccessWithTimeout('Mode paiement modifié avec succès !!', 'Notification', 5000);
  }

  showToasterPaiementErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }

  onSubmit() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.editForm.invalid) {
      console.log('error form');
      this.showToasterPaiementErreurModifier();
    }
    const modepaiementId = window.localStorage.getItem('editModePaiementId');
    console.log(this.editForm.value);
    this.ApiModePaiement.updateModePaiement(+modepaiementId, this.editForm.value)
      .pipe(first())
      .subscribe((modepaiement: Modepaiement) => {
        // @ts-ignore
        console.log(modepaiement);
        this.showToasterModePaiementModifier();
        this.router.navigate(['start/modepaiements']);
      });
  }

}
