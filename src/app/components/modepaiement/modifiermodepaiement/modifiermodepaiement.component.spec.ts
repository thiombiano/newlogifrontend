import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifiermodepaiementComponent } from './modifiermodepaiement.component';

describe('ModifiermodepaiementComponent', () => {
  let component: ModifiermodepaiementComponent;
  let fixture: ComponentFixture<ModifiermodepaiementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifiermodepaiementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifiermodepaiementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
