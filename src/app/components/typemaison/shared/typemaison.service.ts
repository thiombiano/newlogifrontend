import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {Typemaison} from './Typemaison.model';

@Injectable({
  providedIn: 'root'
})
export class TypemaisonService {

  constructor(private httpclient: HttpClient) { }

  getAllTypeMaisonList() {
    return  this.httpclient.get<Typemaison[]>(environment.apiUrl + '/typemaisons')
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Impossible les types maisons à obtenir!'))
      );
  }



  addTypeMaison(typemaison: Typemaison) {
    return  this.httpclient.post<Typemaison>(environment.apiUrl + '/typemaisons', typemaison);
  }

  deleteTypeMaison(typemaison: Typemaison) {
    // @ts-ignore
    return this.httpclient.delete<Typemaison>(environment.apiUrl + '/typemaisons/' + typemaison.id);
  }

  updateTypeMaison(id, value: any) {
    return this.httpclient.put<Typemaison>(environment.apiUrl + '/typemaisons/' + id, value );
  }


  getTypeMaison(id: number) {
    return  this.httpclient.get<Typemaison>(environment.apiUrl + '/typemaisons/' + id)
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Impossible obtenir un seul type de maison!'))
      );
  }
}
