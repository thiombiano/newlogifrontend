import { TestBed } from '@angular/core/testing';

import { TypemaisonService } from './typemaison.service';

describe('TypemaisonService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TypemaisonService = TestBed.get(TypemaisonService);
    expect(service).toBeTruthy();
  });
});
