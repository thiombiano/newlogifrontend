import {Etape} from '../../etapeconstruction/shared/etape.model';

export class Typemaison {
  id: number;
  libelletypemaison: string;
  description: string;
  etapes: Etape[];
}
