import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifiertypemaisonComponent } from './modifiertypemaison.component';

describe('ModifiertypemaisonComponent', () => {
  let component: ModifiertypemaisonComponent;
  let fixture: ComponentFixture<ModifiertypemaisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifiertypemaisonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifiertypemaisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
