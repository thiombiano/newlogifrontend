import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Permission} from '../../permission/shared/permission.model';
import {Etape} from '../../etapeconstruction/shared/etape.model';
import {NotificationService} from '../../../utility/notification.service';
import {PermissionService} from '../../permission/shared/permission.service';
import {ToastrService} from 'ngx-toastr';
import {RoleService} from '../../role/shared/role.service';
import {Router} from '@angular/router';
import {TypemaisonService} from '../shared/typemaison.service';
import {EtapeService} from '../../etapeconstruction/shared/etape.service';
import {first} from 'rxjs/operators';
import {Role} from '../../role/shared/role.model';
import {Typemaison} from '../shared/Typemaison.model';

@Component({
  selector: 'app-modifiertypemaison',
  templateUrl: './modifiertypemaison.component.html',
  styleUrls: ['./modifiertypemaison.component.css']
})
export class ModifiertypemaisonComponent implements OnInit {
  firstlevel: string;
  secondlevel: string;
  editForm: FormGroup;
  lesetapes: Etape[];
  etapes: Etape[];
  submitted = false;
  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, public ApiEtape: EtapeService,  private toastr: ToastrService, private cdRef: ChangeDetectorRef, public ApiTypeMaison: TypemaisonService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.ApiEtape.getAllEtapeList().subscribe((etapes: Etape[]) => {
      this.lesetapes = etapes;
    });
    this.firstlevel = 'Type Maison';
    this.secondlevel = 'Modifier type maison';
    const typemaisonId = window.localStorage.getItem('editTypeMaisonId');
    if (!typemaisonId) {
      alert('Invalid action.');
      this.router.navigate(['start/etapes']);
      return;
    }
    this.editForm = this.formBuilder.group({
      id: [],
      libelletypemaison: ['', Validators.required],
      description: [''],
      etapes: ['', Validators.required],
    });

    this.ApiTypeMaison.getTypeMaison(+typemaisonId).subscribe( (data) => {
      console.log('lespermissions pour le role 1 : ' +   this.lesetapes);
      this.editForm.get('id').setValue(data.data[0].id);
      this.editForm.get('libelletypemaison').setValue(data.data[0].libelletypemaison);
      this.editForm.get('description').setValue(data.data[0].description);
      this.editForm.get('etapes').setValue(data.data[0].etapes);
    });
  }


  compareFn(etape1: Etape, etape2: Etape) {
    return etape1 && etape2 ? etape1.id === etape2.id : etape1 === etape2;
  }

  showToasterTypeMaisonEnregistrer() {
    this.notifyService.showSuccessWithTimeout('Type Maison mise à jour avec succès !!', 'Notification', 5000);
  }

  showToasterTypeMaisonErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }

  onSubmit() {

    this.submitted = true;
    console.log(this.editForm.value);
    // stop here if form is invalid
    if (this.editForm.invalid) {
      console.log('error form');
      this.showToasterTypeMaisonErreurModifier();
      return;
    }
    const typemaisonId = window.localStorage.getItem('editTypeMaisonId');
    console.log(this.editForm.value);
    this.ApiTypeMaison.updateTypeMaison(+typemaisonId, this.editForm.value)
      .pipe(first())
      .subscribe((typemaison: Typemaison) => {
        // @ts-ignore
        console.log(typemaison);
        this.showToasterTypeMaisonEnregistrer();
        this.router.navigate(['start/typemaisons']);
      });
  }

}
