import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypemaisonComponent } from './typemaison.component';

describe('TypemaisonComponent', () => {
  let component: TypemaisonComponent;
  let fixture: ComponentFixture<TypemaisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypemaisonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypemaisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
