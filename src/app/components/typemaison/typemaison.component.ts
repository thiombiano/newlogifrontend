import {ChangeDetectorRef, Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {MdbTableDirective, MdbTablePaginationComponent} from 'angular-bootstrap-md';
import {NotificationService} from '../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {RoleService} from '../role/shared/role.service';
import {Router} from '@angular/router';
import {TypemaisonService} from './shared/typemaison.service';
import {Role} from '../role/shared/role.model';
import {Typemaison} from './shared/Typemaison.model';

@Component({
  selector: 'app-typemaison',
  templateUrl: './typemaison.component.html',
  styleUrls: ['./typemaison.component.css']
})
export class TypemaisonComponent implements OnInit {
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, private toastr: ToastrService, private cdRef: ChangeDetectorRef, public ApiTypeMaison: TypemaisonService, private router: Router) { }
  headElements = ['N°', 'Type Maison', 'Etapes', 'Action'];
  searchText = '';
  typesmaisons: Typemaison[];
  totaltypemaisons = 0;
  elements: any = [];
  previous: string;
  firstlevel: string;
  secondlevel: string;
  popoverTitle = 'Suppression de ce type de maison';
  popoverMessage = 'Voulez vous vraiment supprimer ce type de maison';
  cancelClicked = false;
  ngOnInit() {
    this.loadAllTypeMaison();
  }

  loadAllTypeMaison(): void {
    // @ts-ignore
    this.ApiTypeMaison.getAllTypeMaisonList().subscribe(
      data => {
        this.typesmaisons = data.data;
        console.log(this.typesmaisons);
        this.firstlevel = 'Tableau de bord';
        this.secondlevel = 'Type maison';
        this.totaltypemaisons = this.typesmaisons.length;
        this.mdbTable.setDataSource(this.typesmaisons);
        this.elements = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();
      },
      err => console.error(err),
      () => console.log('Liste des types de maisons obtenues')
    );
  }


  @HostListener('input') oninput() {
    this.searchItems();
  }

  searchItems() {
    const prev = this.typesmaisons;

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.elements = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(15);

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }



  editTypeMaison(role: Role): void  {
    window.localStorage.removeItem('editTypeMaisonId');
    window.localStorage.setItem('editTypeMaisonId', role.id.toString());
    this.router.navigate(['start/modifier-typemaison']);
  }

  showToasterRoleSupprimer() {
    this.notifyService.showSuccessWithTimeout('Type maison supprimé avec succès !!', 'Notification', 5000);
  }

  deleteTypeMaison(typemaison: Typemaison) {
    this.ApiTypeMaison.deleteTypeMaison(typemaison).subscribe((data) => {
      this.typesmaisons = this.typesmaisons.filter(s => s !== typemaison);     this.loadAllTypeMaison(); }
    );
    this.showToasterRoleSupprimer();
  }


}
