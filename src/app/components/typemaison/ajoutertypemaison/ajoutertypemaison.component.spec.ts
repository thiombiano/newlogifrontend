import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutertypemaisonComponent } from './ajoutertypemaison.component';

describe('AjoutertypemaisonComponent', () => {
  let component: AjoutertypemaisonComponent;
  let fixture: ComponentFixture<AjoutertypemaisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjoutertypemaisonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutertypemaisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
