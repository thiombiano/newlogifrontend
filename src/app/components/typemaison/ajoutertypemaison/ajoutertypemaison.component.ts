import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Role} from '../../role/shared/role.model';
import {Permission} from '../../permission/shared/permission.model';
import {Etape} from '../../etapeconstruction/shared/etape.model';
import {NotificationService} from '../../../utility/notification.service';
import {PermissionService} from '../../permission/shared/permission.service';
import {ToastrService} from 'ngx-toastr';
import {RoleService} from '../../role/shared/role.service';
import {Router} from '@angular/router';
import {EtapeService} from '../../etapeconstruction/shared/etape.service';
import {TypemaisonService} from '../shared/typemaison.service';
import {Typemaison} from '../shared/Typemaison.model';

@Component({
  selector: 'app-ajoutertypemaison',
  templateUrl: './ajoutertypemaison.component.html',
  styleUrls: ['./ajoutertypemaison.component.css']
})
export class AjoutertypemaisonComponent implements OnInit {
  firstlevel: string;
  secondlevel: string;
  addForm: FormGroup;
  submitted = false;
  role: Role;
  etapes: Etape[];
  toppings: FormControl;
  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, public ApiEtape: EtapeService,  private toastr: ToastrService, private cdRef: ChangeDetectorRef, public ApiTypeMaison: TypemaisonService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.ApiEtape.getAllEtapeList().subscribe((etapes: Etape[]) => {
      this.etapes = etapes;
    });
    this.toppings = new FormControl();
    this.firstlevel = 'Type maison';
    this.secondlevel = 'Ajouter type maison';
    this.addForm = this.formBuilder.group({
      libelletypemaison: ['', Validators.required],
      description: [''],
      etapes: ['', Validators.required],
    });
  }

  showToasterTypeMaisonEnregistrer() {
    this.notifyService.showSuccessWithTimeout('Type maison enregistré avec succès !!', 'Notification', 5000);
  }

  showToasterTypeMaisonErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }


  onSubmit() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.addForm.invalid) {
      this.showToasterTypeMaisonErreurModifier();
      return;
    }
    console.log(this.addForm.value);
    this.ApiTypeMaison.addTypeMaison(this.addForm.value).subscribe((typemaison: Typemaison) => {
      this.router.navigate(['start/typemaisons']);
      this.showToasterTypeMaisonEnregistrer();
    });

  }


}
