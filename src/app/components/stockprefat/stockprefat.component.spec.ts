import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockprefatComponent } from './stockprefat.component';

describe('StockprefatComponent', () => {
  let component: StockprefatComponent;
  let fixture: ComponentFixture<StockprefatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockprefatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockprefatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
