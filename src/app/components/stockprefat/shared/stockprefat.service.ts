import { Injectable } from '@angular/core';
import {Stocks} from '../../stocks/shared/stocks';
import {MouvementStock} from '../../stocks/shared/mouvement-stock';
import {Stockprefat} from './stockprefat';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {MouvementStockEmplacement} from './mouvement-stock-emplacement';

@Injectable({
  providedIn: 'root'
})
export class StockprefatService {
  stockPrefatArticle: Stockprefat;
  listChantierStocksPrefat: Stockprefat[];
  listChantierStocksFiltreePrefat: Stockprefat[];
  listChantierArticleMvtStocksPrefat: MouvementStockEmplacement[];
  constructor(private http: HttpClient) { }

  getChantierStocksPrefatList(idChantier: number) {
    this.http
      .get(environment.apiUrl + '/chantiers/' + idChantier + '/inventairesprefat')
      .toPromise()
      .then((x: StocksPrefatApiResponse) => {
        this.listChantierStocksPrefat = x.data;
        this.listChantierStocksFiltreePrefat = x.data;
        console.log('Stock: ' + JSON.stringify(this.listChantierStocksPrefat));

      });
  }

  getChantierArticleMouvementStocksPrefat(idChantier: number, idArticle: number) {
    this.http
      .get(environment.apiUrl + '/chantiers/' + idChantier + '/articles/' + idArticle)
      .toPromise()
      .then((x: MvtStocksPrefatApiResponse) => {
        this.listChantierArticleMvtStocksPrefat = x.data;
      });
  }

  getChantierArticleStockPrefat(idChantier: number, idArticle: number) {
    this.http
      .get(environment.apiUrl + '/chantiers/' + idChantier + '/inventaires/' + idArticle)
      .toPromise()
      .then((x: Stockprefat) => {
        // tslint:disable-next-line: comment-format
        //console.log("data: ", x);
        this.stockPrefatArticle = x;
      });
  }

  filtrerStock(term: string) {
    this.listChantierStocksFiltreePrefat = this.listChantierStocksPrefat.filter(stk =>
      stk.article.toLowerCase().startsWith(term.toLowerCase())
    );
  }
}

interface StocksPrefatApiResponse {
  data: Stockprefat[];
}

interface MvtStocksPrefatApiResponse {
  data: MouvementStockEmplacement[];
}
