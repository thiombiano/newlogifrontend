import { TestBed } from '@angular/core/testing';

import { StockprefatService } from './stockprefat.service';

describe('StockprefatService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StockprefatService = TestBed.get(StockprefatService);
    expect(service).toBeTruthy();
  });
});
