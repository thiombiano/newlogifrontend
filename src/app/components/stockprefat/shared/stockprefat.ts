export class Stockprefat {

  id: number;
  emplacement: string;
  emplacement_id: number;
  article: string;
  article_id: number;
  quantiteDisponible: number;
  stockMinimal: number;
  commentaires: string;

}
