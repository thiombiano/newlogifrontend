import { Component, OnInit } from '@angular/core';
import {Article} from '../../article/Shared/article';
import {StocksService} from '../../stocks/shared/stocks.service';
import {ArticleService} from '../../article/Shared/article.service';
import {ActivatedRoute, Router} from '@angular/router';
import {StockprefatService} from '../shared/stockprefat.service';

@Component({
  selector: 'app-mouvement-stocks-prefat',
  templateUrl: './mouvement-stocks-prefat.component.html',
  styleUrls: ['./mouvement-stocks-prefat.component.css']
})
export class MouvementStocksPrefatComponent implements OnInit {
  idChantier: number;
  idArticle: number;
  article: Article;
  firstlevel: string;
  secondlevel: string;
  thirdlevel: string;
  headElements = ['ID', 'Date', 'Type' , 'Doc Ref', 'Entrée', 'Sortie', 'Stock', 'Status'];
  constructor(public stockPrefatService: StockprefatService,
              public articleService: ArticleService,
              private router: Router,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.firstlevel = 'Tableau de bord';
    this.thirdlevel = 'Stocks';
    this.secondlevel = 'Suivi du stocks';
    this.idChantier = +window.localStorage.getItem('LIdChantierP');
    this.idArticle = +window.localStorage.getItem('idStockP');
    this.stockPrefatService.getChantierArticleMouvementStocksPrefat(
      this.idChantier,
      this.idArticle
    );
    console.log(this.idChantier, this.idArticle );
    this.articleService.getLArticle(this.idArticle);
    this.article = this.articleService.selectedArticle;
    this.stockPrefatService.getChantierArticleStockPrefat(this.idChantier, this.idArticle);
  }

}
