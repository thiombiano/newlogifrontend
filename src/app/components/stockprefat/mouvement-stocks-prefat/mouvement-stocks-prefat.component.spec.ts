import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MouvementStocksPrefatComponent } from './mouvement-stocks-prefat.component';

describe('MouvementStocksPrefatComponent', () => {
  let component: MouvementStocksPrefatComponent;
  let fixture: ComponentFixture<MouvementStocksPrefatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MouvementStocksPrefatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MouvementStocksPrefatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
