import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {MdbTableDirective, MdbTablePaginationComponent} from 'angular-bootstrap-md';
import {Stocks} from '../stocks/shared/stocks';
import {Stockprefat} from './shared/stockprefat';
import {ChantierService} from '../chantier/shared/chantier.service';
import {StocksService} from '../stocks/shared/stocks.service';
import {Router} from '@angular/router';
import {StockprefatService} from './shared/stockprefat.service';

@Component({
  selector: 'app-stockprefat',
  templateUrl: './stockprefat.component.html',
  styleUrls: ['./stockprefat.component.css']
})
export class StockprefatComponent implements OnInit {
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  stocksprefat: Stockprefat[];
  searchText = '';
  totalstocks = 0;
  elements: any = [];
  previous: string;
  firstlevel: string;
  secondlevel: string;
  // tslint:disable-next-line:max-line-length
  constructor(public ChantierAPI: ChantierService, private cdRef: ChangeDetectorRef, private ApiStockPrefat: StockprefatService, private router: Router) { }

  headElements = ['N°', 'Référence', 'Désignation' , 'Seuil', 'Stock Dispo', 'Status'];

  ngOnInit() {
    this.firstlevel = 'Tableau de bord';
    this.secondlevel = 'Stocks de la préfat';
    const idchantier = window.localStorage.getItem('Idchantier');
    const theid = +idchantier;
    this.ApiStockPrefat.getChantierStocksPrefatList(theid);
  }

  valuechange($event) {
    console.log($event.target.value);
    this.ApiStockPrefat.filtrerStock($event.target.value);
  }

  detailsstockprefat(idChantier, idStockP) {
    window.localStorage.removeItem('LIdChantierP');
    window.localStorage.removeItem('idStockP');
    window.localStorage.setItem('idStockP', idStockP);
    window.localStorage.setItem('LIdChantierP', idChantier);
    this.router.navigate(['start/mouvement-stocks-prefat']);
  }

  get listChantierStocksPrefatFiltree() {
    return this.ApiStockPrefat.listChantierStocksFiltreePrefat;
  }
}
