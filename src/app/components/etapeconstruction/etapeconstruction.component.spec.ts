import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EtapeconstructionComponent } from './etapeconstruction.component';

describe('EtapeconstructionComponent', () => {
  let component: EtapeconstructionComponent;
  let fixture: ComponentFixture<EtapeconstructionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtapeconstructionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtapeconstructionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
