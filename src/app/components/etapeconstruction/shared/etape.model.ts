export class Etape {
  id: number;
  libelleetape: string;
  description: string;
  montant_brut: number;
  montant_macon: number;
  montant_verse_entreprise: number;
}
