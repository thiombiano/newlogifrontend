import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {catchError, map} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import {Etape} from './etape.model';
import {Article} from '../../article/Shared/article';

@Injectable({
  providedIn: 'root'
})
export class EtapeService {
  public selectedOneEtape: Etape;

  constructor(private httpclient: HttpClient) { }

  getAllEtapeList() {
    return  this.httpclient.get<Etape[]>(environment.apiUrl + '/etapes')
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Its a Trap!'))
      );
  }

  getAllEtapeListByIdMaison($idmaison) {
    return  this.httpclient.get<Etape[]>(environment.apiUrl + '/getetapebymaison/' + $idmaison)
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Impossible dobtenir les etapes!'))
      );
  }


  getAllEtapeListByTypeMaison($idtypemaison) {
    return  this.httpclient.get<Etape[]>(environment.apiUrl + '/getetapebytypemaison/' + $idtypemaison)
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Impssoble dobtenir les etapes par type de maison!'))
      );
  }


  getAllEtapeDisponibleByMaison($idmaison) {
    return  this.httpclient.get<Etape[]>(environment.apiUrl + '/getetapedisponiblebymaison/' + $idmaison)
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Impssoble dobtenir les etapes disponible pour la maison!'))
      );
  }



  getEtapeForBesoin(id: number) {
    return  this.httpclient.get<Etape>(environment.apiUrl + '/etapes/' + id)
      .toPromise()
      .then(x => {
        this.selectedOneEtape = x;
      });
  }

  updateEtape(id: number, value: any): Observable<Etape> {
    return this.httpclient
      .put<Etape>(environment.apiUrl + '/etapes/' + id, value);
  }

  getEtape(id: number) {
    return  this.httpclient.get<Etape>(environment.apiUrl + '/etapes/' + id);
  }



  addEtape(etape: Etape) {
    return  this.httpclient.post<Etape>(environment.apiUrl + '/etapes', etape);
  }

  deleteEtape(etape: Etape) {
    // @ts-ignore
    return this.httpclient.delete<Etape>(environment.apiUrl + '/etapes/' + etape.id);
  }
}
