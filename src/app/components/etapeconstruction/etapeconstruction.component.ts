import {ChangeDetectorRef, Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {MdbTableDirective, MdbTablePaginationComponent} from 'angular-bootstrap-md';
import {Etape} from './shared/etape.model';
import {NotificationService} from '../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {EtapeService} from './shared/etape.service';
@Component({
  selector: 'app-etapeconstruction',
  templateUrl: './etapeconstruction.component.html',
  styleUrls: ['./etapeconstruction.component.css']
})
export class EtapeconstructionComponent implements OnInit {
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  headElements = ['N°',  'Etape', 'Montant brut', 'Montant des maisons', 'Montant versées  à entreprise', 'Description', 'Action'];
  searchText = '';
  totaletape = 0;
  elements: any = [];
  etapes: Etape[];
  previous: string;
  firstlevel: string;
  secondlevel: string;
  popoverTitle = 'Suppression de cet mode de paiement';
  popoverMessage = 'Voulez vous vraiment supprimer ce mode de paiement';
  cancelClicked = false;
  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, private toastr: ToastrService, private cdRef: ChangeDetectorRef, private ApiEtape: EtapeService, private router: Router) { }

  ngOnInit() {
    this.loadAllEtapes();
  }


  loadAllEtapes(): void {
    this.ApiEtape.getAllEtapeList().subscribe(
      data => {
        this.etapes = data;
        console.log(this.etapes);
        this.firstlevel = 'Tableau de bord';
        this.secondlevel = 'Etapes';
        this.totaletape = this.etapes.length;
        this.mdbTable.setDataSource(this.etapes);
        this.elements = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();
      },
      err => console.error(err),
      () => console.log('liste des étapes obtenue')
    );
  }


  @HostListener('input') oninput() {
    this.searchItems();
  }


  searchItems() {
    const prev = this.etapes;

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.elements = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(15);

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }


  editEtape(etape: Etape): void  {
    window.localStorage.removeItem('editEtapeId');
    window.localStorage.setItem('editEtapeId', etape.id.toString());
    this.router.navigate(['start/modifier-etape']);
  }

  showToasterEtapeSupprimer() {
    this.notifyService.showSuccessWithTimeout('Etape supprimé avec succès !!', 'Notification', 5000);
  }

  deleteEtape(etape: Etape) {
    this.ApiEtape.deleteEtape(etape).subscribe((data) => {
      this.etapes = this.etapes.filter(s => s !== etape);     this.loadAllEtapes(); }
    );
    this.showToasterEtapeSupprimer();
  }

}
