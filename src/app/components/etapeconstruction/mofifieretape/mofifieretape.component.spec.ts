import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MofifieretapeComponent } from './mofifieretape.component';

describe('MofifieretapeComponent', () => {
  let component: MofifieretapeComponent;
  let fixture: ComponentFixture<MofifieretapeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MofifieretapeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MofifieretapeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
