import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {EtapeService} from '../shared/etape.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Etape} from '../shared/etape.model';
import {first} from 'rxjs/operators';


@Component({
  selector: 'app-mofifieretape',
  templateUrl: './mofifieretape.component.html',
  styleUrls: ['./mofifieretape.component.css']
})
export class MofifieretapeComponent implements OnInit {
  firstlevel: string;
  secondlevel: string;
  editForm: FormGroup;
  submitted = false;
  etape: Etape;
  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, private toastr: ToastrService, private cdRef: ChangeDetectorRef, public ApiEtape: EtapeService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    const etapeId = window.localStorage.getItem('editEtapeId');
    if (!etapeId) {
      alert('Invalid action.');
      this.router.navigate(['start/etapes']);
      return;
    }

    this.firstlevel = 'Etape';
    this.secondlevel = 'Modifier une étape';

    this.editForm = this.formBuilder.group({
      id: [''],
      libelleetape: ['', Validators.required],
      description: [''],
      montant_brut: [''],
      montant_macon: [''],
      montant_verse_entreprise: ['']
    });

    this.ApiEtape.getEtape(+etapeId).subscribe( (data) => {
      console.log(data);
      this.editForm.setValue(data);
    });
  }


  showToasterEtapeModifier() {
    this.notifyService.showSuccessWithTimeout('Etape modifiée avec succès !!', 'Notification', 5000);
  }

  showToasterEtapeErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }


  onSubmit() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.editForm.invalid) {
      console.log('error form');
      this.showToasterEtapeErreurModifier();
    }
    const etapeId = window.localStorage.getItem('editEtapeId');
    console.log(this.editForm.value);
    this.ApiEtape.updateEtape(+etapeId, this.editForm.value)
      .pipe(first())
      .subscribe((etape: Etape) => {
        // @ts-ignore
        console.log(etape);
        this.showToasterEtapeModifier();
        this.router.navigate(['start/etapes']);
      });
  }

}
