import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {ModepaiementService} from '../../modepaiement/shared/modepaiement.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {EtapeService} from '../shared/etape.service';
import {Modepaiement} from '../../modepaiement/shared/modepaiement';
import {Etape} from '../shared/etape.model';

@Component({
  selector: 'app-ajouteretape',
  templateUrl: './ajouteretape.component.html',
  styleUrls: ['./ajouteretape.component.css']
})
export class AjouteretapeComponent implements OnInit {
  firstlevel: string;
  secondlevel: string;
  addForm: FormGroup;
  submitted = false;
  etape: Etape;
  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, private toastr: ToastrService, private cdRef: ChangeDetectorRef, public ApiEtape: EtapeService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.firstlevel = 'Etape';
    this.secondlevel = 'Ajouter une étape de construction';
    this.addForm = this.formBuilder.group({
      libelleetape: ['', Validators.required],
      description: [''],
      montant_brut: [''],
      montant_macon: [''],
      montant_verse_entreprise: ['']
    });
  }

  showToasterEtapeEnregistrer() {
    this.notifyService.showSuccessWithTimeout('Etape enregistrée avec succès !!', 'Notification', 5000);
  }

  showToasterEtapeErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }

  onSubmit() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.addForm.invalid) {
      this.showToasterEtapeErreurModifier();
      return;
    }
    console.log(this.addForm.value);

    this.ApiEtape.addEtape(this.addForm.value).subscribe((etape: Etape) => {
      this.router.navigate(['start/etapes']);
      this.showToasterEtapeEnregistrer();
    });

  }

}
