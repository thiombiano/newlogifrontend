import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjouteretapeComponent } from './ajouteretape.component';

describe('AjouteretapeComponent', () => {
  let component: AjouteretapeComponent;
  let fixture: ComponentFixture<AjouteretapeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjouteretapeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjouteretapeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
