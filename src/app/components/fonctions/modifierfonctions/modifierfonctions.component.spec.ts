import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifierfonctionsComponent } from './modifierfonctions.component';

describe('ModifierfonctionsComponent', () => {
  let component: ModifierfonctionsComponent;
  let fixture: ComponentFixture<ModifierfonctionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifierfonctionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifierfonctionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
