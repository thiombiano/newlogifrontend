import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {ArticleService} from '../../article/Shared/article.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FonctionsService} from '../shared/fonctions.service';
import {first} from 'rxjs/operators';
import {Article} from '../../article/Shared/article';
import {Fonction} from '../shared/fonction';

@Component({
  selector: 'app-modifierfonctions',
  templateUrl: './modifierfonctions.component.html',
  styleUrls: ['./modifierfonctions.component.css']
})
export class ModifierfonctionsComponent implements OnInit {
  editForm: FormGroup;
  submitted: boolean;
  firstlevel: string;
  secondlevel: string;
  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, private toastr: ToastrService, private cdRef: ChangeDetectorRef, private ApiFonction: FonctionsService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {

    this.firstlevel = 'Fonction';
    this.secondlevel = 'Modifier fonction';

    const fonctionId = window.localStorage.getItem('editFonctionId');
    if (!fonctionId) {
      alert('Invalid action.');
      this.router.navigate(['start/fonctions']);
      return;
    }

    this.editForm = this.formBuilder.group({
      id: [''],
      codeFonction: ['', Validators.required],
      libelleFonction: ['', Validators.required],
    });

    this.ApiFonction.getFonctionById(+fonctionId).subscribe( (data) => {
      console.log(data);
      this.editForm.setValue(data);
    });
  }


  showToasterFonctionModifier() {
    this.notifyService.showSuccessWithTimeout('Fonction modifiée avec succès !!', 'Notification', 5000);
  }

  showToasterFonctionErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }

  onSubmit() {
    this.submitted = true;
    if (this.editForm.invalid) {
      console.log('error form');
      this.showToasterFonctionErreurModifier();
    }
    const fonctionId = window.localStorage.getItem('editFonctionId');
    console.log(this.editForm.value);
    this.ApiFonction.updateFonction(+fonctionId, this.editForm.value)
      .pipe(first())
      .subscribe((fonction: Fonction) => {
        // @ts-ignore
        console.log(fonction);
        this.showToasterFonctionModifier();
        this.router.navigate(['start/fonctions']);
      });
  }

}
