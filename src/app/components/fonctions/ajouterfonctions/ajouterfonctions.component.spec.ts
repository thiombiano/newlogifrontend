import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjouterfonctionsComponent } from './ajouterfonctions.component';

describe('AjouterfonctionsComponent', () => {
  let component: AjouterfonctionsComponent;
  let fixture: ComponentFixture<AjouterfonctionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjouterfonctionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjouterfonctionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
