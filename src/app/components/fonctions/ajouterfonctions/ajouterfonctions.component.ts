import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {ArticleService} from '../../article/Shared/article.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FonctionsService} from '../shared/fonctions.service';
import {Article} from '../../article/Shared/article';
import {Fonction} from '../shared/fonction';

@Component({
  selector: 'app-ajouterfonctions',
  templateUrl: './ajouterfonctions.component.html',
  styleUrls: ['./ajouterfonctions.component.css']
})
export class AjouterfonctionsComponent implements OnInit {
  firstlevel: string;
  addForm: FormGroup;
  secondlevel: string;
  submitted: boolean;
  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, private toastr: ToastrService, private cdRef: ChangeDetectorRef, private ApiFonction: FonctionsService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.firstlevel = 'Fonction';
    this.secondlevel = 'Ajouter fonction';
    this.addForm = this.formBuilder.group({
      codeFonction: ['', Validators.required],
      libelleFonction: ['', Validators.required],
    });
  }

  showToasterFonctionEnregistrer() {
    this.notifyService.showSuccessWithTimeout('Fonction enregistré avec succès !!', 'Notification', 5000);
  }

  showToasterFonctionErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }

  onSubmit() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.addForm.invalid) {
      this.showToasterFonctionErreurModifier();
      return;
    }
    console.log(this.addForm.value);
    this.ApiFonction.addFonction(this.addForm.value).subscribe((fonction: Fonction) => {
      this.router.navigate(['start/fonctions']);
      this.showToasterFonctionEnregistrer();
    });

  }

}
