import {ChangeDetectorRef, Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {Fonction} from './shared/fonction';
import {FonctionsService} from './shared/fonctions.service';
import {MdbTableDirective, MdbTablePaginationComponent} from 'angular-bootstrap-md';
import {NotificationService} from '../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {ArticleService} from '../article/Shared/article.service';
import {Router} from '@angular/router';
import {Article} from '../article/Shared/article';

@Component({
  selector: 'app-fonctions',
  templateUrl: './fonctions.component.html',
  styleUrls: ['./fonctions.component.css']
})
export class FonctionsComponent implements OnInit {
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  fonctions: Fonction[];
  elements: any = [];
  previous: string;
  searchText = '';
  firstlevel: string;
  secondlevel: string;
  headElements = ['N°', 'Code fonction', 'Libellé fontion', 'Action'];
  popoverTitle = 'Suppression de cet article';
  popoverMessage = 'Voulez vous vraiment supprimer cet article';
  cancelClicked = false;
  totalfonction: number;
  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, private router: Router, private toastr: ToastrService, private cdRef: ChangeDetectorRef, private apiFonction: FonctionsService) { }

  LoadAllFonction() {
    // @ts-ignore
    this.apiFonction.getAllFonction().subscribe((fonctions: Fonction []) => {
      this.fonctions = fonctions;
      this.totalfonction = fonctions.length;
      this.firstlevel = 'Tableau de bord';
      this.secondlevel = 'Fonctions';
      this.mdbTable.setDataSource(fonctions);
      this.elements = this.mdbTable.getDataSource();
      this.previous = this.mdbTable.getDataSource();
    });
  }

  @HostListener('input') oninput() {
    this.searchItems();
  }

  get ListFonction() {
    return this.apiFonction.getAllFonction();
  }

  searchItems() {
    const prev = this.fonctions;

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.elements = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(6);

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }


  editFonction(fonction: Fonction): void  {
    window.localStorage.removeItem('editFonctionId');
    window.localStorage.setItem('editFonctionId', fonction.id.toString());
    this.router.navigate(['start/modifier-fonction']);
  }

  showToasterFonctionSupprimer() {
    this.notifyService.showSuccessWithTimeout('Fonction supprimé avec succès !!', 'Notification', 5000);
  }

  deleteFonction(fonction: Fonction) {
    console.log(fonction);
    this.apiFonction.deleteFontion(fonction).subscribe((data) => {
      this.fonctions = this.fonctions.filter(s => s !== fonction);
      this.LoadAllFonction(); });
    this.showToasterFonctionSupprimer();
  }

  ngOnInit() {
    this.LoadAllFonction();
  }

}
