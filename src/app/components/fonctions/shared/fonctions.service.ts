import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Article} from '../../article/Shared/article';
import {environment} from '../../../../environments/environment';
import {Fonction} from './fonction';
import {Observable} from 'rxjs';
import {Maison} from '../../maison/shared/maison.model';
import {Employe} from '../../employe/shared/employe.model';
import {Personnel} from '../../personnels/shared/personnel';

@Injectable({
  providedIn: 'root'
})
export class FonctionsService {
  public selectedOneFonction: Fonction;
  constructor(private http: HttpClient) { }

  getAllFonction() {
    return this.http.get<Fonction[]>(environment.apiUrl + '/fonctions');
  }

  deleteFontion(fonction: Fonction) {
    return this.http.delete<Fonction>(environment.apiUrl + '/fonctions/' + fonction.id);
  }

  getFonctionById(id): Observable<Fonction> {
    return this.http.get<Fonction>(environment.apiUrl + '/fonctions/' + id);
  }

  // @ts-ignore
  updateFonction(id: number , value: any): Observable<Fonction>  {
    return this.http.put<Fonction>(environment.apiUrl + '/fonctions/' + id, value);
  }

  addFonction(fonction: Fonction) {
   return  this.http.post<Fonction>(environment.apiUrl + '/fonctions', fonction);
  }

  getFonctionForBesoin(id: number) {
    return  this.http.get<Fonction>(environment.apiUrl + '/fonctions/' + id)
      .toPromise()
      .then(x => {
        this.selectedOneFonction = x;
      });
  }

  getFonctionByMaison(idmaison: number) {
    return  this.http.get<Fonction[]>(environment.apiUrl + '/fonctions/getfonctionbyidmaison/' + idmaison);
  }

  getFonctionByPrefat(idprefat: number) {
    return  this.http.get<Fonction[]>(environment.apiUrl + '/fonctions/getfonctionbyidprefat/' + idprefat);
  }

  getFonctionByMaisonOne(idmaison: number) {
    return  this.http.get<Fonction[]>(environment.apiUrl + '/fonctions/getmaisonbyidmaison/' + idmaison);
  }

  getPrestataireByMaisonAndByFonction(idmaison: number, idfonction: number) {
    // tslint:disable-next-line:max-line-length
    return this.http.get<Personnel[]>(environment.apiUrl + '/fonctions/getprestatairebyidmaisonandidfonction/' + idmaison  + '/' + idfonction);
  }

  getPrestataireByPrefatAndByFonction(idprefat: number, idfonction: number) {
    // tslint:disable-next-line:max-line-length
    return this.http.get<Personnel[]>(environment.apiUrl + '/fonctions/getprestatairebyidprefatandidfonction/' + idprefat  + '/' + idfonction);
  }

}
