import { TestBed } from '@angular/core/testing';

import { FonctionsService } from './fonctions.service';

describe('FonctionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FonctionsService = TestBed.get(FonctionsService);
    expect(service).toBeTruthy();
  });
});
