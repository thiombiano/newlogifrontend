import {ChangeDetectorRef, Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {MdbTableDirective, MdbTablePaginationComponent} from 'angular-bootstrap-md';
import {NotificationService} from '../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {AuditingService} from './shared/auditing.service';
import {Auditing} from './shared/auditing.model';

@Component({
  selector: 'app-auditing',
  templateUrl: './auditing.component.html',
  styleUrls: ['./auditing.component.css']
})
export class AuditingComponent implements OnInit {
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  // tslint:disable-next-line:max-line-length
  headElements = ['ID',  'Utilisateur',  'Email', 'Adresse IP' , 'Evènement', 'Url', 'Navigateur' , 'Anciennes données' , 'Nouvelles données', 'Action'];
  searchText = '';
  totalauditing = 0;
  auditing: Auditing[];
  elements: any = [];
  previous: string;
  firstlevel: string;
  secondlevel: string;
  popoverTitle = 'Suppression de cet audit';
  popoverMessage = 'Voulez vous vraiment supprimer cet audit';
  cancelClicked = false;
  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, private toastr: ToastrService, private cdRef: ChangeDetectorRef, private ApiAuditing: AuditingService, private router: Router) { }

  ngOnInit() {
    this.loadAllAuditing();
  }


  loadAllAuditing(): void {
    this.ApiAuditing.getAllAuditingList().subscribe(
      data => {
        console.log(data);
        this.auditing = data;
        console.log(this.auditing);
        this.firstlevel = 'Tableau de bord';
        this.secondlevel = 'Auditing';
        this.totalauditing = this.auditing.length;
        this.mdbTable.setDataSource(this.auditing);
        this.elements = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();
      },
      err => console.error(err),
      () => console.log('liste des audits obtenues')
    );
  }

  @HostListener('input') oninput() {
    this.searchItems();
  }

  searchItems() {
    const prev = this.auditing;

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.elements = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(6);

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

}
