import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Employe} from '../../employe/shared/employe.model';
import {environment} from '../../../../environments/environment';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {Auditing} from './auditing.model';

@Injectable({
  providedIn: 'root'
})
export class AuditingService {

  constructor(private httpclient: HttpClient) { }

  getAllAuditingList() {
    return  this.httpclient.get<Auditing[]>(environment.apiUrl + '/audits')
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Its a Trap!'))
      );
  }

}
