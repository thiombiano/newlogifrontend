export class Auditing {
  id: number;
  user_id: number;
  event: string;
  auditable_type: string;
  auditable_id: number;
  old_values: [];
  new_values: [];
  url: string;
  ip_address: string;
  created_at: Date;
  updated_at: Date;
  user: [];
}
