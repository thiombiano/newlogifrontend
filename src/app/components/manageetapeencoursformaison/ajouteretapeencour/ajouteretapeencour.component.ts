import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {EtapeencoursService} from '../shared/etapeencours.service';
import {EtapeService} from '../../etapeconstruction/shared/etape.service';
import {SectionService} from '../../section/shared/section.service';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {DatePipe} from '@angular/common';
import {EtapeencoursModel} from '../shared/etapeencours.model';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormControl, Validators} from '@angular/forms';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';

@Component({
  selector: 'app-ajouteretapeencour',
  templateUrl: './ajouteretapeencour.component.html',
  styleUrls: ['./ajouteretapeencour.component.css'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
  ]
})
export class AjouteretapeencourComponent {

  // tslint:disable-next-line:max-line-length
  constructor(public apiEtapeEnCour: EtapeencoursService, public ApiEtape: EtapeService, public ApiSection: SectionService, public dialogRef: MatDialogRef<AjouteretapeencourComponent>, private dateAdapter: DateAdapter<Date>, private datePipe: DatePipe, private _adapter: DateAdapter<any>,
              @Inject(MAT_DIALOG_DATA) public data: EtapeencoursModel) { this._adapter.setLocale('fr'); }
  thedate: Date;

  @Output() passEntry: EventEmitter<any> = new EventEmitter();

  formControl = new FormControl('', [
    Validators.required
  ]);


  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Le champ est requis' : '';
  }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    const idmaison = window.localStorage.getItem('iddelamaison');
    this.data.maison_id = +idmaison;
    this.apiEtapeEnCour.addEtapeEncoursForMaison(this.data).subscribe((dataetapencours) => {
      this.passEntry.emit(dataetapencours);
      console.log(dataetapencours);
      });
  }

}
