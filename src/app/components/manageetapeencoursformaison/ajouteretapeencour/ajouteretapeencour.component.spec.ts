import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjouteretapeencourComponent } from './ajouteretapeencour.component';

describe('AjouteretapeencourComponent', () => {
  let component: AjouteretapeencourComponent;
  let fixture: ComponentFixture<AjouteretapeencourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjouteretapeencourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjouteretapeencourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
