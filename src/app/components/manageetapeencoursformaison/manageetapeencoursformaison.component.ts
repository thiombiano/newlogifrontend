import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {NotificationService} from '../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {ChantierService} from '../chantier/shared/chantier.service';
import {SnotifyService} from 'ng-snotify';
import {Router} from '@angular/router';
import {EtapeencoursService} from './shared/etapeencours.service';
import {EtapeencoursModel} from './shared/etapeencours.model';
import {Section} from '../section/shared/section.model';
import {Etape} from '../etapeconstruction/shared/etape.model';
import {EtapeService} from '../etapeconstruction/shared/etape.service';
import {AjoutersectionComponent} from '../section/ajoutersection/ajoutersection.component';
import {MatDialog} from '@angular/material/dialog';
import {AjouteretapeencourComponent} from './ajouteretapeencour/ajouteretapeencour.component';
import {ReceptionneretapeencourComponent} from './receptionneretapeencour/receptionneretapeencour.component';
import {ImageetapeModel} from "./shared/imageetape.model";
import {ImageetapeService} from "./shared/imageetape.service";
import {VoirimageetapeComponent} from "./voirimageetape/voirimageetape.component";
import {EtatdefinitifetapeComponent} from "./etatdefinitifetape/etatdefinitifetape.component";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-manageetapeencoursformaison',
  templateUrl: './manageetapeencoursformaison.component.html',
  styleUrls: ['./manageetapeencoursformaison.component.css']
})
export class ManageetapeencoursformaisonComponent implements OnInit {
  searchText;
  public page: number;
  public isNull: string;
  public pageSize: number;
  public isDisplayed: boolean;
  public bootstrapcolumn: string;
  public texttodisplay: string;
  p = 1;
  count = 28;
  nextLabel = 'Suivant';
  previousLabel = 'Précédent';
  maxSize = 9;
  directionLinks = true;
  autoHide = false;
  responsive = true;
  etapes: Etape[];
  screenReaderPaginationLabel = 'Pagination';
  screenReaderPageLabel = 'page';
  screenReaderCurrentLabel = 'Vous êtes sur cette page';
  firstlevel: string;
  secondlevel: string;
  listeetatetape: EtapeencoursModel[];
  headEtatEnCours = ['No',  'Etape avancement', 'Date de début', 'Date de fin', 'Nombre de jour', 'Etat', 'Action'];
  public roles: { A: string; T: string; L: string; M: string;  C: string; SA: string; CSR: string; CDC: string};
  public totaletatetape = 0;
  public libelledelamaison: string;
  public libelledetypemaison: string;
  public libellechantier: string;
  public images: ImageetapeModel[];
  // tslint:disable-next-line:max-line-length
  constructor(public apiImageEtape: ImageetapeService, public ApiEtape: EtapeService, public ApiEtapeEncour: EtapeencoursService,  private SpinnerService: NgxSpinnerService, private notifyService: NotificationService, private toastr: ToastrService, private Apichantier: ChantierService, private cdRef: ChangeDetectorRef,
              private snotifyService: SnotifyService,  public dialog: MatDialog, private router: Router) { }

  ngOnInit() {
    this.roles = environment.roles;
    this.firstlevel = 'Chantiers / Maisons';
    this.secondlevel = 'Etape avancement en cours';
    const idmaison = window.localStorage.getItem('iddelamaison');
    this.libelledelamaison = window.localStorage.getItem('libelledemaison');
    this.libelledetypemaison = window.localStorage.getItem('libelledetypemaison');
    this.libellechantier = window.localStorage.getItem('libellechantier');
    this.loadEtatEtapeByMaison(+idmaison);
    this.LoadEtapeDisponibleForThisHouse(+idmaison);
  }

  loadEtatEtapeByMaison(id: number) {
    this.ApiEtapeEncour.getEtatdesEtapesAvancementByMaison(+id).subscribe((data) => {
      this.listeetatetape = data.data;
      this.totaletatetape = data.data.length;
      console.log(this.listeetatetape);
    });
  }

  LoadEtapeDisponibleForThisHouse(idmaison: number) {
    this.ApiEtape.getAllEtapeDisponibleByMaison(+idmaison).subscribe((data) => {
      this.etapes = data;
    });
  }

  showToasterEtapeEnregistrer() {
    this.notifyService.showSuccessWithTimeout('Etape démarrée avec succès !!', 'Notification', 5000);
  }

  showToasterReceptionneEtapeEnregistrer() {
    // tslint:disable-next-line:max-line-length
    this.notifyService.showSuccessEndingWithTimeout('Etape réceptionnée avec succès et en attente de validation définitive !!', 'Notification', 5000);
  }

  showToasterReceptionnOuRejete() {
    // tslint:disable-next-line:max-line-length
    this.notifyService.showSuccessEndingWithTimeout('Opération effectuée avec succès !!', 'Notification', 5000);
  }

  addNewEtapeEncours() {
    const dialogRef = this.dialog.open(AjouteretapeencourComponent, {
      data: {etapeencours: EtapeencoursModel, getalletapes: this.etapes},
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        dialogRef.componentInstance.passEntry.subscribe((valeurecue) => {
          console.log('La valeur recu est : ' + valeurecue);
          const idmaison = window.localStorage.getItem('iddelamaison');
          this.loadEtatEtapeByMaison(+idmaison);
          this.LoadEtapeDisponibleForThisHouse(+idmaison);
        });
        this.showToasterEtapeEnregistrer();
      }
    });
  }

  receptionneEtapeEncours(i: number, id: number, maison_id: number, etape_construction_id: number) {

    const dialogRef = this.dialog.open(ReceptionneretapeencourComponent, {
      data: {etapeencours: EtapeencoursModel, id,  maison_id , etape_construction_id,   getalletapes: this.etapes},
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        dialogRef.componentInstance.passEntry.subscribe((valeurecue) => {
          console.log('La valeur recu est : ' + valeurecue);
          const idmaison = window.localStorage.getItem('iddelamaison');
          this.loadEtatEtapeByMaison(+idmaison);
          this.LoadEtapeDisponibleForThisHouse(+idmaison);
        });
        this.showToasterReceptionneEtapeEnregistrer();
      }
    });
  }


  receptionneOuRejeteEtapeEncours(i: number, id: number) {

    const dialogRef = this.dialog.open(EtatdefinitifetapeComponent, {
      data: {etapeencours: EtapeencoursModel, id},
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        dialogRef.componentInstance.passEntry.subscribe((valeurecue) => {
          console.log('La valeur recu est : ' + valeurecue);
          const idmaison = window.localStorage.getItem('iddelamaison');
          this.loadEtatEtapeByMaison(+idmaison);
          this.LoadEtapeDisponibleForThisHouse(+idmaison);
        });
        this.showToasterReceptionnOuRejete();
      }
    });
  }


  getImageForThisStape(idstate) {
    this.apiImageEtape.getImageEtapeForThisReceptionne(+idstate).subscribe((imageetape: ImageetapeModel[]) => {
      this.images = imageetape;
      console.log('LES IMS SONT : ' + imageetape);
    });
  }

  VoirPieceEtape(id: any) {
    this.apiImageEtape.getImageEtapeForThisReceptionne(+id).subscribe((imageetape: ImageetapeModel[]) => {
      this.images = imageetape;
      const dialogRef = this.dialog.open(VoirimageetapeComponent, {
        data: {imagesetapes: ImageetapeModel,  getallimages: this.images},
      });
      console.log('LES IMS SONT : ' + imageetape);
    });
  }

  showToasterArticleSupprimer() {
    this.notifyService.showSuccessWithTimeout('Etape aannuler avec succès !!', 'Notification', 5000);
  }

  SupprimerEtape(idetapeencour) {
    this.ApiEtapeEncour.deleteEtapeEncour(+idetapeencour).subscribe((etapencour: EtapeencoursModel) => {
      const idmaison = window.localStorage.getItem('iddelamaison');
      this.loadEtatEtapeByMaison(+idmaison);
      this.LoadEtapeDisponibleForThisHouse(+idmaison);
      this.showToasterArticleSupprimer();
    });
  }
}
