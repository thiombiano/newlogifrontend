import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {EtapeencoursService} from '../shared/etapeencours.service';
import {EtapeService} from '../../etapeconstruction/shared/etape.service';
import {SectionService} from '../../section/shared/section.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {DateAdapter} from '@angular/material/core';
import {DatePipe} from '@angular/common';
import {EtapeencoursModel} from '../shared/etapeencours.model';
import {FormControl, Validators} from '@angular/forms';
import {ImageetapeModel} from '../shared/imageetape.model';
import {ImageetapeService} from '../shared/imageetape.service';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-voirimageetape',
  templateUrl: './voirimageetape.component.html',
  styleUrls: ['./voirimageetape.component.css']
})
export class VoirimageetapeComponent {

  // tslint:disable-next-line:max-line-length
  constructor(public apiImageEtape: ImageetapeService, public ApiEtape: EtapeService, public ApiSection: SectionService, public dialogRef: MatDialogRef<VoirimageetapeComponent>, private dateAdapter: DateAdapter<Date>, private datePipe: DatePipe, private _adapter: DateAdapter<any>,
              @Inject(MAT_DIALOG_DATA) public data: ImageetapeModel) { }
              public path: string = environment.apiImage;
  @Output() passEntry: EventEmitter<any> = new EventEmitter();
  images: ImageetapeModel[];
  formControl = new FormControl('', [
    Validators.required
  ]);


  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Le champ est requis' : '';
  }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }


}
