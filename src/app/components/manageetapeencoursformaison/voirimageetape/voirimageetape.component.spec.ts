import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoirimageetapeComponent } from './voirimageetape.component';

describe('VoirimageetapeComponent', () => {
  let component: VoirimageetapeComponent;
  let fixture: ComponentFixture<VoirimageetapeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoirimageetapeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoirimageetapeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
