import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EtatdefinitifetapeComponent } from './etatdefinitifetape.component';

describe('EtatdefinitifetapeComponent', () => {
  let component: EtatdefinitifetapeComponent;
  let fixture: ComponentFixture<EtatdefinitifetapeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtatdefinitifetapeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtatdefinitifetapeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
