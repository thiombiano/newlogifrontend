import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {EtapeencoursService} from '../shared/etapeencours.service';
import {EtapeService} from '../../etapeconstruction/shared/etape.service';
import {SectionService} from '../../section/shared/section.service';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {DatePipe} from '@angular/common';
import {EtapeencoursModel} from '../shared/etapeencours.model';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-etatdefinitifetape',
  templateUrl: './etatdefinitifetape.component.html',
  styleUrls: ['./etatdefinitifetape.component.css'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
  ]
})
export class EtatdefinitifetapeComponent {

  // tslint:disable-next-line:max-line-length
  constructor(public router: Router, matDialog: MatDialog, public apiEtapeEnCour: EtapeencoursService, public ApiEtape: EtapeService, public ApiSection: SectionService, public dialogRef: MatDialogRef<EtatdefinitifetapeComponent>, private dateAdapter: DateAdapter<Date>, private datePipe: DatePipe, private _adapter: DateAdapter<any>,
              @Inject(MAT_DIALOG_DATA) public data: EtapeencoursModel) {  this._adapter.setLocale('fr'); }


  formControl = new FormControl('', [
    Validators.required
  ]);


  @Output() passEntry: EventEmitter<any> = new EventEmitter();

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Le champ est requis' : '';
  }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmReceptionDefinitif(): void {
    const formData = new FormData();
    // @ts-ignore
    formData.append('id', this.data.id);
    this.apiEtapeEnCour.receptionneDefinitivementEtapeEncoursForMaison(formData).subscribe((dataetapencours) => {
      this.passEntry.emit(dataetapencours);
      console.log(dataetapencours);
    });
  }

  public confirmAnnulerReception(): void {
    const formData = new FormData();
    // @ts-ignore
    formData.append('id', this.data.id);
    this.apiEtapeEnCour.annulerEtapeEncoursForMaison(formData).subscribe((dataetapencours) => {
      this.passEntry.emit(dataetapencours);
      console.log(dataetapencours);
    });
  }

}
