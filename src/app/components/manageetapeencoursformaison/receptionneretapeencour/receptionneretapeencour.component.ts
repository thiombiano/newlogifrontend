import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {EtapeencoursService} from '../shared/etapeencours.service';
import {EtapeService} from '../../etapeconstruction/shared/etape.service';
import {SectionService} from '../../section/shared/section.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {DatePipe} from '@angular/common';
import {EtapeencoursModel} from '../shared/etapeencours.model';
import {FormControl, Validators} from '@angular/forms';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {filter, tap} from 'rxjs/operators';
import {NavigationStart, Router, RouterEvent} from '@angular/router';

@Component({
  selector: 'app-receptionneretapeencour',
  templateUrl: './receptionneretapeencour.component.html',
  styleUrls: ['./receptionneretapeencour.component.css'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
  ]
})
export class ReceptionneretapeencourComponent {
  // tslint:disable-next-line:max-line-length
  constructor(public router: Router, matDialog: MatDialog, public apiEtapeEnCour: EtapeencoursService, public ApiEtape: EtapeService, public ApiSection: SectionService, public dialogRef: MatDialogRef<ReceptionneretapeencourComponent>, private dateAdapter: DateAdapter<Date>, private datePipe: DatePipe, private _adapter: DateAdapter<any>,
              @Inject(MAT_DIALOG_DATA) public data: EtapeencoursModel) {
    this._adapter.setLocale('fr');
  }
  public imagePath;
  referencevalutilisateur = '';
  imgURL: any;
  public message: string;
  selectedFile: File[];
  public path: string;
  thedate: Date;

  formControl = new FormControl('', [
    Validators.required
  ]);


  @Output() passEntry: EventEmitter<any> = new EventEmitter();

  preview(files) {
    if (files.length === 0) {
      return;
    }

    // if (event.target.files.length > 0) {
    //   const file = event.target.files[0];
    //   this.addForm.get('avatar').setValue(file);
    // }

    const mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = 'Only images are supported.';
      return;
    }

    const reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    console.log('le o : ' + files[0]);
    // tslint:disable-next-line:variable-name
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    };
    console.log('Le ficher est : ' +   this.imgURL);
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files;
    const reader = new FileReader();
    this.imagePath = event.target.files;
    reader.readAsDataURL(event.target.files[0]);
    // tslint:disable-next-line:variable-name
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    };
    console.log('Le ficher est : ' +   this.selectedFile);
  }


  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Le champ est requis' : '';
  }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmEtapeReceptionne(): void {
    const formData = new FormData();
    // @ts-ignore
    formData.append('datefermetureetape', this.data.datefermetureetape);
    // if (this.selectedFile) {
    //   formData.append('avatar', this.selectedFile);
    // }
    if (this.selectedFile) {
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < this.selectedFile.length; i++) {
        formData.append('avatar' + [i], this.selectedFile[i],  this.selectedFile[i].name);
        console.log('liste des images one by one : ' + this.selectedFile[i] + ' plus ' + this.selectedFile[i].name );
      }
      // @ts-ignore
      formData.append('length', this.selectedFile.length);
      console.log('La taille des images est : ' + this.selectedFile.length);
    }

    // @ts-ignore
    // formData.append('avatar', JSON.stringify(this.selectedFile) );
    // @ts-ignore
    formData.append('id', this.data.id);
    console.log('Données formData : ' + JSON.stringify(formData) );
    console.log('ID OF ETPA EN COUR : ' + this.data.id);
    this.apiEtapeEnCour.receptionneEtapeEncoursForMaison(formData).subscribe((dataetapencours) => {
      this.passEntry.emit(dataetapencours);
      console.log(dataetapencours);
    });
  }

}
