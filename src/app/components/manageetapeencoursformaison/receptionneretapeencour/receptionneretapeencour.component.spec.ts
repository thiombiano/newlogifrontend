import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceptionneretapeencourComponent } from './receptionneretapeencour.component';

describe('ReceptionneretapeencourComponent', () => {
  let component: ReceptionneretapeencourComponent;
  let fixture: ComponentFixture<ReceptionneretapeencourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceptionneretapeencourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceptionneretapeencourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
