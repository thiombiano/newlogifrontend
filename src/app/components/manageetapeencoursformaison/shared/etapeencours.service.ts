import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {EtapeencoursModel} from './etapeencours.model';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {Article} from "../../article/Shared/article";

@Injectable({
  providedIn: 'root'
})
export class EtapeencoursService {
  private headers: HttpHeaders;
  constructor(private http: HttpClient) { }

  addEtapeEncoursForMaison(etapeencour: any) {
    return  this.http.post<EtapeencoursModel>(environment.apiUrl +  '/etapemaisonencour', etapeencour);
  }

  receptionneEtapeEncoursForMaison(value: any) {
    this.headers = new HttpHeaders();
    this.headers.append('Content-Type', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    return  this.http.post<EtapeencoursModel>(environment.apiUrl +  '/receptionneetapeencour',  value, {headers: this.headers});
  }

  receptionneDefinitivementEtapeEncoursForMaison(value: any) {
    return  this.http.post<EtapeencoursModel>(environment.apiUrl +  '/receptionnedefinitivementetapeencour',  value);
  }

  annulerEtapeEncoursForMaison(value: any) {
    return  this.http.post<EtapeencoursModel>(environment.apiUrl +  '/annulerreceptionetapeencour',  value);
  }

  getEtatdesEtapesAvancementByMaison(idmaison: number) {
    return  this.http.get<EtapeencoursModel [] >(environment.apiUrl + '/etapemaisonencour/' + idmaison)
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Impossible obtenir la liste des etats des étapes en cour!'))
      );
  }

  deleteEtapeEncour(idetapeencour: number) {
    // @ts-ignore
    return this.http.delete<EtapeencoursModel>(environment.apiUrl + '/etapemaisonencour/' + idetapeencour);
  }

}
