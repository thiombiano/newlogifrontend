import {Maison} from '../../maison/shared/maison.model';
import {Etape} from '../../etapeconstruction/shared/etape.model';

export class EtapeencoursModel {
  id: number;
  maison_id: number;
  etape_construction_id: number;
  dateouvertureetape: Date;
  datefermetureetape: Date;
  etapeState: boolean;
  isFinished: boolean;
  nombredejour: number;
  commentaires: string;
  nombretotaletape: number;
  numeroetapencour: number;
  maison: Maison[];
  etape: Etape[];
}
