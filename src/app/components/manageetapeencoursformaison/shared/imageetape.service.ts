import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {ImageetapeModel} from './imageetape.model';

@Injectable({
  providedIn: 'root'
})
export class ImageetapeService {

  constructor(private http: HttpClient) { }

  getImageEtapeForThisReceptionne(idstate) {
    return  this.http.get<ImageetapeModel [] >(environment.apiUrl + '/imageetapeencour/' + idstate);
  }

}
