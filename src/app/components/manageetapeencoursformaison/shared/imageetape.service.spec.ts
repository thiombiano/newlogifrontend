import { TestBed } from '@angular/core/testing';

import { ImageetapeService } from './imageetape.service';

describe('ImageetapeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ImageetapeService = TestBed.get(ImageetapeService);
    expect(service).toBeTruthy();
  });
});
