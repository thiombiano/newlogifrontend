import { TestBed } from '@angular/core/testing';

import { EtapeencoursService } from './etapeencours.service';

describe('EtapeencoursService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EtapeencoursService = TestBed.get(EtapeencoursService);
    expect(service).toBeTruthy();
  });
});
