import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageetapeencoursformaisonComponent } from './manageetapeencoursformaison.component';

describe('ManageetapeencoursformaisonComponent', () => {
  let component: ManageetapeencoursformaisonComponent;
  let fixture: ComponentFixture<ManageetapeencoursformaisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageetapeencoursformaisonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageetapeencoursformaisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
