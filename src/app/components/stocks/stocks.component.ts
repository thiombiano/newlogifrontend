import {ChangeDetectorRef, Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {MdbTableDirective, MdbTablePaginationComponent} from 'angular-bootstrap-md';
import {Article} from '../article/Shared/article';
import {Stocks} from './shared/stocks';
import {ArticleService} from '../article/Shared/article.service';
import {Router} from '@angular/router';
import {StocksService} from './shared/stocks.service';
import {ChantierService} from '../chantier/shared/chantier.service';

@Component({
  selector: 'app-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnInit {
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  stocks: Stocks[];
  searchText = '';
  totalstocks = 0;
  elements: any = [];
  previous: string;
  firstlevel: string;
  secondlevel: string;
  // tslint:disable-next-line:max-line-length
  constructor(public ChantierAPI: ChantierService, private cdRef: ChangeDetectorRef, private ApiStock: StocksService, private router: Router) { }

  headElements = ['N°', 'Référence', 'Désignation' , 'Seuil', 'Stock Dispo', 'Status', 'Action'];

  // loadAllStocks() {
  //   const idchantier = window.localStorage.getItem('Idchantier');
  //   const theid = +idchantier;
  //   this.ApiStock.getChantierStocksListOther(theid).subscribe((stocks: Stocks[]) => {
  //     this.stocks = stocks;
  //     console.log(stocks);
  //     this.firstlevel = 'Tableau de bord';
  //     this.secondlevel = 'Stocks';
  //     this.totalstocks = stocks.length;
  //     this.mdbTable.setDataSource(stocks);
  //     this.elements = this.mdbTable.getDataSource();
  //     this.previous = this.mdbTable.getDataSource();
  //   });
  // }


  // this.stocks =
  //   this.firstlevel = 'Tableau de bord';
  // this.secondlevel = 'Stocks';
  // this.totalstocks = stocks.length;
  // this.mdbTable.setDataSource(stocks);
  // this.elements = this.mdbTable.getDataSource();
  // this.previous = this.mdbTable.getDataSource();

  ngOnInit() {
    this.firstlevel = 'Tableau de bord';
    this.secondlevel = 'Stocks';
    const idchantier = window.localStorage.getItem('Idchantier');
    const theid = +idchantier;
    // if (this.ChantierAPI.selectedChantier) {
    this.ApiStock.getChantierStocksList(theid);
    // }

    // this.loadAllStocks();

  }

  valuechange($event) {
    console.log($event.target.value);
    this.ApiStock.filtrerStock($event.target.value);
  }

  //
  // @HostListener('input') oninput() {
  //   this.searchItems();
  // }
  //
  //
  // searchItems() {
  //   const prev = this.stocks;
  //
  //   if (!this.searchText) {
  //     this.mdbTable.setDataSource(this.previous);
  //     this.elements = this.mdbTable.getDataSource();
  //   }
  //
  //   if (this.searchText) {
  //     this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
  //     this.mdbTable.setDataSource(prev);
  //   }
  // }
  //
  // // tslint:disable-next-line:use-lifecycle-interface
  // ngAfterViewInit() {
  //   this.mdbTablePagination.setMaxVisibleItemsNumberTo(6);
  //   this.mdbTablePagination.calculateFirstItemIndex();
  //   this.mdbTablePagination.calculateLastItemIndex();
  //   this.cdRef.detectChanges();
  // }

  detailsstock(idChantier, idStock) {
    window.localStorage.removeItem('LIdChantier');
    window.localStorage.removeItem('idStock');
    window.localStorage.setItem('IdStock', idStock);
    window.localStorage.setItem('LIdChantier', idChantier);
    this.router.navigate(['start/mouvement-stocks']);
  }

  get listChantierStocksFiltree() {
    return this.ApiStock.listChantierStocksFiltree;
  }

}
