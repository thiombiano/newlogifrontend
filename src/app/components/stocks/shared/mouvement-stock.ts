export class MouvementStock {

 id: number;
 date: string;
 nature: number;
 numero_piece: string;
 entree: number;
 sortie: number;
 stock: number;

}
