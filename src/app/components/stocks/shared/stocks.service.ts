import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Stocks} from './stocks';
import {environment} from '../../../../environments/environment';
import {MouvementStock} from './mouvement-stock';

@Injectable({
  providedIn: 'root'
})
export class StocksService {
  stockArticle: Stocks;
  listChantierStocks: Stocks[];
  listChantierStocksFiltree: Stocks[];
  listChantierArticleMvtStocks: MouvementStock[];
  constructor(private http: HttpClient) { }

  getChantierStocksList(idChantier: number) {
    this.http
      .get(environment.apiUrl + '/chantiers/' + idChantier + '/inventaires')
      .toPromise()
      .then((x: StocksApiResponse) => {
        this.listChantierStocks = x.data;
        this.listChantierStocksFiltree = x.data;
        console.log('Stock: ' + JSON.stringify(this.listChantierStocks));

      });
  }

  getChantierStocksListOther(idChantier: number) {
    return this.http.get<Stocks[]>(environment.apiUrl + '/chantiers/' + idChantier + '/inventaires');
  }

  getQuantiteDisponible(idChantier: number, idArticle: number) {
    return this.http.get(environment.apiUrl + '/stocks/getquantitedisponible/' + idChantier + '/' + idArticle);
  }

  getChantierArticleMouvementStocks(idChantier: number, idArticle: number) {
    this.http
      .get(environment.apiUrl + '/chantiers/' + idChantier + '/articles/' + idArticle)
      .toPromise()
      .then((x: MvtStocksApiResponse) => {
        this.listChantierArticleMvtStocks = x.data;
      });
  }

  getChantierArticleStock(idChantier: number, idArticle: number) {
    this.http
      .get(environment.apiUrl + '/chantiers/' + idChantier + '/inventaires/' + idArticle)
      .toPromise()
      .then((x: Stocks) => {
        // tslint:disable-next-line: comment-format
        //console.log("data: ", x);
        this.stockArticle = x;
      });
  }


  filtrerStock(term: string) {
    this.listChantierStocksFiltree = this.listChantierStocks.filter(stk =>
      stk.article.toLowerCase().startsWith(term.toLowerCase())
    );
  }


}

interface StocksApiResponse {
  data: Stocks[];
}

interface MvtStocksApiResponse {
  data: MouvementStock[];
}
