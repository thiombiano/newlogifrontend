export class Stocks {

  id: number;
  depot: string;
  depot_id: number;
  article: string;
  article_id: number;
  quantiteDisponible: number;
  stockMinimal: number;
  commentaires: string;

}
