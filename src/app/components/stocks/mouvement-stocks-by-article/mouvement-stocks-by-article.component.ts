import { Component, OnInit } from '@angular/core';
import {Article} from '../../article/Shared/article';
import {StocksService} from '../shared/stocks.service';
import {ArticleService} from '../../article/Shared/article.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-mouvement-stocks-by-article',
  templateUrl: './mouvement-stocks-by-article.component.html',
  styleUrls: ['./mouvement-stocks-by-article.component.css']
})
export class MouvementStocksByArticleComponent implements OnInit {
  idChantier: number;
  idArticle: number;
  article: Article;
  firstlevel: string;
  secondlevel: string;
  thirdlevel: string;
  articles: Article[];
  headElements = ['ID', 'Date', 'Type' , 'Doc Ref', 'Entrée', 'Sortie', 'Stock', 'Status'];
  constructor(public ApiArticle: ArticleService,
              public stockService: StocksService,
              public articleService: ArticleService,
              private router: Router,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.firstlevel = 'Tableau de bord';
    this.thirdlevel = 'Stocks';
    this.secondlevel = 'Suivi du stocks par article';
    this.loadAllArticles();
    this.ClearListeMvt();
  }

  loadAllArticles(): void {
    this.ApiArticle.getAllArticleList().subscribe(
      data => {
        this.articles = data;
      },
      err => console.error(err),
      () => console.log('Article bien recu')
    );
  }

  ClearListeMvt() {
    this.stockService.listChantierArticleMvtStocks = undefined;
    this.stockService.stockArticle = undefined;
    this.articleService.selectedArticle = undefined;
  }

  GetMvtStockArticle(value) {
    console.log('ID Article : ' + value);
    this.articleService.getLArticle(value);
    this.idChantier = +window.localStorage.getItem('Idchantier');
    this.stockService.getChantierArticleStock(this.idChantier, +value);
    this.stockService.getChantierArticleMouvementStocks(this.idChantier, +value);
  }
}
