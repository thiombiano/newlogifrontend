import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MouvementStocksByArticleComponent } from './mouvement-stocks-by-article.component';

describe('MouvementStocksByArticleComponent', () => {
  let component: MouvementStocksByArticleComponent;
  let fixture: ComponentFixture<MouvementStocksByArticleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MouvementStocksByArticleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MouvementStocksByArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
