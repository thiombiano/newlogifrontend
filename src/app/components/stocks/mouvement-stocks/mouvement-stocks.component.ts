import { Component, OnInit } from '@angular/core';
import {Article} from '../../article/Shared/article';
import {StocksService} from '../shared/stocks.service';
import {ArticleService} from '../../article/Shared/article.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-mouvement-stocks',
  templateUrl: './mouvement-stocks.component.html',
  styleUrls: ['./mouvement-stocks.component.css']
})
export class MouvementStocksComponent implements OnInit {
  idChantier: number;
  idArticle: number;
  article: Article;
  firstlevel: string;
  secondlevel: string;
  thirdlevel: string;
  headElements = ['ID', 'Date', 'Type' , 'Doc Ref', 'Entrée', 'Sortie', 'Stock', 'Status'];
  constructor(   public stockService: StocksService,
                 public articleService: ArticleService,
                 private router: Router,
                 private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.firstlevel = 'Tableau de bord';
    this.thirdlevel = 'Stocks';
    this.secondlevel = 'Suivi du stocks';
    this.idChantier = +window.localStorage.getItem('Idchantier');
    this.idArticle = +window.localStorage.getItem('IdStock');
    this.stockService.getChantierArticleMouvementStocks(
      this.idChantier,
      this.idArticle
    );
    console.log(this.idChantier, this.idArticle );
    this.articleService.getLArticle(this.idArticle);
    this.article = this.articleService.selectedArticle;
    this.stockService.getChantierArticleStock(this.idChantier, this.idArticle);
  }

}
