import { Component, OnInit } from '@angular/core';
import {BesoinjournalierService} from '../shared/besoinjournalier.service';
import {Alldetailbesoinlevelone} from '../shared/alldetailbesoinlevelone';
import {Router} from '@angular/router';

@Component({
  selector: 'app-voiralldetailbesoinlevelone',
  templateUrl: './voiralldetailbesoinlevelone.component.html',
  styleUrls: ['./voiralldetailbesoinlevelone.component.css']
})
export class VoiralldetailbesoinleveloneComponent implements OnInit {
  firstlevel: string;
  secondlevel: string;
  nextLabel = 'Suivant';
  previousLabel = 'Précédent';
  maxSize = 9;
  directionLinks = true;
  autoHide = false;
  responsive = true;
  screenReaderPaginationLabel = 'Pagination';
  screenReaderPageLabel = 'page';
  screenReaderCurrentLabel = 'Vous êtes sur cette page';
  alldetaillevelone: Alldetailbesoinlevelone[];
  headElements = ['N°',  'Date' , 'Maison' , 'Fonction', 'Etat de construction', 'Action'];
  headElementsInPrefat = ['N°',  'Date' , 'Prefat' , 'Fonction', 'Action'];
  p = 1;
  count = 10;
  searchText;
  cahierjournalier: string;
  public libelletypebesoin: string;
  constructor(public  ApiBesoin: BesoinjournalierService, public router: Router) { }

  ngOnInit() {
    const idbesoin =   window.localStorage.getItem('IDBBbesoin');
    console.log('id:' + idbesoin);
    this.cahierjournalier = window.localStorage.getItem('LIBELLEcahierjournalier');
    this.libelletypebesoin = window.localStorage.getItem('libelletypebesoin');
    if (this.libelletypebesoin === 'Vers les maisons' ) {
      this.firstlevel = 'BESOIN JOURNALIER';
      this.secondlevel = 'Besoin Journalier par maison';
      this.ApiBesoin.getSelectedBesoinDetailLevelOne(+idbesoin).subscribe((data) => {
      this.alldetaillevelone = data;
      console.log(data);
    });
    } else {
      this.firstlevel = 'BESOIN JOURNALIER';
      this.secondlevel = 'Besoin Journalier par préfat';
      this.ApiBesoin.getSelectedBesoinDetailLevelOneInPrefat(+idbesoin).subscribe((data) => {
        this.alldetaillevelone = data;
        console.log(data);
      });
    }
  }

  voirDetailArticle(idmaison, libellemaison, libelleetape, libelleFonction, nom, prenom) {
    window.localStorage.removeItem('LIDmaison');
    window.localStorage.removeItem('LIBELLEmaison');
    window.localStorage.removeItem('LIBELLEetape');
    window.localStorage.removeItem('LIBELLEfonction');
    window.localStorage.removeItem('nom');
    window.localStorage.removeItem('prenom');
    window.localStorage.setItem('LIDmaison', idmaison);
    window.localStorage.setItem('LIBELLEmaison', libellemaison);
    window.localStorage.setItem('LIBELLEetape', libelleetape);
    window.localStorage.setItem('LIBELLEfonction', libelleFonction);
    window.localStorage.setItem('nom', nom);
    window.localStorage.setItem('prenom', prenom);
    this.router.navigate(['/start/voir-detailbesoinjournalierforonehouseorprefat']);
  }

  voirDetailArticleInprefat(idprefat, libelleprefat, libelleFonction) {
    window.localStorage.removeItem('LIDprefat');
    window.localStorage.removeItem('LIBELLEprefat');
    window.localStorage.removeItem('LIBELLEfonction');
    window.localStorage.setItem('LIDprefat', idprefat);
    window.localStorage.setItem('LIBELLEprefat', libelleprefat);
    window.localStorage.setItem('LIBELLEfonction', libelleFonction);
    this.router.navigate(['/start/voir-detailbesoinjournalierforonehouseorprefat']);
  }
}
