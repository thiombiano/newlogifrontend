import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoiralldetailbesoinleveloneComponent } from './voiralldetailbesoinlevelone.component';

describe('VoiralldetailbesoinleveloneComponent', () => {
  let component: VoiralldetailbesoinleveloneComponent;
  let fixture: ComponentFixture<VoiralldetailbesoinleveloneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoiralldetailbesoinleveloneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoiralldetailbesoinleveloneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
