import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoiralldetailbesoinbymaisonleveltwoComponent } from './voiralldetailbesoinbymaisonleveltwo.component';

describe('VoiralldetailbesoinbymaisonleveltwoComponent', () => {
  let component: VoiralldetailbesoinbymaisonleveltwoComponent;
  let fixture: ComponentFixture<VoiralldetailbesoinbymaisonleveltwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoiralldetailbesoinbymaisonleveltwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoiralldetailbesoinbymaisonleveltwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
