import { Component, OnInit } from '@angular/core';
import {Alldetailbesoinlevelone} from '../shared/alldetailbesoinlevelone';
import {BesoinjournalierService} from '../shared/besoinjournalier.service';
import {Detailarticlebymaison} from '../shared/detailarticlebymaison';

@Component({
  selector: 'app-voiralldetailbesoinbymaisonleveltwo',
  templateUrl: './voiralldetailbesoinbymaisonleveltwo.component.html',
  styleUrls: ['./voiralldetailbesoinbymaisonleveltwo.component.css']
})
export class VoiralldetailbesoinbymaisonleveltwoComponent implements OnInit {
  firstlevel: string;
  secondlevel: string;
  nextLabel = 'Suivant';
  previousLabel = 'Précédent';
  maxSize = 9;
  directionLinks = true;
  autoHide = false;
  responsive = true;
  screenReaderPaginationLabel = 'Pagination';
  screenReaderPageLabel = 'page';
  screenReaderCurrentLabel = 'Vous êtes sur cette page';
  alldetailleveltwo: Detailarticlebymaison[];
  headElements = ['N°', 'Article', 'Quantité'];
  p = 1;
  count = 10;
  searchText;
  cahierjournalier: string;
  libelleetape: string;
  libellemaison: string;
  nomtechnicien: string;
  libellefonction: string;
  nom: string;
  prenom: string;
  dateBJ: string;
  public libelletypebesoin: string;
  public libellePREFAT: string;
  constructor(public  ApiBesoin: BesoinjournalierService) { }

  ngOnInit() {
    const idbesoin =   window.localStorage.getItem('IDBBbesoin');
    this.libelletypebesoin = window.localStorage.getItem('libelletypebesoin');
    console.log('id:' + idbesoin);
    const idmaison = window.localStorage.getItem('LIDmaison');
    this.libellemaison = window.localStorage.getItem('LIBELLEmaison');
    this.libellePREFAT = window.localStorage.getItem('LIBELLEprefat');
    this.nom = window.localStorage.getItem('nom');
    this.prenom = window.localStorage.getItem('prenom');
    this.libelleetape = window.localStorage.getItem('LIBELLEetape');
    this.dateBJ = window.localStorage.getItem('dateBJ');
    this.nomtechnicien = window.localStorage.getItem('nomtechnicien');
    this.libellefonction = window.localStorage.getItem('LIBELLEfonction');
    this.cahierjournalier = window.localStorage.getItem('LIBELLEcahierjournalier');
    if (this.libelletypebesoin === 'Vers les maisons' ) {
    this.ApiBesoin.getSelectedBesoinDetailByMaisonLevelTwo(+idbesoin, +idmaison).subscribe((data) => {
      this.firstlevel = 'BESOIN JOURNALIER';
      this.secondlevel = 'Besoin Journalier pour la maison ';
      this.alldetailleveltwo = data;
      console.log(data);
    });
    } else {
      this.firstlevel = 'BESOIN JOURNALIER';
      this.secondlevel = 'Besoin Journalier pour la préfat ';
      const idprefat =  window.localStorage.getItem('LIDprefat');
      this.ApiBesoin.getSelectedBesoinDetailByPrefatLevelTwo(+idbesoin, +idprefat).subscribe((data) => {
        this.alldetailleveltwo = data;
        console.log(data);
      });
    }
  }
}
