import { Component, OnInit } from '@angular/core';
import {MaisonService} from '../../maison/shared/maison.service';
import {EtapeService} from '../../etapeconstruction/shared/etape.service';
import {ArticleService} from '../../article/Shared/article.service';
import {DateAdapter, MatDialog} from '@angular/material';
import {LotService} from '../../lot/shared/lot.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {ChantierService} from '../../chantier/shared/chantier.service';
import {Router} from '@angular/router';
import {BesoinjournalierService} from '../shared/besoinjournalier.service';
import {DatePipe, formatDate} from '@angular/common';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Unitemesure} from '../../article/Shared/unitemesure';
import {Maison} from '../../maison/shared/maison.model';
import {Article} from '../../article/Shared/article';
import {BesoinJItem, Besoinjournalier} from '../shared/besoinjournalier.model';
import {Etape} from '../../etapeconstruction/shared/etape.model';
import {AdddetailComponent} from '../adddetail/adddetail.component';
import {UpdatedetailComponent} from '../updatedetail/updatedetail.component';

@Component({
  selector: 'app-modifierbesoinjournalier',
  templateUrl: './modifierbesoinjournalier.component.html',
  styleUrls: ['./modifierbesoinjournalier.component.css']
})
export class ModifierbesoinjournalierComponent implements OnInit {
  nextLabel = 'Suivant';
  previousLabel = 'Précédent';
  maxSize = 9;
  directionLinks = true;
  autoHide = false;
  responsive = true;
  screenReaderPaginationLabel = 'Pagination';
  screenReaderPageLabel = 'page';
  screenReaderCurrentLabel = 'Vous êtes sur cette page';
  public page: number;
  public isNull: string;
  public pageSize: number;
  p = 1;
  count = 10;
  searchText;
  headElement = ['N°', 'Fonction', 'Maison', 'Article', 'Quantité', 'Etape avancement', 'Action'];
  currentdate =  formatDate(new Date(), 'dd/MM/yyyy:HH:mm:ss', 'en');
  displayDetails = false;
  secondlevel: string;
  unitemesure: Unitemesure[];
  today: string;
  firstlevel: string;
  maisons: Maison[];
  articles: Article[];
  addForm: FormGroup;
  public unite: any;
  public quantite: any;
  // @ts-ignore
  besoinjournalier: Besoinjournalier = new Besoinjournalier();
  thedate: Date;
  public touslesetapes: Etape[];
  public libellemaison: string;
  public libelleetape: string;
  public libellearticle: string;
  get f() { return this.addForm.controls; }
  // tslint:disable-next-line:max-line-length
  constructor(public ApiMaison: MaisonService, public ApiEtape: EtapeService, public ApiArticle: ArticleService, public dialog: MatDialog, public ApiLot: LotService, private SpinnerService: NgxSpinnerService, private dateAdapter: DateAdapter<Date>, private notifyService: NotificationService, private toastr: ToastrService, private chantierService: ChantierService, private router: Router, public ApiBesoin: BesoinjournalierService, public articleService: ArticleService, private datePipe: DatePipe, private _adapter: DateAdapter<any>, private _formBuilder: FormBuilder) { }

  ngOnInit() {
    const idbesoin =   window.localStorage.getItem('IDMbesoin');
    this.firstlevel = 'Besoin journalier';
    this.secondlevel = 'Mis à jour du besoin journalier';
    this.ApiBesoin.getSelectedBesoin(+idbesoin).subscribe((data) => {
      this.besoinjournalier = data.data;
      console.log(data);
    });
    const ChantierId = window.localStorage.getItem('Idchantier');
    this.getAllMaisonByChantier(+ChantierId);
    this.getAllArticle();
    this.getAllEtape();
    this.besoinjournalier = {
      journee_id: null,
      cahierjournalier: null,
      dateBJ: null,
      chantier_id: null,
      typebesoin: null,
      nomtechnicien: null,
      technicien_id: null,
      articles: new Array<BesoinJItem>(),
      id: null
    };
  }

  getAllMaisonByChantier(idchantier: number) {
    this.ApiLot.getALLMaisonByChantier(+idchantier).subscribe((data) => {
      console.log(data.data);
      this.maisons = data.data;
    });
  }

  getAllArticle() {
    this.ApiArticle.getAllArticleList().subscribe((data) => {
      console.log(data);
      this.articles = data;
    });
  }
  getAllEtape() {
    this.ApiEtape.getAllEtapeList().subscribe((etape: Etape[]) => {
      this.touslesetapes = etape;
    });
  }

  getLibelleOfMaisonEtapeArticle(idarticle: number, idmaison: number, idetape: number) {
    this.articleService.getArticle(+idarticle).subscribe((article: Article) => {
      this.libellearticle = article.libelle_article;
    });

    this.ApiMaison.getMaisonByIdInBesoin(+idmaison).subscribe((maison: Maison) => {
      this.libellemaison = maison.libellemaison;
    });

    this.ApiEtape.getEtape(+idetape).subscribe((etape: Etape) => {
      this.libelleetape = etape.libelleetape;
    });

  }

  addNewDetail() {
    // tslint:disable-next-line:max-line-length
    const dialogRef = this.dialog.open(AdddetailComponent, {
      data: {
        besoin: Besoinjournalier,
        getallmaisons: this.maisons,
        getallarticles: this.articles
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        const idmaison = JSON.stringify(dialogRef.componentInstance.thebesoin.maison_id);
        const idetape = JSON.stringify(dialogRef.componentInstance.thebesoin.etape_construction_id);
        const idarticle = JSON.stringify(dialogRef.componentInstance.thebesoin.article_id);
        this.getLibelleOfMaisonEtapeArticle(+idarticle, +idmaison, +idetape);
        console.log('la maison : ' + this.libellemaison);
        console.log('la result : ' + JSON.stringify(dialogRef.componentInstance.thebesoin));
        this.besoinjournalier.articles.push({
          id: 0,
          article_id: dialogRef.componentInstance.thebesoin.article_id,
          libellemaison: dialogRef.componentInstance.thebesoin.libellemaison,
          libelleprefat: '',
          prefat_id: 0,
          libelleetape: dialogRef.componentInstance.thebesoin.libelleetape,
          quantiteBesoin: dialogRef.componentInstance.thebesoin.quantiteBesoin,
          commentaires: '',
          article: dialogRef.componentInstance.thebesoin.article,
          etape_construction_id: dialogRef.componentInstance.thebesoin.etape_construction_id,
          maison_id: dialogRef.componentInstance.thebesoin.maison_id,
          fonction_id: dialogRef.componentInstance.thebesoin.fonction_id,
          libellefonction: dialogRef.componentInstance.thebesoin.libellefonction,
        });
      }
    });
  }


  supprimerLaLigneDetail(lignebesoin) {
    const index  =  this.besoinjournalier.articles.indexOf(lignebesoin);
    if (index !== -1) {
      this.besoinjournalier.articles.splice(index, 1);
    }
  }

  startEditBesoin(i: number, id: number, maison_id: number, article_id: number , etape_construction_id: number, quantiteBesoin: number) {
    const dialogRef = this.dialog.open(UpdatedetailComponent, {
      // tslint:disable-next-line:max-line-length
      data: {id, maison_id, article_id, etape_construction_id, quantiteBesoin, getallmaisons: this.maisons, getallarticles: this.articles, getalletapes: this.touslesetapes}
    });
    const pos = i;
    // @ts-ignore

    // @ts-ignore
    // tslint:disable-next-line:max-line-length

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        const besoinj: BesoinJItem = new BesoinJItem(
          0,
          dialogRef.componentInstance.thebesoin.maison_id,
          0,
          dialogRef.componentInstance.thebesoin.etape_construction_id,
          dialogRef.componentInstance.thebesoin.article_id,
          dialogRef.componentInstance.thebesoin.quantiteBesoin,
          'null',
          dialogRef.componentInstance.thebesoin.article,
          dialogRef.componentInstance.thebesoin.libelleetape,
          dialogRef.componentInstance.thebesoin.libellemaison,
          'null',
          dialogRef.componentInstance.thebesoin.fonction_id,
          dialogRef.componentInstance.thebesoin.libellefonction,
        );
        this.besoinjournalier.articles[i] =  besoinj;
      }
    });
  }


  showToasterBesoinEnregistrer() {
    this.notifyService.showSuccessWithTimeout('Besoin mis à jour avec succès !!', 'Notification', 5000);
  }

  MiseAjourLeBesoinJournalier() {
    const ChantierId = window.localStorage.getItem('Idchantier');
    const idutilisateur = window.localStorage.getItem('idutilisateur');
    const idbesoin =   window.localStorage.getItem('IDMbesoin');
    this.besoinjournalier.technicien_id = +idutilisateur;
    this.besoinjournalier.chantier_id = +ChantierId;
    this.besoinjournalier.journee_id = 1;
    console.log('le besoin journalier est : ' + JSON.stringify(this.besoinjournalier.articles) );
    this.ApiBesoin.updateChantierBesoin(+idbesoin,  this.besoinjournalier).subscribe((besoin: Besoinjournalier) => {
      this.showToasterBesoinEnregistrer();
      this.besoinjournalier.articles = null;
      this.router.navigate(['/start/besoinjournaliers']);
    });
  }

}
