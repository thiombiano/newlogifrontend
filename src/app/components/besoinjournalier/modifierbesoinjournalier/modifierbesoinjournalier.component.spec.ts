import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifierbesoinjournalierComponent } from './modifierbesoinjournalier.component';

describe('ModifierbesoinjournalierComponent', () => {
  let component: ModifierbesoinjournalierComponent;
  let fixture: ComponentFixture<ModifierbesoinjournalierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifierbesoinjournalierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifierbesoinjournalierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
