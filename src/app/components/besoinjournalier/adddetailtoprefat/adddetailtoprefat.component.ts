import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {StocksService} from '../../stocks/shared/stocks.service';
import {FonctionsService} from '../../fonctions/shared/fonctions.service';
import {ArticleService} from '../../article/Shared/article.service';
import {EtapeService} from '../../etapeconstruction/shared/etape.service';
import {BesoinjournalierService} from '../shared/besoinjournalier.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {BesoinJItem} from '../shared/besoinjournalier.model';
import {MaisonService} from '../../maison/shared/maison.service';
import {Etape} from '../../etapeconstruction/shared/etape.model';
import {Fonction} from '../../fonctions/shared/fonction';
import {Personnel} from '../../personnels/shared/personnel';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-adddetailtoprefat',
  templateUrl: './adddetailtoprefat.component.html',
  styleUrls: ['./adddetailtoprefat.component.css']
})
export class AdddetailtoprefatComponent {
  @Output() passEntry: EventEmitter<any> = new EventEmitter();
  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, private toastr: ToastrService, public ApiStock: StocksService, public fonctionApi: FonctionsService, public articleService: ArticleService,  public ApiEtape: EtapeService, public ApiBesoinJournalier: BesoinjournalierService, public dialogRef: MatDialogRef<AdddetailtoprefatComponent>,
              @Inject(MAT_DIALOG_DATA) public data: BesoinJItem,
              public ApiMaison: MaisonService) { }


  etape: string;
  etapes: Etape[];
  maison: string;
  article: string ;
  leprestaire: string;
  libelleetape: string;
  fonctions: Fonction[];
  prestataire: Personnel;
  thebesoinofprefat: BesoinJItem;
  formControl = new FormControl('', [
    Validators.required
  ]);

  getFonctionByPrefat(value) {
    window.localStorage.removeItem('idprefat');
    window.localStorage.removeItem('libelleprefat');
    window.localStorage.setItem('libelleprefat', value.libelleprefat);
    window.localStorage.setItem('idprefat', value.id);
    this.fonctionApi.getFonctionByPrefat(value.id).subscribe((fonction: Fonction[]) => {
      this.fonctions = fonction;
    });
  }


  setSelectedArticle(value) {
    this.articleService.getArticleForBesoin(value);
  }

  setSelectedFonction(value) {
    this.fonctionApi.getFonctionForBesoin(value);
  }

  setPrestaire(value: any) {
    const idprefat = window.localStorage.getItem('idprefat');
    this.setSelectedFonction(value);
    this.fonctionApi.getPrestataireByPrefatAndByFonction(+idprefat, value ).subscribe((prestataire: Personnel[]) => {
      this.prestataire = prestataire[0];
      this.leprestaire = prestataire[0].prenom + ' ' + prestataire[0].nom;
      console.log('le prestataire est : ' + JSON.stringify(this.prestataire)  );
    });
  }

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Le champ est requis' : '';
  }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  showToasterEnlevementErreurQuantite() {
    this.notifyService.showErrorWithTimeout('Erreur: Quantité indisponible !!', 'Notification', 5000);
  }



  public confirmAdd(): void {
    let libelleprefats =    window.localStorage.getItem('libelleprefat');
    let idprefat =     window.localStorage.getItem('idprefat');
    // tslint:disable-next-line:label-position
    // @ts-ignore
    this.thebesoinofprefat = ({
      prefat_id: +idprefat,
      article_id: this.data.article_id,
      quantiteBesoin: this.data.quantiteBesoin,
      commentaires: '',
      article: this.articleService.selectedOneArticle.libelle_article,
      fonction_id: this.data.fonction_id,
      libelleprefat: libelleprefats,
      libellefonction: this.fonctionApi.selectedOneFonction.libelleFonction
    });
    console.log('ALL DATA MUST EMITTED : ' + JSON.stringify(this.thebesoinofprefat) );
  }
}
