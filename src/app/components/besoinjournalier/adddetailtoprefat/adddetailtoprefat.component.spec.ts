import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdddetailtoprefatComponent } from './adddetailtoprefat.component';

describe('AdddetailtoprefatComponent', () => {
  let component: AdddetailtoprefatComponent;
  let fixture: ComponentFixture<AdddetailtoprefatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdddetailtoprefatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdddetailtoprefatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
