import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {RecueilbesoinService} from '../../recueilbesoin/shared/recueilbesoin.service';
import {ChantierService} from '../../chantier/shared/chantier.service';
import {BesoinjournalierService} from '../shared/besoinjournalier.service';
import {Recueilbesoin} from '../../recueilbesoin/shared/recueilbesoin.model';
import {Besoinjournalier} from '../shared/besoinjournalier.model';

@Component({
  selector: 'app-voirdetailbesoin',
  templateUrl: './voirdetailbesoin.component.html',
  styleUrls: ['./voirdetailbesoin.component.css']
})
export class VoirdetailbesoinComponent implements OnInit {
  firstlevel: string;
  secondlevel: string;
  besoinjournalier: Besoinjournalier;
  // tslint:disable-next-line:max-line-length
  constructor(private router: Router, private cdRef: ChangeDetectorRef, public ApiBesoin: BesoinjournalierService, public chantierService: ChantierService) { }

  ngOnInit() {
    const idbesoin =   window.localStorage.getItem('IDBesoin');
    console.log('id:' + idbesoin);
    this.firstlevel = 'Détail du besoin journalier';
    this.secondlevel = 'Besoin Journalier';
    this.ApiBesoin.getSelectedBesoin(+idbesoin).subscribe((data) => {
      this.besoinjournalier = data.data;
    });
  }

}
