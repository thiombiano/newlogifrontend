import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoirdetailbesoinComponent } from './voirdetailbesoin.component';

describe('VoirdetailbesoinComponent', () => {
  let component: VoirdetailbesoinComponent;
  let fixture: ComponentFixture<VoirdetailbesoinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoirdetailbesoinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoirdetailbesoinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
