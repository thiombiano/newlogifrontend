import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BesoinjournalierComponent } from './besoinjournalier.component';

describe('BesoinjournalierComponent', () => {
  let component: BesoinjournalierComponent;
  let fixture: ComponentFixture<BesoinjournalierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BesoinjournalierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BesoinjournalierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
