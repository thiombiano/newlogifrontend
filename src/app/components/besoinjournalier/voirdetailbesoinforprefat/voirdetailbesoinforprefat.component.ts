import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Besoinjournalier} from '../shared/besoinjournalier.model';
import {Router} from '@angular/router';
import {BesoinjournalierService} from '../shared/besoinjournalier.service';
import {ChantierService} from '../../chantier/shared/chantier.service';

@Component({
  selector: 'app-voirdetailbesoinforprefat',
  templateUrl: './voirdetailbesoinforprefat.component.html',
  styleUrls: ['./voirdetailbesoinforprefat.component.css']
})
export class VoirdetailbesoinforprefatComponent implements OnInit {
  firstlevel: string;
  secondlevel: string;
  besoinjournalier: Besoinjournalier;
  // tslint:disable-next-line:max-line-length
  constructor(private router: Router, private cdRef: ChangeDetectorRef, public ApiBesoin: BesoinjournalierService, public chantierService: ChantierService) { }

  ngOnInit() {
    const idbesoin =   window.localStorage.getItem('IDBesoin');
    console.log('id:' + idbesoin);
    this.firstlevel = 'Détail du besoin journalier';
    this.secondlevel = 'Besoin Journalier';
    this.ApiBesoin.getSelectedBesoin(+idbesoin).subscribe((data) => {
      this.besoinjournalier = data.data;
    });
  }

}
