import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoirdetailbesoinforprefatComponent } from './voirdetailbesoinforprefat.component';

describe('VoirdetailbesoinforprefatComponent', () => {
  let component: VoirdetailbesoinforprefatComponent;
  let fixture: ComponentFixture<VoirdetailbesoinforprefatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoirdetailbesoinforprefatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoirdetailbesoinforprefatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
