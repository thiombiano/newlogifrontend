import { TestBed } from '@angular/core/testing';

import { BesoinjournalierService } from './besoinjournalier.service';

describe('BesoinjournalierService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BesoinjournalierService = TestBed.get(BesoinjournalierService);
    expect(service).toBeTruthy();
  });
});
