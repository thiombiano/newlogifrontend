export interface Alldetailbesoinlevelone {
  idbesoinjournalier: number;
  idmaison: number;
  dateBJ: Date;
  libellemaison: string;
  libelleFonction: string;
  libelleetape: string;
  nom: string;
  prenom: string;

}
