import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {DetailsBesoin, Recueilbesoin} from '../../recueilbesoin/shared/recueilbesoin.model';
import {BesoinJItem, Besoinjournalier} from './besoinjournalier.model';
import {environment} from '../../../../environments/environment';
import {catchError, map} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Access-Control-Allow-Origin': '*'
  })
};

@Injectable()
export class BesoinjournalierService {
  formData: {cahierjournalier: null, journee_id: null, chantier_id: null, technicien_id: null, id: null,  articles: BesoinJItem[] };

  constructor(public http: HttpClient) { }

  getAllBesoinList(idChantier: number, idutilisateur: number) {
    return this.http.get(environment.apiUrl + '/besoinsbytechnicien/' + idChantier + '/' + idutilisateur)
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Impossible dobtenir la liste de touts besoins journalier!'))
      );
  }

  getAllBesoinForThisDay(idChantier: number) {
    return this.http.get(environment.apiUrl + '/chantiers/' + idChantier + '/todaybesionjournalier')
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Impossible dobtenir la liste de touts besoins journalier du jour!'))
      );
  }

  getSelectedBesoin(id: number) {
    return  this.http.get(environment.apiUrl + '/besoins/' + id) .pipe(map((data: any) => data ),
      catchError(error => throwError('Impossible dobtenir la liste de toutes les besoins!'))
    );
  }

  getSelectedBesoinDetailLevelOne(id: number) {
    return  this.http.get(environment.apiUrl + '/besoins/alldetailbesoin/' + id) .pipe(map((data: any) => data ),
      catchError(error => throwError('Impossible dobtenir la liste de toutes les details besoins level one!'))
    );
  }

  getSelectedBesoinDetailLevelOneInPrefat(id: number) {
    return  this.http.get(environment.apiUrl + '/besoins/alldetailbesoininprefat/' + id) .pipe(map((data: any) => data ),
      catchError(error => throwError('Impossible dobtenir la liste de toutes les details besoins dans la préfat level one!'))
    );
  }

  getSelectedBesoinDetailByMaisonLevelTwo(idbesoin: number, idmaison: number) {
    // tslint:disable-next-line:max-line-length
    return  this.http.get(environment.apiUrl + '/besoins/alldetailbesoinbymaison/' + idbesoin + '/' + idmaison) .pipe(map((data: any) => data ),
      catchError(error => throwError('Impossible dobtenir la liste de toutes les details besoins level two!'))
    );
  }

  getSelectedBesoinDetailByPrefatLevelTwo(idbesoin: number, idprefat: number) {
    // tslint:disable-next-line:max-line-length
    return  this.http.get(environment.apiUrl + '/besoins/alldetailbesoinbyprefat/' + idbesoin + '/' + idprefat) .pipe(map((data: any) => data ),
      catchError(error => throwError('Impossible dobtenir la liste de toutes les details besoins  dans la préfat level two!'))
    );
  }

  addChantierBesoin(idChantier: number, besoin: Besoinjournalier) {
    return this.http
      .post<Besoinjournalier>(
        environment.apiUrl + '/chantiers/' + idChantier + '/besoinjournaliers',
        besoin,
        httpOptions
      );
  }

  isEvented(besoins: BesoinJItem): BesoinJItem {
    return besoins;
  }

  updateAnnulerChantierBesoin(idBesoin, value: any) {
    return this.http
      .post<Besoinjournalier>(
        environment.apiUrl + '/annulationbesoinjournalier/' + idBesoin , value
      );
  }

  updateChantierBesoin(idBesoin, value: any) {
    const id =  window.localStorage.getItem('Idchantier');
    return this.http
      .put<Besoinjournalier>(
        environment.apiUrl + '/chantiers/' + idBesoin + '/besoinjournaliers/' + id , value
      );
  }

}
