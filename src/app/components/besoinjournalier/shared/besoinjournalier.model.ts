import {Employe} from '../../employe/shared/employe.model';

export class Besoinjournalier {
  constructor(
    public journee_id: number,
    public cahierjournalier: string,
    public chantier_id: number,
    public technicien_id: number,
    public dateBJ: Date,
    public typebesoin: string,
    public nomtechnicien: string,
    public articles: BesoinJItem[],
    public id?: number) {}
}

export class BesoinJItem {
  // @ts-ignore
  constructor(
    // public besoin_journalier_id: number,
    public id: number,
    public maison_id: number,
    public prefat_id: number,
    public etape_construction_id: number,
    public article_id: number,
    public quantiteBesoin: number,
    public commentaires: string,
    public article: string,
    public libelleetape: string,
    public libellemaison: string,
    public libelleprefat: string,
    public fonction_id: number,
    public libellefonction: string
  ) {}

}
