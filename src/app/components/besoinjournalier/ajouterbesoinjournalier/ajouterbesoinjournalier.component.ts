import { Component, OnInit } from '@angular/core';
import {DatePipe, formatDate} from '@angular/common';
import {Unitemesure} from '../../article/Shared/unitemesure';
import {FormBuilder, FormGroup} from '@angular/forms';
import {NgxSpinnerService} from 'ngx-spinner';
import {DateAdapter, MatDialog} from '@angular/material';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {ChantierService} from '../../chantier/shared/chantier.service';
import {Router} from '@angular/router';
import {RecueilbesoinService} from '../../recueilbesoin/shared/recueilbesoin.service';
import {ArticleService} from '../../article/Shared/article.service';
import {BesoinJItem, Besoinjournalier} from '../shared/besoinjournalier.model';
import {Maison} from '../../maison/shared/maison.model';
import {LotService} from '../../lot/shared/lot.service';
import {AdddetailComponent} from '../adddetail/adddetail.component';
import {Article} from '../../article/Shared/article';
import {Etape} from '../../etapeconstruction/shared/etape.model';
import {EtapeService} from '../../etapeconstruction/shared/etape.service';
import {MaisonService} from '../../maison/shared/maison.service';
import {BesoinjournalierService} from '../shared/besoinjournalier.service';
import {ModifierlotComponent} from '../../lot/modifierlot/modifierlot.component';
import {UpdatedetailComponent} from '../updatedetail/updatedetail.component';
import {FonctionsService} from '../../fonctions/shared/fonctions.service';
import {Fonction} from '../../fonctions/shared/fonction';
import {StocksService} from '../../stocks/shared/stocks.service';
import {AdddetailtoprefatComponent} from '../adddetailtoprefat/adddetailtoprefat.component';
import {Prefat} from '../../prefat/shared/prefat.model';
import {PrefatService} from '../../prefat/shared/prefat.service';
import {isEmpty} from 'rxjs/operators';
import {Section} from "../../section/shared/section.model";
import {Lot} from "../../lot/shared/lot.model";
import {SectionService} from "../../section/shared/section.service";

@Component({
  selector: 'app-ajouterbesoinjournalier',
  templateUrl: './ajouterbesoinjournalier.component.html',
  styleUrls: ['./ajouterbesoinjournalier.component.css']
})
export class AjouterbesoinjournalierComponent implements OnInit {
  nextLabel = 'Suivant';
  previousLabel = 'Précédent';
  maxSize = 9;
  directionLinks = true;
  autoHide = false;
  responsive = true;
  screenReaderPaginationLabel = 'Pagination';
  screenReaderPageLabel = 'page';
  screenReaderCurrentLabel = 'Vous êtes sur cette page';
  public page: number;
  public isNull: string;
  public pageSize: number;
  p = 1;
  count = 10;
  searchText;
  headElement = ['N°', 'Fonction', 'Maison', 'Article', 'Quantité', 'Etape avancement', 'Action'];
  headElementPrefat = ['N°', 'Fonction', 'Prefat', 'Article', 'Quantité', 'Action'];
  currentdate =  formatDate(new Date(), 'dd/MM/yyyy:HH:mm:ss', 'en');
  displayDetails = false;
  secondlevel: string;
  date_RB: Date;
  fonctions: Fonction[];
  unitemesure: Unitemesure[];
  today: string;
  firstlevel: string;
  maisons: Maison[];
  articles: Article[];
  addForm: FormGroup;
  sections: Section[] = null;
  etapes: Etape[];
  lotofsections: Lot[];
  lotofsectionsprime: Lot[];
  public unite: any;
  public quantite: any;
  // @ts-ignore
  besoinjournalier: Besoinjournalier = new Besoinjournalier();
  // @ts-ignore
  besoinjournalierprefat: Besoinjournalier = new Besoinjournalier();
  thedate: Date;
  public touslesetapes: Etape[];
  public libellemaison: string;
  public libelleetape: string;
  public libellearticle: string;
  public isQuantityValide = 0;
  public isDataInTheTable: boolean;
  public prefats: Prefat[];
  public isDataInTheTableInPrefat: boolean;
  get f() { return this.addForm.controls; }
  // tslint:disable-next-line:max-line-length
  constructor(public ApiSection: SectionService, public ApiPrefat: PrefatService, public ApiStock: StocksService, public fonctionApi: FonctionsService, public ApiMaison: MaisonService, public ApiEtape: EtapeService, public ApiArticle: ArticleService, public dialog: MatDialog, public ApiLot: LotService, private SpinnerService: NgxSpinnerService, private dateAdapter: DateAdapter<Date>, private notifyService: NotificationService, private toastr: ToastrService, private chantierService: ChantierService, private router: Router, public ApiBesoin: BesoinjournalierService, public articleService: ArticleService, private datePipe: DatePipe, private _adapter: DateAdapter<any>, private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.isDataInTheTable = false;
    this.isDataInTheTableInPrefat = false;
    this._adapter.setLocale('fr');
    this.firstlevel = 'Besoin journalier';
    this.secondlevel = 'Ajouter un besoin journalier';
    const ChantierId = window.localStorage.getItem('Idchantier');
    this.ListSectionOfChantier(+ChantierId);
    this.getAllMaisonByChantier(+ChantierId);
    this.getAllPrefat(+ChantierId);
    this.getAllArticle();
    this.getAllEtape();
    this.besoinjournalier = {
      journee_id: null,
      cahierjournalier: null,
      dateBJ: null,
      chantier_id: null,
      typebesoin: null,
      technicien_id: null,
      nomtechnicien: null,
      articles: new Array<BesoinJItem>(),
      id: null
    };

    this.besoinjournalierprefat = {
      journee_id: null,
      cahierjournalier: null,
      dateBJ: null,
      chantier_id: null,
      typebesoin: null,
      technicien_id: null,
      nomtechnicien: null,
      articles: new Array<BesoinJItem>(),
      id: null
    };
  }

  getAllMaisonByChantier(idchantier: number) {
    this.ApiLot.getALLMaisonByChantier(+idchantier).subscribe((data) => {
      console.log(data.data);
      this.maisons = data.data;
    });
  }

  getAllArticle() {
      this.ApiArticle.getAllArticleList().subscribe((data) => {
      console.log(data);
      this.articles = data;
    });
  }
  getAllEtape() {
    this.ApiEtape.getAllEtapeList().subscribe((etape: Etape[]) => {
      this.touslesetapes = etape;
    });
  }

  getAllPrefat(idchantier: number) {
    this.ApiPrefat.getPrefatList(+idchantier).subscribe((prefat: Prefat[]) => {
      this.prefats = prefat;
    });
  }

  getLibelleOfMaisonEtapeArticle(idarticle: number, idmaison: number, idetape: number) {
    this.articleService.getArticle(+idarticle).subscribe((article: Article) => {
      this.libellearticle = article.libelle_article;
    });

    this.ApiMaison.getMaisonByIdInBesoin(+idmaison).subscribe((maison: Maison) => {
      this.libellemaison = maison.libellemaison;
    });

    this.ApiEtape.getEtape(+idetape).subscribe((etape: Etape) => {
      this.libelleetape = etape.libelleetape;
    });

  }

  showToasterEnlevementErreurQuantite() {
    this.notifyService.showErrorWithTimeout('Erreur: Quantité inssufisante !!', 'Notification', 5000);
  }


  /* DEBUT AJOUTER LES DETAILS DANS LE BESOIN JOURNALIER POUR LES MAISONS */

  addNewDetail() {
    // tslint:disable-next-line:max-line-length
    const dialogRef = this.dialog.open(AdddetailComponent, {
      data: {
        besoin: Besoinjournalier,
        getallmaisons: this.maisons,
        getallsections: this.sections,
        getallarticles: this.articles
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        const idmaison = JSON.stringify(dialogRef.componentInstance.thebesoin.maison_id);
        const idetape = JSON.stringify(dialogRef.componentInstance.thebesoin.etape_construction_id);
        const idarticle = JSON.stringify(dialogRef.componentInstance.thebesoin.article_id);
        this.getLibelleOfMaisonEtapeArticle(+idarticle, +idmaison, +idetape);
        console.log('la maison : ' + this.libellemaison);
        console.log('la result : ' + JSON.stringify(dialogRef.componentInstance.thebesoin));
        const idchantier =  window.localStorage.getItem('Idchantier');
        const lidarticle = dialogRef.componentInstance.thebesoin.article_id;
        const quantiteBesoin = dialogRef.componentInstance.thebesoin.quantiteBesoin;

        const article_id = dialogRef.componentInstance.thebesoin.article_id;
        const libellemaison = dialogRef.componentInstance.thebesoin.libellemaison;
        const libelleetape = dialogRef.componentInstance.thebesoin.libelleetape;
        const LquantiteBesoin = dialogRef.componentInstance.thebesoin.quantiteBesoin;
        const commentaires = '';
        const Larticle = dialogRef.componentInstance.thebesoin.article;
        const etape_construction_id = dialogRef.componentInstance.thebesoin.etape_construction_id;
        const maison_id = dialogRef.componentInstance.thebesoin.maison_id;
        const fonction_id = dialogRef.componentInstance.thebesoin.fonction_id;
        const libellefonction =  dialogRef.componentInstance.thebesoin.libellefonction;

        this.ApiStock.getQuantiteDisponible(+idchantier, +lidarticle).subscribe((data) => {
          console.log('la quantité disponible pour cet article est : ' + data);
          if (quantiteBesoin > data ) {
            this.showToasterEnlevementErreurQuantite();
            return;
          }
          this.besoinjournalier.articles.push({
            id: 0,
            article_id,
            prefat_id: null,
            libellemaison,
            libelleetape,
            quantiteBesoin: LquantiteBesoin,
            commentaires,
            article: Larticle,
            etape_construction_id,
            maison_id,
            fonction_id,
            libellefonction,
            libelleprefat: ''
          });
          this.isDataInTheTable = true;
        });
      }
    });
  }

  /* FIN AJOUTER LES DETAILS DANS LE BESOIN JOURNALIER POUR LES MAISONS */




  /* DEBUT AJOUTER LES DETAILS DANS LA PREFAT  */

  addNewDetailforPrefat() {
    // tslint:disable-next-line:max-line-length
    const dialogRef = this.dialog.open(AdddetailtoprefatComponent, {
      data: {
        besoin: Besoinjournalier,
        getallprefats: this.prefats,
        getallarticles: this.articles
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        const idprefat = JSON.stringify(dialogRef.componentInstance.thebesoinofprefat.prefat_id);
        const idarticle = JSON.stringify(dialogRef.componentInstance.thebesoinofprefat.article_id);
        const idchantier =  window.localStorage.getItem('Idchantier');
        const lidarticle = dialogRef.componentInstance.thebesoinofprefat.article_id;
        const quantiteBesoin = dialogRef.componentInstance.thebesoinofprefat.quantiteBesoin;

        const article_id = dialogRef.componentInstance.thebesoinofprefat.article_id;
        const libelleprefat = dialogRef.componentInstance.thebesoinofprefat.libelleprefat;
        const LquantiteBesoin = dialogRef.componentInstance.thebesoinofprefat.quantiteBesoin;
        const commentaires = '';
        const Larticle = dialogRef.componentInstance.thebesoinofprefat.article;
        const prefat_id = dialogRef.componentInstance.thebesoinofprefat.prefat_id;
        const fonction_id = dialogRef.componentInstance.thebesoinofprefat.fonction_id;
        const libellefonction =  dialogRef.componentInstance.thebesoinofprefat.libellefonction;

        this.ApiStock.getQuantiteDisponible(+idchantier, +lidarticle).subscribe((data) => {
          console.log('la quantité disponible pour cet article est : ' + data);
          if (quantiteBesoin > data ) {
            this.showToasterEnlevementErreurQuantite();
            return;
          }
          this.besoinjournalierprefat.articles.push({
            id: 0,
            article_id,
            prefat_id,
            libelleprefat,
            libelleetape: 'null',
            quantiteBesoin: LquantiteBesoin,
            commentaires,
            article: Larticle,
            etape_construction_id: null,
            maison_id: null,
            fonction_id,
            libellefonction,
            libellemaison: 'null'
          });
          this.isDataInTheTableInPrefat = true;
        });
      }
    });
  }

  /* FIN AJOUTER LES DETAILS DANS LA PREFATPOUR LES MAISONS */





  supprimerLaLigneDetail(lignebesoin) {
    const index  =  this.besoinjournalier.articles.indexOf(lignebesoin);
    if (index !== -1) {
      this.besoinjournalier.articles.splice(index, 1);
    }

    if (this.besoinjournalier.articles.length === 0) {
      this.isDataInTheTable = false;
    }
  }


  supprimerLaLigneDetailInPrefat(lignebesoin) {
    const index  =  this.besoinjournalierprefat.articles.indexOf(lignebesoin);
    if (index !== -1) {
      this.besoinjournalierprefat.articles.splice(index, 1);
    }

    if (this.besoinjournalierprefat.articles.length === 0) {
      this.isDataInTheTableInPrefat = false;
    }
  }

  // tslint:disable-next-line:max-line-length
  startEditBesoin(i: number, id: number, maison_id: number, fonction_id: number, article_id: number , etape_construction_id: number, quantiteBesoin: number) {
    console.log('la fonction sélectionner est : ' + maison_id);
    this.fonctionApi.getAllFonction().subscribe((fonction: Fonction[]) => {
      this.fonctions = fonction;
      console.log('la liste des maisons : ' + JSON.stringify(this.fonctions) );
    });
    const dialogRef = this.dialog.open(UpdatedetailComponent, {
      // tslint:disable-next-line:max-line-length
      data: {id, maison_id, fonction_id, article_id, etape_construction_id, quantiteBesoin, getallmaisons: this.maisons, getallarticles: this.articles, getalletapes: this.touslesetapes, getallfonctions: this.fonctions}
    });
    const pos = i;
    // @ts-ignore

    // @ts-ignore
    // tslint:disable-next-line:max-line-length

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        const besoinj: BesoinJItem = new BesoinJItem(
          0,
          dialogRef.componentInstance.thebesoin.maison_id,
          0,
          dialogRef.componentInstance.thebesoin.etape_construction_id,
          dialogRef.componentInstance.thebesoin.article_id,
          dialogRef.componentInstance.thebesoin.quantiteBesoin,
          'null',
          dialogRef.componentInstance.thebesoin.article,
          dialogRef.componentInstance.thebesoin.libelleetape,
          dialogRef.componentInstance.thebesoin.libellemaison,
          'null',
          dialogRef.componentInstance.thebesoin.fonction_id,
          dialogRef.componentInstance.thebesoin.libellefonction,
        );
        this.besoinjournalier.articles[i] =  besoinj;
      }
    });
  }


  showToasterBesoinEnregistrer() {
    this.notifyService.showSuccessWithTimeout('Besoin enregistré avec succès !!', 'Notification', 5000);
  }

  EnregistrerLeBesoinJournalier() {
    const ChantierId = window.localStorage.getItem('Idchantier');
    const idutilisateur = window.localStorage.getItem('idutilisateur');
    this.besoinjournalier.technicien_id = +idutilisateur;
    this.besoinjournalier.chantier_id = +ChantierId;
    this.besoinjournalier.typebesoin = 'Vers les maisons';
    this.besoinjournalier.journee_id = 1;
    this.ApiBesoin.addChantierBesoin(+ChantierId,  this.besoinjournalier).subscribe((besoin: Besoinjournalier) => {
      this.showToasterBesoinEnregistrer();
      this.besoinjournalier.articles = null;
      this.router.navigate(['/start/besoinjournaliers']);
    });
  }


  EnregistrerLeBesoinJournalierPourLaPrefat() {
    const ChantierId = window.localStorage.getItem('Idchantier');
    const idutilisateur = window.localStorage.getItem('idutilisateur');
    this.besoinjournalierprefat.technicien_id = +idutilisateur;
    this.besoinjournalierprefat.chantier_id = +ChantierId;
    this.besoinjournalier.typebesoin = 'Vers les préfats';
    this.besoinjournalierprefat.journee_id = 1;
    console.log(this.besoinjournalierprefat);
    this.ApiBesoin.addChantierBesoin(+ChantierId,  this.besoinjournalierprefat).subscribe((besoin: Besoinjournalier) => {
      this.showToasterBesoinEnregistrer();
      this.besoinjournalierprefat.articles = null;

      this.router.navigate(['/start/besoinjournaliers']);
    });
  }

  clearDataOfBesoinJournalierPrefat() {
    this.besoinjournalierprefat.articles = [];
    this.isDataInTheTableInPrefat = false;
  }

  clearDataOfBesoinJournalier() {
    this.besoinjournalier.articles = [];
    this.isDataInTheTable = false;
  }

  ListSectionOfChantier(id: number) {
    this.ApiSection.getSectionList(+id).subscribe((section: Section[]) => {
      this.sections = section;
      console.log(section);
    });
  }

}
