import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjouterbesoinjournalierComponent } from './ajouterbesoinjournalier.component';

describe('AjouterbesoinjournalierComponent', () => {
  let component: AjouterbesoinjournalierComponent;
  let fixture: ComponentFixture<AjouterbesoinjournalierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjouterbesoinjournalierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjouterbesoinjournalierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
