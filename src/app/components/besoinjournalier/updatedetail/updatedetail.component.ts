import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {ArticleService} from '../../article/Shared/article.service';
import {EtapeService} from '../../etapeconstruction/shared/etape.service';
import {BesoinjournalierService} from '../shared/besoinjournalier.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {BesoinJItem} from '../shared/besoinjournalier.model';
import {MaisonService} from '../../maison/shared/maison.service';
import {Etape} from '../../etapeconstruction/shared/etape.model';
import {FormControl, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';
import {Maison} from '../../maison/shared/maison.model';
import {FonctionsService} from '../../fonctions/shared/fonctions.service';
import {Personnel} from '../../personnels/shared/personnel';
import {Fonction} from '../../fonctions/shared/fonction';

@Component({
  selector: 'app-updatedetail',
  templateUrl: './updatedetail.component.html',
  styleUrls: ['./updatedetail.component.css']
})
export class UpdatedetailComponent {
  @Output() passEntry: EventEmitter<any> = new EventEmitter();
  // tslint:disable-next-line:max-line-length
  constructor(public fonctionApi: FonctionsService, public articleService: ArticleService,  public ApiEtape: EtapeService, public ApiBesoinJournalier: BesoinjournalierService, public dialogRef: MatDialogRef<UpdatedetailComponent>,
              @Inject(MAT_DIALOG_DATA) public data: BesoinJItem,
              public ApiMaison: MaisonService, public ApiFonction: FonctionsService) { }

  etape: string;
  etapes: Etape[];
  maison: string;
  article: string ;
  libelleetape: string;
  prestataire: Personnel;
  fonctions: Fonction[];
  leprestaire: string;
  thebesoin: BesoinJItem;
  formControl = new FormControl('', [
    Validators.required
  ]);

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  getEtapeByMaison(value) {
    this.ApiEtape.getAllEtapeListByIdMaison(+value).subscribe((data) => {
      this.data['getalletapes'] = data;
    });
    this.ApiMaison.getMaisonForBesoin(value);
    window.localStorage.removeItem('idmaison');
    window.localStorage.setItem('idmaison', value);
    this.ApiMaison.getMaisonForBesoin(value);
    this.leprestaire = null;
    this.etapes = null;
    this.fonctions = null;
    this.getFonctionByMaison(value);
  }

  setSelectedArticle(value) {
    this.articleService.getArticleForBesoin(value);
  }

  getFonctionByMaison(value) {
    this.fonctionApi.getFonctionByMaison(value).subscribe((fonction: Fonction[]) => {
      this.fonctions = fonction;
    });
  }

  setSelectedEtape(value) {
    this.ApiEtape.getEtapeForBesoin(value);
  }

  setPrestaire(value: any) {
    const idmaison = window.localStorage.getItem('idmaison');
    this.fonctionApi.getPrestataireByMaisonAndByFonction(+idmaison, value ).subscribe((prestataire: Personnel[]) => {
      this.prestataire = prestataire[0];
      this.leprestaire = prestataire[0].prenom + ' ' + prestataire[0].nom;
      console.log('le prestataire est : ' + JSON.stringify(this.prestataire)  );
    });
  }

  setSelectedFonction(value) {
    this.ApiMaison.getMaisonForBesoin(value);
  }

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Le champ est requis' : '';
  }

  stopEdit(): void {
    this.thebesoin = ({
      id: 0,
      maison_id: this.data.maison_id,
      etape_construction_id: this.data.etape_construction_id,
      article_id: this.data.article_id,
      quantiteBesoin: this.data.quantiteBesoin,
      commentaires: '',
      article: this.articleService.selectedOneArticle.libelle_article,
      libelleetape: this.ApiEtape.selectedOneEtape.libelleetape,
      libellemaison: this.ApiMaison.selectedOneMaison[0].libellemaison,
      fonction_id: this.data.fonction_id,
      libellefonction: this.ApiFonction.selectedOneFonction[0].libellefonction,
      libelleprefat: 'null',
      prefat_id: 0
    });
    console.log('Voici article : ' + JSON.stringify( this.ApiMaison.selectedOneMaison[0]));
    console.log('ALL DATA MUST EMITTED : ' + JSON.stringify(this.thebesoin) );
  }

}
