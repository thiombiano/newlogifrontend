import {ChangeDetectorRef, Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {MdbTableDirective, MdbTablePaginationComponent} from 'angular-bootstrap-md';
import {Recueilbesoin} from '../recueilbesoin/shared/recueilbesoin.model';
import {Besoinjournalier} from './shared/besoinjournalier.model';
import {Router} from '@angular/router';
import {RecueilbesoinService} from '../recueilbesoin/shared/recueilbesoin.service';
import {ChantierService} from '../chantier/shared/chantier.service';
import {BesoinjournalierService} from './shared/besoinjournalier.service';

@Component({
  selector: 'app-besoinjournalier',
  templateUrl: './besoinjournalier.component.html',
  styleUrls: ['./besoinjournalier.component.css']
})
export class BesoinjournalierComponent implements OnInit {
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  elements: any = [];
  display = 'none';
  curDate = new Date();
  besoinsjournaliers: Besoinjournalier[];
  previous: string;
  searchText = '';
  totalbesoinjournalier: number;
  firstlevel: string;
  secondlevel: string;
  headElements = ['N°',  'BESOIN JOURNALIER' , 'TYPE BESOIN', 'DATE BJ', 'Actions'];
  // tslint:disable-next-line:max-line-length
  constructor(private router: Router, private cdRef: ChangeDetectorRef, public ApiBesoin: BesoinjournalierService, public chantierService: ChantierService) { }

  ngOnInit() {
    this.firstlevel = 'Tableau de bord';
    this.secondlevel = 'Besoins journaliers';
    const id =  window.localStorage.getItem('Idchantier');
    this.getListeBesoinJournalier(+id);
  }


  searchItems() {
    const prev = this.besoinsjournaliers;

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.elements = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(15);
    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

  @HostListener('input') oninput() {
    this.searchItems();
  }

  voirdetailbesoinJournalier(id, typebesoin) {
    window.localStorage.removeItem('IDBesoin');
    window.localStorage.setItem('IDBesoin', id);
    if (typebesoin === 'Vers les maisons') {
      this.router.navigate(['/start/voir-besoinjournalier']);
    } else {
      this.router.navigate(['/start/voir-besoinjournalierforprefat']);
    }
  }

  effectuerunenlevement(id) {
    window.localStorage.removeItem('IDBesoin');
    window.localStorage.setItem('IDBesoin', id);
    this.router.navigate(['/start/effectuer-enlevement']);
  }

  ModifierBesoinJournalier(id, typebesoin) {
    window.localStorage.removeItem('IDMbesoin');
    window.localStorage.setItem('IDMbesoin', id);
    if(typebesoin === 'Vers les maisons') {
      this.router.navigate(['/start/modifier-besoinjournalier']);
    } else {
      this.router.navigate(['/start/modifier-besoinjournalierforprefat']);
    }

  }

  getListeBesoinJournalier(id) {
    const idutilisateur =  window.localStorage.getItem('idutilisateur');
    this.ApiBesoin.getAllBesoinList(id, +idutilisateur).subscribe(
      data => {
        this.besoinsjournaliers = data.data;
        this.totalbesoinjournalier =  this.besoinsjournaliers.length;
        this.mdbTable.setDataSource(this.besoinsjournaliers);
        this.elements = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();
      },
      err => console.error(err),
      () => console.log('liste besoins journaliers obtenue')
    );
  }

  voirdetailBesoinForeachHouseOrPrefat(id, cahierjournalier, dateBJ, nomtechnicien, typebesoin) {
    window.localStorage.removeItem('libelletypebesoin');
    window.localStorage.removeItem('IDBBbesoin');
    window.localStorage.removeItem('nomtechnicien');
    window.localStorage.removeItem('dateBJ');
    window.localStorage.removeItem('LIBELLEcahierjournalier');
    window.localStorage.setItem('LIBELLEcahierjournalier', cahierjournalier);
    window.localStorage.setItem('libelletypebesoin', typebesoin);
    window.localStorage.setItem('dateBJ', dateBJ);
    window.localStorage.setItem('nomtechnicien', nomtechnicien);
    window.localStorage.setItem('IDBBbesoin', id);
    this.router.navigateByUrl('/start/voir-detailbesoinjournalier');
  }
}
