import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifierbesoinjournalierforprefatComponent } from './modifierbesoinjournalierforprefat.component';

describe('ModifierbesoinjournalierforprefatComponent', () => {
  let component: ModifierbesoinjournalierforprefatComponent;
  let fixture: ComponentFixture<ModifierbesoinjournalierforprefatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifierbesoinjournalierforprefatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifierbesoinjournalierforprefatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
