import { Component, OnInit } from '@angular/core';
import {DatePipe, formatDate} from '@angular/common';
import {Unitemesure} from '../../article/Shared/unitemesure';
import {Maison} from '../../maison/shared/maison.model';
import {Article} from '../../article/Shared/article';
import {FormBuilder, FormGroup} from '@angular/forms';
import {BesoinJItem, Besoinjournalier} from '../shared/besoinjournalier.model';
import {Etape} from '../../etapeconstruction/shared/etape.model';
import {MaisonService} from '../../maison/shared/maison.service';
import {EtapeService} from '../../etapeconstruction/shared/etape.service';
import {ArticleService} from '../../article/Shared/article.service';
import {DateAdapter, MatDialog} from '@angular/material';
import {LotService} from '../../lot/shared/lot.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {ChantierService} from '../../chantier/shared/chantier.service';
import {Router} from '@angular/router';
import {BesoinjournalierService} from '../shared/besoinjournalier.service';
import {PrefatService} from '../../prefat/shared/prefat.service';
import {Prefat} from '../../prefat/shared/prefat.model';
import {AdddetailtoprefatComponent} from '../adddetailtoprefat/adddetailtoprefat.component';
import {StocksService} from '../../stocks/shared/stocks.service';

@Component({
  selector: 'app-modifierbesoinjournalierforprefat',
  templateUrl: './modifierbesoinjournalierforprefat.component.html',
  styleUrls: ['./modifierbesoinjournalierforprefat.component.css']
})
export class ModifierbesoinjournalierforprefatComponent implements OnInit {
  nextLabel = 'Suivant';
  previousLabel = 'Précédent';
  maxSize = 9;
  directionLinks = true;
  autoHide = false;
  responsive = true;
  screenReaderPaginationLabel = 'Pagination';
  screenReaderPageLabel = 'page';
  screenReaderCurrentLabel = 'Vous êtes sur cette page';
  public page: number;
  public isNull: string;
  public pageSize: number;
  p = 1;
  count = 10;
  searchText;
  headElement = ['N°', 'Fonction', 'Préfat', 'Article', 'Quantité', 'Action'];
  currentdate =  formatDate(new Date(), 'dd/MM/yyyy:HH:mm:ss', 'en');
  displayDetails = false;
  secondlevel: string;
  unitemesure: Unitemesure[];
  today: string;
  firstlevel: string;
  prefats: Prefat[];
  articles: Article[];
  addForm: FormGroup;
  public unite: any;
  public quantite: any;
  // @ts-ignore
  besoinjournalier: Besoinjournalier = new Besoinjournalier();
  thedate: Date;
  public libelleprefat: string;
  public libellearticle: string;
  get f() { return this.addForm.controls; }
  // tslint:disable-next-line:max-line-length
  constructor(public ApiStock: StocksService, public ApiPrefat: PrefatService, public ApiArticle: ArticleService, public dialog: MatDialog, public ApiLot: LotService, private SpinnerService: NgxSpinnerService, private dateAdapter: DateAdapter<Date>, private notifyService: NotificationService, private toastr: ToastrService, private chantierService: ChantierService, private router: Router, public ApiBesoin: BesoinjournalierService, public articleService: ArticleService, private datePipe: DatePipe, private _adapter: DateAdapter<any>, private _formBuilder: FormBuilder) { }

  ngOnInit() {
    const idbesoin =   window.localStorage.getItem('IDMbesoin');
    this.firstlevel = 'Besoin journalier pour Préfat';
    this.secondlevel = 'Mis à jour du besoin journalier pour la préfat';
    this.ApiBesoin.getSelectedBesoin(+idbesoin).subscribe((data) => {
      this.besoinjournalier = data.data;
      console.log(data);
    });
    const ChantierId = window.localStorage.getItem('Idchantier');
    this.getAllPrefat(+ChantierId);
    this.getAllArticle();
    this.besoinjournalier = {
      journee_id: null,
      cahierjournalier: null,
      dateBJ: null,
      chantier_id: null,
      typebesoin: null,
      nomtechnicien: null,
      technicien_id: null,
      articles: new Array<BesoinJItem>(),
      id: null
    };
  }

  getAllPrefat(ChantierId: number) {
    this.ApiPrefat.getPrefatList(+ChantierId).subscribe((data) => {
      console.log(data);
      this.prefats = data;
    });
  }

  getAllArticle() {
    this.ApiArticle.getAllArticleList().subscribe((data) => {
      console.log(data);
      this.articles = data;
    });
  }

  supprimerLaLigneDetail(lignebesoin) {
    const index  =  this.besoinjournalier.articles.indexOf(lignebesoin);
    if (index !== -1) {
      this.besoinjournalier.articles.splice(index, 1);
    }
  }

  showToasterEnlevementErreurQuantite() {
    this.notifyService.showErrorWithTimeout('Erreur: Quantité inssufisante !!', 'Notification', 5000);
  }


  addNewDetailforPrefat() {
    // tslint:disable-next-line:max-line-length
    const dialogRef = this.dialog.open(AdddetailtoprefatComponent, {
      data: {
        besoin: Besoinjournalier,
        getallprefats: this.prefats,
        getallarticles: this.articles
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        const idprefat = JSON.stringify(dialogRef.componentInstance.thebesoinofprefat.prefat_id);
        const idarticle = JSON.stringify(dialogRef.componentInstance.thebesoinofprefat.article_id);
        const idchantier =  window.localStorage.getItem('Idchantier');
        const lidarticle = dialogRef.componentInstance.thebesoinofprefat.article_id;
        const quantiteBesoin = dialogRef.componentInstance.thebesoinofprefat.quantiteBesoin;

        let article_id = dialogRef.componentInstance.thebesoinofprefat.article_id;
        let libelleprefat = dialogRef.componentInstance.thebesoinofprefat.libelleprefat;
        let LquantiteBesoin = dialogRef.componentInstance.thebesoinofprefat.quantiteBesoin;
        let commentaires = '';
        let Larticle = dialogRef.componentInstance.thebesoinofprefat.article;
        let prefat_id = dialogRef.componentInstance.thebesoinofprefat.prefat_id;
        let fonction_id = dialogRef.componentInstance.thebesoinofprefat.fonction_id;
        let libellefonction =  dialogRef.componentInstance.thebesoinofprefat.libellefonction;

        this.ApiStock.getQuantiteDisponible(+idchantier, +lidarticle).subscribe((data) => {
          console.log('la quantité disponible pour cet article est : ' + data);
          if(quantiteBesoin > data ) {
            this.showToasterEnlevementErreurQuantite();
            return;
          }
          this.besoinjournalier.articles.push({
            id: 0,
            article_id: article_id,
            prefat_id: prefat_id,
            libelleprefat: libelleprefat,
            libelleetape: 'null',
            quantiteBesoin: LquantiteBesoin,
            commentaires: commentaires,
            article: Larticle,
            etape_construction_id: null,
            maison_id: null,
            fonction_id: fonction_id,
            libellefonction: libellefonction,
            libellemaison: 'null'
          });
        });


      }
    });
  }

  showToasterBesoinEnregistrer() {
    this.notifyService.showSuccessWithTimeout('Besoin mis à jour avec succès !!', 'Notification', 5000);
  }

  MettreAjourLeBesoinJournalierPourLaPrefat() {
    const idbesoin =   window.localStorage.getItem('IDMbesoin');
    const ChantierId = window.localStorage.getItem('Idchantier');
    const idutilisateur = window.localStorage.getItem('idutilisateur');
    this.besoinjournalier.technicien_id = +idutilisateur;
    this.besoinjournalier.chantier_id = +ChantierId;
    this.besoinjournalier.typebesoin = 'Vers les préfats';
    this.besoinjournalier.journee_id = 1;
    console.log(this.besoinjournalier);
    this.ApiBesoin.updateChantierBesoin(+idbesoin,  this.besoinjournalier).subscribe((besoin: Besoinjournalier) => {
      this.showToasterBesoinEnregistrer();
      this.besoinjournalier.articles = null;
      this.router.navigate(['/start/besoinjournaliers']);
    });
  }


}
