import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {PersonnelsService} from '../../personnels/shared/personnels.service';
import {DateAdapter, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DatePipe} from '@angular/common';
import {PrestationService} from '../../chantier/manageprestataire/shared/prestation.service';
import {Prestation} from '../../chantier/manageprestataire/shared/prestation.model';
import {MaisonService} from '../../maison/shared/maison.service';
import {BesoinjournalierService} from '../shared/besoinjournalier.service';
import {EtapeService} from '../../etapeconstruction/shared/etape.service';
import {Etape} from '../../etapeconstruction/shared/etape.model';
import {BesoinJItem, Besoinjournalier} from '../shared/besoinjournalier.model';
import {FormControl, Validators} from '@angular/forms';
import {ArticleService} from '../../article/Shared/article.service';
import {Maison} from '../../maison/shared/maison.model';
import {Article} from '../../article/Shared/article';
import {Observable, Subscription} from 'rxjs';
import {FonctionsService} from '../../fonctions/shared/fonctions.service';
import {Fonction} from '../../fonctions/shared/fonction';
import {Employe} from '../../employe/shared/employe.model';
import {Personnel} from '../../personnels/shared/personnel';
import {StocksService} from '../../stocks/shared/stocks.service';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {Lot} from '../../lot/shared/lot.model';
import {SectionService} from '../../section/shared/section.service';
import {LotService} from '../../lot/shared/lot.service';

@Component({
  selector: 'app-adddetail',
  templateUrl: './adddetail.component.html',
  styleUrls: ['./adddetail.component.css']
})
export class AdddetailComponent {
  @Output() passEntry: EventEmitter<any> = new EventEmitter();
  // tslint:disable-next-line:max-line-length
  constructor(public ApiLot: LotService, public ApiSection: SectionService, private notifyService: NotificationService, private toastr: ToastrService, public ApiStock: StocksService, public fonctionApi: FonctionsService, public articleService: ArticleService,  public ApiEtape: EtapeService, public ApiBesoinJournalier: BesoinjournalierService, public dialogRef: MatDialogRef<AdddetailComponent>,
              @Inject(MAT_DIALOG_DATA) public data: BesoinJItem,
              public ApiMaison: MaisonService) { }
  public isNull: string;
  sectionselectedinmaison: number;
  sectionselected: number;
  maisons: Maison[];
  lotofsectionsprime: Lot[];
  etape: string;
  etapes: Etape[];
  maison: string;
  article: string ;
  leprestaire: string;
  libelleetape: string;
  fonctions: Fonction[];
  prestataire: Personnel;
  thebesoin: BesoinJItem;
  formControl = new FormControl('', [
    Validators.required
  ]);

  getEtapeByMaison(value) {
    this.ApiEtape.getAllEtapeListByIdMaison(+value).subscribe((data) => {
      this.etapes = data;
    });
    window.localStorage.removeItem('idmaison');
    window.localStorage.setItem('idmaison', value);
    this.ApiMaison.getMaisonForBesoin(value);
    this.leprestaire = null;
    this.etapes = null;
    this.fonctions = null;
    this.getFonctionByMaison(value);

  }

  getFonctionByMaison(value) {
    this.fonctionApi.getFonctionByMaison(value).subscribe((fonction: Fonction[]) => {
      this.fonctions = fonction;
    });
  }


  setSelectedArticle(value) {
    this.articleService.getArticleForBesoin(value);
  }

  setSelectedFonction(value) {
    this.fonctionApi.getFonctionForBesoin(value);
  }

  setSelectedEtape(value) {
    this.ApiEtape.getEtapeForBesoin(value);
  }

  setPrestaire(value: any) {
    const idmaison = window.localStorage.getItem('idmaison');
    this.setSelectedFonction(value);
    this.fonctionApi.getPrestataireByMaisonAndByFonction(+idmaison, value ).subscribe((prestataire: Personnel[]) => {
      this.prestataire = prestataire[0];
      this.leprestaire = prestataire[0].prenom + ' ' + prestataire[0].nom;
      console.log('le prestataire est : ' + JSON.stringify(this.prestataire)  );
    });
  }

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Le champ est requis' : '';
  }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  showToasterEnlevementErreurQuantite() {
    this.notifyService.showErrorWithTimeout('Erreur: Quantité indisponible !!', 'Notification', 5000);
  }



  GetLotBySectionPrime(value) {
    console.log(value);
    window.localStorage.removeItem('sectionid');
    this.lotofsectionsprime = undefined;
    window.localStorage.setItem('sectionid', value);
    this.sectionselected = value;
    this.sectionselectedinmaison = value;
    this.ApiSection.getOneSectionOfLotPRIME(value).subscribe((datalot) =>  {
      this.lotofsectionsprime = datalot;
    });
    this.isNull = null;
    this.ApiLot.getMaisonBySection(+value).subscribe((data) =>  {
      if (data.data.length !== 0) {
        console.log(data.data);
        this.maisons = data.data[0].maisons;
        console.log(this.maisons);
      } else {
        this.maisons = [];
        console.log(this.maisons);
      }
    });
  }


  GetMaisonByLot(value) {
    console.log(value);
    window.localStorage.removeItem('lotid');
    window.localStorage.setItem('lotid', value);
    this.ApiLot.getLotByMaison(value).subscribe((data) =>  {
      this.data['getallmaisons'] = data.data[0].maisons;
      // this.maisons = data.data[0].maisons;
      console.log(this.maisons);
    });
  }

  public confirmAdd(): void {

    // tslint:disable-next-line:label-position
    // @ts-ignore
    this.thebesoin = ({
      maison_id: this.data.maison_id,
      etape_construction_id: this.data.etape_construction_id,
      article_id: this.data.article_id,
      quantiteBesoin: this.data.quantiteBesoin,
      commentaires: '',
      article: this.articleService.selectedOneArticle.libelle_article,
      libelleetape: this.ApiEtape.selectedOneEtape.libelleetape,
      fonction_id: this.data.fonction_id,
      libellemaison: this.ApiMaison.selectedOneMaison[0].libellemaison,
      libellefonction: this.fonctionApi.selectedOneFonction.libelleFonction
    });
    // console.log('Voici votre maison : ' + JSON.stringify(this.fonctionApi.selectedOneFonction[0].libelleFonction));
    console.log('ALL DATA MUST EMITTED : ' + JSON.stringify(this.thebesoin) );

  }


}
