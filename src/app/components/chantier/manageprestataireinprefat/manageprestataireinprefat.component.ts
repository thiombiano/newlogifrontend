import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Prestation} from '../manageprestataire/shared/prestation.model';
import {TabdisplaytodashModel} from '../manageprestataire/shared/tabdisplaytodash.model';
import {Fonction} from '../../fonctions/shared/fonction';
import {Typecontrat} from '../../typecontrat/shared/typecontrat.model';
import {Etape} from '../../etapeconstruction/shared/etape.model';
import {Personnel} from '../../personnels/shared/personnel';
import {EtapeService} from '../../etapeconstruction/shared/etape.service';
import {PersonnelsService} from '../../personnels/shared/personnels.service';
import {TypecontratService} from '../../typecontrat/shared/typecontrat.service';
import {FonctionsService} from '../../fonctions/shared/fonctions.service';
import {MaisonService} from '../../maison/shared/maison.service';
import {PrestationService} from '../manageprestataire/shared/prestation.service';
import {TypemaisonService} from '../../typemaison/shared/typemaison.service';
import {SocieteconstructionService} from '../../societeconstruction/shared/societeconstruction.service';
import {MatDialog} from '@angular/material';
import {SectionService} from '../../section/shared/section.service';
import {EmployeService} from '../../employe/shared/employe.service';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {ChantierService} from '../shared/chantier.service';
import {FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';
import {PrefatService} from '../../prefat/shared/prefat.service';
import {AjouterprestataireComponent} from '../manageprestataire/ajouterprestataire/ajouterprestataire.component';
import {ModifierprestataireComponent} from '../manageprestataire/modifierprestataire/modifierprestataire.component';
import {SupprimerprestataireComponent} from '../manageprestataire/supprimerprestataire/supprimerprestataire.component';
import {AjouterprestataireinprefatComponent} from './ajouterprestataireinprefat/ajouterprestataireinprefat.component';
import {ModifierprestataireinprefatComponent} from './modifierprestataireinprefat/modifierprestataireinprefat.component';
import {SupprimerprestataireinprefatComponent} from './supprimerprestataireinprefat/supprimerprestataireinprefat.component';

@Component({
  selector: 'app-manageprestataireinprefat',
  templateUrl: './manageprestataireinprefat.component.html',
  styleUrls: ['./manageprestataireinprefat.component.css']
})
export class ManageprestataireinprefatComponent implements OnInit {
  nextLabel = 'Suivant';
  previousLabel = 'Précédent';
  maxSize = 9;
  directionLinks = true;
  autoHide = false;
  responsive = true;
  screenReaderPaginationLabel = 'Pagination';
  screenReaderPageLabel = 'page';
  screenReaderCurrentLabel = 'Vous êtes sur cette page';
  public page: number;
  public isNull: string;
  public pageSize: number;
  p = 1;
  count = 10;
  searchText;
  firstlevel: string;
  secondlevel: string;
  thirdlevel: string;
  prestation: Prestation[];
  totalprestaireinprefat = 0;
  denominationchantier: string;
  tabtodisplay: TabdisplaytodashModel;
  fonctions: Fonction[];
  typecontrats: Typecontrat[];
  etapes: Etape[];
  public libelleprefatselected: string;
  prestataires: Personnel[];
  // tslint:disable-next-line:max-line-length
  headprestataireInPrefat = ['N°', 'Fonction', 'Prestataire', 'Société', 'Doc. identité', 'Date début', 'Date fin', 'Type contrat', 'Action'];
  // tslint:disable-next-line:max-line-length
  constructor(public ApiPrefat: PrefatService,  public ApiEtape: EtapeService, public ApiPrestataire: PersonnelsService, public ApiTypeContrat: TypecontratService, public ApiFonction: FonctionsService, public ApiMaison: MaisonService, public ApiPrestation: PrestationService, public ApiTypeMaison: TypemaisonService, public ApiSocieteConstructive: SocieteconstructionService, private changeDetectorRefs: ChangeDetectorRef, public dialog: MatDialog, public ApiSection: SectionService , public ApiEmploye: EmployeService, private notifyService: NotificationService, private toastr: ToastrService, public ApiChantier: ChantierService, private formBuilder: FormBuilder,  private router: Router) { }

  ngOnInit() {
    this.libelleprefatselected =   window.localStorage.getItem('libelleprefat');
    this.firstlevel = 'Tableau de bord';
    this.thirdlevel = 'prefats';
    this.secondlevel = 'Prestataires';
    this.getPrestationByPrefat();
    this.getallfonction();
    this.getalltypecontrat();
    this.getallPrestataire();
  }

  getallfonction() {
    this.ApiFonction.getAllFonction().subscribe((fonction: Fonction[]) => {
      this.fonctions = fonction;
    });
  }

  getalltypecontrat() {
    this.ApiTypeContrat.getAllTypeContrat().subscribe((typecontrat: Typecontrat[]) => {
      this.typecontrats = typecontrat;
    });
  }

  getallPrestataire() {
    this.ApiPrestataire.getAllPersonnelList().subscribe((personnel: Personnel[]) => {
      this.prestataires = personnel;
    });
  }

  getPrestationByPrefat() {
    const idprefat = window.localStorage.getItem('PrefatId');
    console.log('id prefat : ' + idprefat);
    this.ApiPrefat.getPrefatById(+idprefat).subscribe((t: TabdisplaytodashModel) => {
      this.tabtodisplay = t[0];
      console.log(this.tabtodisplay);
    });

    this.ApiPrestation.getPrestationByPrefatId(+idprefat).subscribe((data) => {
      this.prestation = data.data;
      this.totalprestaireinprefat = data.data.length;
    });
  }

  addNewPrestataireInPrefat() {
    // tslint:disable-next-line:max-line-length
    const dialogRef = this.dialog.open(AjouterprestataireinprefatComponent, {data: {prestation: Prestation , getallfonctions: this.fonctions , getalltypecontrats: this.typecontrats} });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        dialogRef.componentInstance.passEntry.subscribe((valeurecue) => {
          this.getPrestationByPrefat();
        });
      }
    });
  }

  // tslint:disable-next-line:max-line-length
  startEditPrestationInPrefat(i: number, id: number, prestataire_id: number, prefat_id: number, datedebut: Date, datefin: Date, typecontrat: string, isCanceled: boolean) {
    // index row is used just for debugging proposes and can be removed
    const dialogRef = this.dialog.open(ModifierprestataireinprefatComponent, {
      // tslint:disable-next-line:max-line-length
      data: {id, prestataire_id, prefat_id, datedebut, datefin, typecontrat, isCanceled, getallfonctions: this.fonctions , getalltypecontrats: this.typecontrats , getallprestataires: this.prestataires  }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        dialogRef.componentInstance.passEntry.subscribe((valeurecue) => {
          this.getPrestationByPrefat();
        });
      }
    });
  }

  // tslint:disable-next-line:max-line-length
  deletePrestationInPrefat(i: number, id: number, prestataire_id: number, prefat_id: number, datedebut: Date, datefin: Date, typecontrat: string, isCanceled: boolean) {

    const dialogRef = this.dialog.open(SupprimerprestataireinprefatComponent, {
      // tslint:disable-next-line:max-line-length
      data: {i, id, prestataire_id, prefat_id, datedebut, datefin, typecontrat, isCanceled}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        dialogRef.componentInstance.passEntry.subscribe((valeurecue) => {
          this.getPrestationByPrefat();
        });
      }
    });
  }

}
