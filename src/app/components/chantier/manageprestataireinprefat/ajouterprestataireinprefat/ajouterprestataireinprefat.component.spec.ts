import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjouterprestataireinprefatComponent } from './ajouterprestataireinprefat.component';

describe('AjouterprestataireinprefatComponent', () => {
  let component: AjouterprestataireinprefatComponent;
  let fixture: ComponentFixture<AjouterprestataireinprefatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjouterprestataireinprefatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjouterprestataireinprefatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
