import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {Personnel} from '../../../personnels/shared/personnel';
import {DatePipe, formatDate} from '@angular/common';
import {PersonnelsService} from '../../../personnels/shared/personnels.service';
import {PrestationService} from '../../manageprestataire/shared/prestation.service';
import {Prestation} from '../../manageprestataire/shared/prestation.model';
import {MaisonService} from '../../../maison/shared/maison.service';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-ajouterprestataireinprefat',
  templateUrl: './ajouterprestataireinprefat.component.html',
  styleUrls: ['./ajouterprestataireinprefat.component.css'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
  ]
})
export class AjouterprestataireinprefatComponent  {
  public prestataires: Personnel[];
  currentdate =  formatDate(new Date(), 'dd/MM/yyyy:HH:mm:ss', 'en');
  // tslint:disable-next-line:max-line-length
  constructor(public ApiPersonnel: PersonnelsService, private dateAdapter: DateAdapter<Date>, private datePipe: DatePipe, private _adapter: DateAdapter<any>, public ApiPrestation: PrestationService, public dialogRef: MatDialogRef<AjouterprestataireinprefatComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Prestation,
              public ApiMaison: MaisonService) { this._adapter.setLocale('fr'); }

  thedate: Date;
  @Output() passEntry: EventEmitter<any> = new EventEmitter();

  formControl = new FormControl('', [
    Validators.required
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Le champ est requis' : '';
  }

  submit() {
    // emppty stuff
  }

  GetPrestataireByFonction(value) {
    console.log(value);
    this.ApiPersonnel.getemployebyfunction(value).subscribe((data) =>  {
      this.prestataires = data;
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    this.data.prefat_id = +window.localStorage.getItem('PrefatId');
    this.ApiPrestation.addPrestation(this.data).subscribe((prestation: Prestation) => {
      this.passEntry.emit(prestation);
      console.log(prestation);
    });
  }


}
