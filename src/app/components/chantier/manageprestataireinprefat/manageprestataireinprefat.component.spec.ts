import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageprestataireinprefatComponent } from './manageprestataireinprefat.component';

describe('ManageprestataireinprefatComponent', () => {
  let component: ManageprestataireinprefatComponent;
  let fixture: ComponentFixture<ManageprestataireinprefatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageprestataireinprefatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageprestataireinprefatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
