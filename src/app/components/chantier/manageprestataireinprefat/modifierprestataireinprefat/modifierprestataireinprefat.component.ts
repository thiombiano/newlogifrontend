import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {Personnel} from '../../../personnels/shared/personnel';
import {DatePipe, formatDate} from '@angular/common';
import {PersonnelsService} from '../../../personnels/shared/personnels.service';
import {PrestationService} from '../../manageprestataire/shared/prestation.service';
import {Prestation} from '../../manageprestataire/shared/prestation.model';
import {MaisonService} from '../../../maison/shared/maison.service';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-modifierprestataireinprefat',
  templateUrl: './modifierprestataireinprefat.component.html',
  styleUrls: ['./modifierprestataireinprefat.component.css'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
  ]
})
export class ModifierprestataireinprefatComponent {
  public prestataires: Personnel[];
  currentdate =  formatDate(new Date(), 'dd/MM/yyyy:HH:mm:ss', 'en');
  // tslint:disable-next-line:max-line-length
  constructor(public ApiPersonnel: PersonnelsService, private dateAdapter: DateAdapter<Date>, private datePipe: DatePipe, private _adapter: DateAdapter<any>, public ApiPrestation: PrestationService, public dialogRef: MatDialogRef<ModifierprestataireinprefatComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Prestation,
              public ApiMaison: MaisonService) { this.dateAdapter.setLocale('fr'); }

  thedate: Date;
  @Output() passEntry: EventEmitter<any> = new EventEmitter();

  formControl = new FormControl('', [
    Validators.required
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Le champ est requis' : '';
  }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  stopEdit(): void {
    this.data.prefat_id = +window.localStorage.getItem('PrefatId');
    const idprestation = this.data.id;
    this.ApiPrestation.updatePrestation(idprestation, this.data).subscribe((prestation: Prestation) => {
      this.passEntry.emit(prestation);
      console.log(prestation);
    });
  }

}
