import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifierprestataireinprefatComponent } from './modifierprestataireinprefat.component';

describe('ModifierprestataireinprefatComponent', () => {
  let component: ModifierprestataireinprefatComponent;
  let fixture: ComponentFixture<ModifierprestataireinprefatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifierprestataireinprefatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifierprestataireinprefatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
