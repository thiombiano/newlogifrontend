import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupprimerprestataireinprefatComponent } from './supprimerprestataireinprefat.component';

describe('SupprimerprestataireinprefatComponent', () => {
  let component: SupprimerprestataireinprefatComponent;
  let fixture: ComponentFixture<SupprimerprestataireinprefatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupprimerprestataireinprefatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupprimerprestataireinprefatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
