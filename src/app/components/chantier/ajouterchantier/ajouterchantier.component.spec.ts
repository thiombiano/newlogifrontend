import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjouterchantierComponent } from './ajouterchantier.component';

describe('AjouterchantierComponent', () => {
  let component: AjouterchantierComponent;
  let fixture: ComponentFixture<AjouterchantierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjouterchantierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjouterchantierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
