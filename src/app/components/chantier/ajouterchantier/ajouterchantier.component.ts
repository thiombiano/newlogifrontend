import { Component, OnInit } from '@angular/core';
import {RoleService} from '../../role/shared/role.service';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {ChantierService} from '../shared/chantier.service';
import {Fonction} from '../../fonctions/shared/fonction';
import {Employe} from '../../employe/shared/employe.model';
import {Role} from '../../role/shared/role.model';
import {Typechantier} from '../../typechantier/shared/Typechantier.model';
import {Chantier} from '../shared/chantier';
import {EmployeService} from '../../employe/shared/employe.service';
import {TypechantierService} from '../../typechantier/shared/typechantier.service';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-ajouterchantier',
  templateUrl: './ajouterchantier.component.html',
  styleUrls: ['./ajouterchantier.component.css']
})
export class AjouterchantierComponent implements OnInit {
  firstlevel: string;
  secondlevel: string;
  typechantiers: Typechantier[];
  addForm: FormGroup;
  submitted = false;
  employes: Employe[];
  chantiers: Chantier;
  public imagePath;
  imgURL: any;
  public message: string;
  selectedFile: File;
  public path: string;
  // tslint:disable-next-line:max-line-length
  constructor(public ApiEmploye: EmployeService, public ApiTypechantier: TypechantierService,  private apiRole: RoleService, private notifyService: NotificationService, private toastr: ToastrService, public ApiChantier: ChantierService, private formBuilder: FormBuilder,  private router: Router) { }

  preview(files) {
    if (files.length === 0) {
      return;
    }

    const mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = 'Only images are supported.';
      return;
    }

    const reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    console.log('le o : ' + files[0]);
    // tslint:disable-next-line:variable-name
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    };
    console.log('Le ficher est : ' +   this.imgURL);
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
    const reader = new FileReader();
    this.imagePath = event.target.files;
    reader.readAsDataURL(event.target.files[0]);
    // tslint:disable-next-line:variable-name
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    };
    console.log('Le ficher est : ' +   this.selectedFile.name);
  }

  ngOnInit() {
    this.path = environment.apiImage;
    const userimage = 'chantier/PhotoChantiers.jpg';
    this.imgURL = this.path + '/' + userimage;
    this.firstlevel = 'Chantier';
    this.secondlevel = 'Ajouter chantier';
    this.ApiEmploye.getAllEmployeList().subscribe((data) => {
      this.employes = data.data;
    });

    this.ApiTypechantier.getAllTypeChantierList().subscribe((typechantier: Typechantier[]) => {
      this.typechantiers = typechantier;
    });

    this.addForm = this.formBuilder.group({
      code_chantier: ['', Validators.required],
      denomination: ['', Validators.required],
      description: [''],
      chef_chantier_id: ['', Validators.required],
      type_chantier_id: ['', Validators.required],
      adresse: [''],
      longitude: [''],
      latitude: [''],
      url_photo: ['']
    });
  }

  showToasterChantierEnregistrer() {
    this.notifyService.showSuccessWithTimeout('Chantier enregistré avec succès !!', 'Notification', 5000);
  }

  showToasterChantierErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }


  onSubmit() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.addForm.invalid) {
      this.showToasterChantierErreurModifier();
      return;
    }
    console.log('For the form ' + this.addForm.value);
    // @ts-ignore
    const formdata = new FormData();
    formdata.append('code_chantier', this.addForm.get('code_chantier').value );
    formdata.append('denomination', this.addForm.get('denomination').value );
    formdata.append('description', this.addForm.get('description').value );
    formdata.append('adresse', this.addForm.get('adresse').value );
    formdata.append('longitude', this.addForm.get('longitude').value );
    formdata.append('latitude', this.addForm.get('latitude').value );
    formdata.append('chef_chantier_id', this.addForm.get('chef_chantier_id').value );
    formdata.append('type_chantier_id', this.addForm.get('type_chantier_id').value );

    if (this.selectedFile) {
      formdata.append('url_photo', this.selectedFile);
    }
    console.log('For the form chef chantier ' + formdata.get('chef_chantier_id'));
    console.log('For the form type chantier' + formdata.get('type_chantier_id'));


    // @ts-ignore
    this.ApiChantier.addChantier(formdata).subscribe((chantier: Chantier) => {
      this.showToasterChantierEnregistrer();
      console.log(chantier);
      this.router.navigate(['start/chantiers']);
    });

  }

}
