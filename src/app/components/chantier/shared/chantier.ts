import {Employe} from '../../employe/shared/employe.model';

export class Chantier {

  public code_chantier: string;
  public denomination: string;
  public description: string;
  public adresse: string;
  public chef_chantier_id: number;
  public longitude: string;
  public latitude: string;
  public url_photo: string;
  public chef_chantier: Employe;
  public id: number;

}
