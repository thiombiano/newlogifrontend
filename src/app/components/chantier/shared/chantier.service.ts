import { Injectable } from '@angular/core';
import {Chantier} from './chantier';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Employe} from '../../employe/shared/employe.model';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChantierService {
  private headers: HttpHeaders;
  listChantiers: Chantier[];
  selectedChantier: Chantier;
  constructor(private http: HttpClient) { }


  getChantier(id: number) {
    return  this.http.get(environment.apiUrl + '/chantiers/' + id).toPromise();

    //   .then( (x: ChantierApiResponseOne) => {
    //     console.log('JSON X : ' + JSON.stringify(x));
    //     this.selectedChantier = x.data;
    //     console.log('JSON selected : ' + JSON.stringify(this.selectedChantier));
    //   });
  }

  getLeChantier(id: number) {
    return  this.http.get(environment.apiUrl + '/chantiers/' + id)
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Impossible dobtenir les infos du chantier!'))
      );
  }

  getAllChantierList() {
    return  this.http.get<Chantier[]>(environment.apiUrl + '/chantiers');
  }

  getChantierList() {
    this.http
      .get<Chantier[]>(environment.apiUrl + '/chantiers')
      .toPromise()
      .then(x => {
        this.listChantiers = x;
        console.log('data: ' + this.listChantiers);
      });
  }

  addChantier(value: any) {
    this.headers = new HttpHeaders();
    this.headers.append('Content-Type', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    return  this.http.post<Chantier>(environment.apiUrl + '/chantiers', value, {headers: this.headers});
  }

  deleteChantier(chantier: Chantier) {
    // @ts-ignore
    return this.http.delete<Chantier>(environment.apiUrl + '/chantiers/' + chantier.id);
  }

  updateChantier(id, value: any) {
    this.headers = new HttpHeaders();
    this.headers.append('Content-Type', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    // @ts-ignore
    return this.http.post(environment.apiUrl + '/chantiers/' + id, value, {headers: this.headers} );
  }

}
