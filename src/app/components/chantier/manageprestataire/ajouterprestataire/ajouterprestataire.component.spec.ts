import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjouterprestataireComponent } from './ajouterprestataire.component';

describe('AjouterprestataireComponent', () => {
  let component: AjouterprestataireComponent;
  let fixture: ComponentFixture<AjouterprestataireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjouterprestataireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjouterprestataireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
