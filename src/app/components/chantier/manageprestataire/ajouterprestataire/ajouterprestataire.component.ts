import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {SectionService} from '../../../section/shared/section.service';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Maison} from '../../../maison/shared/maison.model';
import {MaisonService} from '../../../maison/shared/maison.service';
import {PrestationService} from '../shared/prestation.service';
import {Prestation} from '../shared/prestation.model';
import {FormControl, Validators} from '@angular/forms';
import {EmployeService} from '../../../employe/shared/employe.service';
import {Employe} from '../../../employe/shared/employe.model';
import {PersonnelsService} from '../../../personnels/shared/personnels.service';
import {Personnel} from '../../../personnels/shared/personnel';
import {DatePipe, formatDate} from '@angular/common';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {ArticleService} from '../../../article/Shared/article.service';

@Component({
  selector: 'app-ajouterprestataire',
  templateUrl: './ajouterprestataire.component.html',
  styleUrls: ['./ajouterprestataire.component.css'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
  ]
})
export class AjouterprestataireComponent {
  public prestataires: Personnel[];
  currentdate =  formatDate(new Date(), 'dd/MM/yyyy:HH:mm:ss', 'en');
  // tslint:disable-next-line:max-line-length
  constructor(public ApiPersonnel: PersonnelsService, private dateAdapter: DateAdapter<Date>, private datePipe: DatePipe, private _adapter: DateAdapter<any>, public ApiPrestation: PrestationService, public dialogRef: MatDialogRef<AjouterprestataireComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Prestation,
              public ApiMaison: MaisonService) {   this._adapter.setLocale('fr'); }
  thedate: Date;
  @Output() passEntry: EventEmitter<any> = new EventEmitter();

  formControl = new FormControl('', [
    Validators.required
  ]);


  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Le champ est requis' : '';
  }

  submit() {
    // emppty stuff
  }

  GetPrestataireByFonction(value) {
    console.log(value);
    this.ApiPersonnel.getemployebyfunction(value).subscribe((data) =>  {
      this.prestataires = data;
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    this.data.maison_id = +window.localStorage.getItem('MaisonId');
    this.ApiPrestation.addPrestation(this.data).subscribe((prestation: Prestation) => {
      this.passEntry.emit(prestation);
      console.log(prestation);
    });
  }

}
