import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {TypemaisonService} from '../../typemaison/shared/typemaison.service';
import {SocieteconstructionService} from '../../societeconstruction/shared/societeconstruction.service';
import {MatDialog} from '@angular/material';
import {SectionService} from '../../section/shared/section.service';
import {EmployeService} from '../../employe/shared/employe.service';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {ChantierService} from '../shared/chantier.service';
import {FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';
import {PrestationService} from './shared/prestation.service';
import {Prestation} from './shared/prestation.model';
import {MaisonService} from '../../maison/shared/maison.service';
import {TabdisplaytodashModel} from './shared/tabdisplaytodash.model';
import {AjouterlotComponent} from '../../lot/ajouterlot/ajouterlot.component';
import {Lot} from '../../lot/shared/lot.model';
import {AjouterprestataireComponent} from './ajouterprestataire/ajouterprestataire.component';
import {Fonction} from '../../fonctions/shared/fonction';
import {FonctionsService} from '../../fonctions/shared/fonctions.service';
import {Typecontrat} from '../../typecontrat/shared/typecontrat.model';
import {TypecontratService} from '../../typecontrat/shared/typecontrat.service';
import {Etape} from '../../etapeconstruction/shared/etape.model';
import {ModifierlotComponent} from '../../lot/modifierlot/modifierlot.component';
import {ModifierprestataireComponent} from './modifierprestataire/modifierprestataire.component';
import {Personnel} from '../../personnels/shared/personnel';
import {PersonnelsService} from '../../personnels/shared/personnels.service';
import {SupprimermaisonComponent} from '../../maison/supprimermaison/supprimermaison.component';
import {SupprimerprestataireComponent} from './supprimerprestataire/supprimerprestataire.component';
import {EtapeService} from '../../etapeconstruction/shared/etape.service';
import {EtapeencoursModel} from "../../manageetapeencoursformaison/shared/etapeencours.model";

@Component({
  selector: 'app-manageprestataire',
  templateUrl: './manageprestataire.component.html',
  styleUrls: ['./manageprestataire.component.css']
})
export class ManageprestataireComponent implements OnInit {
  nextLabel = 'Suivant';
  previousLabel = 'Précédent';
  maxSize = 9;
  directionLinks = true;
  autoHide = false;
  responsive = true;
  screenReaderPaginationLabel = 'Pagination';
  screenReaderPageLabel = 'page';
  screenReaderCurrentLabel = 'Vous êtes sur cette page';
  public page: number;
  public isNull: string;
  public pageSize: number;
  p = 1;
  count = 10;
  searchText;
  firstlevel: string;
  secondlevel: string;
  thirdlevel: string;
  prestation: Prestation[];
  totalprestaire = 0;
  denominationchantier: string;
  tabtodisplay: TabdisplaytodashModel;
  fonctions: Fonction[];
  typecontrats: Typecontrat[];
  etapes: EtapeencoursModel[];
  etapesencours: Etape[];
  prestataires: Personnel[];
  // tslint:disable-next-line:max-line-length
  headprestataire = ['N°', 'Fonction', 'Etape', 'Prestataire', 'Société', 'Doc. identité', 'Date début', 'Date fin', 'Type contrat', 'Action'];
  // tslint:disable-next-line:max-line-length
  constructor(public ApiEtape: EtapeService, public ApiPrestataire: PersonnelsService, public ApiTypeContrat: TypecontratService, public ApiFonction: FonctionsService, public ApiMaison: MaisonService, public ApiPrestation: PrestationService, public ApiTypeMaison: TypemaisonService, public ApiSocieteConstructive: SocieteconstructionService, private changeDetectorRefs: ChangeDetectorRef, public dialog: MatDialog, public ApiSection: SectionService , public ApiEmploye: EmployeService, private notifyService: NotificationService, private toastr: ToastrService, public ApiChantier: ChantierService, private formBuilder: FormBuilder,  private router: Router) { }

  ngOnInit() {
    this.firstlevel = 'Tableau de bord';
    this.thirdlevel = 'chantiers';
    this.secondlevel = 'Prestataires';
    this.denominationchantier = window.localStorage.getItem('nomchantier');
    this.getPrestationByMaison();
    this.getallfonction();
    this.getalltypecontrat();
    this.getallPrestataire();
    this.getMaisonByEtape();
  }

  getallfonction() {
   this.ApiFonction.getAllFonction().subscribe((fonction: Fonction[]) => {
     this.fonctions = fonction;
   });
  }

  getalltypecontrat() {
    this.ApiTypeContrat.getAllTypeContrat().subscribe((typecontrat: Typecontrat[]) => {
      this.typecontrats = typecontrat;
    });
  }

  getallPrestataire() {
    this.ApiPrestataire.getAllPersonnelList().subscribe((personnel: Personnel[]) => {
      this.prestataires = personnel;
    });
  }

  getPrestationByMaison() {
   const idmaison =  window.localStorage.getItem('MaisonId');
   console.log('id maison : ' + idmaison );
   this.ApiMaison.getMaisonById(+idmaison).subscribe((t: TabdisplaytodashModel) => {
      this.tabtodisplay = t[0];
      console.log(this.tabtodisplay);
   });

   this.ApiPrestation.getPrestationByMaisonId(+idmaison).subscribe((data) => {
      this.prestation = data.data;
      this.totalprestaire = data.data.length;
      if (data.data.length !== 0) {
      this.etapes = data.data[0].maison[0].typemaison[0].etapes;
      }
    });
  }

  getMaisonByEtape() {
    const idmaison =  window.localStorage.getItem('MaisonId');
    this.ApiEtape.getAllEtapeListByIdMaison(+idmaison).subscribe((data) => {
        this.etapesencours = data;
    });
  }


  addNewPrestataire() {
    // tslint:disable-next-line:max-line-length
    const dialogRef = this.dialog.open(AjouterprestataireComponent, {data: {prestation: Prestation , getallfonctions: this.fonctions , getalltypecontrats: this.typecontrats , getalletapes: this.etapesencours} });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        dialogRef.componentInstance.passEntry.subscribe((valeurecue) => {
          this.getPrestationByMaison();
        });
      }
    });
  }


  // tslint:disable-next-line:max-line-length
  startEditPrestation(i: number, id: number, prestataire_id: number, maison_id: number, etape_construction_id: number, datedebut: Date, datefin: Date, typecontrat: string, isCanceled: boolean) {
    // index row is used just for debugging proposes and can be removed
    const dialogRef = this.dialog.open(ModifierprestataireComponent, {
      // tslint:disable-next-line:max-line-length
      data: {id, prestataire_id, maison_id, etape_construction_id, datedebut, datefin, typecontrat, isCanceled, getallfonctions: this.fonctions , getalltypecontrats: this.typecontrats , getalletapes: this.etapes , getallprestataires: this.prestataires  }
    });

    dialogRef.afterClosed().subscribe(result => {
        if (result === 1) {
          dialogRef.componentInstance.passEntry.subscribe((valeurecue) => {
            this.getPrestationByMaison();
          });
        }
    });
  }

  // tslint:disable-next-line:max-line-length
  deletePrestation(i: number, id: number, prestataire_id: number, maison_id: number, etape_construction_id: number, datedebut: Date, datefin: Date, typecontrat: string, isCanceled: boolean) {

    const dialogRef = this.dialog.open(SupprimerprestataireComponent, {
      // tslint:disable-next-line:max-line-length
      data: {i, id, prestataire_id, maison_id, etape_construction_id, datedebut, datefin, typecontrat, isCanceled}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        dialogRef.componentInstance.passEntry.subscribe((valeurecue) => {
          this.getPrestationByMaison();
        });
      }
    });
  }

}
