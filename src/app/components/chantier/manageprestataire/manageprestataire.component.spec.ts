import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageprestataireComponent } from './manageprestataire.component';

describe('ManageprestataireComponent', () => {
  let component: ManageprestataireComponent;
  let fixture: ComponentFixture<ManageprestataireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageprestataireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageprestataireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
