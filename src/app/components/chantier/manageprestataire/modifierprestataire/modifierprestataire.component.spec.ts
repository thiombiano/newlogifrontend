import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifierprestataireComponent } from './modifierprestataire.component';

describe('ModifierprestataireComponent', () => {
  let component: ModifierprestataireComponent;
  let fixture: ComponentFixture<ModifierprestataireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifierprestataireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifierprestataireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
