import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {DatePipe, formatDate} from '@angular/common';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {PersonnelsService} from '../../../personnels/shared/personnels.service';
import {PrestationService} from '../shared/prestation.service';
import {Prestation} from '../shared/prestation.model';
import {MaisonService} from '../../../maison/shared/maison.service';
import {Personnel} from '../../../personnels/shared/personnel';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MAT_DIALOG_DATA, MatDialogRef, MatStepper} from '@angular/material';
@Component({
  selector: 'app-modifierprestataire',
  templateUrl: './modifierprestataire.component.html',
  styleUrls: ['./modifierprestataire.component.css'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
  ]
})
export class ModifierprestataireComponent {
  public prestataires: Personnel[];
  currentdate =  formatDate(new Date(), 'dd/MM/yyyy:HH:mm:ss', 'en');
  // tslint:disable-next-line:max-line-length
  constructor(public ApiPersonnel: PersonnelsService, private dateAdapter: DateAdapter<Date>, private datePipe: DatePipe, private _adapter: DateAdapter<any>, public ApiPrestation: PrestationService, public dialogRef: MatDialogRef<ModifierprestataireComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Prestation,
              public ApiMaison: MaisonService) {
    this.dateAdapter.setLocale('fr');
  }

  thedate: Date;
  @Output() passEntry: EventEmitter<any> = new EventEmitter();

  formControl = new FormControl('', [
    Validators.required
  ]);


  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Le champ est requis' : '';
  }

  submit() {
    // emppty stuff
  }

  GetPrestataireByFonction(value) {
    console.log(value);
    this.ApiPersonnel.getemployebyfunction(value).subscribe((data) =>  {
      this.data['getallprestataires'] = data;
    });
  }


  onNoClick(): void {
    this.dialogRef.close();
  }

  stopEdit(): void {
    this.data.maison_id = +window.localStorage.getItem('MaisonId');
    const idprestation = this.data.id;
    this.ApiPrestation.updatePrestation(idprestation, this.data).subscribe((prestation: Prestation) => {
      this.passEntry.emit(prestation);
      console.log(prestation);
    });
  }
}
