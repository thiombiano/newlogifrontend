import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {PersonnelsService} from '../../../personnels/shared/personnels.service';
import {DateAdapter, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DatePipe} from '@angular/common';
import {PrestationService} from '../shared/prestation.service';
import {Prestation} from '../shared/prestation.model';
import {MaisonService} from '../../../maison/shared/maison.service';
import {Lot} from '../../../lot/shared/lot.model';

@Component({
  selector: 'app-supprimerprestataire',
  templateUrl: './supprimerprestataire.component.html',
  styleUrls: ['./supprimerprestataire.component.css']
})
export class SupprimerprestataireComponent  {
  @Output() passEntry: EventEmitter<any> = new EventEmitter();
  // tslint:disable-next-line:max-line-length
  constructor(public ApiPersonnel: PersonnelsService, private dateAdapter: DateAdapter<Date>, private datePipe: DatePipe, private _adapter: DateAdapter<any>, public ApiPrestation: PrestationService, public dialogRef: MatDialogRef<SupprimerprestataireComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Prestation,
              public ApiMaison: MaisonService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    if(this.data.id) {
      this.ApiPrestation.deletePrestation(this.data.id).subscribe((prestation: Prestation) => {
        console.log(prestation);
        this.passEntry.emit(prestation);
      });
    }
  }

}
