import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupprimerprestataireComponent } from './supprimerprestataire.component';

describe('SupprimerprestataireComponent', () => {
  let component: SupprimerprestataireComponent;
  let fixture: ComponentFixture<SupprimerprestataireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupprimerprestataireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupprimerprestataireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
