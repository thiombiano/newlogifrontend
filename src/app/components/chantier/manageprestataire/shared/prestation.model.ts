import {Etape} from '../../../etapeconstruction/shared/etape.model';
import {Employe} from '../../../employe/shared/employe.model';
import {Maison} from '../../../maison/shared/maison.model';

export class Prestation {
  id: number;
  prestataire_id: number;
  maison_id: number;
  prefat_id: number;
  etape_construction_id: number;
  datedebut: Date;
  datefin: Date;
  typecontrat: string;
  etape: Etape[];
  prestataire: Employe[];
  maison: Maison[];
}
