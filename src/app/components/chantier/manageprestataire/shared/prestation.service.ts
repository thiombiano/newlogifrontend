import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Maison} from '../../../maison/shared/maison.model';
import {environment} from '../../../../../environments/environment';
import {Prestation} from './prestation.model';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PrestationService {

  constructor(private http: HttpClient) { }

  getAllPrestation() {
    return  this.http.get(environment.apiUrl + '/prestations')
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Impossible dobtenir les infos sur la prestation!'))
      );
  }

  getOnePrestation(id: number) {
    return  this.http.get(environment.apiUrl + '/prestations/' + id)
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Impossible dobtenir les infos sur une prestation!'))
      );
  }

  getPrestationByMaisonId(id: number) {
    return  this.http.get(environment.apiUrl + '/prestationbymaison/' + id)
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Impossible dobtenir les infos sur une prestation par id maison!'))
      );
  }

  getPrestationByPrefatId(id: number) {
    return  this.http.get(environment.apiUrl + '/prestationbyprefat/' + id)
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Impossible dobtenir les infos sur une prestation par id maison!'))
      );
  }

  addPrestation(prestation: Prestation) {
    return  this.http.post<Prestation>(environment.apiUrl +  '/prestations', prestation);
  }

  deletePrestation(id: number) {
    // @ts-ignore
    return this.http.delete<Prestation>(environment.apiUrl +  '/prestations/' + id);
  }

  updatePrestation(id, value: any ) {
    return this.http.put<Prestation>(environment.apiUrl + '/prestations/' + id, value );
  }

}
