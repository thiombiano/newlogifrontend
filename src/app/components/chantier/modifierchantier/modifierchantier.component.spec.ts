import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifierchantierComponent } from './modifierchantier.component';

describe('ModifierchantierComponent', () => {
  let component: ModifierchantierComponent;
  let fixture: ComponentFixture<ModifierchantierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifierchantierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifierchantierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
