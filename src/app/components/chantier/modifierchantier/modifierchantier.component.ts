import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Employe} from '../../employe/shared/employe.model';
import {Chantier} from '../shared/chantier';
import {Typechantier} from '../../typechantier/shared/Typechantier.model';
import {EmployeService} from '../../employe/shared/employe.service';
import {TypechantierService} from '../../typechantier/shared/typechantier.service';
import {RoleService} from '../../role/shared/role.service';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {ChantierService} from '../shared/chantier.service';
import {Router} from '@angular/router';
import {environment} from '../../../../environments/environment';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-modifierchantier',
  templateUrl: './modifierchantier.component.html',
  styleUrls: ['./modifierchantier.component.css']
})
export class ModifierchantierComponent implements OnInit {
  public imagePath;
  imgURL: any;
  firstlevel: string;
  editForm: FormGroup;
  submitted = false;
  employes: Employe[];
  chantiers: Chantier;
  typechantiers: Typechantier[];
  secondlevel: string;
  selectedFile: File;
  public path: string;
  public message: string;
  userimage: string;
  preview(files) {
    if (files.length === 0) {
      return;
    }

    // if (event.target.files.length > 0) {
    //   const file = event.target.files[0];
    //   this.addForm.get('avatar').setValue(file);
    // }

    const mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = 'Only images are supported.';
      return;
    }

    const reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    console.log('le o : ' + files[0]);
    // tslint:disable-next-line:variable-name
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    };
    console.log('Le ficher est : ' +   this.imgURL);
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    // tslint:disable-next-line:variable-name
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    };
    console.log('Le ficher est : ' +   this.selectedFile.name);
  }
  // tslint:disable-next-line:max-line-length
  constructor(public ApiEmploye: EmployeService, public ApiTypechantier: TypechantierService,  private apiRole: RoleService, private notifyService: NotificationService, private toastr: ToastrService, public ApiChantier: ChantierService, private formBuilder: FormBuilder,  private router: Router) { }

  ngOnInit() {
    const ChantierId = window.localStorage.getItem('editChantierId');
    this.path = environment.apiImage;
    this.firstlevel = 'Chantier';
    this.secondlevel = 'Modifier chantier';
    this.ApiEmploye.getAllEmployeList().subscribe((data) => {
      this.employes = data.data;
    });

    this.ApiTypechantier.getAllTypeChantierList().subscribe((typechantier: Typechantier[]) => {
      this.typechantiers = typechantier;
    });

    this.editForm = this.formBuilder.group({
      id: [''],
      code_chantier: ['', Validators.required],
      denomination: ['', Validators.required],
      description: [''],
      chef_chantier_id: ['', Validators.required],
      type_chantier_id: ['', Validators.required],
      adresse: [''],
      longitude: [''],
      latitude: [''],
      url_photo: ['']
    });

    this.ApiChantier.getLeChantier(+ChantierId).subscribe( (data) => {
      this.editForm.get('id').setValue(data.id);
      this.editForm.get('code_chantier').setValue(data.code_chantier);
      this.editForm.get('denomination').setValue(data.denomination);
      this.editForm.get('description').setValue(data.description);
      this.editForm.get('chef_chantier_id').setValue(data.chef_chantier_id);
      this.editForm.get('type_chantier_id').setValue(data.type_chantier_id);
      this.editForm.get('adresse').setValue(data.adresse);
      this.editForm.get('longitude').setValue(data.longitude);
      this.editForm.get('latitude').setValue(data.latitude);
      this.userimage = data.url_photo;
      this.imgURL = this.path + '/' + this.userimage;
    });
  }

  showToasterChantierMisAJour() {
    this.notifyService.showSuccessWithTimeout('Chantier mis à jour avec succès !!', 'Notification', 5000);
  }

  showToasterChantierErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }



  onSubmit() {

    this.submitted = true;
    console.log(this.editForm.value);
    // stop here if form is invalid
    if (this.editForm.invalid) {
      console.log('error form');
      this.showToasterChantierErreurModifier();
      return;
    }
    const ChantierId = window.localStorage.getItem('editChantierId');
    // @ts-ignore
    const formdata = new FormData();

    formdata.append('code_chantier', this.editForm.get('code_chantier').value );
    formdata.append('denomination', this.editForm.get('denomination').value );
    formdata.append('description', this.editForm.get('description').value );
    formdata.append('adresse', this.editForm.get('adresse').value );
    formdata.append('longitude', this.editForm.get('longitude').value );
    formdata.append('latitude', this.editForm.get('latitude').value );
    formdata.append('chef_chantier_id', this.editForm.get('chef_chantier_id').value );
    formdata.append('type_chantier_id', this.editForm.get('type_chantier_id').value );

    if (this.selectedFile) {
      formdata.append('url_photo', this.selectedFile);
    } else {
      formdata.append('url_photo', null);
    }

    formdata.append('_method', 'PUT');
    // @ts-ignore
    this.ApiChantier.updateChantier(+ChantierId, formdata)
      .pipe(first())
      .subscribe((chantier: Chantier) => {
        // @ts-ignore
        console.log(this.editForm.value);
        this.showToasterChantierMisAJour();
        this.router.navigate(['start/chantiers']);
      });

  }

}
