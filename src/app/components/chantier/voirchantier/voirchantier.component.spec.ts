import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoirchantierComponent } from './voirchantier.component';

describe('VoirchantierComponent', () => {
  let component: VoirchantierComponent;
  let fixture: ComponentFixture<VoirchantierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoirchantierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoirchantierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
