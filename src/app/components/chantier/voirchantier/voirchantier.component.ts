import {ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {EmployeService} from '../../employe/shared/employe.service';
import {TypechantierService} from '../../typechantier/shared/typechantier.service';
import {RoleService} from '../../role/shared/role.service';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {ChantierService} from '../shared/chantier.service';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Chantier} from '../shared/chantier';
import {environment} from '../../../../environments/environment';
import {Employe} from '../../employe/shared/employe.model';
import {SectionService} from '../../section/shared/section.service';
import {Section} from '../../section/shared/section.model';
import {Appearance} from '@angular-material-extensions/google-maps-autocomplete';
import PlaceResult = google.maps.places.PlaceResult;
import {Title} from '@angular/platform-browser';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {AjoutersectionComponent} from '../../section/ajoutersection/ajoutersection.component';
import {log} from 'util';
import {ModifiersectionComponent} from '../../section/modifiersection/modifiersection.component';
import {SupprimersectionComponent} from '../../section/supprimersection/supprimersection.component';
import {Lot} from '../../lot/shared/lot.model';
import {AjouterlotComponent} from '../../lot/ajouterlot/ajouterlot.component';
import {ModifierlotComponent} from '../../lot/modifierlot/modifierlot.component';
import {SupprimerlotComponent} from '../../lot/supprimerlot/supprimerlot.component';
import {Societeconstruction} from '../../societeconstruction/shared/societeconstruction.model';
import {Typemaison} from '../../typemaison/shared/Typemaison.model';
import {TypemaisonService} from '../../typemaison/shared/typemaison.service';
import {SocieteconstructionService} from '../../societeconstruction/shared/societeconstruction.service';
import {Maison} from '../../maison/shared/maison.model';
import {AjoutermaisonComponent} from '../../maison/ajoutermaison/ajoutermaison.component';
import {ModifiermaisonComponent} from '../../maison/modifiermaison/modifiermaison.component';
import {SupprimermaisonComponent} from '../../maison/supprimermaison/supprimermaison.component';
import {LotService} from '../../lot/shared/lot.service';
import {MaisonService} from '../../maison/shared/maison.service';
import {Observable} from 'rxjs';
import {NgxSpinnerService} from 'ngx-spinner';
import {Prefat} from '../../prefat/shared/prefat.model';
import {PrefatService} from '../../prefat/shared/prefat.service';
import {AjouterprefatComponent} from '../../prefat/ajouterprefat/ajouterprefat.component';
import {ModifierprefatComponent} from '../../prefat/modifierprefat/modifierprefat.component';
import {SupprimerprefatComponent} from '../../prefat/supprimerprefat/supprimerprefat.component';
import {EtapeService} from '../../etapeconstruction/shared/etape.service';
import {Etape} from '../../etapeconstruction/shared/etape.model';

@Component({
  selector: 'app-voirchantier',
  templateUrl: './voirchantier.component.html',
  styleUrls: ['./voirchantier.component.css']
})
export class VoirchantierComponent implements OnInit {
  public items: any[];
  // tslint:disable-next-line:max-line-length
  constructor(public ApiEtape: EtapeService,  public ApiPrefat: PrefatService, private SpinnerService: NgxSpinnerService, public ApiLot: LotService, public ApiTypeMaison: TypemaisonService, public ApiSocieteConstructive: SocieteconstructionService, private changeDetectorRefs: ChangeDetectorRef, public dialog: MatDialog, public ApiSection: SectionService , public ApiEmploye: EmployeService, public ApiTypechantier: TypechantierService,  private apiRole: RoleService, private notifyService: NotificationService, private toastr: ToastrService, public ApiChantier: ChantierService, private formBuilder: FormBuilder,  private router: Router) { }
  chantiers: Chantier;
  public roles: { A: string; T: string; L: string; M: string;  C: string; SA: string; CSR: string; CDC: string};
  public imagePath;
  sections: Section[] = null;
  etapes: Etape[];
  lotofsections: Lot[];
  lotofsectionsprime: Lot[];
  societeconstruction: Societeconstruction[];
  typemaison: Typemaison[];
  technicien: Employe[];
  controleur: Employe[];
  magasinier: Employe[];
  employe: Employe;
  totalsection = 0;
  totalmaison = 0;
  maisons: Maison[];
  prefats: Prefat[];
  totalprefat = 0;
  searchText;
  totallot = 0;
  listelots: Lot[];
  sectionselected: number;
  sectionselectedinlot: number;
  sectionselectedinmaison: number;
  nextLabel = 'Suivant';
  previousLabel = 'Précédent';
  maxSize = 9;
  directionLinks = true;
  autoHide = false;
  responsive = true;
  screenReaderPaginationLabel = 'Pagination';
  screenReaderPageLabel = 'page';
  screenReaderCurrentLabel = 'Vous êtes sur cette page';
  imgURL: any;
  firstlevel: string;
  secondlevel: string;
  public path: string;
  public message: string;
  userimage: string;
  index: number;
  id: number;
  thetechnicien: any [];
  headSections = ['No',  'Section', 'Lots', 'Action'];
  headLots = ['No',  'Lot', 'Action'];
  headMaisons = ['No',  'Maison', 'Type', 'Technicien', 'Construction', 'Action'];
  headPrefat = ['No',  'Préfat', 'Technicien', 'Magasinier', 'Action'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild('filter',  {static: true}) filter: ElementRef;
  public appearance = Appearance;
  public zoom: string;
  public latitude: number;
  public longitude: number;
  pageOfItems: Array<any>;
  public itemsmaisons: [];
  public page: number;
  public isNull: string;
  public pageSize: number;
  public isDisplayed: boolean;
  public bootstrapcolumn: string;
  public texttodisplay: string;
  p = 1;
  count = 10;
  ngOnInit() {
    this.roles = environment.roles;
    this.isDisplayed = true;
    this.bootstrapcolumn = 'col-md-9';
    this.texttodisplay = 'Cachez les infos du chantier';
    this.zoom = '40';
    this.path = environment.apiImage;
    const ChantierId = window.localStorage.getItem('voirChantierId');
    this.ListSectionOfChantier(+ChantierId);
    console.log('Toutes données : '  + this.ApiSection.getAllSection(+ChantierId));
    this.SpinnerService.show();
    this.ApiChantier.getLeChantier(+ChantierId).subscribe((chantier: Chantier) => {
      console.log(chantier);
      window.localStorage.removeItem('libellechantier');
      window.localStorage.setItem('libellechantier', chantier.denomination);
      this.chantiers = chantier;
      this.userimage = chantier.url_photo;
      this.employe = chantier.chef_chantier;
      this.latitude = +chantier.latitude;
      this.longitude = +chantier.longitude;
      this.imgURL = this.path + '/' + this.userimage;
      this.SpinnerService.hide();
    });

    this.firstlevel = 'Chantier';
    this.secondlevel = 'Vue sur le chantier';

    this.ApiSocieteConstructive.getAllSocieteList().subscribe((data) => {
      this.societeconstruction = data.data;
    });

    this.ApiTypeMaison.getAllTypeMaisonList().subscribe((data) => {
      this.typemaison = data.data;
      console.log('Les types de maisons : ' + data.data);
    });

    this.ApiEmploye.getAllTechnicienList().subscribe((data) => {
      console.log('Les techniciens : ' + data);
      this.technicien = data;
    });

    this.ApiEmploye.getAllControleurList().subscribe((data) => {
      console.log('Les controleurs : ' + data);
      this.controleur = data;
    });

    this.ApiEmploye.getAllMagasinierList().subscribe((data) => {
      console.log('Les magasiniers : ' + data);
      this.magasinier = data;
    });

    this.ApiLot.getLotList(+ChantierId).subscribe((data) => {
      this.listelots = data;
    });

    this.ApiLot.getALLMaisonByChantier(+ChantierId).subscribe((data) => {
      console.log(data.data);
      this.maisons = data.data;
      this.totalmaison = data.data.length;
    });

    this.ApiPrefat.getPrefatList(+ChantierId).subscribe((data) => {
      console.log(data);
      this.prefats = data;
      this.totalprefat = data.length;
    });

    this.page = 1;
    this.pageSize = 2;
  }

  ListSectionOfChantier(id: number) {
    this.ApiSection.getSectionList(+id).subscribe((section: Section[]) => {
      this.sections = section;
      this.totalsection = section.length;
      console.log(section);
    });
  }

  ChangeSateOfVue(event) {
    console.log( event);
    const isdisplayed = event.checked;
    if (isdisplayed === true) {
      this.isDisplayed = false;
      this.bootstrapcolumn = 'col-md-12';
      this.texttodisplay = 'Afficher les infos du chantier';
    } else {
      this.isDisplayed = true;
      this.bootstrapcolumn = 'col-md-9';
      this.texttodisplay = 'Cachez les infos du chantier';
    }
  }

  ListSectionOfChantierAfterInserted(id: number) {
    this.ApiSection.getSectionList(+id).subscribe((section: Section[]) => {
      console.log('toutes les données après insertions : ' + section);
      this.totalsection = section.length;
      const lastsection: any = section[section.length - 1];
      console.log('La derniere données enregitré : ' + lastsection.libellesection);
      this.sections.push( {
        chantier_id: lastsection.chantier_id,
        id: lastsection.id,
        is_actif: lastsection.is_actif,
        lots: lastsection.lots,
        libellesection:  lastsection.libellesection
      });
    });
  }

  addNewSection() {
    // @ts-ignore
    // @ts-ignore
    // tslint:disable-next-line:label-position no-unused-expression
    const dialogRef = this.dialog.open(AjoutersectionComponent, {
      data: {section: Section},
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        dialogRef.componentInstance.passEntry.subscribe((valeurecue) => {
          console.log('La valeur recu est : ' + valeurecue.libellesection);
          const ChantierId = window.localStorage.getItem('voirChantierId');
          this.ListSectionOfChantierAfterInserted(+ChantierId);
        });
        this.showToasterSectionEnregistrer();
      }
    });
  }

  startEditSection(i: number, id: number, libellesection: string) {
    this.id = id;
    // index row is used just for debugging proposes and can be removed
    this.index = i;
    console.log(this.index);
    const dialogRef = this.dialog.open(ModifiersectionComponent, {
      data: {id, libellesection}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        dialogRef.componentInstance.passEntry.subscribe((valeurecue) => {
          console.log('La valeur recu est : ' + valeurecue.libellesection);
          const ChantierId = window.localStorage.getItem('voirChantierId');
          this.ListSectionOfChantier(+ChantierId);
        });
      }
    });
  }


  deleteSection(i: number, id: number, libellesection: string) {
    this.index = i;
    this.id = id;
    const dialogRef = this.dialog.open(SupprimersectionComponent, {
      data: {id, libellesection}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        dialogRef.componentInstance.passEntry.subscribe((valeurecue) => {
          console.log('La valeur recu est : ' + valeurecue.libellesection);
          const ChantierId = window.localStorage.getItem('voirChantierId');
          this.ListSectionOfChantier(+ChantierId);
        });
      }
    });
  }


  // ------------- FIN OPERATION CONCERNANT LA SECTION ------------------ //

  // ------------- DEBUT OPERATION CONCERNANT LES LOTS ------------------ //

  GetLotBySection(value) {
    console.log(value);
    const ChantierId = window.localStorage.getItem('voirChantierId');
    window.localStorage.removeItem('sectionid');
    window.localStorage.setItem('sectionid', value);
    this.ApiSection.getOneSectionOfLotPRIME(value).subscribe((data) =>  {
      this.sectionselectedinlot = value;
      this.lotofsections = data;
      this.totallot =  data.length;
      console.log('total des lot : ' + data.length);
    });
  }


  GetLotBySectionPrime(value) {
    console.log(value);
    window.localStorage.removeItem('sectionid');
    this.lotofsectionsprime = undefined;
    window.localStorage.setItem('sectionid', value);
    this.sectionselected = value;
    this.sectionselectedinmaison = value;
    this.ApiSection.getOneSectionOfLotPRIME(value).subscribe((datalot) =>  {
      this.lotofsectionsprime = datalot;
    });
    this.isNull = null;
    this.ApiLot.getMaisonBySection(+value).subscribe((data) =>  {
      if (data.data.length !== 0) {
        console.log(data.data);
        this.maisons = data.data[0].maisons;
        console.log(this.maisons);
        this.totalmaison =  data.length;
      } else {
        this.maisons = [];
        console.log(this.maisons);
        this.totalmaison =  0;
      }
    });
  }


  GetMaisonByLot(value) {
    console.log(value);
    window.localStorage.removeItem('lotid');
    window.localStorage.setItem('lotid', value);
    this.ApiLot.getLotByMaison(value).subscribe((data) =>  {
      this.maisons = data.data[0].maisons;
      console.log(this.maisons);
      this.totalmaison =  data.data[0].maisons.length;
    });
  }

  listeMaisonByLot() {
    const lotId = window.localStorage.getItem('lotid');
    window.localStorage.removeItem('lotid');
    window.localStorage.setItem('lotid', lotId);
    this.ApiLot.getLotByMaison(+lotId).subscribe((data) =>  {

      this.maisons = data.data[0].maisons;
      console.log(this.maisons);
      this.totalmaison =  data.length;

    });
  }

  listeLotBySection() {
    const sectionId = window.localStorage.getItem('sectionid');
    const ChantierId = window.localStorage.getItem('voirChantierId');
    this.ApiSection.getOneSectionOfLot(+ChantierId, +sectionId).subscribe((data) =>  {

      this.lotofsections = data[0].lots;
      this.totallot =  data[0].lots.length;

    });
  }

  showToasterSectionEnregistrer() {
    this.notifyService.showSuccessWithTimeout('Section enregistrée avec succès !!', 'Notification', 5000);
  }

  showToasterSectionModifier() {
    this.notifyService.showSuccessWithTimeout('Section mise à jour avec succès !!', 'Notification', 5000);
  }

  showToasterSectionSupprimer() {
    this.notifyService.showSuccessWithTimeout('Section supprimée avec succès !!', 'Notification', 5000);
  }

  showToasterLotEnregistrer() {
    this.notifyService.showSuccessWithTimeout('Lot enregistré avec succès !!', 'Notification', 5000);
  }

  showToasterLotModifier() {
    this.notifyService.showSuccessWithTimeout('Lot mis à jour avec succès !!', 'Notification', 5000);
  }


  showToasterPrefatModifier() {
    this.notifyService.showSuccessWithTimeout('Prefat mise à jour avec succès !!', 'Notification', 5000);
  }

  showToasterLotSupprimer() {
    this.notifyService.showSuccessWithTimeout('Lot supprimer avec succès !!', 'Notification', 5000);
  }

  showToasterPrefatSupprimer() {
    this.notifyService.showSuccessWithTimeout('Prefat supprimer avec succès !!', 'Notification', 5000);
  }

  showToasterMaisonEnregistrer() {
    this.notifyService.showSuccessWithTimeout('Maison enregistrée avec succès !!', 'Notification', 5000);
  }

  showToasterPrefatEnregistrer() {
    this.notifyService.showSuccessWithTimeout('Préfat enregistrée avec succès !!', 'Notification', 5000);
  }

  showToasterMaisonModifier() {
    this.notifyService.showSuccessWithTimeout('Maison mis à jour avec succès !!', 'Notification', 5000);
  }

  showToasterMaisonSupprimer() {
    this.notifyService.showSuccessWithTimeout('Maison supprimer avec succès !!', 'Notification', 5000);
  }

  addNewLot() {
    // @ts-ignore
    // @ts-ignore
    // tslint:disable-next-line:label-position no-unused-expression
    // @ts-ignore
    const dialogRef = this.dialog.open(AjouterlotComponent, {data: {lot: Lot , getallsections: this.sections} });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        dialogRef.componentInstance.passEntry.subscribe((valeurecue) => {
          console.log('La valeur recu est : ' + valeurecue.libellelot);
          const ChantierId = window.localStorage.getItem('voirChantierId');
          this.ListSectionOfChantier(+ChantierId);
          if (this.sectionselectedinlot) {
            this.listeLotBySection();
          }
          this.ApiLot.getLotList(+ChantierId).subscribe((data) => {
            this.listelots = data;
          });
          this.showToasterLotEnregistrer();
        });
      }
    });
  }

  startEditLot(i: number, id: number, libellelot: string, section_id: number) {
    this.id = id;
    // index row is used just for debugging proposes and can be removed
    this.index = i;
    console.log(this.index);
    const dialogRef = this.dialog.open(ModifierlotComponent, {
      data: {id, libellelot, section_id, getallsections: this.sections}
    });

    dialogRef.afterClosed().subscribe(result => {
      dialogRef.componentInstance.passEntry.subscribe((valeurecue) => {
        if (result === 1) {
          this.listeLotBySection();
          const ChantierId = window.localStorage.getItem('voirChantierId');
          this.ApiLot.getLotList(+ChantierId).subscribe((data) => {
            this.listelots = data;
          });
          this.showToasterLotModifier();
        }
      });
    });
  }


  deleteLot(i: number, id: number, libellelot: string, section_id: number) {
    this.index = i;
    this.id = id;
    const dialogRef = this.dialog.open(SupprimerlotComponent, {
      data: {id, libellelot, section_id}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        dialogRef.componentInstance.passEntry.subscribe((valeurecue) => {
          console.log('La valeur recu est : ' + valeurecue.libellelot);
          const ChantierId = window.localStorage.getItem('voirChantierId');
          this.ListSectionOfChantier(+ChantierId);
          this.listeLotBySection();
        });
        this.showToasterLotSupprimer();
      }
    });
  }

  // ------------- FIN OPERATION CONCERNANT LES LOTS ------------------ //


  // ------------- DEBUT OPERATION CONCERNANT LES MAISONS ------------------ //


  addNewMaison() {
    // tslint:disable-next-line:max-line-length
    const dialogRef = this.dialog.open(AjoutermaisonComponent, {data: {maison: Maison, getallsections: this.sections , getalllots: this.listelots, getalltechniciens: this.technicien, getallcontroleurs: this.controleur, getallsocietes: this.societeconstruction, getalltypemaison: this.typemaison} });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        dialogRef.componentInstance.passEntry.subscribe((valeurecue) => {
          console.log('La valeur recu est : ' + valeurecue.libellelot);
          if (this.sectionselectedinmaison) {
            this.listeMaisonByLot();
          }
          const ChantierId = window.localStorage.getItem('voirChantierId');
          this.ApiLot.getALLMaisonByChantier(+ChantierId).subscribe((data) => {
            console.log(data.data);
            this.maisons = data.data;
            this.totalmaison = data.data.length;
          });
          this.showToasterMaisonEnregistrer();
        });
      }
    });
  }

  // tslint:disable-next-line:max-line-length
  startEditMaison(i: number, id: number, libellemaison: string, superficie: string, codevirtuel: string, client: string, lot_id: number, type_maison_id: number, controleur_id: number, technicien_id: number, societe_id: number, description: string) {
    this.id = id;
    // index row is used just for debugging proposes and can be removed
    this.index = i;
    console.log(this.index);
    console.log('value of selection selected : ' + this.sections);
    const dialogRef = this.dialog.open(ModifiermaisonComponent, {
      // tslint:disable-next-line:max-line-length
      data: {id, libellemaison, superficie, codevirtuel, client,  lot_id, type_maison_id , controleur_id, technicien_id , societe_id , description , section_id: this.sectionselected, getallsections: this.sections,  getalllots: this.listelots , getallcontroleurs: this.controleur, getalltechniciens: this.technicien, getallsocietes: this.societeconstruction, getalltypemaison: this.typemaison}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        dialogRef.componentInstance.passEntry.subscribe((valeurecue) => {
          // this.listeMaisonByLot();
          const ChantierId = window.localStorage.getItem('voirChantierId');
          this.ApiLot.getALLMaisonByChantier(+ChantierId).subscribe((data) => {
            console.log(data.data);
            this.maisons = data.data;
            this.totalmaison = data.data.length;
          });
        });
        this.showToasterMaisonModifier();
      }
    });
  }

  // tslint:disable-next-line:max-line-length
  deleteMaison(i: number, id: number, libellemaison: string, superficie: string, codevirtuel: string, client: string, lot_id: number, type_maison_id: number, controleur_id: number, technicien_id: number, societe_id: number, description: string) {
    this.index = i;
    this.id = id;
    const dialogRef = this.dialog.open(SupprimermaisonComponent, {
      // tslint:disable-next-line:max-line-length
      data: {id, libellemaison, superficie, codevirtuel, client, lot_id, type_maison_id , technicien_id , societe_id , description,  getalllots: this.listelots, getallcontroleurs: this.controleur, getalltechniciens: this.technicien, getallsocietes: this.societeconstruction, getalltypemaison: this.typemaison}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        dialogRef.componentInstance.passEntry.subscribe((valeurecue) => {
          console.log('La valeur recu est : ' + valeurecue.libellemaison);
          this.listeMaisonByLot();
        });
        this.showToasterMaisonSupprimer();
      }
    });
  }


  editChantier(chantier: Chantier): void  {
    window.localStorage.removeItem('editChantierId');
    window.localStorage.setItem('editChantierId', chantier.id.toString());
    this.router.navigate(['start/modifier-chantier']);
  }

  ManagePrestataire(id: number): void  {
    window.localStorage.removeItem('MaisonId');
    window.localStorage.removeItem('nomchantier');
    window.localStorage.setItem('nomchantier', this.chantiers.denomination);
    window.localStorage.setItem('MaisonId', String(+id));
    this.router.navigate(['start/manage-prestataire']);
  }

  ManagePrestataireInPrefat(id: number, libelleprefat: string): void  {
    window.localStorage.removeItem('libelleprefat');
    window.localStorage.setItem('libelleprefat', libelleprefat);
    window.localStorage.removeItem('PrefatId');
    window.localStorage.removeItem('nomchantier');
    window.localStorage.setItem('nomchantier', this.chantiers.denomination);
    window.localStorage.setItem('PrefatId', String(id));
    this.router.navigate(['start/manage-prestataire-in-prefat']);
  }

  addNewPrefat() {
    // tslint:disable-next-line:max-line-length
    const dialogRef = this.dialog.open(AjouterprefatComponent, {data: {prefat: Prefat, getalltechniciens: this.technicien, getallmagasiniers: this.magasinier }});

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        dialogRef.componentInstance.passEntry.subscribe((valeurecue) => {
          console.log('La valeur recu est : ' + valeurecue.libelleprefat);
          const ChantierId = window.localStorage.getItem('voirChantierId');
          this.ApiPrefat.getPrefatList(+ChantierId).subscribe((data) => {
            console.log(data);
            this.prefats = data;
            this.totalprefat = data.length;
          });
          this.showToasterPrefatEnregistrer();
        });
      }
    });
  }


  startEditPrefat(i: number, id: number, libelleprefat: string, technicien_id: number, magasinier_id: number) {
    this.id = id;
    // index row is used just for debugging proposes and can be removed
    this.index = i;
    console.log(this.index);
    const dialogRef = this.dialog.open(ModifierprefatComponent, {
      data: {id, libelleprefat, technicien_id, magasinier_id, getalltechniciens: this.technicien, getallmagasiniers: this.magasinier}
    });

    dialogRef.afterClosed().subscribe(result => {
      dialogRef.componentInstance.passEntry.subscribe((valeurecue) => {
        if (result === 1) {
          this.listeLotBySection();
          const ChantierId = window.localStorage.getItem('voirChantierId');
          this.ApiPrefat.getPrefatList(+ChantierId).subscribe((data) => {
            this.prefats = data;
          });
          this.showToasterPrefatModifier();
        }
      });
    });
  }


  deletePrefat(i: number, id: number, libelleprefat: string, technicien_id: number, magasinier_id: number) {
    this.index = i;
    this.id = id;
    const dialogRef = this.dialog.open(SupprimerprefatComponent, {
      data: {id, libelleprefat, technicien_id, magasinier_id}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        dialogRef.componentInstance.passEntry.subscribe((valeurecue) => {
          console.log('La valeur recu est : ' + valeurecue.libellelot);
          const ChantierId = window.localStorage.getItem('voirChantierId');
          this.ApiPrefat.getPrefatList(+ChantierId).subscribe((data) => {
            this.prefats = data;
          });
        });
        this.showToasterPrefatSupprimer();
      }
    });
  }

  ActualiserLesSections() {
    const ChantierId = window.localStorage.getItem('voirChantierId');
    this.ApiSection.getSectionList(+ChantierId).subscribe((section: Section[]) => {
        this.sections = section;
        this.totalsection = section.length;
        console.log(section);
    });
  }

  ActualiserLesLots() {
    const ChantierId = window.localStorage.getItem('voirChantierId');
    this.ApiLot.getLotList(+ChantierId).subscribe((data) => {
      this.listelots = data;
      console.log(data);
    });
  }

  ActualiserLesMaisons() {
    const ChantierId = window.localStorage.getItem('voirChantierId');
    this.ApiLot.getALLMaisonByChantier(+ChantierId).subscribe((data) => {
      console.log(data.data);
      this.maisons = data.data;
      this.totalmaison = data.data.length;
    });
  }

  ActualiserLesPrefats() {
    const ChantierId = window.localStorage.getItem('voirChantierId');
    this.ApiPrefat.getPrefatList(+ChantierId).subscribe((data) => {
      console.log(data);
      this.prefats = data;
      this.totalprefat = data.length;
    });
  }

  ManageEtapeAvancement(id: any, libellemaison: any, libelletypemaison: string) {
    window.localStorage.removeItem('iddelamaison');
    window.localStorage.removeItem('libelledemaison');
    window.localStorage.removeItem('libelledetypemaison');
    window.localStorage.setItem('iddelamaison', id);
    window.localStorage.setItem('libelledemaison', libellemaison);
    window.localStorage.setItem('libelledetypemaison', libelletypemaison);
    this.router.navigateByUrl('start/manageetapencoursformaison');
  }
}

