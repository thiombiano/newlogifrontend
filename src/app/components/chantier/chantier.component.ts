import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {ChantierService} from './shared/chantier.service';
import {SnotifyService} from 'ng-snotify';
import {Router} from '@angular/router';
import {MdbTableDirective, MdbTablePaginationComponent} from 'angular-bootstrap-md';
import {Article} from '../article/Shared/article';
import {Chantier} from './shared/chantier';
import {Personnel} from '../personnels/shared/personnel';
import {Employe} from '../employe/shared/employe.model';
import {environment} from '../../../environments/environment';
import {NotificationService} from '../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-chantier',
  templateUrl: './chantier.component.html',
  styleUrls: ['./chantier.component.css']
})
export class ChantierComponent implements OnInit {
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  public path: string;
  public roles: { A: string; T: string; L: string; M: string };
  // tslint:disable-next-line:max-line-length
  constructor(private SpinnerService: NgxSpinnerService, private notifyService: NotificationService, private toastr: ToastrService, private Apichantier: ChantierService, private cdRef: ChangeDetectorRef,
              private snotifyService: SnotifyService, private router: Router) { }
  headElements = ['N°', 'Code chantier', 'Denomination' , 'Adresse', 'Description'];
  chantiers: Chantier[];
  firstlevel: string;
  secondlevel: string;
  totalchantier = 0;
  searchText = '';
  previous: string;
  elements: any = [];
  popoverTitle = 'Suppression de ce chantier';
  popoverMessage = 'Voulez vous vraiment supprimer ce chantier';
  cancelClicked = false;
  ngOnInit() {
    this.loadAllChantier();
    this.path = environment.apiImage;
    this.roles = environment.roles;
  }

  loadAllChantier() {
    this.Apichantier.getAllChantierList().subscribe((chantiers: Chantier[]) => {
      this.chantiers = chantiers;
      console.log(chantiers);
      this.firstlevel = 'Tableau de bord';
      this.secondlevel = 'Chantiers';
      this.totalchantier = chantiers.length;
      this.elements = chantiers;
      // this.mdbTable.setDataSource(chantiers);
      // this.elements = this.mdbTable.getDataSource();
      // this.previous = this.mdbTable.getDataSource();
    });

  }

  get listChantiers() {
    return this.Apichantier.listChantiers;
  }

  // searchItems() {
  //   const prev = this.chantiers;
  //
  //   if (!this.searchText) {
  //     this.mdbTable.setDataSource(this.previous);
  //     this.elements = this.mdbTable.getDataSource();
  //   }
  //
  //   if (this.searchText) {
  //     this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
  //     this.mdbTable.setDataSource(prev);
  //   }
  // }

  showToasterChantierSupprimer() {
    this.notifyService.showSuccessWithTimeout('Chantier supprimé avec succès !!', 'Notification', 5000);
  }

  editChantier(chantier: Chantier): void  {
    window.localStorage.removeItem('editChantierId');
    window.localStorage.setItem('editChantierId', chantier.id.toString());
    this.router.navigate(['start/modifier-chantier']);
  }

  deleteChantier(chantier: Chantier) {
    this.Apichantier.deleteChantier(chantier).subscribe((data) => {
      this.chantiers = this.chantiers.filter(s => s !== chantier);
      this.loadAllChantier();
      this.showToasterChantierSupprimer(); });
  }

  voirChantier(chantier: Chantier) {
    window.localStorage.removeItem('voirChantierId');
    window.localStorage.setItem('voirChantierId', chantier.id.toString());
    this.router.navigate(['start/voir-chantier']);
  }

  // tslint:disable-next-line:use-lifecycle-interface
  // ngAfterViewInit() {
  //   this.mdbTablePagination.setMaxVisibleItemsNumberTo(6);
  //
  //   this.mdbTablePagination.calculateFirstItemIndex();
  //   this.mdbTablePagination.calculateLastItemIndex();
  //   this.cdRef.detectChanges();
  // }

}
