import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {TokenService} from '../../services/token.service';
import {LoginService} from '../../services/login.service';
import {SnotifyService} from 'ng-snotify';
import {ChantierService} from '../chantier/shared/chantier.service';
import {Chantier} from '../chantier/shared/chantier';
import {NotificationService} from '../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {EmployeService} from '../employe/shared/employe.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  public form = {
    login: null,
    password: null,
    lechantier: null
  };

  public error: 'Erreur de connexion';

  listechantier: Chantier[];

  // @ts-ignore
  constructor(
    private ApiEmploye: EmployeService,
    private chantierService: ChantierService,
    private notifyService: NotificationService, private toastr: ToastrService,
    private router: Router,
    private loginService: LoginService,
    private tokenService: TokenService,
    private authService: AuthService) { }


  loadChantier() {
    this.chantierService.getAllChantierList().subscribe((listechantier: Chantier[]) => {
      this.listechantier = listechantier;
      console.log(this.listechantier);
    });
  }

  ngOnInit() {
    this.loadChantier();
  }

  get listChantiers() {
    return this.chantierService.listChantiers;
  }
  // tslint:disable-next-line:one-line
  onSubmit(){
    console.log(this.form.lechantier);
    window.localStorage.removeItem('Idchantier');
    window.localStorage.setItem('Idchantier', this.form.lechantier);
    if (this.form.lechantier != null) {
      this.loginService.login(this.form).subscribe(
        data => this.handleResponse(data),
        error => this.handleError(error)
      );
    } else {
      this.notifyService.showErrorWithTimeout('Erreur de connexion', 'Notification', 5000);
    }
  }


  showToasterConnecte() {
    this.notifyService.showSuccessWithTimeout('Bienvenu sur l\'application de Logiq SA !!', 'Notification', 5000);
  }

  showToasterConnectRefuser() {
    this.notifyService.showErrorWithTimeout('Connexion refusée', 'Notification', 5000);
  }



  handleResponse(data) {
    console.log(data); console.log(data.user.name);
    window.localStorage.setItem('idutilisateur', data.user.id);
    this.ApiEmploye.getEmploye(+data.user.id).subscribe( (data2) => {
      console.log(data2);
    });
    this.authService.username = data.user.name;
    if (data.user.isActif) {
      const username =  window.localStorage.setItem('nomutilisateur', data.user.name);
      window.localStorage.setItem('imageutilisateur', data.user.avatar);
      this.tokenService.handle(data.access_token);
      this.authService.changeAuthStatus(true);
      this.error = null;
      this.showToasterConnecte();
      const id =  window.localStorage.getItem('Idchantier');
      this.chantierService.getChantier(+id);
      // console.log('Id :' + id + 'Chantier : ' + JSON.stringify( this.chantierService.selectedChantier));
      this.router.navigateByUrl('/start/dashboard');
    } else {
      this.tokenService.remove();
      this.showToasterConnectRefuser();
    }


  }


  handleError(error) {
    this.error = error.error.error;
    this.notifyService.showErrorWithTimeout(this.error, 'Notification', 5000);
  }
}
