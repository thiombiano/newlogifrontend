export class Livreur {
  id: number;
  nomlivreur: string;
  prenomlivreur: string;
  telephonelivreur: string;
  cniblivreur: string;
}
