import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {Livreur} from './livreur.model';

@Injectable({
  providedIn: 'root'
})
export class LivreurService {
  private headers: HttpHeaders;
  constructor(public http: HttpClient) { }

  getAllLivreurList() {
    return  this.http.get<Livreur[]>(environment.apiUrl + '/livreurs')
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Impossible dobtenir les livreurs!'))
      );
  }


  addLivreur(livreur: Livreur) {
    this.headers = new HttpHeaders();
    this.headers.append('Content-Type', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    return  this.http.post<Livreur>(environment.apiUrl + '/livreurs', livreur, {headers: this.headers});
  }

  deleteLivreur(livreur: Livreur) {
    return this.http.delete<Livreur>(environment.apiUrl + '/livreurs/' + livreur.id);
  }

  updateLivreur(id, value: any) {
    this.headers = new HttpHeaders();
    this.headers.append('Content-Type', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    // @ts-ignore
    return this.http.post(environment.apiUrl + '/livreurs/' + id, value, {headers: this.headers} );
  }


  getLivreur(id: number) {
    return  this.http.get<Livreur>(environment.apiUrl + '/livreurs/' + id)
      .pipe(map((data: any) => data ),
        catchError(error => throwError('Erreur d\'obtention du livreur'))
      );
  }
}
