export class Fournisseur {
  public id: number;
  public code_fournisseur: string;
  public raison_social: string;
  public adresse: string;
  public tel: string;
  public email: string;
  public rec: string;
  public ninea: string;
  public commentforcanceled: string;
  public is_canceled: boolean;
}
