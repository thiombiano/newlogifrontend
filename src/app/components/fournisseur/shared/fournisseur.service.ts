import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Router} from '@angular/router';
import {Fournisseur} from './fournisseur';
import { Observable } from 'rxjs';
import {Commande} from '../../commandes/shared/commande';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Access-Control-Allow-Origin': '*'
    // 'Authorization': 'Bearer '+ this.token
  })
};

@Injectable()
export class FournisseurService {
  listFournisseurs: Fournisseur[];
  listFournisseursnonblacklister: Fournisseur[];
  selectedFournisseur: Fournisseur;

  constructor(private http: HttpClient) { }

  updateBlacklisterFournisseur(idFournisseur, value: any) {
    return this.http
      .post<Fournisseur>(
        environment.apiUrl + '/blacklisterfournisseur/' + idFournisseur , value
      );
  }

  updateAutoriserFournisseur(idFournisseur, value: any) {
    return this.http
      .post<Fournisseur>(
        environment.apiUrl + '/autoriserfournisseur/' + idFournisseur , value
      );
  }

  getAllFournisseurList() {
    return  this.http.get<Fournisseur[]>(environment.apiUrl + '/fournisseurs');
  }

  getFournisseurList() {
    this.http
      .get<Fournisseur[]>(environment.apiUrl + '/fournisseurs')
      .toPromise()
      .then(x => {
        this.listFournisseurs = x;
        console.log('data: ' + this.listFournisseurs);
      });
  }

  getFournisseurNonBlackLister() {
    this.http
      .get<Fournisseur[]>(environment.apiUrl + '/listefournisseurnonblacklister')
      .toPromise()
      .then(x => {
        this.listFournisseursnonblacklister = x;
        console.log('data: ' + this.listFournisseursnonblacklister);
      });
  }

  updateFournisseurs(id, value: any) {
    return this.http.put<Fournisseur>(environment.apiUrl + '/fournisseurs/' + id, value );
  }

  getFournisseur(id: number) {
    // @ts-ignore
    return  this.http.get(environment.apiUrl + '/fournisseurs/' + id);
      // .toPromise()
      // .then( (x: FournisseurApiResponseOne) => {
      //       console.log('JSON X : ' + JSON.stringify(x));
      //       this.selectedFournisseur = x.data;
      //       console.log('JSON selected : ' + JSON.stringify(this.selectedFournisseur));
      //     });
  }

  addFournisseur(fournsisseur: Fournisseur): Observable<Fournisseur> {
   return  this.http.post<Fournisseur>(environment.apiUrl + '/fournisseurs', fournsisseur);
  }

  deleteFournisseur(fournisseur: Fournisseur) {
    // @ts-ignore
    return this.http.delete<Fournisseur>(environment.apiUrl + '/fournisseurs/' +  fournisseur.id);
  }


}

interface FournisseurApiResponseOne {
  data: Fournisseur;
}
