import {ChangeDetectorRef, Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {MdbTableDirective, MdbTablePaginationComponent} from 'angular-bootstrap-md';
import {FournisseurService} from './shared/fournisseur.service';
import {Router} from '@angular/router';
import {Fournisseur} from './shared/fournisseur';
import {NotificationService} from '../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import {ConfirmationDialogComponent} from '../confirmation-dialog/confirmation-dialog.component';
import {MatDialog} from '@angular/material';
import {BlacklistfournisseurComponent} from './blacklistfournisseur/blacklistfournisseur.component';
import {first} from 'rxjs/operators';
import {Commande} from '../commandes/shared/commande';
import {ActiverfournisseurComponent} from './activerfournisseur/activerfournisseur.component';
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-fournisseur',
  templateUrl: './fournisseur.component.html',
  styleUrls: ['./fournisseur.component.css']
})
export class FournisseurComponent implements OnInit {
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  fournisseurs: Fournisseur[];
  // tslint:disable-next-line:max-line-length
  constructor(public dialog: MatDialog, private notifyService: NotificationService, private toastr: ToastrService, private cdRef: ChangeDetectorRef, private ApiFournisseur: FournisseurService, private router: Router) { }
  // tslint:disable-next-line:max-line-length
  headElements = ['N°', 'Code Fournisseur', 'Raison sociale', 'Adresse' , 'Blacklister', 'Téléphone' , 'Email', 'Action'];
  public roles: { A: string; T: string; L: string; M: string;  C: string; SA: string; CSR: string; CDC: string};
  searchText = '';
  totalfournisseur = 0;
  elements: any = [];
  previous: string;
  firstlevel: string;
  secondlevel: string;
  popoverTitle = 'Suppression de cet fournisseur';
  popoverMessage = 'Voulez vous vraiment supprimer cet fournisseur';
  cancelClicked = false;
  ngOnInit() {
    this.roles = environment.roles;
    this.loadAllFournisseur();
  }

  loadAllFournisseur() {
    this.ApiFournisseur.getAllFournisseurList().subscribe((fournisseurs: Fournisseur[]) => {
      this.fournisseurs = fournisseurs;
      this.firstlevel = 'Tableau de bord';
      this.secondlevel = 'Fournisseurs';
      this.totalfournisseur = fournisseurs.length;
      this.mdbTable.setDataSource(fournisseurs);
      this.elements = this.mdbTable.getDataSource();
      this.previous = this.mdbTable.getDataSource();
    });
  }

  @HostListener('input') oninput() {
    this.searchItems();
  }

  get ListFournisseur() {
   return this.ApiFournisseur.getFournisseurList();
  }

  searchItems() {
    const prev = this.fournisseurs;

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.elements = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(15);

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

  editFournisseur(fournisseur: Fournisseur): void  {
    window.localStorage.removeItem('editFournisseurId');
    window.localStorage.setItem('editFournisseurId', fournisseur.id.toString());
    this.ApiFournisseur.getFournisseur(fournisseur.id);
    this.router.navigate(['start/modifier-fournisseur']);
  }

  showToasterFournisseurSupprimer() {
    this.notifyService.showSuccessWithTimeout('Info du fournisseur mis à jour avec succès !!', 'Notification', 5000);
  }

  showToasterFournisseurBlackLister() {
    this.notifyService.showSuccessWithTimeout('Fournisseur blacklister avec avec succès !!', 'Notification', 5000);
  }

  deleteFournisseur(fournisseur: Fournisseur) {
    this.ApiFournisseur.deleteFournisseur(fournisseur).subscribe((data) => {
      this.fournisseurs = this.fournisseurs.filter(s => s !== fournisseur);
      this.loadAllFournisseur();
      this.showToasterFournisseurSupprimer(); });
  }

  AnnulerFournisseur(idoffournisseur) {
    const dialogRef = this.dialog.open(BlacklistfournisseurComponent, {
      data: {idfournisseur: idoffournisseur}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        dialogRef.componentInstance.passEntry.subscribe((valeurecue) => {
          this.showToasterFournisseurBlackLister();
          this.loadAllFournisseur();
        });
      }
    });
  }

  ActiverFournisseur(idoffournisseur) {
    window.localStorage.removeItem('theidofthefournisseurA');
    window.localStorage.setItem('theidofthefournisseurA', idoffournisseur);

    const dialogRef = this.dialog.open(ActiverfournisseurComponent, {
      data: {idfournisseur: idoffournisseur}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        dialogRef.componentInstance.passEntry.subscribe((valeurecue) => {
          this.showToasterFournisseurBlackLister();
          this.loadAllFournisseur();
        });
      }
    });
  }
}
