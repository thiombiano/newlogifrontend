import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlacklistfournisseurComponent } from './blacklistfournisseur.component';

describe('BlacklistfournisseurComponent', () => {
  let component: BlacklistfournisseurComponent;
  let fixture: ComponentFixture<BlacklistfournisseurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlacklistfournisseurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlacklistfournisseurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
