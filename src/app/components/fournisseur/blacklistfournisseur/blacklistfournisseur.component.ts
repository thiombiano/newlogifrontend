import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Fournisseur} from '../shared/fournisseur';
import {FournisseurService} from '../shared/fournisseur.service';
import {FormControl, Validators} from '@angular/forms';
import {FournisseurInterface} from '../shared/fournisseur.interface';
import {Router} from '@angular/router';

@Component({
  selector: 'app-blacklistfournisseur',
  templateUrl: './blacklistfournisseur.component.html',
  styleUrls: ['./blacklistfournisseur.component.css']
})
export class BlacklistfournisseurComponent {
  // tslint:disable-next-line:max-line-length
  constructor(public router: Router, public ApiFournisseur: FournisseurService,  public dialogRef: MatDialogRef<BlacklistfournisseurComponent>,
               @Inject(MAT_DIALOG_DATA) public data: FournisseurInterface) { }
  @Output() passEntry: EventEmitter<any> = new EventEmitter();

  formControl = new FormControl('', [
    Validators.required
  ]);
  commentforcanceled: any;

  onNoClick(): void {
    this.dialogRef.close();
  }

  submit() {
    // emppty stuff
  }

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Le champ est requis' : '';
  }


  public confirmAdd(): void {
    const idfournisseur = window.localStorage.getItem('idoffournisseur');
    console.log('IdOfFournisseur : ' + this.data['idfournisseur'] );
    console.log(this.data['idfournisseur']);
    this.ApiFournisseur.updateBlacklisterFournisseur(+this.data['idfournisseur'], this.data).subscribe((fournisseur: Fournisseur) => {
      this.passEntry.emit(fournisseur);
      console.log(fournisseur);
    });
  }

}
