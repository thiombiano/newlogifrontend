import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FournisseurService} from '../shared/fournisseur.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';
import {Fournisseur} from '../shared/fournisseur';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-modifierfournisseur',
  templateUrl: './modifierfournisseur.component.html',
  styleUrls: ['./modifierfournisseur.component.css']
})
export class ModifierfournisseurComponent implements OnInit {
  firstlevel: string;
  secondlevel: string;
  editForm: FormGroup;
  submitted = false;
fournisseur: Fournisseur;
  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, private toastr: ToastrService, private formBuilder: FormBuilder,  private ApiFournisseur: FournisseurService, private router: Router) { }

  ngOnInit() {
    const fournisseurId = window.localStorage.getItem('editFournisseurId');
    if (!fournisseurId) {
      alert('Invalid action.');
      this.router.navigate(['start/fournisseurs']);
      return;
    }

    console.log('ID FOURNISSEUR :  ' + fournisseurId);

    this.firstlevel = 'Fournisseur';
    this.secondlevel = 'Modifier fournisseur';


    this.editForm = this.formBuilder.group({
      id: [''],
      code_fournisseur: ['', Validators.required],
      raison_social: ['', Validators.required],
      adresse: ['inconnu', Validators.required],
      tel: [''],
      email: ['', [Validators.email]],
      rc: [''],
      ninea: [''],
      commentforcanceled: [''],
      is_canceled: ['']
    });

    this.ApiFournisseur.getFournisseur(+fournisseurId).subscribe( (data) => {
      console.log(data);
      this.editForm.setValue(data);
    });


  }

  showToasterFournisseurModifier() {
    this.notifyService.showSuccessWithTimeout('Info du fournisseur mis à jour avec succès !!', 'Notification', 5000);
  }


  get leFournisseur() {
    return  this.ApiFournisseur.selectedFournisseur;
  }


  onSubmit() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.editForm.invalid) {
      console.log('error form');
    }
    const fournisseurId = window.localStorage.getItem('editFournisseurId');
    console.log(this.editForm.value);
    this.ApiFournisseur.updateFournisseurs(+fournisseurId, this.editForm.value)
      .pipe(first())
      .subscribe((fournisseur: Fournisseur) => {
        // @ts-ignore
        console.log(fournisseur);
        this.showToasterFournisseurModifier();
        this.router.navigate(['start/fournisseurs']);
      });
  }

}
