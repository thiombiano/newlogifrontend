import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiverfournisseurComponent } from './activerfournisseur.component';

describe('ActiverfournisseurComponent', () => {
  let component: ActiverfournisseurComponent;
  let fixture: ComponentFixture<ActiverfournisseurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActiverfournisseurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiverfournisseurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
