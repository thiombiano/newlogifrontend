import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {Fournisseur} from '../shared/fournisseur';
import {Router} from '@angular/router';
import {FournisseurService} from '../shared/fournisseur.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FournisseurInterface} from '../shared/fournisseur.interface';

@Component({
  selector: 'app-activerfournisseur',
  templateUrl: './activerfournisseur.component.html',
  styleUrls: ['./activerfournisseur.component.css']
})
export class ActiverfournisseurComponent {

  constructor(public router: Router, public ApiFournisseur: FournisseurService,  public dialogRef: MatDialogRef<ActiverfournisseurComponent>,
              @Inject(MAT_DIALOG_DATA) public data: FournisseurInterface) { }

  @Output() passEntry: EventEmitter<any> = new EventEmitter();

  formControl = new FormControl('', [
    Validators.required
  ]);
  commentforcanceled: any;

  onNoClick(): void {
    this.dialogRef.close();
  }

  submit() {
    // emppty stuff
  }

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Le champ est requis' : '';
  }


  public confirmAdd(): void {
    this.ApiFournisseur.updateAutoriserFournisseur(+this.data['idfournisseur'], this.data).subscribe((fournisseur: Fournisseur) => {
      this.passEntry.emit(fournisseur);
      console.log(fournisseur);
    });
  }

}
