import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FournisseurService} from '../shared/fournisseur.service';
import {Router} from '@angular/router';
import {Fournisseur} from '../shared/fournisseur';
import {NotificationService} from '../../../utility/notification.service';
import {ToastrService} from 'ngx-toastr';
import * as moment from 'moment';

@Component({
  selector: 'app-ajouterfournisseur',
  templateUrl: './ajouterfournisseur.component.html',
  styleUrls: ['./ajouterfournisseur.component.css']
})
export class AjouterfournisseurComponent implements OnInit {
  firstlevel: string;
  secondlevel: string;
  addForm: FormGroup;
  submitted = false;
  fournisseur: Fournisseur;
  totalfournisseur = 0;
  referencevalfournisseur = '';

  // tslint:disable-next-line:max-line-length
  constructor(private notifyService: NotificationService, private toastr: ToastrService, private Apifournisseur: FournisseurService, private formBuilder: FormBuilder,  private router: Router) { }

  showToasterFournisseurEnregistrer() {
    this.notifyService.showSuccessWithTimeout('Fournisseur enregistré avec succès !!', 'Notification', 5000);
  }

  showToasterFournisseurErreurModifier() {
    this.notifyService.showErrorWithTimeout('Echec: Vérifier votre saisie !!', 'Notification', 5000);
  }

  ngOnInit() {

    this.Apifournisseur.getAllFournisseurList().subscribe((fournisseurs: Fournisseur[]) => {
      this.referencevalfournisseur =  moment(new Date()).format('YYYY') + '/FRN/' + (fournisseurs.length + 1);
    });

    this.firstlevel = 'Fournisseur';
    this.secondlevel = 'Ajouter fournisseur';
    this.addForm = this.formBuilder.group({
      code_fournisseur: ['', Validators.required],
      raison_social: ['', Validators.required],
      adresse: ['inconnu', Validators.required],
      tel: [''],
      email:  ['', [Validators.email]],
      rc: [''],
      ninea: [''],
      commentforcanceled: [''],
      is_canceled: ['']
    });
  }


  onSubmit() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.addForm.invalid) {
      this.showToasterFournisseurErreurModifier();
      return;
    }
    console.log(this.addForm.value);

    this.Apifournisseur.addFournisseur(this.addForm.value).subscribe((fournisseur: Fournisseur) => {
      this.showToasterFournisseurEnregistrer();
      this.router.navigate(['start/fournisseurs']);
    });

  }

}
