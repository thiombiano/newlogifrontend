import { Injectable } from '@angular/core';
import {ToastrService} from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private toastr: ToastrService) { }

  showSuccess(message, title) {
    // tslint:disable-next-line:indent
    this.toastr.success(message, title);
  }


  showErrorWithTimeout(message, title, timespan) {
    this.toastr.error(message, title , {
      timeOut : timespan
    });
  }

  showSuccessWithTimeout(message, title, timespan) {
    this.toastr.success(message, title , {
      timeOut : timespan
    });
  }

  showSuccessEndingWithTimeout(message, title, timespan) {
    this.toastr.warning(message, title , {
      timeOut : timespan
    });
  }

  showHTMLMessage(message, title) {
    this.toastr.success(message, title, {
      enableHtml : true
    });
  }
}
