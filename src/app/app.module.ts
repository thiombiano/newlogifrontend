import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { StarterComponent } from './starter/starter.component';
import { StarterContentComponent } from './starter/starter-content/starter-content.component';
import { StarterFooterComponent } from './starter/starter-footer/starter-footer.component';
import { StarterHeaderComponent } from './starter/starter-header/starter-header.component';
import { StarterLeftSideComponent } from './starter/starter-left-side/starter-left-side.component';
import { StarterControlSidebarComponent } from './starter/starter-control-sidebar/starter-control-sidebar.component';
import {RouterModule} from '@angular/router';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { LoginComponent } from './components/login/login.component';
import {HttpClientModule} from '@angular/common/http';
import {SnotifyModule, SnotifyService, ToastDefaults} from 'ng-snotify';
import {TokenService} from './services/token.service';
import {AfterLoginService} from './services/after-login.service';
import {AuthService} from './services/auth.service';
import {LoginService} from './services/login.service';
import {BeforeLoginServiceService} from './services/before-login-service.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { FournisseurComponent } from './components/fournisseur/fournisseur.component';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import localeFrExtra from '@angular/common/locales/extra/fr';
import {
  MatButtonModule,
  MatCardModule,
  MatTableModule,
  MatFormFieldModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatRadioModule,
  MatNativeDateModule,
  MatSelectModule,
  MatInputModule, MatStepperModule, MatDialogModule, MatPaginatorModule, MatIconModule
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {InputsModule, TableModule} from 'angular-bootstrap-md';
import { AjouterfournisseurComponent } from './components/fournisseur/ajouterfournisseur/ajouterfournisseur.component';
import {FournisseurService} from './components/fournisseur/shared/fournisseur.service';
import {ConfirmationPopoverModule} from 'angular-confirmation-popover';
import { ModifierfournisseurComponent } from './components/fournisseur/modifierfournisseur/modifierfournisseur.component';
import { ArticleComponent } from './components/article/article.component';
import { AjouterarticleComponent } from './components/article/ajouterarticle/ajouterarticle.component';
import { ModifierarticleComponent } from './components/article/modifierarticle/modifierarticle.component';
import { ChantierComponent } from './components/chantier/chantier.component';
import { StocksComponent } from './components/stocks/stocks.component';
import {SepratorPipe} from './separator.pipe';
import { MouvementStocksComponent } from './components/stocks/mouvement-stocks/mouvement-stocks.component';
import { ReceptionsComponent } from './components/receptions/receptions.component';
import { CommandesComponent } from './components/commandes/commandes.component';
import {CommandeService} from './components/commandes/shared/commande.service';
import {ToastrModule} from 'ngx-toastr';
import { AjoutercommandeComponent } from './components/commandes/ajoutercommande/ajoutercommande.component';
import {ArticleService} from './components/article/Shared/article.service';
import {HttpErrorHandler} from './services/http-error-handler.service';
import {MessageService} from './services/message.service';
import {DatePipe} from '@angular/common';
import {map} from 'rxjs/operators';
import Stepper from 'bs-stepper';
import { VoirdetailcommandesComponent } from './components/commandes/voirdetailcommandes/voirdetailcommandes.component';
import { EffectuerunereceptionComponent } from './components/receptions/effectuerunereception/effectuerunereception.component';
import { VoirunereceptionComponent } from './components/receptions/voirunereception/voirunereception.component';
import { PersonnelsComponent } from './components/personnels/personnels.component';
import { FonctionsComponent } from './components/fonctions/fonctions.component';
import { AjouterfonctionsComponent } from './components/fonctions/ajouterfonctions/ajouterfonctions.component';
import { ModifierfonctionsComponent } from './components/fonctions/modifierfonctions/modifierfonctions.component';
import { AjouterpersonnelsComponent } from './components/personnels/ajouterpersonnels/ajouterpersonnels.component';
import { ModifierpersonnelsComponent } from './components/personnels/modifierpersonnels/modifierpersonnels.component';
import {OwlDateTimeModule, OwlNativeDateTimeModule} from 'ng-pick-datetime';
import {NgbDateParserFormatter, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {MomentDateFormatter} from './components/dateformat/MomentDateFormatter';
import { ModifiercommandeComponent } from './components/commandes/modifiercommande/modifiercommande.component';
import {NgxPrintModule} from 'ngx-print';
import { ModepaiementComponent } from './components/modepaiement/modepaiement.component';
import { AjoutermodepaiementComponent } from './components/modepaiement/ajoutermodepaiement/ajoutermodepaiement.component';
import { ModifiermodepaiementComponent } from './components/modepaiement/modifiermodepaiement/modifiermodepaiement.component';
import { AnnulercommandeComponent } from './components/commandes/annulercommande/annulercommande.component';
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import { PermissionComponent } from './components/permission/permission.component';
import { AjouterpermissionComponent } from './components/permission/ajouterpermission/ajouterpermission.component';
import { ModifierpermissionComponent } from './components/permission/modifierpermission/modifierpermission.component';
import { RoleComponent } from './components/role/role.component';
import { AjouterroleComponent } from './components/role/ajouterrole/ajouterrole.component';
import { ModifierroleComponent } from './components/role/modifierrole/modifierrole.component';
import { EmployeComponent } from './components/employe/employe.component';
import { AjouteremployeComponent } from './components/employe/ajouteremploye/ajouteremploye.component';
import { ModifieremployeComponent } from './components/employe/modifieremploye/modifieremploye.component';
import {NgxPermissionsModule, NgxPermissionsRestrictStubModule} from 'ngx-permissions';
import { AuditingComponent } from './components/auditing/auditing.component';
import { TypechantierComponent } from './components/typechantier/typechantier.component';
import { AjoutertypechantierComponent } from './components/typechantier/ajoutertypechantier/ajoutertypechantier.component';
import { ModifiertypechantierComponent } from './components/typechantier/modifiertypechantier/modifiertypechantier.component';
import { AjouterchantierComponent } from './components/chantier/ajouterchantier/ajouterchantier.component';
import { ModifierchantierComponent } from './components/chantier/modifierchantier/modifierchantier.component';
import { VoirchantierComponent } from './components/chantier/voirchantier/voirchantier.component';
import { SectionComponent } from './components/section/section.component';
import { LotComponent } from './components/lot/lot.component';
import {SectionService} from './components/section/shared/section.service';
import {MatGoogleMapsAutocompleteModule} from '@angular-material-extensions/google-maps-autocomplete';
import {AgmCoreModule} from '@agm/core';
import {environment} from '../environments/environment';
import { AjoutersectionComponent } from './components/section/ajoutersection/ajoutersection.component';
import { ModifiersectionComponent } from './components/section/modifiersection/modifiersection.component';
import { SupprimersectionComponent } from './components/section/supprimersection/supprimersection.component';
import { AjouterlotComponent } from './components/lot/ajouterlot/ajouterlot.component';
import { ModifierlotComponent } from './components/lot/modifierlot/modifierlot.component';
import { SupprimerlotComponent } from './components/lot/supprimerlot/supprimerlot.component';
import { SupprimermaisonComponent } from './components/maison/supprimermaison/supprimermaison.component';
import {AjoutermaisonComponent} from './components/maison/ajoutermaison/ajoutermaison.component';
import { TypemaisonComponent } from './components/typemaison/typemaison.component';
import { SocieteconstructionComponent } from './components/societeconstruction/societeconstruction.component';
import {ModifiermaisonComponent} from './components/maison/modifiermaison/modifiermaison.component';
import {MaisonComponent} from './components/maison/maison.component';
import {JwPaginationComponent} from 'jw-angular-pagination';
import {NgxPaginationModule} from 'ngx-pagination';
import {Ng2SearchPipeModule} from 'ng2-search-filter';
import { EtapeconstructionComponent } from './components/etapeconstruction/etapeconstruction.component';
import { AjouteretapeComponent } from './components/etapeconstruction/ajouteretape/ajouteretape.component';
import { MofifieretapeComponent } from './components/etapeconstruction/mofifieretape/mofifieretape.component';
import { AjoutertypemaisonComponent } from './components/typemaison/ajoutertypemaison/ajoutertypemaison.component';
import { ModifiertypemaisonComponent } from './components/typemaison/modifiertypemaison/modifiertypemaison.component';
import { AjoutersocieteComponent } from './components/societeconstruction/ajoutersociete/ajoutersociete.component';
import { ModifiersocieteComponent } from './components/societeconstruction/modifiersociete/modifiersociete.component';
import { RecueilbesoinComponent } from './components/recueilbesoin/recueilbesoin.component';
import { AjouterbesoinComponent } from './components/recueilbesoin/ajouterbesoin/ajouterbesoin.component';
import { ModifierbesoinComponent } from './components/recueilbesoin/modifierbesoin/modifierbesoin.component';
import {RecueilbesoinService} from './components/recueilbesoin/shared/recueilbesoin.service';
import { VoirbesoinComponent } from './components/recueilbesoin/voirbesoin/voirbesoin.component';
import { AnnulerbesoinComponent } from './components/recueilbesoin/annulerbesoin/annulerbesoin.component';
import {NgxSpinnerModule} from 'ngx-spinner';
import { ManageprestataireComponent } from './components/chantier/manageprestataire/manageprestataire.component';
import { TypecontratComponent } from './components/typecontrat/typecontrat.component';
import {MaisonService} from './components/maison/shared/maison.service';
import { AjouterprestataireComponent } from './components/chantier/manageprestataire/ajouterprestataire/ajouterprestataire.component';
import { ModifierprestataireComponent } from './components/chantier/manageprestataire/modifierprestataire/modifierprestataire.component';
import { SupprimerprestataireComponent } from './components/chantier/manageprestataire/supprimerprestataire/supprimerprestataire.component';
import { BesoinjournalierComponent } from './components/besoinjournalier/besoinjournalier.component';
import { AjouterbesoinjournalierComponent } from './components/besoinjournalier/ajouterbesoinjournalier/ajouterbesoinjournalier.component';
// tslint:disable-next-line:max-line-length
import { ModifierbesoinjournalierComponent } from './components/besoinjournalier/modifierbesoinjournalier/modifierbesoinjournalier.component';
import {BesoinjournalierService} from './components/besoinjournalier/shared/besoinjournalier.service';
import {AdddetailComponent} from './components/besoinjournalier/adddetail/adddetail.component';
import { UpdatedetailComponent } from './components/besoinjournalier/updatedetail/updatedetail.component';
import { VoirdetailbesoinComponent } from './components/besoinjournalier/voirdetailbesoin/voirdetailbesoin.component';
import { EnlevementComponent } from './components/enlevement/enlevement.component';
import { EffectuerenlevementComponent } from './components/enlevement/effectuerenlevement/effectuerenlevement.component';
import { VoiralldetailbesoinleveloneComponent } from './components/besoinjournalier/voiralldetailbesoinlevelone/voiralldetailbesoinlevelone.component';
import { VoiralldetailbesoinbymaisonleveltwoComponent } from './components/besoinjournalier/voiralldetailbesoinbymaisonleveltwo/voiralldetailbesoinbymaisonleveltwo.component';
import { VoirenlevementComponent } from './components/enlevement/voirenlevement/voirenlevement.component';
import { EnlevementmaisonComponent } from './components/receptions/enlevementmaison/enlevementmaison.component';
import { AdddetailenlevmentComponent } from './components/receptions/enlevementmaison/adddetailenlevment/adddetailenlevment.component';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import { BlacklistfournisseurComponent } from './components/fournisseur/blacklistfournisseur/blacklistfournisseur.component';
import { ActiverfournisseurComponent } from './components/fournisseur/activerfournisseur/activerfournisseur.component';
import { EnlevementprefatComponent } from './components/receptions/enlevementprefat/enlevementprefat.component';
import { AdddetailprefatComponent } from './components/receptions/enlevementprefat/adddetailprefat/adddetailprefat.component';
import { StockprefatComponent } from './components/stockprefat/stockprefat.component';
import { MouvementStocksPrefatComponent } from './components/stockprefat/mouvement-stocks-prefat/mouvement-stocks-prefat.component';
import { PrefatComponent } from './components/prefat/prefat.component';
import { AjouterprefatComponent } from './components/prefat/ajouterprefat/ajouterprefat.component';
import { ModifierprefatComponent } from './components/prefat/modifierprefat/modifierprefat.component';
import { SupprimerprefatComponent } from './components/prefat/supprimerprefat/supprimerprefat.component';
import { LivreurComponent } from './components/livreur/livreur.component';
import { MouvementStocksByArticleComponent } from './components/stocks/mouvement-stocks-by-article/mouvement-stocks-by-article.component';
import { AdddetailtoprefatComponent } from './components/besoinjournalier/adddetailtoprefat/adddetailtoprefat.component';
import { ManageprestataireinprefatComponent } from './components/chantier/manageprestataireinprefat/manageprestataireinprefat.component';
// tslint:disable-next-line:max-line-length
import { AjouterprestataireinprefatComponent } from './components/chantier/manageprestataireinprefat/ajouterprestataireinprefat/ajouterprestataireinprefat.component';
// tslint:disable-next-line:max-line-length
import { ModifierprestataireinprefatComponent } from './components/chantier/manageprestataireinprefat/modifierprestataireinprefat/modifierprestataireinprefat.component';
// tslint:disable-next-line:max-line-length
import { SupprimerprestataireinprefatComponent } from './components/chantier/manageprestataireinprefat/supprimerprestataireinprefat/supprimerprestataireinprefat.component';
// tslint:disable-next-line:max-line-length
import { VoirdetailbesoinforprefatComponent } from './components/besoinjournalier/voirdetailbesoinforprefat/voirdetailbesoinforprefat.component';
// tslint:disable-next-line:max-line-length
import { ModifierbesoinjournalierforprefatComponent } from './components/besoinjournalier/modifierbesoinjournalierforprefat/modifierbesoinjournalierforprefat.component';
import { ManageetapeencoursformaisonComponent } from './components/manageetapeencoursformaison/manageetapeencoursformaison.component';
import { AjouteretapeencourComponent } from './components/manageetapeencoursformaison/ajouteretapeencour/ajouteretapeencour.component';
import { ReceptionneretapeencourComponent } from './components/manageetapeencoursformaison/receptionneretapeencour/receptionneretapeencour.component';
import { VoirimageetapeComponent } from './components/manageetapeencoursformaison/voirimageetape/voirimageetape.component';
import { EtatdefinitifetapeComponent } from './components/manageetapeencoursformaison/etatdefinitifetape/etatdefinitifetape.component';
registerLocaleData(localeFr, 'fr-FR', localeFrExtra);
// @ts-ignore
// @ts-ignore
// @ts-ignore
// @ts-ignore
// @ts-ignore

const googleMapsParams = {
  apiKey: environment.GOOGLE_MAPS_API_KEY,
  libraries: ['places'],
  language: 'en',
  // region: 'DE'
};

@NgModule({
  declarations: [
    AppComponent,
    StarterComponent,
    StarterContentComponent,
    StarterFooterComponent,
    SepratorPipe,
    StarterHeaderComponent,
    StarterLeftSideComponent,
    StarterControlSidebarComponent,
    LoginComponent,
    DashboardComponent,
    FournisseurComponent,
    AjouterfournisseurComponent,
    ModifierfournisseurComponent,
    ArticleComponent,
    AjouterarticleComponent,
    ModifierarticleComponent,
    ChantierComponent,
    StocksComponent,
    MouvementStocksComponent,
    ReceptionsComponent,
    CommandesComponent,
    AjoutercommandeComponent,
    VoirdetailcommandesComponent,
    EffectuerunereceptionComponent,
    VoirunereceptionComponent,
    PersonnelsComponent,
    FonctionsComponent,
    AjouterfonctionsComponent,
    ModifierfonctionsComponent,
    AjouterpersonnelsComponent,
    ModifierpersonnelsComponent,
    ModifiercommandeComponent,
    ModepaiementComponent,
    AjoutermodepaiementComponent,
    ModifiermodepaiementComponent,
    AnnulercommandeComponent,
    ConfirmationDialogComponent,
    PermissionComponent,
    AjouterpermissionComponent,
    ModifierpermissionComponent,
    RoleComponent,
    AjouterroleComponent,
    ModifierroleComponent,
    EmployeComponent,
    AjouteremployeComponent,
    ModifieremployeComponent,
    AuditingComponent,
    TypechantierComponent,
    AjoutertypechantierComponent,
    ModifiertypechantierComponent,
    AjouterchantierComponent,
    ModifierchantierComponent,
    VoirchantierComponent,
    SectionComponent,
    LotComponent,
    AjoutersectionComponent,
    ModifiersectionComponent,
    SupprimersectionComponent,
    AjouterlotComponent,
    ModifierlotComponent,
    SupprimerlotComponent,
    AjoutermaisonComponent,
    MaisonComponent,
    ModifiermaisonComponent,
    SupprimermaisonComponent,
    TypemaisonComponent,
    SocieteconstructionComponent,
    JwPaginationComponent,
    EtapeconstructionComponent,
    AjouteretapeComponent,
    MofifieretapeComponent,
    AjoutertypemaisonComponent,
    ModifiertypemaisonComponent,
    AjoutersocieteComponent,
    ModifiersocieteComponent,
    RecueilbesoinComponent,
    AjouterbesoinComponent,
    ModifierbesoinComponent,
    VoirbesoinComponent,
    AnnulerbesoinComponent,
    ManageprestataireComponent,
    TypecontratComponent,
    AjouterprestataireComponent,
    ModifierprestataireComponent,
    SupprimerprestataireComponent,
    BesoinjournalierComponent,
    AjouterbesoinjournalierComponent,
    ModifierbesoinjournalierComponent,
    AdddetailComponent,
    UpdatedetailComponent,
    VoirdetailbesoinComponent,
    EnlevementComponent,
    EffectuerenlevementComponent,
    VoiralldetailbesoinleveloneComponent,
    VoiralldetailbesoinbymaisonleveltwoComponent,
    VoirenlevementComponent,
    EnlevementmaisonComponent,
    AdddetailenlevmentComponent,
    BlacklistfournisseurComponent,
    ActiverfournisseurComponent,
    EnlevementprefatComponent,
    AdddetailprefatComponent,
    StockprefatComponent,
    MouvementStocksPrefatComponent,
    PrefatComponent,
    AjouterprefatComponent,
    ModifierprefatComponent,
    SupprimerprefatComponent,
    LivreurComponent,
    MouvementStocksByArticleComponent,
    AdddetailtoprefatComponent,
    ManageprestataireinprefatComponent,
    AjouterprestataireinprefatComponent,
    ModifierprestataireinprefatComponent,
    SupprimerprestataireinprefatComponent,
    VoirdetailbesoinforprefatComponent,
    ModifierbesoinjournalierforprefatComponent,
    ManageetapeencoursformaisonComponent,
    AjouteretapeencourComponent,
    ReceptionneretapeencourComponent,
    VoirimageetapeComponent,
    EtatdefinitifetapeComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    HttpClientModule,
    NgxPrintModule,
    MatStepperModule,
    SnotifyModule,
    NgxPermissionsModule.forRoot({
      permissionsIsolate: true,
      rolesIsolate: true
    }),
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    RouterModule.forRoot([], {useHash: true}),
    AppRoutingModule,
    FormsModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatTableModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatRadioModule,
    MatSelectModule,
    MatDialogModule,
    MatNativeDateModule,
    OwlDateTimeModule,
    NgbModule,
    OwlNativeDateTimeModule,
    TableModule,
    InputsModule,
    ReactiveFormsModule,
    ConfirmationPopoverModule.forRoot({confirmButtonType: 'danger'}),
    MatStepperModule,
    NgxPermissionsRestrictStubModule,
    AgmCoreModule.forRoot(googleMapsParams),
    MatGoogleMapsAutocompleteModule.forRoot(),
    MatPaginatorModule,
    MatIconModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
    NgxSpinnerModule,
    MatButtonToggleModule
  ],
  entryComponents: [
    ConfirmationDialogComponent,
    UpdatedetailComponent,
    AjoutersectionComponent,
    SupprimerprestataireComponent,
    AdddetailComponent,
    ModifierprestataireComponent,
    AjouterprestataireComponent,
    ModifiersectionComponent,
    SupprimersectionComponent,
    AjouterlotComponent,
    SupprimerlotComponent,
    ModifierlotComponent,
    AjoutermaisonComponent,
    ModifiermaisonComponent,
    SupprimermaisonComponent,
    AdddetailenlevmentComponent,
    BlacklistfournisseurComponent,
    ActiverfournisseurComponent,
    AdddetailprefatComponent,
    AjouterprefatComponent,
    ModifierprefatComponent,
    SupprimerprefatComponent,
    AdddetailprefatComponent,
    AdddetailtoprefatComponent,
    AjouterprestataireinprefatComponent,
    ModifierprestataireinprefatComponent,
    SupprimerprestataireinprefatComponent,
    AjouteretapeencourComponent,
    ReceptionneretapeencourComponent,
    VoirimageetapeComponent,
    EtatdefinitifetapeComponent
  ],
  providers: [
    BeforeLoginServiceService,
    RecueilbesoinService,
    BesoinjournalierService,
    LoginService,
    CommandeService,
    MaisonService,
    TokenService,
    AuthService,
    SectionService,
    FournisseurService,
    ArticleService,
    AfterLoginService,
    DatePipe,
    {provide: 'SnotifyToastConfig', useValue: ToastDefaults},
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    SnotifyService,
    MessageService,
    {provide: NgbDateParserFormatter, useValue: new MomentDateFormatter('MM/DD/YYYY')}
    ],
  bootstrap: [AppComponent]
})

export class AppModule { }
