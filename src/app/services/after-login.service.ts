import { Injectable } from '@angular/core';
import {TokenService} from './token.service';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AfterLoginService  implements CanActivate  {


  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):
    boolean | Observable<boolean> |  Promise<boolean> {

    // @ts-ignore
    if (this.token.loggedIn() === true) {
      return  true;
    }
    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
    return false;

  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> |  Promise<boolean> {
    return this.canActivate(route, state);
  }

  constructor(private token: TokenService, private router: Router) { }
}
