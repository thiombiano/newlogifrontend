import { Injectable } from '@angular/core';
import {LoginService} from './login.service';
import {BehaviorSubject} from 'rxjs';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  private iss = {
    login : 'http://localhost:8000/api/login',
    signup : 'http://localhost:8000/api/signup'
  };
  constructor(private loginService: LoginService, private router: Router) { }
  handle(token) {
    this.set(token);
    console.log(this.isValid());

  }

  set(token) {
    localStorage.setItem('token', token);
  }

  get() {
    return localStorage.getItem('token');
  }

  remove() {
    localStorage.removeItem('token');
  }

  isValid() {
    const token = this.get();
    if (token) {
      const payload = this.payload(token);
      if (payload) {
        console.log('SALAM HAHA : ' + payload.iss);
        return true;
      }
    }
    return false;
  }

  payload(token) {
    const payload = token.split('.')[1];
    return this.decode(payload);
  }

  decode(payload) {
    return JSON.parse(atob(payload));
  }

  loggedIn() {
    return this.isValid();
  }
}
