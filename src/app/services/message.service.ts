import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  // tslint:disable-next-line:ban-types
   messages: String[] = [];

  add(message: string) {
    this.messages.push( message );
  }

  clearMessage() {
    this.messages = [];
  }


}
